<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('estado',1);
            $table->char('tipo',1);
            $table->decimal('monto_enviado',8,2);
            $table->decimal('monto_recibido',8,2);
            $table->date('fecha_operacion');
            $table->unsignedInteger('id_tasa');
            $table->foreign('id_tasa')->references('id')->on('tasas')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->unsignedInteger('id_cupon');
            $table->foreign('id_cupon')->references('id')->on('cupones')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->unsignedBigInteger('id_perfil');
            $table->foreign('id_perfil')->references('id')->on('perfiles')->onDelete('RESTRICT');
            $table->unsignedBigInteger('id_cuenta_origen');
            $table->foreign('id_cuenta_origen')->references('id')->on('cuentas_bancarias')->onDelete('RESTRICT');
            $table->unsignedBigInteger('id_cuenta_destino');
            $table->foreign('id_cuenta_destino')->references('id')->on('cuentas_bancarias')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operaciones');
    }
}
