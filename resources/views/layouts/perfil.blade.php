<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
	<title>@yield('title')</title>
	<link rel="icon" type="image/png" href="{{asset('favicon-open-dolar.png')}}" sizes="16x16">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/styles.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/font-awesome.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/select2.min.css') }}"/>
</head>
<body class="d-flex flex-wrap">

	<div class="nav-header py-3 py-lg-0 px-5 px-lg-0">
		<img src="{{ asset('assets-web/img/Grupo 19.png') }}" class="logo-open">
	</div>

	@yield('content')

<script src="{{ asset('assets-web/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets-web/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets-web/js/font-awesome.js') }}"></script>
<script src="{{ asset('assets-web/js/toastr.min.js') }}"></script>
<script src="{{ asset('assets-web/js/datepicker.js') }}"></script>

	<script src="{{ asset('assets-web/js/select2.min.js') }}"></script>
	<script>
		$( document ).ready(function() {
			$('select').select2();
		});
	</script>

	<script>
		// Mostrar mensaje del error de la validacion Laravel con toastr
		@if ($errors->any())
			@foreach ($errors->all() as $error)
				toastr.error("{{ $error }}");
			@endforeach
		@endif
		@if(Session::has('message'))
			var type = "{{ Session::get('alert-type', 'info') }}";
			switch(type){
				case 'info':
					toastr.info("{{ Session::get('message') }}");
					break;

				case 'warning':
					toastr.warning("{{ Session::get('message') }}");
					break;

				case 'success':
					toastr.success("{{ Session::get('message') }}");
					break;

				case 'error':
					toastr.error("{{ Session::get('message') }}");
					break;
			}
		@endif
		$('.date').datepicker({
			multidate: true,
			format: 'dd-mm-yyyy'
		});
</script>

	@yield('scripts')
</body>
</html>