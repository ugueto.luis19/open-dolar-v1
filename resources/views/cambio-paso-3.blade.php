@extends('layouts.index_app')
@section('content')
    <!-- card de necesitas ayuda-->
    <div class="div-flotante">
        <img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 40%">
    </div>

    <div class="div-flotante-azul px-5 py-4">
        <button class="btn btn-close-ayuda">
            <i class="fa-solid fa-circle-xmark icon-close-ayuda"></i>
        </button>
        <h4 class="Arialregular titulo-ayuda"><img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 8%;">Necesitas ayuda?</h4>
        <h5 class="ArialBold mt-5 mb-2">Temas recurrentes</h5>
        <p class="Arialregular p-link-temas my-0"><a href="">Problemas con transacción</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Perfil y registro</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Privacidad y seguridad</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Cancelaciones y devoluciones</a></p>
        <h5 class="ArialBold mt-5 mb-2">Contacto</h5>
        <p class="Arialregular">
            Con el fin de ahorrar tu tiempo, recomendamos revisar las preguntas frecuentes. Si sigues sin encontrar respuesta, puedes contactarnos de la manera que mas te acomode:
        </p>
        <ul class="ps-0">
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-phone me-3"></i>
                (+51) 1 1234567
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-brands fa-whatsapp me-3"></i>
                998877665
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-envelope me-3"></i>
                confianza@opendolar.com
            </li>
            <li class="d-flex Arialregular align-items-start list-contacto">
                <i class="fa-solid fa-location-dot me-3"></i>
                Centro Empresarial El Trigal, Calle Antares 320, Of. 802-C, Torre B, Surco - Lima.
            </li>
        </ul>
        <div class="d-flex justify-content-start mt-4">
            <a href="" class="me-4">
                <i class="fa-solid fa-share-nodes icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-whatsapp icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-facebook-f icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-linkedin-in icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-instagram icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="">
                <i class="fa-solid fa-envelope icon-redes-verde" style="color: #fff !important;"></i>
            </a>
        </div>
    </div>

    <div class="conten_panel_administrador" style="position: relative;">
        <h1 class="titulo-general-alerta PoppinsBold my-0 text-center text-xl-start"><span>A Cambiar</span></h1>

        @if(!is_object($operacion->origenFondos))

            <?php
                $suma_monto_total_usd=0;

                $bandera=0;
                if(count($operaciones_evaluar)>0)
                    {
                        foreach ($operaciones_evaluar as $operacion_evaluar)
                            {
                                if($operacion_evaluar->tipo==0)
                                    {
                                        $suma_monto_total_usd+=$operacion_evaluar->monto_recibido;
                                    }
                                else{
                                        $suma_monto_total_usd+=$operacion_evaluar->monto_enviado;
                                    }
                            }
                    }


                if($operacion->tipo==0)
                    {
                        $suma_monto_total_usd+=$operacion->monto_recibido;
                    }
                else
                    {
                        $suma_monto_total_usd+=$operacion->monto_enviado;
                    }

            ?>

            @if($suma_monto_total_usd>=5000)
                <div class="div-conten-rojo" id="div-conten-rojo">
                    <form method="post" id="form-origen-fondos" style="width: 100%;">
                        <input name="id_operacion" type="hidden" value="{{$operacion->id}}">
                        <div class="div-circle-rojo">
                            <div class="div-rojo-circle">
                                <img src="{{ asset('assets-web/img/statement.png') }}" width="60%">
                            </div>
                        </div>
                        <div class="div-card-rojo px-3 pb-3 pt-5">
                            <p class="text-center">
                                @if($suma_monto_total_usd>=5000)
                                    <span class="PoppinsRegular">
                                        Estimado cliente, sus ordenes
                                    </span>
                                    <span class="PoppinsBold">
                                        superan a los $5,000 en el día.
                                    </span>
                                    <span class="PoppinsRegular">
                                        Por favor declare el origen de los fondos utilizados en este cambio
                                    </span>
                                @else
                                    <span class="PoppinsRegular">Estimado cliente, su operacion</span> <span class="PoppinsBold">supera a los $5,000.</span> <span class="PoppinsRegular">Por favor declare el origen delos fondos utilizados en este cambio</span>
                                @endif
                            </p>
                            <div class="div-form-login">
                                <select class="select-fomr text-start" name="origen" id="selectOrigen" required>
                                    <option value="" disabled selected>Seleccione una Opcion</option>
                                    <option value="1">Alquiler de bienes muebles</option>
                                    <option value="2">Alquiler de bienes inmuebles</option>
                                    <option value="3">Ingresos por trabajo independiente</option>
                                    <option value="4">Ingresos por trabajo dependiente</option>
                                    <option value="5">Préstamos</option>
                                    <option value="6">Venta de bien mueble</option>
                                    <option value="7">Venta de bien inmueble</option>
                                    <option value="8">Ingresos por actividad de la empresa</option>
                                    <option value="9">Otros</option>

                                </select>
                                <i class="fa fa-angle-down icon-select"></i>
                            </div>
                            <div class="div-form-login mt-3">
                                <textarea class="select-fomr textarea-alert px-3 py-3" style="display:none;" name="descripcion" id="areaOrigen" placeholder="Ingrese aquí el origen de los fondos"></textarea>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn PoppinsMedium mt-3 btn-top-natural px-5" style="color: #137188 !important;background-color: white;padding: 15px;border-radius: 10px; -webkit-box-shadow: 10px 10px 29px -11px rgba(0,0,0,0.75);
    -moz-box-shadow: 10px 10px 29px -11px rgba(0,0,0,0.75);
    box-shadow: 10px 10px 29px -11px rgba(0,0,0,0.75);">Enviar</button>
                            </div>
                        </div>
                        @csrf
                    </form>
                </div>
            @endif
        @endif
        <div class="row mx-0 mt-4 justify-content-center">
            <div class="col-12">
                <a href="{{ \URL::previous() }}" class="btn btn-volver-azul PoppinsRegular">< Volver</a>
            </div>

            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 px-0 px-md-4 px-lg-4 px-xl-3" style="position: relative;">
                <div class="div-check">
                    <div class="div-chec-radius">
                        <i class="fa-solid fa-circle-check check-activado"></i>
                        <i class="fa-solid fa-circle-check check-activado"></i>
                        <i class="fa-solid fa-circle-check"></i>
                    </div>
                </div>
                <form action="{{route('continuar-cambio3')}}" method="post" id="formularioProceso">
                    <input type="hidden" id="id" name="id" value="{{ $operacion->id }}">
                    <div class="card-blanco card-padding-top mt-4 mb-5 mb-md-0">
                        <h2 class="PoppinsMedium subtitulo-card text-center" id="titulo">Transfiere los fondos a cambiar</h2>
                        <?php

                        if($operacion->tipo=='0')
                        {
                            $moneda_1='S/';
                            $moneda_2='$';
                            if(is_object($operacion->cupon))
                            {
                                $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta-$operacion->cupon->valor_soles;
                            }
                            else
                            {
                                $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta;
                            }
                        }
                        else{
                            $moneda_1='$';
                            $moneda_2='S/';
                            if(is_object($operacion->cupon))
                            {
                                $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra+($operacion->cupon->valor_soles/$operacion->tasa->tasa_compra);
                            }
                            else
                            {
                                $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra;
                            }
                        }
                        ?>
                        <div class="row mx-0 mt-1" >
                            <div class="col-12 mb-1 mt-2 PoppinsRegular" style="color: #137188; text-align: center;">
                                <b> N° de orden {{$operacion->id}} </b>
                            </div>
                            <div class="col-12 mb-2 mt-3 PoppinsRegular" style="color: #137188; text-align: center;">
                                Para finalizar la transacción, transfiere {{$moneda_1}} {{number_format($operacion->monto_enviado,2,'.','')}} a la cuenta indicada:
                            </div>

                            <div class="col-12 mb-2 mt-3 px-0">
                                <div class="row mx-0">
                                    <div class="col-7 p-azul-transferir mb-3">
                                        <span class="PoppinsBold">Numero de cuenta</span>
                                        <br>
                                        <span class="PoppinsRegular">{{$cuenta_bancaria_admin->nro_cuenta}}</span>
                                    </div>
                                    <div class="col-5 p-azul-transferir mb-3">
                                        <span class="PoppinsBold">Tipo de cuenta</span>
                                        <br>
                                        <span class="PoppinsRegular">{{$cuenta_bancaria_admin->tipo->nombre}}</span>
                                    </div>
                                </div>
                                <div class="row mx-0">
                                    <div class="col-7 p-azul-transferir mb-3">
                                        <span class="PoppinsBold">Banco</span>
                                        <br>
                                        <span class="PoppinsRegular">{{$cuenta_bancaria_admin->banco->nombre}}</span>
                                    </div>
                                    <div class="col-5 p-azul-transferir mb-3">
                                        <span class="PoppinsBold">Moneda</span>
                                        <br>
                                        <span class="PoppinsRegular">{{$cuenta_bancaria_admin->moneda->nombre}}</span>
                                    </div>
                                </div>
                                <div class="row mx-0">
                                    <div class="col-7 p-azul-transferir mb-3">
                                        <span class="PoppinsBold">Titular de la cuenta</span>
                                        <br>
                                        <span class="PoppinsRegular">{{$cuenta_bancaria_admin->titular}}</span>
                                    </div>
                                    <div class="col-5 p-azul-transferir mb-3">
                                        <span class="PoppinsBold">RUC</span>
                                        <br>
                                        <span class="PoppinsRegular">{{$cuenta_bancaria_admin->ruc}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 mb-2 mt-3 px-3" style="background-color: #FEF8DD; border-radius: 20px;">
                                <p class="my-3 text-center PoppinsRegular" style="color: #137188;font-size: 14px;">Recuerda que el tipo de cambio es dinámico y por eso solo podemos guardartelo por 15 minutos. Tu operacion inicio a las {{date('H:i:s',strtotime($operacion->fecha_operacion))}}.</p>
                            </div>

                            <div class="col-12 mb-2 mt-3">
                                <div class="row mx-0">
                                    <div class="col-12 text-center" style="color: #137188;">
                                        <strong style="color: #137188;">Operación vence en:</strong>
                                        <br>
                                        <?php
                                        $hora_vencimiento= date('i:s',strtotime($operacion->fecha_operacion.' +15 minute'));
                                        ?>

                                        <a id="anularOperacion" style="display: none;" class="link-anular PoppinsLight" href='{{route("anular-cambio",["id"=>$operacion->id])}}'    >< Anular orden</a>

                                        <b style="font-size: 28px;" id="contador"><span id="minutes"></span>:<span id="seconds"></span></b>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 text-center">
                                @if(!is_object($operacion->origenFondos) || is_null($operacion->origenFondos))
                                    @if(($operacion->tipo == 0 && $operacion->monto_recibido > 5000) || ($operacion->tipo == 1 && $operacion->monto_enviado > 5000))
                                        <button type="submit" id="buttonProcess" class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-5" style="color: #137188 !important;" disabled>Ya tranferi</button>
                                    @else
                                        <button type="submit" id="buttonProcess" class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-5" style="color: #137188 !important;">Ya tranferi</button>
                                    @endif
                                @else
                                    <button type="submit" id="buttonProcess" class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-5" style="color: #137188 !important;">Ya tranferi</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    @csrf
                </form>

            </div>
        </div>

        <div class="row mx-0 mt-4 justify-content-center">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 px-0 px-md-4 px-lg-4 px-xl-3 py-3 text-center text-sm-start" >
                <div class="py-3" style="border: 1px solid #FD3232; border-radius: 15px;background-color: #FEC6C6;position: relative;">
                    <i class="fa-solid fa-triangle-exclamation icon-location-azul icon-triangulo-top"></i>
                    <p class="PoppinsRegular p-azul-mensaje mb-0 mt-3 mt-sm-0">
                        Antes de confirmar la operación, por favor revisa con atención los detalles.
                        <a data-bs-toggle="modal" data-bs-target="#exampleModal">Ver detalles aqui</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div>

    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-detalle-operacion">
            <div class="modal-content">
                <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body px-0 py-0">
                    <div class="row mx-0 justify-content-center mt-5 align-items-center">

                        <h2 class="PoppinsMedium subtitulo-card text-center mb-4" id="titulo">Detalles de la operación:</h2>

                        <?php
                        switch($operacion->estado){
                            case '0':
                                $o_alerta = "o_pendientes";
                                $class_alerta = "pendientes";
                                $spanEstado = "Pendiente";
                                break;
                            case '1':
                                $o_alerta = "o_proceso";
                                $class_alerta = "proceso";
                                $spanEstado = "En Proceso";
                                break;
                            case '2':
                                $o_alerta = "o_realizada";
                                $class_alerta = "realizada";
                                $spanEstado = "Realizado";
                                break;
                            case '3':
                                $o_alerta = "o_devolucion";
                                $class_alerta = "devolucion";
                                $spanEstado = "Devolución";
                                break;
                            case '4':
                                $o_alerta = "o_devuelta";
                                $class_alerta = "devuelta";
                                $spanEstado = "Devuelto";
                                break;
                            case '5':
                                $o_alerta = "o_observado";
                                $class_alerta = "observado";
                                $spanEstado = "Observados";
                                break;
                            case '6':
                                $o_alerta = "o_anulado";
                                $class_alerta = "anulado";
                                $spanEstado = "Anulado";
                                break;
                            case '7':
                                $o_alerta = "o_verificado";
                                $class_alerta = "verificado";
                                $spanEstado = "Verificado";
                                break;
                            default:
                                $o_alerta = null;
                                $class_alerta = null;
                                $spanEstado = null;
                                break;
                        }

                        ?>

                        <div class="col-10 col-sm-7">
                            <div class="row mx-0">
                                <div class="col-4 ps-0">
                                    <p class="PoppinsBold p-azul-transferir">
                                        Estado:
                                    </p>
                                </div>
                                <div class="col-8 pe-0">
                                    <p class="PoppinsRegular p-azul-transferir">
                                        {{$spanEstado}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-10 col-sm-7">
                            <div class="row mx-0">
                                <div class="col-4 ps-0">
                                    <p class="PoppinsBold p-azul-transferir">
                                        Fecha:
                                    </p>
                                </div>
                                <div class="col-8 pe-0">
                                    <p class="PoppinsRegular p-azul-transferir">
                                        {{date('d-m-Y H:i:s',strtotime($operacion->fecha_operacion))}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        @if(is_object($operacion->cupon))
                            <div class="col-10 col-sm-7">
                                <div class="row mx-0">
                                    <div class="col-4 ps-0">
                                        <p class="PoppinsBold p-azul-transferir">
                                            Cupon:
                                        </p>
                                    </div>
                                    <div class="col-8 pe-0">
                                        <p class="PoppinsRegular p-azul-transferir">
                                            {{$operacion->cupon->codigo}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="col-10 col-sm-7">
                            <div class="row mx-0">
                                <div class="col-4 ps-0">
                                    <p class="PoppinsBold p-azul-transferir">
                                        Monto:
                                    </p>
                                </div>
                                <div class="col-8 pe-0">
                                    <p class="PoppinsRegular p-azul-transferir">
                                        {{$moneda_1}} {{number_format($operacion->monto_enviado,2,'.','')}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-10 col-sm-7">
                            <div class="row mx-0">
                                <div class="col-4 ps-0">
                                    <p class="PoppinsBold p-azul-transferir">
                                        Recibido:
                                    </p>
                                </div>
                                <div class="col-8 pe-0">
                                    <p class="PoppinsRegular p-azul-transferir">
                                        {{$moneda_2}} {{number_format($operacion->monto_recibido,2,'.','')}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-7">
                            <div class="row mx-0">
                                <div class="col-4 ps-0">
                                    <p class="PoppinsBold p-azul-transferir">
                                        Cambio:
                                    </p>
                                </div>
                                <div class="col-8 pe-0">
                                    <p class="PoppinsRegular p-azul-transferir">
                                        {{number_format($monto_tasa,3)}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-7">
                            <div class="row mx-0">
                                <div class="col-3 ps-0">
                                    <p class="PoppinsBold p-azul-transferir">
                                        Origen:
                                    </p>
                                </div>
                                <div class="col-9 pe-0">

                                        @if(is_object($operacion->origen))
                                        <div class="text-center d-flex align-items-center td-color-datos PoppinsMedium justify-content-center">
                                            @if($operacion->origen->banco->logo==null || $operacion->origen->banco->logo=='')
                                                <span class="span-circulo-destino"></span>
                                            @else
                                                {!! $operacion->origen->banco->logo !!}
                                            @endif
                                                {{$operacion->origen->nro_cuenta}}
                                        </div>
                                        @endif

                                </div>
                            </div>
                        </div>
                        @if(is_object($operacion->destino))
                            <div class="col-12 col-sm-7">
                                <div class="row mx-0">
                                    <div class="col-3 ps-0">
                                        <p class="PoppinsBold p-azul-transferir">
                                            Destino:
                                        </p>
                                    </div>
                                    <div class="col-9 pe-0">
                                        <div class="text-center d-flex align-items-center td-color-datos PoppinsMedium justify-content-center">
                                            @if($operacion->destino->banco->logo==null || $operacion->destino->banco->logo=='')
                                                <span class="span-circulo-destino"></span>
                                            @else
                                                {!! $operacion->destino->banco->logo !!}
                                            @endif
                                            {{$operacion->destino->nro_cuenta}}
                                        </div>
                                    </div>
                                    <div class="col-3 ps-0">
                                        <p class="PoppinsBold p-azul-transferir">
                                            Monto:
                                        </p>
                                    </div>
                                    <div class="col-9 pe-0">
                                        <p class="PoppinsRegular p-azul-transferir">
                                            {{$moneda_2}} @if($operacion->monto_banco_destino==null) {{number_format($operacion->monto_recibido,2,'.','')}} @else {{number_format($operacion->monto_banco_destino,2,'.','')}} @endif
                                        </p>
                                    </div>
                                    <div class="col-3 ps-0 referencias" @if($operacion->nro_referencia!=null && $operacion->nro_referencia!='') style="display:block;" @else style="display:none;" @endif >
                                        <p class="PoppinsBold p-azul-transferir">
                                            Nro Transferencia:
                                        </p>
                                    </div>
                                    <div class="col-9 pe-0 referencias" @if($operacion->nro_referencia!=null && $operacion->nro_referencia!='') style="display:block;" @else style="display:none;" @endif >
                                        <input type="text" class="input-login input-numero-operacion text-center input-ref" style="margin-left: 40px !important; width: 75% !important;" name="referencia_cuenta[]" value="{{$operacion->nro_referencia}}" disabled title="Referencia">
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($operacion->tipo_transferencia=='1')
                            @if(count($operacion->bancosDestinos)>0)
                                @foreach($operacion->bancosDestinos as $bancoDestino)
                                    <div class="col-10 col-sm-7">
                                        <div class="row mx-0">
                                            <div class="col-3 ps-0">
                                                <p class="PoppinsBold p-azul-transferir">
                                                    Destino:
                                                </p>
                                            </div>
                                            <div class="col-9 pe-0">

                                                <div class="text-center d-flex align-items-center td-color-datos PoppinsMedium justify-content-center">
                                                    @if($bancoDestino->destino->banco->logo==null || $bancoDestino->destino->banco->logo=='')
                                                        <span class="span-circulo-destino"></span>
                                                    @else
                                                        {!! $bancoDestino->destino->banco->logo !!}
                                                    @endif
                                                    {{$bancoDestino->destino->nro_cuenta}}
                                                </div>
                                            </div>
                                            <div class="col-3 ps-0">
                                                <p class="PoppinsBold p-azul-transferir">
                                                    Monto:
                                                </p>
                                            </div>
                                            <div class="col-9 pe-0">
                                                <p class="PoppinsRegular p-azul-transferir">
                                                    {{$moneda_2}} {{number_format($bancoDestino->monto_banco_destino,2,'.','')}}
                                                </p>
                                            </div>
                                            <div class="col-3 ps-0 referencias" @if($bancoDestino->nro_referencia!=null && $bancoDestino->nro_referencia!='') style="display:block;" @else style="display:none;" @endif >
                                                <p class="PoppinsBold p-azul-transferir">
                                                    Nro Transferencia:
                                                </p>
                                            </div>
                                            <div class="col-9 pe-0 referencias" @if($bancoDestino->nro_referencia!=null && $bancoDestino->nro_referencia!='') style="display:block;" @else style="display:none;" @endif >
                                                <input type="text" class="input-login input-numero-operacion text-center input-ref" style="margin-left: 40px !important; width: 75% !important;" name="referencia_cuenta[]" disabled value="{{$bancoDestino->nro_referencia}}" title="Referencia">
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        @endif
                        <div class="col-12 text-center mt-3">
                            <button type="button" data-bs-dismiss="modal" aria-label="Close" id="buttonRegresar" class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-5" style="color: #137188 !important;">Regresar</button><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
@section('scripts')
    <script>
        //// CONTADOR
        let DATE_TARGET = new Date('{{date("Y-m-d H:i:s",strtotime($operacion->fecha_operacion."+15 minute"))}}');

        let SPAN_MINUTES = document.querySelector('span#minutes');
        let SPAN_SECONDS = document.querySelector('span#seconds');

        // CALCULOS EN MILISEGUNDOS
        let MILLISECONDS_OF_A_SECOND = 1000;
        let MILLISECONDS_OF_A_MINUTE = MILLISECONDS_OF_A_SECOND * 60;
        let MILLISECONDS_OF_A_HOUR = MILLISECONDS_OF_A_MINUTE * 60;
        let MILLISECONDS_OF_A_DAY = MILLISECONDS_OF_A_HOUR * 24;

        var TIEMPO;

        var getDate = () => {
            $.ajax({
                url: "{{ route('get-time') }}",
                method: 'GET',
                success: function (data) {
                    TIEMPO=data.date;
                },
                error:function (error) {
                }
            });

        };

        var updateCountdown = () => {
            if(TIEMPO != undefined){
                var NOW = new Date(TIEMPO);
                let DURATION = DATE_TARGET - NOW;
                let REMAINING_DAYS = Math.floor(DURATION / MILLISECONDS_OF_A_DAY);
                let REMAINING_HOURS = Math.floor((DURATION % MILLISECONDS_OF_A_DAY) / MILLISECONDS_OF_A_HOUR);
                let REMAINING_MINUTES = Math.floor((DURATION % MILLISECONDS_OF_A_HOUR) / MILLISECONDS_OF_A_MINUTE);
                let REMAINING_SECONDS = Math.floor((DURATION % MILLISECONDS_OF_A_MINUTE) / MILLISECONDS_OF_A_SECOND);

                if((REMAINING_MINUTES == 0 && REMAINING_SECONDS == 0) || (REMAINING_MINUTES < 0 && REMAINING_SECONDS < 0)){ // SI EL CONTADOR LLEGA A 0, REFRESCAR PÁGINA
                    $("#contador").html('Anulada');
                    SPAN_MINUTES.textContent = 0;
                    SPAN_SECONDS.textContent = 0;
                    $("#buttonProcess").attr('disabled', 'disabled');
                    window.location.href = '{{route("anular-cambio",["id"=>$operacion->id])}}';
                    return;
                }

                SPAN_MINUTES.textContent = REMAINING_MINUTES;

                if(REMAINING_SECONDS < 10)
                    SPAN_SECONDS.textContent = "0"+REMAINING_SECONDS;
                else
                    SPAN_SECONDS.textContent = REMAINING_SECONDS;
            }
        }
        updateCountdown();
        setInterval(updateCountdown, MILLISECONDS_OF_A_SECOND);  // REFRESCAR CADA SEGUNDO
        getDate();
        setInterval(getDate, MILLISECONDS_OF_A_SECOND);  // REFRESCAR CADA SEGUNDO

        ///////////////////// FIN CONTADOR /////////////////////////////////////////////////
        $( document ).ready(function() {

            $("#selectOrigen").change(function () {
                if ($(this).val() == 9) {
                    $("#areaOrigen").fadeIn();
                    $("#areaOrigen").attr('required', 'required');
            } else {
            $("#areaOrigen").fadeOut();
                    $("#areaOrigen").removeAttr('required');
            }
            });

            $("#abrir-resumen").click(function () {
                $("#exampleModal").modal('show');
            });
            $("#buttonRegresar").click(function () {
                $("#exampleModal").modal('hide');
            });

            $("#form-origen-fondos").on('submit',function(e){
                e.preventDefault();
                var data = new FormData(document.getElementById('form-origen-fondos'));

                $.ajax({
                    url:'{{route("registrar-origen-fondos-ajax")}}',
                    type: "POST",
                    dataType: "JSON",
                    processData: false,
                    contentType: false,
                    cache:false,
                    data: data
                })
                    .done((res)=>{
                        console.log(res);
                        if(res.status){
                            toastr.success("Declaración de Origen de Fondos registrada correctamente!");
                            $("#div-conten-rojo").css('display','none');
                            $("#buttonProcess").removeAttr('disabled', 'disabled');
                        }
                    })
                    .fail((fail)=>{
                        $("#div-conten-rojo").css('display','none');
                        console.log(fail);
                        var errors = fail.responseJSON.errors;
                        $.each(errors,(i, item)=>{
                            toastr.error(item[0]);
                        });
                    })
            });

        });
    </script>
@endsection