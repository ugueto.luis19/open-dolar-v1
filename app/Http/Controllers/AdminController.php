<?php

namespace App\Http\Controllers;

use App\Models\Comision;
use App\Models\Perfil;
use App\Models\TasaBanco;
use App\Models\TasaCambista;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Institucion;
use App\Models\Operacion;
use App\Models\OperacionBancoDestino;
use App\Models\Verificacion;
use App\Models\Tasa;
use App\Models\Horario;
use App\Models\ComisionReferidoConfig;
use App\Models\Cupon;
use App\Models\CuentaBancariaAdmin;
use App\Models\Banco;
use App\Models\Moneda;
use App\Models\TiposCuenta;
use Redirect;
use Session;
use DB;
use View;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\RolesPermissions;
use App\Models\HasRole;
use App\Models\Permissions;
use App\Models\Roles;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('permission:tasas_cambio_listado', ['only' => ['getTasasCambio']]);
        $this->middleware('permission:tasas_cambio_agregar', ['only' => ['registrarTasaCambio']]);
        $this->middleware('permission:cupones_listado', ['only' => ['getCupones']]);
        $this->middleware('permission:cupones_agregar', ['only' => ['registrarCupon']]);
        $this->middleware('permission:cupones_eliminar', ['only' => ['eliminarCupon', 'activarCupon']]);
        $this->middleware('permission:horario_atencion_ver', ['only' => ['getHorarios']]);
        $this->middleware('permission:horario_atencion_editar', ['only' => ['editarHorario']]);
        $this->middleware('permission:operaciones_listado', ['only' => ['getOperaciones']]);
        $this->middleware('permission:operaciones_cambio_estado', ['only' => ['editarOperacion']]);
        $this->middleware('permission:verificaciones_listado', ['only' => ['getVerificaciones']]);
        $this->middleware('permission:verificaciones_aprobar', ['only' => ['procesarVerificacion']]);
        $this->middleware('permission:verificaciones_anular', ['only' => ['anularVerificacion']]);
        $this->middleware('permission:configuracion_opencreditos_listado', ['only' => ['getComisionesConfig']]);
        $this->middleware('permission:configuracion_opencreditos_agregar', ['only' => ['registrarComisionConfig']]);
        $this->middleware('permission:instituciones_listado', ['only' => ['getInstituciones']]);
        $this->middleware('permission:instituciones_editar', ['only' => ['editarInstitucion']]);
        $this->middleware('permission:instituciones_eliminar', ['only' => ['eliminarInstitucion']]);
        $this->middleware('permission:clientes_listado', ['only' => ['getClientes']]);
        $this->middleware('permission:clientes_ver_historial', ['only' => ['verOperaciones']]);
        $this->middleware('permission:clientes_bloquear_acceso', ['only' => ['cambiarEstadoUsuario']]);
        $this->middleware('permission:usuarios_listado', ['only' => ['getUsuarios']]);
        $this->middleware('permission:usuarios_agregar', ['only' => ['registrarUsuario']]);
        $this->middleware('permission:usuarios_editar', ['only' => ['editarUsuario']]);
        $this->middleware('permission:usuarios_eliminar', ['only' => ['eliminarUsuario']]);
        $this->middleware('permission:roles_listado', ['only' => ['getRoles']]);
        $this->middleware('permission:roles_agregar', ['only' => ['registrarRol']]);
        $this->middleware('permission:roles_editar', ['only' => ['editarRol']]);
        $this->middleware('permission:cuentas_bancarias_ad_listado', ['only' => ['getCuentasBancariasAdmin']]);
        $this->middleware('permission:cuentas_bancarias_ad_agregar', ['only' => ['registrarCuentasBancariasAdmin']]);
        $this->middleware('permission:cuentas_bancarias_ad_editar', ['only' => ['editarCuentasBancariasAdmin']]);
        $this->middleware('permission:cuentas_bancarias_ad_eliminar', ['only' => ['eliminarCuentasBancariasAdmin']]);
    }

    public function getClientes(){
        $menu=4;
        $usuarios = User::role('Cliente')->paginate(10);

        return view('admin.usuarios.index', compact('menu','usuarios'));
    }

    public function verPerfiles($id_usuario){
        $menu=4;
        $perfiles = Perfil::where('id_cuenta',$id_usuario)->paginate(10);

        return view('admin.usuarios.ver-perfiles', compact('menu','perfiles'));
    }

    public function verInformacionCompleta($id_perfil){
        $menu=4;
        $perfil=Perfil::find($id_perfil);
        $operaciones = Operacion::where('id_perfil',$id_perfil)->orderby('id','desc')->get();
        $verificacion = Verificacion::where('id_perfil', $id_perfil)->orderBy('created_at', 'desc')->first();
        $comisiones = Comision::orderBy('id','desc')
            ->where('id_perfil_padre',$id_perfil)
            ->get();
        return view('admin.usuarios.ver-informacion-completa-perfil', compact('menu','operaciones','perfil','verificacion','comisiones'));
    }

    public function getInstituciones(){
        $menu=6;
        $instituciones = Institucion::paginate(10);

        return view('admin.instituciones.index', compact('menu','instituciones'));
    }

    public function getVerificaciones(){
        $menu=3;
        $verificaciones = Verificacion::orderBy('id','desc')->paginate(10);

        return view('admin.verificaciones.index', compact('menu','verificaciones'));
    }

    public function getOperaciones(){
        $menu=2;
        $operaciones = Operacion::orderBy('created_at','desc')
            ->with('perfil','destino.banco','origen.banco')
            ->paginate(10);

        return view('admin.operaciones.index', compact('menu','operaciones'));
    }

    public function getTasasCambio(){
        $menu = 7;
        $tasas = Tasa::orderBy('id','desc')->paginate(10);
        $tasa_actual=Tasa::orderBy('id','desc')->first();
        $tasas_cambistas = TasaCambista::orderBy('id','desc')->paginate(10);
        $tasa_cambista_actual=TasaCambista::orderBy('id','desc')->first();
        $tasas_bancos = TasaBanco::orderBy('id','desc')->paginate(10);
        $tasa_banco_actual = TasaBanco::orderBy('id','desc')->first();
        return view('admin.tasas_cambio.index', compact('menu','tasas','tasas_cambistas','tasas_bancos','tasa_actual','tasa_cambista_actual','tasa_banco_actual'));
    }

    public function filtrarTasas(Request $request)
    {
        $this->validate($request, [
            'desde' => 'required',
            'hasta' => 'required'
        ]);

        $menu=7;
        $desde = $request->desde;
        $hasta = $request->hasta;

        $tabs=$request->tabs;
        session(['tabs' => $tabs]);
        if($tabs=='operaciones-tab')
        {
            $tasas = Tasa::
                whereDate('created_at','>=',date("Y-m-d", strtotime($desde)))
                ->whereDate('created_at','<=',date("Y-m-d", strtotime($hasta)))
                ->orderBy('id','desc')
                ->paginate(10);
        }
        else
        {
            $tasas = Tasa::orderBy('id','desc')->paginate(10);
        }

        if($tabs=='cambistas-tab')
        {
            $tasas_cambistas = TasaCambista::whereDate('created_at','>=',date("Y-m-d", strtotime($desde)))
                ->whereDate('created_at','<=',date("Y-m-d", strtotime($hasta)))
                ->orderBy('id','desc')
                ->paginate(10);
        }
        else
        {
            $tasas_cambistas = TasaCambista::orderBy('id','desc')->paginate(10);
        }

        if($tabs=='bancos-tab')
        {
            $tasas_bancos = TasaBanco::whereDate('created_at','>=',date("Y-m-d", strtotime($desde)))
                ->whereDate('created_at','<=',date("Y-m-d", strtotime($hasta)))
                ->orderBy('id','desc')
                ->paginate(10);
        }
        else
        {
            $tasas_bancos = TasaBanco::orderBy('id','desc')->paginate(10);
        }
        $tasa_actual=Tasa::orderBy('id','desc')->first();
        $tasa_cambista_actual=TasaCambista::orderBy('id','desc')->first();
        $tasa_banco_actual = TasaBanco::orderBy('id','desc')->first();
        return view('admin.tasas_cambio.index',compact('menu','tasas','desde', 'hasta','tabs','tasas_cambistas','tasas_bancos','tasa_actual','tasa_cambista_actual','tasa_banco_actual'));
    }

    public function getHorarios(){
        $menu = 1;
        $horarios = Horario::orderBy('id','asc')->get();

        return view('admin.horarios.index', compact('menu','horarios'));
    }

    public function registrarTasaCambio(Request $request){
        $this->validate($request, [
            'tasa_compra' => 'required',
            'tasa_venta' => 'required',
            'pips_compra' => 'required',
            'pips_venta' => 'required',
        ]);

        $tabs=$request->tabs;
        session(['tabs' => $tabs]);
        $tasa = new Tasa($request->all());
        $tasa->save();

        $notification = array(
            'message' => 'La tasa de cambio registro correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('tasas-cambio')->with($notification);

    }

    public function registrarTasaCambioCambistas(Request $request){
        $this->validate($request, [
            'tasa_compra' => 'required',
            'tasa_venta' => 'required',
        ]);
        $tabs=$request->tabs;
        session(['tabs' => $tabs]);
        $tasa = new TasaCambista($request->all());
        $tasa->save();

        $notification = array(
            'message' => 'La tasa de cambio Cambista se registro correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('tasas-cambio')->with($notification);
    }

    public function registrarTasaCambioBancos(Request $request){
        $this->validate($request, [
            'tasa_compra' => 'required',
            'tasa_venta' => 'required',
        ]);
        $tabs=$request->tabs;
        session(['tabs' => $tabs]);
        $tasa = new TasaBanco($request->all());
        $tasa->save();

        $notification = array(
            'message' => 'La tasa de cambio Banco se registro correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('tasas-cambio')->with($notification);
    }

    public function registrarInstitucion(Request $request){
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $inst = new Institucion($request->all());
        $inst->save();

        $notification = array(
            'message' => 'La institución se registro correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('instituciones')->with($notification);
    }

    public function eliminarInstitucion($id){
        $institucion = Institucion::find($id);
        $institucion->estado = 0;
        $institucion->update();

        $notification = array(
            'message' => 'La institución se eliminó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('instituciones')->with($notification);
    }

    public function activarInstitucion($id){
        $institucion = Institucion::find($id);
        $institucion->estado = 1;
        $institucion->update();

        $notification = array(
            'message' => 'La institución se activó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('instituciones')->with($notification);
    }

    public function cambiarEstadoVerificacion(Request $request){

        if($request->ajax())
        {
            $verificacion = Verificacion::find($request->id);

            if($verificacion->estado == 0 || $verificacion->estado == 2)
                $verificacion->estado = 1;
            elseif($verificacion->estado == 1)
                $verificacion->estado = 2;

            $verificacion->update();

            return response()->json(['resultado' => $request->all()]);
        }
    }

    public function anularVerificacion(Request $request, $id){
        if($request->ajax())
        {
            $verificacion = Verificacion::find($id);

            $estado_old=$verificacion->estado;

            $verificacion->estado = 2;
            $verificacion->update();

            return response()->json(['estado_old' => $estado_old]);
        }
    }

    public function procesarVerificacion($id){
        $verificacion = Verificacion::find($id);
        $verificacion->estado = 1;
        $verificacion->update();

        $notification = array(
            'message' => 'Verificación procesada correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('verificaciones')->with($notification);
    }

    public function editarOperacion(Request $request){
        $operacion = Operacion::find($request->id);


        if(intval($request->estado)==7)
        {
            // Ejecutamos el envio de correo de que la orden esta verificada
        }
        elseif(intval($request->estado)==2)
        {

            // Creamos la factura con la api de nubefact

            // PASO 1: Token y url

            // RUTA para enviar documentos
            $ruta = "https://api.nubefact.com/api/v1/d5672fab-a2d3-42cd-9696-5cf3f12e2fbf";

            //TOKEN para enviar documentos
            $token = "920a9c9011384f8da623c64f2f35cfbe5fb477b28d3743ffabaec7e53e5821a1";

            // PASO 2: GENERAR EL ARCHIVO PARA ENVIAR A NUBEFACT

            $cliente_tipo_de_documento=0;
            $cliente_numero_de_documento=0;
            $cliente_denominacion=null;
            $cliente_direccion=null;
            $tipo_comprobante=0;
            $serie=null;
            if($operacion->perfil->tipo=='2')
            {
                // ruc
                $tipo_comprobante=1;
                $cliente_tipo_de_documento=6;
                $cliente_numero_de_documento=$operacion->perfil->ruc;

                $cliente_denominacion=$operacion->perfil->nombre_juridico;
                $cliente_direccion=$operacion->perfil->direccion_juridico;
                $serie="FFF1";
            }
            else
            {
                $tipo_comprobante=2;
                $serie="BBB1";
                if($operacion->perfil->tipo_doc=='1')
                {
                    // dni
                    $cliente_tipo_de_documento=1;
                }
                elseif($operacion->perfil->tipo_doc=='3')
                {
                    // CE
                    $cliente_tipo_de_documento=4;
                }
                elseif($operacion->perfil->tipo_doc=='2')
                {
                    // PTP
                    $cliente_tipo_de_documento=7;
                }
                $cliente_numero_de_documento=$operacion->perfil->nro_doc;
                $cliente_denominacion=$operacion->perfil->nombres.' '.$operacion->perfil->apellido_paterno.' '.$operacion->perfil->apellido_materno;
                $cliente_direccion='N/A.';
            }

            if($operacion->tipo=='0')
            {
                $moneda=1;
                if(is_object($operacion->cupon))
                {
                    $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta-$operacion->cupon->valor_soles;
                }
                else
                {
                    $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta;
                }
                $mensaje="Cambio de Soles a Dolares";
            }
            else
            {
                $moneda=2;
                if(is_object($operacion->cupon))
                {
                    $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra+($operacion->cupon->valor_soles/$operacion->tasa->tasa_compra);
                }
                else
                {
                    $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra;
                }
                $mensaje="Cambio de Dolares a Soles";
            }



            $data = array(
                "operacion"				=> "generar_comprobante",
                "tipo_de_comprobante"               => $tipo_comprobante,
                "serie"                             => $serie,
                "numero"				=> $operacion->id,
                "sunat_transaction"			=> "1",
                "cliente_tipo_de_documento"		=> $cliente_tipo_de_documento,
                "cliente_numero_de_documento"	=> $cliente_numero_de_documento,
                "cliente_denominacion"              => $cliente_denominacion,
                "cliente_direccion"                 => $cliente_direccion,
                "cliente_email"                     => $operacion->perfil->usuario->email,
                "cliente_email_1"                   => "",
                "cliente_email_2"                   => "",
                "fecha_de_emision"                  => date('d-m-Y'),
                "fecha_de_vencimiento"              => "",
                "moneda"                            => $moneda,
                "tipo_de_cambio"                    => number_format($monto_tasa,3,'.',''),
                "porcentaje_de_igv"                 => "18.00",
                "descuento_global"                  => "",
                "total_descuento"                   => "",
                "total_anticipo"                    => "",
                "total_gravada"                     => "",
                "total_inafecta"                    => "",
                "total_exonerada"                   => "",
                "total_igv"                         => "0",
                "total_gratuita"                    => number_format($operacion->monto_recibido,2,'.',''),
                "total_otros_cargos"                => "",
                "total"                             => number_format($operacion->monto_recibido,2,'.',''),
                "percepcion_tipo"                   => "",
                "percepcion_base_imponible"         => "",
                "total_percepcion"                  => "",
                "total_incluido_percepcion"         => "",
                "detraccion"                        => false,
                "observaciones"                     => "",
                "documento_que_se_modifica_tipo"    => "",
                "documento_que_se_modifica_serie"   => "",
                "documento_que_se_modifica_numero"  => "",
                "tipo_de_nota_de_credito"           => "",
                "tipo_de_nota_de_debito"            => "",
                "enviar_automaticamente_a_la_sunat" => true,
                "enviar_automaticamente_al_cliente" => true,
                "codigo_unico"                      => "",
                "condiciones_de_pago"               => "",
                "medio_de_pago"                     => "",
                "placa_vehiculo"                    => "",
                "orden_compra_servicio"             => "",
                "tabla_personalizada_codigo"        => "",
                "formato_de_pdf"                    => "A4",
                "items" => array(
                    array(
                        "unidad_de_medida"          => "ZZ",
                        "codigo"                    => $operacion->id,
                        "descripcion"               => $mensaje,
                        "cantidad"                  => number_format($operacion->monto_enviado,2,'.',''),
                        "valor_unitario"            => number_format($monto_tasa,3,'.',''),
                        "precio_unitario"           => number_format($monto_tasa,3,'.',''),
                        "descuento"                 => "",
                        "subtotal"                  => number_format($operacion->monto_recibido,2,'.',''),
                        "tipo_de_igv"               => "17",
                        "igv"                       => "0",
                        "total"                     => number_format($operacion->monto_recibido,2,'.',''),
                        "anticipo_regularizacion"   => false,
                        "anticipo_documento_serie"  => "",
                        "anticipo_documento_numero" => ""
                    )
                )
            );

            $data_json = json_encode($data);

            //PASO 3: ENVIAR EL ARCHIVO A NUBEFACT
            //Invocamos el servicio de NUBEFACT
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $ruta);
            curl_setopt(
                $ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Token token="'.$token.'"',
                    'Content-Type: application/json',
                )
            );
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $respuesta  = curl_exec($ch);
            curl_close($ch);

            //PASO 4: LEER RESPUESTA DE NUBEFACT
            $leer_respuesta = json_decode($respuesta, true);
            if (isset($leer_respuesta['errors'])) {
                //Mostramos los errores si los hay
                $notification = array(
                    'message' => $leer_respuesta['errors'],
                    'alert-type' => 'error'
                );
                return redirect()->route('operaciones')->with($notification);
            } else {
                // leer respuesta -> $leer_respuesta
                $operacion->url_factura=$leer_respuesta['enlace_del_pdf'];
                $operacion->key_nubefact=$leer_respuesta['key'];
                $operacion->save();
            }
            // FIN API
        }

        $operacion->update($request->all());
        if($operacion->tipo_transferencia == 1 && $request->estado == 2){
            foreach ($request->referencia_cuenta as $key => $referencia)
            {
                if($key==0)
                {
                    $operacion->nro_referencia=$referencia;
                    $operacion->save();
                }
                else
                {
                    $operacionBanco = OperacionBancoDestino::find($operacion->bancosDestinos[$key-1]->id);
                    $operacionBanco->nro_referencia = $referencia;
                    $operacionBanco->update();
                }
            }
        }
        elseif($operacion->tipo_transferencia != 1 && $request->estado == 2)
        {
            foreach ($request->referencia_cuenta as $key => $referencia)
            {
                if($key==0)
                {
                    $operacion->nro_referencia=$referencia;
                    $operacion->save();
                }
            }
        }

        if($request->estado==2)
        {
            $notification = array(
                'message' => 'La operación se Realizo correctamente.',
                'alert-type' => 'success'
            );
        }
        else
        {
            $notification = array(
                'message' => 'La operación se modificó correctamente.',
                'alert-type' => 'success'
            );
        }

        return redirect()->route('operaciones')->with($notification);
    }

    public function editarHorario(Request $request){
        $this->validate($request, [
            'hora_inicio' => 'required',
            'hora_cierre' => 'required'
        ]);

        $horario = Horario::find($request->id);
        $horario->update($request->all());

        $notification = array(
            'message' => 'El horario se modificó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('horarios')->with($notification);
    }

    public function editarInstitucion(Request $request){
        $this->validate($request, [
            'id' => 'required',
            'nombre' => 'required'
        ]);

        $horario = Institucion::find($request->id);
        $horario->update($request->all());

        $notification = array(
            'message' => 'La instutición se modificó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('instituciones')->with($notification);
    }

    public function getComisionesConfig(){
        $menu = 5;
        $comisiones = ComisionReferidoConfig::orderBy('id','desc')->paginate(10);

        return view('admin.comisiones_config.index', compact('menu','comisiones'));
    }

    public function registrarComisionConfig(Request $request){
         $this->validate($request, [
            'monto_ganar' => 'required',
            'monto_operacion_minimo' => 'required',
            'tasa_opencredit' => 'required'
        ]);

        $comisiones = new ComisionReferidoConfig($request->all());
        $comisiones->save();

        $notification = array(
            'message' => 'La Comisión Config se registró correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('comisiones-config')->with($notification);
    }

    public function filtrarOperaciones(Request $request)
    {
        $this->validate($request, [
            'desde' => 'required',
            'hasta' => 'required'
        ]);

        $menu=2;
        $desde = $request->desde;
        $hasta = $request->hasta;

        $operaciones = Operacion::orderBy('id','desc')
            ->whereDate('fecha_operacion','>=',date("Y-m-d", strtotime($desde)))
            ->whereDate('fecha_operacion','<=',date("Y-m-d", strtotime($hasta)))
            ->with('perfil','destino.banco','origen.banco')
            ->get();

        return view('admin.operaciones.index',compact('menu','operaciones','desde', 'hasta'));
    }

    public function getCupones(){
        $menu = 8;
        $cupones = Cupon::orderBy('fecha_caducidad', 'asc')
            ->orderBy('estado', 'desc')
            ->paginate(10);

        return view('admin.cupones.index', compact('menu','cupones'));

    }

    public function registrarCupon(Request $request){
        $data = $request->all();
        $data['valor_pips'] = $request->valor_soles*10000;

        $cupon = new Cupon($data);
        $cupon->save();

        $notification = array(
            'message' => 'El cupon se registró correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('cupones')->with($notification);
    }


    public function eliminarCupon($id){
        $cupon = Cupon::find($id);
        $cupon->estado = 0;
        $cupon->update();

        $notification = array(
            'message' => 'El cupon se eliminó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('cupones')->with($notification);
    }

    public function activarCupon($id){
        $cupon = Cupon::find($id);
        $cupon->estado = 1;
        $cupon->update();

        $notification = array(
            'message' => 'El cupon se activó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('cupones')->with($notification);
    }

    public function cambiarEstadoUsuario(Request $request)
    {
        if($request->ajax())
        {
            $user = User::find($request->id);

            if($user->estado == 0)
                $user->estado = 1;
            elseif($user->estado == 1)
                $user->estado = 0;

            $user->update();

            return response()->json(['resultado' => $request->all()]);
        }
    }

    public function cambiarEstadoCupon(Request $request)
    {
        if($request->ajax())
        {
            $cupon = Cupon::find($request->id);

            if($cupon->estado == 0)
                $cupon->estado = 1;
            elseif($cupon->estado == 1)
                $cupon->estado = 0;

            $cupon->update();

            return response()->json(['resultado' => $request->all()]);
        }
    }

    public function getRoles(){
        $menu = 9;
        $roles = Roles::where('name', '!=', 'Administrador')->with('permisos')->paginate(10);
        $permisos = Permissions::all();
     
        return view('admin.roles.index', compact('menu','roles', 'permisos'));
    }

    public function registrarRol(Request $request){
        $this->validate($request, [
            'name' => 'required'
        ]);
        $role = new Role();
        $role->name = $request->name;
        $role->save();

        if(DB::table('role_has_permissions')->where('role_id', $role->id)->count() > 0)
            DB::table('role_has_permissions')->where('role_id', $role->id)->delete();

        foreach($request->permission as $permission){
            $role->givePermissionTo($permission);
        }

        $notification = array(
            'message' => 'El rol se registró correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('roles')->with($notification);
    }

    public function editarRol(Request $request){
        $this->validate($request, [
            'name' => 'required'
        ]);

        $role = Role::find($request->id);
        $role->update($request->all());

        if(DB::table('role_has_permissions')->where('role_id', $role->id)->count() > 0)
            DB::table('role_has_permissions')->where('role_id', $role->id)->delete();

        foreach($request->permission as $permission){
            $role->givePermissionTo($permission);
        }

        $notification = array(
            'message' => 'El rol se editó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('roles')->with($notification);
    }

    public function eliminarRol($id){
        $role = Role::find($id);
        
        if($role->name == "Cliente"){
            $notification = array(
                'message' => 'No se puede eliminar el Rol.',
                'alert-type' => 'info'
            );
            return redirect()->route('roles')->with($notification);
        }

        Role::destroy($id);

        $notification = array(
            'message' => 'El rol se eliminó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('roles')->with($notification);
    }

    public function getUsuarios(){
        $menu = 10;
        $usuarios = User::whereHas("roles", function($q) {
                $q->where("id", "!=", "1");
            })->paginate(10);
        $roles = Roles::where("id", "!=", 1)->get();
     
        return view('admin.usuarios.usuarios', compact('menu','usuarios','roles'));
    }

    public function registrarUsuario(Request $request){
        $this->validate($request, [
            'email' => 'required|unique:users,email',
            'nro_celular' => 'required|min:8',
            'password' => 'required|min:8',
            'password_confirm' => 'required|min:8|required|same:password',
        ]);

        $request['password']=Hash::make($request->password);
        $new_user=new User($request->all());
        $new_user->save();

        $new_user->assignRole($request->rol);

        $notification = array(
            'message' => 'El usuario se registró correctamente.',
            'alert-type' => 'success'
        );

        return redirect()->route('usuarios')->with($notification);
    }

    public function editarUsuario(Request $request){
        if(!is_null($request->password)){
            $this->validate($request, [
                'email' => 'required',
                'nro_celular' => 'required|min:8',
                'password' => 'required|min:8',
                'password_confirm' => 'required|min:8|required|same:password',
                'rol' => 'required'

            ]);
        }else{
            $this->validate($request, [
                'email' => 'required',
                'nro_celular' => 'required|min:8',
                'rol' => 'required'
            ]);
        }

        $data = $request->all();

        $user = User::find($request->id);
        
        if($request->password==null){
            $data['password'] = $user->password;
        }else{
            $data['password'] = Hash::make($request->password);
        }

        $user->update($data);
        
        DB::table('model_has_roles')->where('model_id',$user->id)->delete();

        $user->assignRole($request->rol);

        $notification = array(
            'message' => 'El usuario se editó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('usuarios')->with($notification);
    }

    public function eliminarUsuario($id){
        User::destroy($id);

        $notification = array(
            'message' => 'El usuario se eliminó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('usuarios')->with($notification);
    }

    public function getCuentasBancariasAdmin(){
        $menu=11;
        $cuentas_bancarias = CuentaBancariaAdmin::paginate(10);
        $bancos=Banco::all();
        $monedas=Moneda::all();
        $tipos_cuentas=TiposCuenta::all();

        return view('admin.cuentas_bancarias_admin.index', compact('menu','bancos','monedas','tipos_cuentas','cuentas_bancarias'));
    }

    public function registrarCuentasBancariasAdmin(Request $request){
        $this->validate($request, [
            'nro_cuenta' => 'required',
            'titular' => 'required',
            'id_banco' => 'required',
            'id_moneda' => 'required',
            'id_tipo_cuenta' => 'required'
        ]);

        $cuenta=new CuentaBancariaAdmin($request->all());
        $cuenta->save();

        $notification = array(
            'message' => 'El usuario se registró correctamente.',
            'alert-type' => 'success'
        );

        return redirect()->route('cuentas_bancarias_admin')->with($notification);
    }

    public function editarCuentasBancariasAdmin(Request $request){
        $this->validate($request, [
            'nro_cuenta' => 'required',
            'titular' => 'required',
            'id_banco' => 'required',
            'id_moneda' => 'required',
            'id_tipo_cuenta' => 'required'
        ]);

        $cuenta = CuentaBancariaAdmin::find($request->id);
        $cuenta->update($request->all());
        

        $notification = array(
            'message' => 'La cuenta bancaria se editó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('cuentas_bancarias_admin')->with($notification);
    }

    public function eliminarCuentasBancariasAdmin($id){
        $cuenta = CuentaBancariaAdmin::find($id);
        $cuenta->estado = 0;
        $cuenta->update();

        $notification = array(
            'message' => 'La cuenta bancaria se eliminó correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('cuentas_bancarias_admin')->with($notification);
    }

    public function buscarInstitucion(Request $request)
    {
        if($request->ajax()) {
            $ins=Institucion::where('estado',1)->where(function ($query) use ($request) {
                $query->where('nombre', 'like', '%' . $request->buscar . '%')
                      ->orWhere('poder', 'like', '%' . $request->buscar . '%');
            })->paginate(5);

            $view = View::make('partes.instituciones')
                ->with('instituciones', $ins)
                ->render();

            return response()->json(['status' => true, 'htmlview' => $view]);
        }
    }

}
