@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Configuración OpenCreditos</h1>

        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
            <div class="div-scrolll">
                <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                    <div class="col-sm-7 col-md-6 col-lg-5 text-center text-sm-start">
                    
                    </div>
                    <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                        @canany('configuracion_opencreditos_agregar')
                            <button class="btn btn-login PoppinsMedium" data-bs-toggle="modal" data-bs-target="#exampleModal">Nueva Configuración OpenCreditos</button>
                        @endcanany
                    </div>
                </div>

                <table class="table table-border-cero" id="comisiones">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Fecha y Hora</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Monto a Ganar</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Tasa Opencreditos</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Monto Mínimo de Operación</th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                    @if(count($comisiones)>0)
                        @foreach($comisiones as $com)
                            <tr class="tr-border-top">
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                   {{date('d-m-Y g:i a',strtotime($com->created_at))}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$com->monto_ganar}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$com->tasa_opencredit}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$com->monto_operacion_minimo}}
                                </th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{ $comisiones->links() }}
            </div>
        </div>
    </div>

    @canany('configuracion_opencreditos_agregar')
       <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center" style="line-height: 1.5;">
                                Agregar nueva Configuración OpenCreditos
                            </h1>
                            <form method="post" action="{{ route('registrar-comision-config') }}">
                                @csrf
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any"  name="monto_ganar" class="input-login PoppinsRegular" placeholder="Ingrese monto a ganar" required>
                                        </div>
                                    </div>
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="monto_operacion_minimo" class="input-login PoppinsRegular" placeholder="Ingrese monto minimo de operación" required>
                                        </div>
                                    </div>
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="tasa_opencredit" class="input-login PoppinsRegular" placeholder="Valor en Soles de 1  OpenCreditos" required>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
           // $('#comisiones').DataTable();
        } );
    </script>

@endsection
