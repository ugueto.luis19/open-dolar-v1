@extends('layouts.index_app')

@section('content')

    <div class="conten_panel conten_panel_login mt-3 mt-lg-0">
            <div class="col-12">
                <a href="{{route('login')}}" class="btn btn-volver-azul PoppinsRegular">&lt; Volver</a>
            </div>
        <div class="d-flex" style="height: 70vh;">
        <div class="div-conten-login pt-0 div-conten-contrasena">
            <div class="d-flex justify-content-center">
                <div class="d-flex justify-content-center" style="margin-top: -120px; margin-bottom: 25px; width: 95px;  height: 95px; -moz-border-radius: 50%;  -webkit-border-radius: 50%; border-radius: 50%; background: #1F9F99;">
                    <img src="/assets-web/img/pass.svg" style="    margin-left: -6px; width: 55px;">
                </div>
            </div>
			<h1 class="PoppinsBold text-center mb-0" >Recupera tu contraseña</h1>
                      
           
            <div class="row mx-0 justify-content-center mt-5 row-padding-left-right-login">
            <p class="PoppinsRegular p-login-contrasena">Te mandaremos un link para establecer una nueva contraseña.</p>
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="col-12 mb-4">
                        <div class="div-form-login">
                            <label class="label-login PoppinsRegular">Ingresa tu correo</label>
                            <input type="email" name="email" :value="old('email')" required class="input-login PoppinsRegular">
                        </div>
                    </div>
                    <!--div class="col-12 text-center px-sm-5">
                        <div class="form-check text-start">
                          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" required>
                          <label class="form-check-label p-login-contrasena" for="flexCheckDefault">
                            Acepto los <a href="">términos y condiciones</a> 
                          </label>
                        </div>
                    </div-->
                    <div class="col-12 text-center mt-4">
                        <button type="submit" class="btn btn-login PoppinsMedium px-5">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </div>

@endsection