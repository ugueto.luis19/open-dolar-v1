<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperacionBancoDestino extends Model
{
    use HasFactory;
    protected $table = 'operacion_banco_destino';
    protected $fillable = [
        'id_operacion',
        'id_cuenta_destino',
        'monto_banco_destino',
        'nro_referencia'
    ];

    public function operacion()
    {
        return $this->hasOne(Tasa::class, 'id', 'id_operacion');
    }

    public function destino()
    {
        return $this->hasOne(CuentaBancaria::class, 'id', 'id_cuenta_destino');
    }

}
