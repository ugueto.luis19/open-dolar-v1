

<?php
if($operacion->tipo=='0')
{
    $moneda_1='S/';
    $moneda_2='$';
    if(is_object($operacion->cupon))
    {
        $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta-$operacion->cupon->valor_soles;
    }
    else
    {
        $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta;
    }
}
else{
    $moneda_1='$';
    $moneda_2='S/';
    if(is_object($operacion->cupon))
    {
        $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra+($operacion->cupon->valor_soles/$operacion->tasa->tasa_compra);
    }
    else
    {
        $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra;
    }
}

?>

<div class="row mx-0 justify-content-center mt-5 align-items-center">

    <h2 class="PoppinsMedium subtitulo-card text-center mb-4" id="titulo">Detalles de la operación:</h2>

    <?php
    switch($operacion->estado){
        case '0':
            $o_alerta = "o_pendientes";
            $class_alerta = "pendientes";
            $spanEstado = "Pendiente";
            break;
        case '1':
            $o_alerta = "o_proceso";
            $class_alerta = "proceso";
            $spanEstado = "En Proceso";
            break;
        case '2':
            $o_alerta = "o_realizada";
            $class_alerta = "realizada";
            $spanEstado = "Realizado";
            break;
        case '3':
            $o_alerta = "o_devolucion";
            $class_alerta = "devolucion";
            $spanEstado = "Devolución";
            break;
        case '4':
            $o_alerta = "o_devuelta";
            $class_alerta = "devuelta";
            $spanEstado = "Devuelto";
            break;
        case '5':
            $o_alerta = "o_observado";
            $class_alerta = "observado";
            $spanEstado = "Observados";
            break;
        case '6':
            $o_alerta = "o_anulado";
            $class_alerta = "anulado";
            $spanEstado = "Anulado";
            break;
        default:
            $o_alerta = null;
            $class_alerta = null;
            $spanEstado = null;
            break;
    }

    ?>

    <div class="col-10 col-sm-7">
        <div class="row mx-0">
            <div class="col-4 ps-0">
                <p class="PoppinsBold p-azul-transferir">
                    Estado:
                </p>
            </div>
            <div class="col-8 pe-0">
                <p class="PoppinsRegular p-azul-transferir">
                    {{$spanEstado}}
                </p>
            </div>
        </div>
    </div>
    <div class="col-10 col-sm-7">
        <div class="row mx-0">
            <div class="col-4 ps-0">
                <p class="PoppinsBold p-azul-transferir">
                    Fecha:
                </p>
            </div>
            <div class="col-8 pe-0">
                <p class="PoppinsRegular p-azul-transferir">
                    {{date('d-m-Y H:i:s',strtotime($operacion->fecha_operacion))}}
                </p>
            </div>
        </div>
    </div>
    @if($operacion->nro_transferencia!='' && $operacion->nro_transferencia!=null)
        <div class="col-10 col-sm-7">
            <div class="row mx-0">
                <div class="col-4 ps-0">
                    <p class="PoppinsBold p-azul-transferir">
                        # Transferencia:
                    </p>
                </div>
                <div class="col-8 pe-0">
                    <p class="PoppinsRegular p-azul-transferir">
                        {{$operacion->nro_transferencia}}
                    </p>
                </div>
            </div>
        </div>
    @endif
    @if($operacion->uso_opendolar!='' && $operacion->uso_opendolar!=null)
        <div class="col-10 col-sm-7">
            <div class="row mx-0">
                <div class="col-4 ps-0">
                    <p class="PoppinsBold p-azul-transferir">
                        OpenCreditos:
                    </p>
                </div>
                <div class="col-8 pe-0">
                    <p class="PoppinsRegular p-azul-transferir">
                        {{$operacion->uso_opendolar}}
                    </p>
                </div>
            </div>
        </div>
    @endif
    @if(is_object($operacion->cupon))
        <div class="col-10 col-sm-7">
            <div class="row mx-0">
                <div class="col-4 ps-0">
                    <p class="PoppinsBold p-azul-transferir">
                        Cupon:
                    </p>
                </div>
                <div class="col-8 pe-0">
                    <p class="PoppinsRegular p-azul-transferir">
                        {{$operacion->cupon->codigo}}
                    </p>
                </div>
            </div>
        </div>
    @endif
    <div class="col-10 col-sm-7">
        <div class="row mx-0">
            <div class="col-4 ps-0">
                <p class="PoppinsBold p-azul-transferir">
                    Monto:
                </p>
            </div>
            <div class="col-8 pe-0">
                <p class="PoppinsRegular p-azul-transferir">
                    {{$moneda_1}} {{number_format($operacion->monto_enviado,2)}}
                </p>
            </div>
        </div>
    </div>
    <div class="col-10 col-sm-7">
        <div class="row mx-0">
            <div class="col-4 ps-0">
                <p class="PoppinsBold p-azul-transferir">
                    Recibido:
                </p>
            </div>
            <div class="col-8 pe-0">
                <p class="PoppinsRegular p-azul-transferir">
                    {{$moneda_2}} {{number_format($operacion->monto_recibido,2)}}
                </p>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-7">
        <div class="row mx-0">
            <div class="col-4 ps-0">
                <p class="PoppinsBold p-azul-transferir">
                    Cambio:
                </p>
            </div>
            <div class="col-8 pe-0">
                <p class="PoppinsRegular p-azul-transferir">
                    {{number_format($monto_tasa,3)}}
                </p>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-7">
        <div class="row mx-0">
            <div class="col-3 ps-0">
                <p class="PoppinsBold p-azul-transferir">
                    Origen:
                </p>
            </div>
            <div class="col-9 pe-0">

                @if(is_object($operacion->origen))
                    <div class="text-center d-flex align-items-center td-color-datos PoppinsMedium justify-content-center">
                        @if($operacion->origen->banco->logo==null || $operacion->origen->banco->logo=='')
                            <span class="span-circulo-destino"></span>
                        @else
                            {!! $operacion->origen->banco->logo !!}
                        @endif
                        {{$operacion->origen->nro_cuenta}}
                    </div>
                @endif

            </div>
        </div>
    </div>
    @if(is_object($operacion->destino))
    <div class="col-12 col-sm-7">
        <div class="row mx-0">
            <div class="col-3 ps-0">
                <p class="PoppinsBold p-azul-transferir">
                    Destino:
                </p>
            </div>
            <div class="col-9 pe-0">
                <div class="text-center d-flex align-items-center td-color-datos PoppinsMedium justify-content-center">
                    @if($operacion->destino->banco->logo==null || $operacion->destino->banco->logo=='')
                        <span class="span-circulo-destino"></span>
                    @else
                        {!! $operacion->destino->banco->logo !!}
                    @endif
                    {{$operacion->destino->nro_cuenta}}
                </div>
            </div>
            <div class="col-3 ps-0">
                <p class="PoppinsBold p-azul-transferir">
                    Monto:
                </p>
            </div>
            <div class="col-9 pe-0">
                <p class="PoppinsRegular p-azul-transferir">
                    {{$moneda_2}} @if($operacion->monto_banco_destino==null) {{number_format($operacion->monto_recibido,2)}} @else {{number_format($operacion->monto_banco_destino,2)}} @endif
                </p>
            </div>
            <div class="col-3 ps-0 referencias" @if($operacion->nro_referencia!=null && $operacion->nro_referencia!='') style="display:block;" @else style="display:none;" @endif >
                <p class="PoppinsBold p-azul-transferir">
                    Nro Transferencia:
                </p>
            </div>
            <div class="col-9 pe-0 referencias" @if($operacion->nro_referencia!=null && $operacion->nro_referencia!='') style="display:block;" @else style="display:none;" @endif >
                <input type="text" class="input-login input-numero-operacion text-center input-ref" style="margin-left: 40px !important; width: 75% !important;" name="referencia_cuenta[]" value="{{$operacion->nro_referencia}}" disabled title="Referencia">
            </div>
        </div>
    </div>
    @endif
    @if($operacion->tipo_transferencia=='1')
        @if(count($operacion->bancosDestinos)>0)
            @foreach($operacion->bancosDestinos as $bancoDestino)
                <div class="col-10 col-sm-7">
                    <div class="row mx-0">
                        <div class="col-3 ps-0">
                            <p class="PoppinsBold p-azul-transferir">
                                Destino:
                            </p>
                        </div>
                        <div class="col-9 pe-0">

                            <div class="text-center d-flex align-items-center td-color-datos PoppinsMedium justify-content-center">
                                @if($bancoDestino->destino->banco->logo==null || $bancoDestino->destino->banco->logo=='')
                                    <span class="span-circulo-destino"></span>
                                @else
                                    {!! $bancoDestino->destino->banco->logo !!}
                                @endif
                                {{$bancoDestino->destino->nro_cuenta}}
                            </div>
                        </div>
                        <div class="col-3 ps-0">
                            <p class="PoppinsBold p-azul-transferir">
                                Monto:
                            </p>
                        </div>
                        <div class="col-9 pe-0">
                            <p class="PoppinsRegular p-azul-transferir">
                                {{$moneda_2}} {{number_format($bancoDestino->monto_banco_destino,2)}}
                            </p>
                        </div>
                        <div class="col-3 ps-0 referencias" @if($bancoDestino->nro_referencia!=null && $bancoDestino->nro_referencia!='') style="display:block;" @else style="display:none;" @endif >
                            <p class="PoppinsBold p-azul-transferir">
                                Nro Transferencia:
                            </p>
                        </div>
                        <div class="col-9 pe-0 referencias" @if($bancoDestino->nro_referencia!=null && $bancoDestino->nro_referencia!='') style="display:block;" @else style="display:none;" @endif >
                            <input type="text" class="input-login input-numero-operacion text-center input-ref" style="margin-left: 40px !important; width: 75% !important;" name="referencia_cuenta[]" disabled value="{{$bancoDestino->nro_referencia}}" title="Referencia">
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    @endif

</div>