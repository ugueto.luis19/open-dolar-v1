<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class HasRole extends Model
{
    use HasFactory;
    protected $table = 'model_has_roles';
    protected $fillable = [
        'role_id',
        'model_id'
    ];

    public function rol()
    {
        return $this->hasOne(Roles::class, 'id', 'role_id');
    }

    
}
