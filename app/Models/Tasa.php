<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tasa extends Model
{
    use HasFactory;
    protected $table = 'tasas';
    protected $fillable = [
        'tasa_compra',
        'tasa_venta',
        'pips_compra',
        'pips_venta',
    ];

}
