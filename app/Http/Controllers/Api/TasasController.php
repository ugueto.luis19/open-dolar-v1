<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Tasa;
use App\Models\TasaBanco;
use App\Models\TasaCambista;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;

class TasasController extends Controller
{

    public function ultimasTasas()
    {
        $tasa=Tasa::orderBy('id', 'desc')->first();
        $tasaBanco = TasaBanco::orderBy('id','desc')->first();
        $tasaCambista = TasaCambista::orderBy('id','desc')->first();

        return response()->json([
            'estado' => 'success',
            'message' => 'Informacion de las diferentes Tasas!',
            'data' => ([
                'tasa_opendolar' => $tasa,
                'tasa_banco' => $tasaBanco,
                'tasa_cambista' => $tasaCambista,
            ])

        ],201);
    }
}
