@extends('layouts.index_app')
@section('content')
    <?php
    $tipo_wp=Session::get('tipo_wp');
    $monto_envio_wp=Session::get('monto_envio_wp');
    if($tipo_wp!=null)
    {
        $tipo=Session::get('tipo_wp');
    }
    if($monto_envio_wp!=null)
    {
        $monto_envio=Session::get('monto_envio_wp');
    }
    ?>
    <style>
        .select2-container .select2-selection--single{
            height: 40px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 40px !important;
        }
        .label-login{
            z-index: 1 !important;
        }
        .icon-azul-ahorro{
            color: #137188;
            cursor: pointer;
        }
    </style>
    <!-- card de necesitas ayuda-->
    <div class="div-flotante">
        <img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 40%">
    </div>

    <div class="div-flotante-azul px-5 py-4">
        <button class="btn btn-close-ayuda">
            <i class="fa-solid fa-circle-xmark icon-close-ayuda"></i>
        </button>
        <h4 class="Arialregular titulo-ayuda"><img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="    width: 8%;
    filter: brightness(28.5);" class="me-3">Necesitas ayuda?</h4>
        <h5 class="ArialBold mt-5 mb-2">Temas recurrentes</h5>
        <p class="Arialregular p-link-temas my-0"><a href="">Problemas con transacción</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Perfil y registro</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Privacidad y seguridad</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Cancelaciones y devoluciones</a></p>
        <h5 class="ArialBold mt-5 mb-2">Contacto</h5>
        <p class="Arialregular">
            Con el fin de ahorrar tu tiempo, recomendamos revisar las preguntas frecuentes. Si sigues sin encontrar respuesta, puedes contactarnos de la manera que mas te acomode:
        </p>
        <ul class="ps-0">
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-phone me-3"></i>
                (+51) 1 1234567
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-brands fa-whatsapp me-3"></i>
                998877665
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-envelope me-3"></i>
                confianza@opendolar.com
            </li>
            <li class="d-flex Arialregular align-items-start list-contacto">
                <i class="fa-solid fa-location-dot me-3"></i>
                Centro Empresarial El Trigal, Calle Antares 320, Of. 802-C, Torre B, Surco - Lima.
            </li>
        </ul>
        <div class="d-flex justify-content-start mt-4">
            <a href="" class="me-4">
                <i class="fa-solid fa-share-nodes icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-whatsapp icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-facebook-f icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-linkedin-in icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-instagram icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="">
                <i class="fa-solid fa-envelope icon-redes-verde" style="color: #fff !important;"></i>
            </a>
        </div>
    </div>

    <div class="conten_panel_administrador">
        <h1 class="titulo-general-alerta PoppinsBold"><span >A cambiar</span></h1>
        <div class="row mx-0
        @if(is_object($verificacion))
        @if($verificacion->estado!='1')
        @if($suma >= 5000)
                justify-content-end
@else
                justify-content-center
@endif
        @else
                justify-content-center
            @endif
        @else
                justify-content-center
@endif
                " id="row-principal">

            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 px-0 px-md-4 px-lg-4 px-xl-3" style="position: relative; margin-top: -25px;">
                <div class="div-check">
                    <div class="div-chec-radius">
                        <i class="fa-solid fa-circle-check check-desactivado"></i>
                        <i class="fa-solid fa-circle-check"></i>
                        <i class="fa-solid fa-circle-check"></i>
                    </div>
                </div>
                <form action="{{route('proceso-cambio1')}}" method="post" id="formularioProceso">
                    <input type="hidden" id="tipo_cuentas" name="tipo_transferencia">
                    <div class="card-blanco mt-4 card-padding-top">
                        <h2 class="PoppinsMedium subtitulo-card text-center" id="titulo">Cambiando soles a dolares</h2>
                        <div class="row mx-0" style="background: #FEF8DD !important; color: #137188; padding: 10px; border-radius: 9px; margin-left: 45px; margin-right: 45px;">
                            <div class="col-6 d-flex justify-content-between col-padding-compra col-border-right py-2 pe-2" style="display: flex; align-items: center;" id="div-compra">
                                <p class="my-0 PoppinsBold p-color-compra-venta">Compra</p>
                                <div class="my-0 PoppinsRegular p-color-compra-venta">
                                    <b style="text-decoration:line-through; color: red; display: none;" id="old-tasa-compra">0.00</b>

                                    <b id="tasa-compra">@if(is_object($tasa)){{number_format(($tasa->tasa_compra+$tasa->pips_compra),3)}}@endif</b>
                                </div>
                            </div>

                            <div class="col-6 d-flex justify-content-between col-padding-compra  py-2 ps-3" style="display: flex; align-items: center;" id="div-venta">
                                <p class="my-0 PoppinsBold p-color-compra-venta">Venta</p>
                                <div class="my-0 PoppinsRegular p-color-compra-venta">
                                    <b style="text-decoration:line-through; color: red; display: none;" id="old-tasa-venta">0.00</b>

                                    <b id="tasa-venta">@if(is_object($tasa)){{number_format(($tasa->tasa_venta+$tasa->pips_venta),3)}}@endif</b>
                                </div>
                            </div>
                        </div>
                        <div class="row mx-0 mt-4" style="position: relative;">
                            <div style="margin-top: 55px;position: absolute;left: 7px;">
                                <button type="button" class="btn btn-cambio-visas" id="boton-cambiar">
                                    <img src="{{ asset('assets-web/img/change.svg') }}">
                                </button>
                            </div>

                            @if(isset($tipo))
                                <input type="hidden" name="tipo" id="tipo_cambio" value="{{$tipo}}">
                            @else
                                @if(isset($operacion))
                                    <input type="hidden" name="id_operacion" value="{{$operacion->id}}">
                                    <input type="hidden" name="tipo" id="tipo_cambio" value="{{$operacion->tipo}}">
                                @else
                                    <input type="hidden" name="tipo" id="tipo_cambio" value="0">
                                @endif
                            @endif


                            <input type="hidden" name="id_tasa" id="id_tasa" value="{{$tasa->id}}">
                            <div class="col-6 ps-0 pe-1" style="padding-right: 2px;">
                                <div class="div-form-login">
                                    <label class="label-login PoppinsRegular">Tengo</label>
                                    <input readonly id="bandera_origen" class="input-login PoppinsRegular">
                                    <span class="spna-absolute-centro PoppinsRegular" style="left: 30px;" id="ban-moneda1">
                                        @if(isset($tipo))
                                            @if($tipo=='0') <img src="{{ asset('/assets-web/images/peru.jpg') }}" id="ban1"> @else <img src="{{ asset('assets-web/images/estados-unidos.jpg') }}" id="ban1"> @endif
                                        @else
                                            @if(isset($operacion)) @if($operacion->tipo=='0') <img src="{{ asset('/assets-web/images/peru.jpg') }}" id="ban1"> @else <img src="{{ asset('assets-web/images/estados-unidos.jpg') }}" id="ban1"> @endif @else <img src="{{ asset('assets-web/images/peru.jpg') }}" id="ban1"> @endif
                                        @endif
                                         </span>
                                    <span class="spna-absolute-centro PoppinsRegular" style="left: 70px;" id="text-moneda1">
                                        @if(isset($tipo))
                                            @if($tipo=='0') PEN @else USD @endif
                                        @else
                                            @if(isset($operacion)) @if($operacion->tipo=='0') PEN @else USD @endif @else PEN @endif
                                        @endif
                                        </span>
                                </div>
                            </div>
                            <div class="col-6 ps-1 pe-0" style="padding-left: 2px;">
                                <div class="div-form-login">
                                    <label class="label-login PoppinsRegular">Recibiré</label>
                                    <input readonly id="bandera_destino" class="input-login PoppinsRegular">
                                    <span class="spna-absolute-centro PoppinsRegular" style="left: 30px;" id="ban-moneda2">
                                        @if(isset($tipo))
                                            @if($tipo=='0') <img src="{{ asset('/assets-web/images/estados-unidos.jpg') }}" id="ban2"> @else <img src="{{ asset('assets-web/images/peru.jpg') }}"  id="ban2" class=""> @endif
                                        @else
                                            @if(isset($operacion)) @if($operacion->tipo=='0') <img src="{{ asset('/assets-web/images/estados-unidos.jpg') }}" id="ban2"> @else <img src="{{ asset('assets-web/images/peru.jpg') }}"  id="ban2" class=""> @endif @else <img src="{{ asset('assets-web/images/estados-unidos.jpg') }}" id="ban2" > @endif
                                        @endif
                                        </span>
                                    <span class="spna-absolute-centro PoppinsRegular" style="left: 70px;" id="text-moneda2">
                                        @if(isset($tipo))
                                            @if($tipo=='0') USD @else PEN @endif
                                        @else
                                            @if(isset($operacion)) @if($operacion->tipo=='0') USD @else PEN @endif @else USD @endif
                                        @endif
                                        </span>
                                </div>
                            </div>
                            <div class="col-6 ps-0" style="padding-right: 2px;margin-top: 5px;">
                                <div class="div-form-login">
                                    <input type="text" name="monto_enviado" min="0" step="any" onkeypress="return check(event)" id="cambio_origen" class="input-login PoppinsRegular input-padding-right"
                                           @if(isset($monto_envio))
                                           value="{{number_format($monto_envio,2,'.','')}}"
                                           @else
                                           @if(isset($operacion)) value="{{number_format($operacion->monto_enviado,2,'.','')}}" @endif
                                            @endif>
                                </div>
                            </div>
                            <div class="col-6 pe-0" style="padding-left: 2px;;margin-top: 5px;">
                                <div class="div-form-login">
                                    <input type="text" name="monto_recibido" min="0" step="any" onkeypress="return check(event)" id="cambio_destino" class="input-login PoppinsRegular input-padding-right"
                                           @if(isset($operacion))
                                           @if($operacion->estado==6)
                                           @if($operacion->tipo==0)
                                           value="{{number_format($operacion->monto_enviado/($tasa->tasa_venta+$tasa->pips_venta),2,'.','')}}"
                                           @else
                                           value="{{number_format($operacion->monto_enviado*($tasa->tasa_compra+$tasa->pips_compra),2,'.','')}}"
                                           @endif
                                           @else
                                           value="{{number_format($operacion->monto_recibido,2,'.','')}}"
                                            @endif
                                            @endif >
                                </div>
                            </div>
                        </div>

                        <div class="row mx-0 mt-2">
                            <div class="col-12 text-center PoppinsRegular d-flex justify-content-center align-items-center">
                                <i class="fa-solid fa-piggy-bank icon-azul-ahorro"></i>
                                <span id="ahorra" style="font-size: 14px; color: #137188;" class="mx-2">Ahorra S/ 0 con OpenDolar</span>
                                <span data-bs-toggle="tooltip" data-bs-placement="right" title="Ahorro calculado con respecto a las entidades bancarias">
                            	<i class="fa-solid fa-circle-info icon-azul-ahorro"></i>
                            </span>
                            </div>
                        </div>

                        <div class="row mx-0 mt-2">
                            <div class="col-6 ps-0" style="padding-right: 2px;">
                                <div class="div-form-login">
                                    <input type="text" name="cupon" id="cupon" class="input-login PoppinsRegular input-padding-right input-height-cupon" placeholder="Ingresa tu cupon" @if(isset($operacion->cupon)) value="{{$operacion->cupon->codigo}}" @endif>
                                    <a href="#" id="procesarCupon" class="btn icon-select btn-cupon">
                                        <i class="fa-solid fa-circle-arrow-right" title="Procesar Cupon" id="icono-cupon-1" style="display: block;"></i>
                                        <i class="fa fa-trash" id="icono-cupon-2" title="Remover Cupon" style="display: none;"></i>
                                    </a>

                                </div>
                            </div>
                            <div class="col-6 pe-0" style="padding-left: 2px;">
                                <div class="div-form-login">
                                    <select class="select-fomr input-height-cupon" name="opencredits" id="opencreditos" required>
                                        <option value="" disabled selected>Usar OpenCredito</option>
                                        <!--option value="0" disabled>Remover</option-->
                                        @if(Session::get('perfil')->totalOpenCredit() >= 5)<option value="5" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==5) selected @endif @endif>5</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 10)<option value="10" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==10) selected @endif @endif>10</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 15)<option value="15" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==15) selected @endif @endif>15</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 20)<option value="20" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==20) selected @endif @endif>20</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 25)<option value="25" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==25) selected @endif @endif>25</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 30)<option value="30" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==30) selected @endif @endif>30</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 35)<option value="35" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==35) selected @endif @endif>35</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 40)<option value="40" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==40) selected @endif @endif>40</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 45)<option value="45" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==45) selected @endif @endif>45</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 50)<option value="50" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==50) selected @endif @endif>50</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 55)<option value="55" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==55) selected @endif @endif>55</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 60)<option value="60" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==60) selected @endif @endif>60</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 65)<option value="65" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==65) selected @endif @endif>65</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 70)<option value="70" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==70) selected @endif @endif>70</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 75)<option value="75" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==75) selected @endif @endif>75</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 80)<option value="80" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==80) selected @endif @endif>80</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 85)<option value="85" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==85) selected @endif @endif>85</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 90)<option value="90" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==90) selected @endif @endif>90</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 95)<option value="95" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==95) selected @endif @endif>95</option>@endif
                                        @if(Session::get('perfil')->totalOpenCredit() >= 100)<option value="100" @if(isset($operacion->uso_opendolar)) @if($operacion->uso_opendolar==100) selected @endif @endif>100</option>@endif
                                    </select>
                                    <i class="fa fa-angle-down icon-select"></i>
                                </div>
                            </div>
                            <div class="col-12 text-center mt-1">
                                <span id="pip-mejora" class="PoppinsRegular" style="font-size: 14px; color: #137188; display: none;"></span>
                                <br>
                                <span id="pip-calculo" class="PoppinsRegular" style="font-size: 14px; color: #137188; display: none;"></span>
                            </div>
                        </div>

                        <div class="row mx-0 mt-3" >
                            <div class="col-12 text-center">
                                <p class="PoppinsRegular" style="font-size: 14px; color: #137188;">El tipo de cambiose actualizará en <b><span id="minutes" class="span-hora-family"></span>:<span id="seconds" class="span-hora-family"></span></b></p>
                            </div>
                        </div>

                        <div class="row mx-0 mt-1" >
                            <div class="col-12 text-center">
                                <button type="button"
                                        @if(is_null($verificacion) || (is_object($verificacion) &&$verificacion->estado != 1))
                                        @if($suma >= 5000)
                                        disabled
                                        title="Debe verificar su Perfil, para seguir realizando cambios en Open Dolar."
                                        @endif
                                        @endif
                                        id="disparar-modal" class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-0" style="color: #137188 !important;">Cambiar</button><br>
                            </div>
                        </div>
                    </div>
                    @csrf
                </form>

            </div>
            @if(is_null($verificacion) || (is_object($verificacion) &&$verificacion->estado != 1))
                @if($suma >= 5000)
                    <div class="col-xl-4 col-xxl-4" style="font-size: 14px;color: #137188;" id="div-suma">
                        <div class="mt-5" style="background-color: #BED7DD; border: #137188 1px solid; padding: 25px; text-align: center; border-radius: 10px;">
                            <div class="mx-auto d-flex justify-content-center align-items-center" style="text-align: center;">
                                <div style="margin-top: -77px; margin-bottom: 25px; width: 95px;  height: 95px; -moz-border-radius: 50%;  -webkit-border-radius: 50%; border-radius: 50%; background: #137188;">
                                    <img src="/assets-web/img/verificacion.svg" style="margin-top: 25px;width: 55px;">
                                </div>
                            </div>
                            <p class="PoppinsRegular" style="color: #137188;">Cuando la suma de tus operaciones superen los USD 5.000 Dolares en un periodo de 30 días debemos cuidar de su identidad de algun posible fraude o suplantación. <br><br>Ten a la mano tu documento de identidad y asegúrate de tener una webcam en buen estado para el proceso.</p>

                            <div class="row justify-content-center align-items-center">
                                <div class="col-12 text-center">
                                    <a href="/registro-biometrico">
                                        <button type="submit" class="btn PoppinsMedium mt-3 btn-top-natural mb-3" style="color: #137188 !important;background-color: white;padding: 14px;border-radius: 10px;width: 170px; -webkit-box-shadow: 10px 10px 29px -11px rgba(0,0,0,0.75);
-moz-box-shadow: 10px 10px 29px -11px rgba(0,0,0,0.75); box-shadow: 0px 5px 10px #00000029; ">Verificar ahora</button></a><br>
                                    <a href="#" id="skip-div" style="text-decoration: underline; letter-spacing: 0px; color: #137188;" class="PoppinsMedium">En otro momento</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        </div>
        @if($dia->hora_inicio<=date('H:i:s') && date('H:i:s')>=$dia->hora_cierre)
            <div class="row mx-0 mt-4 justify-content-center px-0">
                <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 px-0 px-md-4 px-lg-4 px-xl-3 py-3 text-center text-sm-start" >
                    <div class="py-2" style="border: 1px solid #FF0000; border-radius: 5px;background-color: #FFC6C6;position: relative;">
                        <i class="fa-solid fa-clock icon-location-azul" style="margin-bottom: 10px;"></i>
                        <p class="PoppinsRegular p-azul-mensaje mb-0 mt-3 mt-sm-0">
                            Estamos fuera de horario de atención, atenderemos tu solicitud con prioridad. Tu operación se realizará mañana a partir de las {{date('h:i A',strtotime($diaSiguiente->hora_inicio))}}.
                        </p>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="modal fade modal-top" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body px-0 py-0" style="position: relative; background-color: #f9f9f9; border-radius:25px;">
                    <div class="row mx-0 justify-content-center mt-5 align-items-center mb-5">
                        <div class="col-sm-10 col-md-9 col-xl-5 px-sm-1" >
                            <div class="div-card-modal-dasboradr">
                                <a href="#" id="una_cuenta" style="text-decoration: none;">
                                    <div class="div-circulo-cuenta">
                                        <img src="/assets-web/img/una-cuenta.png" class="mx-auto d-block" style="width: 70%;">
                                    </div>
                                    <p class="text-center p-azul-cuenta PoppinsBold mt-4 text-center">
                                        A una sola cuenta
                                    </p>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-10 col-md-9 col-xl-5 mt-5 mt-lg-0 px-sm-1">
                            <div class="div-card-modal-dasboradr">
                                <a href="#" id="varias_cuentas" style="text-decoration: none;">
                                    <div class="div-circulo-cuenta">
                                        <img src="/assets-web/img/pnatural-opem-dolar.png" class="mx-auto d-block" style="width: 70%;">
                                    </div>
                                    <p class="text-center p-azul-cuenta PoppinsBold mt-4 text-center">
                                        A varias cuentas
                                    </p>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>

        function financial(x) {
            return parseFloat(x).toFixed(2);
        }

        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
        var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        });
        var cantidad_enviar=$("#cambio_origen").val();
        var options_oc = $('#opencreditos').html();

        var tasa_venta_js={{number_format(($tasa->tasa_venta+$tasa->pips_venta),3)}};
        var tasa_compra_js={{number_format(($tasa->tasa_compra+$tasa->pips_compra),3)}};
        var tasa_venta_cupon_aplicado=0;
        var tasa_compra_cupon_aplicado=0;
        var opencreditos_aplicado=0;

        //// CONTADOR
        let DATE_TARGET = new Date();
        DATE_TARGET.setMinutes(DATE_TARGET.getMinutes() + 5);

        let SPAN_MINUTES = document.querySelector('span#minutes');
        let SPAN_SECONDS = document.querySelector('span#seconds');

        // CALCULOS EN MILISEGUNDOS
        let MILLISECONDS_OF_A_SECOND = 1000;
        let MILLISECONDS_OF_A_MINUTE = MILLISECONDS_OF_A_SECOND * 60;
        let MILLISECONDS_OF_A_HOUR = MILLISECONDS_OF_A_MINUTE * 60;
        let MILLISECONDS_OF_A_DAY = MILLISECONDS_OF_A_HOUR * 24

        var updateCountdown = () => {
            let NOW = new Date();
            let DURATION = DATE_TARGET - NOW;
            let REMAINING_DAYS = Math.floor(DURATION / MILLISECONDS_OF_A_DAY);
            let REMAINING_HOURS = Math.floor((DURATION % MILLISECONDS_OF_A_DAY) / MILLISECONDS_OF_A_HOUR);
            let REMAINING_MINUTES = Math.floor((DURATION % MILLISECONDS_OF_A_HOUR) / MILLISECONDS_OF_A_MINUTE);
            let REMAINING_SECONDS = Math.floor((DURATION % MILLISECONDS_OF_A_MINUTE) / MILLISECONDS_OF_A_SECOND);

            if((REMAINING_MINUTES == 0 && REMAINING_SECONDS == 0) || (REMAINING_MINUTES < 0 && REMAINING_SECONDS < 0)){ // SI EL CONTADOR LLEGA A 0, REFRESCAR PÁGINA
                window.location.reload();
            }


            SPAN_MINUTES.textContent = REMAINING_MINUTES;

            if(REMAINING_SECONDS < 10)
                SPAN_SECONDS.textContent = "0"+REMAINING_SECONDS;
            else
                SPAN_SECONDS.textContent = REMAINING_SECONDS;
        }

        updateCountdown();
        setInterval(updateCountdown, MILLISECONDS_OF_A_SECOND);  // REFRESCAR CADA SEGUNDO

        ///////////////////// FIN CONTADOR /////////////////////////////////////////////////


        $( document ).ready(function() {

            var estado_cambio=$("#tipo_cambio").val();

            /*
            Funcion para el cambio de cuentas mediante el tipo de cambio selecionado;

            Estado cambio == 1 => Dolares a Soles Tasa de Venta
            Estado cambio == 0 => Soles a Dolares Tasa de Compra
        */
            var cambiosCuentas = () => {

                if(estado_cambio == 1){
                    $("#cuenta_origen_usd").next(".select2-container").show();
                    $("#cuenta_origen_usd").removeAttr('disabled');
                    $("#cuenta_origen").val(0);

                    $("#div-venta").css('opacity', '50%');
                    $("#div-compra").css('opacity', '100%');

                    $("#cuenta_origen_soles").next(".select2-container").hide();
                    $("#cuenta_origen_soles").attr('disabled', 'disabled');

                    $("#cuenta_destino_soles").next(".select2-container").show();
                    $("#cuenta_destino_soles").removeAttr('disabled');

                    $("#cuenta_destino_usd").next(".select2-container").hide();
                    $("#cuenta_destino_usd").attr('disabled', 'disabled');

                    $("#text-moneda1").html('USD');
                    $("#text-moneda2").html('PEN');
                    $("#ban1").attr('src','/assets-web/images/estados-unidos.jpg');
                    $("#ban2").attr('src','/assets-web/images/peru.jpg');
                }

                if(estado_cambio == 0){
                    $("#cuenta_origen_usd").next(".select2-container").hide();
                    $("#cuenta_origen_usd").attr('disabled', 'disabled');
                    $("#cuenta_origen").val(0);

                    $("#div-venta").css('opacity', '100%');
                    $("#div-venta").css('border-left', ' 1px solid #BABABA');
                    $("#div-compra").css('opacity', '50%');

                    $("#cuenta_origen_soles").next(".select2-container").show();
                    $("#cuenta_origen_soles").removeAttr('disabled');

                    $("#cuenta_destino_soles").next(".select2-container").hide();
                    $("#cuenta_destino_soles").attr('disabled', 'disabled');

                    $("#cuenta_destino_usd").next(".select2-container").show();
                    $("#cuenta_destino_usd").removeAttr('disabled');

                    $("#ban1").attr('src','/assets-web/images/peru.jpg');
                    $("#ban2").attr('src','/assets-web/images/estados-unidos.jpg');
                    $("#text-moneda1").html('PEN');
                    $("#text-moneda2").html('USD');
                }

                $("#cuentas").css('display', 'inline-flex');
            }

            /*
            0=Soles a Dolares Tasa de Compra
            1=Dolares a Soles Tasa de Venta
            */
            cambiosCuentas();

                    @if(is_object($tasa))
            var tasa_venta={{$tasa->tasa_venta}};
            var tasa_compra={{$tasa->tasa_compra}};

            var pips_venta={{$tasa->pips_venta}};
            var pips_compra={{$tasa->pips_compra}};
                    @else
            var tasa_venta=0;
            var tasa_compra=0;

            var pips_venta=0;
            var pips_compra=0;
                    @endif


                    @if(is_object($tasaBancos))
            var tasa_venta_banco={{$tasaBancos->tasa_venta}};
            var tasa_compra_banco={{$tasaBancos->tasa_compra}};
                    @else
            var tasa_venta_banco=0;
            var tasa_compra_banco=0;
            @endif

            // RE CALCULANDO VALOR A MOSTRAR EN "AHORRAR CON OPENDOLAR"
            if( $("#cambio_origen").val() > 0){
                if(estado_cambio == 0){
                    resultado=parseFloat($("#cambio_origen").val())/parseFloat(tasa_venta+pips_venta);
                    resultadoBanco=parseFloat($("#cambio_origen").val())/parseFloat(tasa_venta_banco);
                    resultadoFinal = (resultadoBanco.toFixed(3)-resultado.toFixed(3))*tasa_venta_banco;
                }else{
                    resultado=parseFloat($("#cambio_origen").val())*parseFloat(tasa_compra+pips_compra);
                    resultadoBanco=parseFloat($("#cambio_origen").val())*parseFloat(tasa_compra_banco);
                    resultadoFinal = (resultadoBanco.toFixed(3)-resultado.toFixed(3));
                }

                $("#ahorra").html("Ahorra S/ "+Math.abs(resultadoFinal.toFixed(3))+" con OpenDolar");
            }

            // FIN //////////////////
            var estado_opencreditos=0;
            $("#opencreditos").on('change', function(){

                if($(this).val()=='0')
                {
                    // removemos los opencreditos aplicados
                   /* estado_opencreditos=0;
                    $('#remover-opencreditos').prop('disabled', true);
                    $('#opencreditos').select2();*/

                    $("#old-tasa-venta").css('display','none');
                    $("#old-tasa-compra").css('display','none');
                    let cambio_origen = $("#cambio_origen").val();
                    if(estado_cambio==0){
                        $("#cambio_destino").val(financial(cambio_origen/tasa_venta_js));
                    }
                    else
                    {
                        $("#cambio_destino").val(financial(cambio_origen*tasa_compra_js));
                    }

                }
                else
                {
                   /* estado_opencreditos=1;
                    $('#remover-opencreditos').prop('disabled', false);
                    $('#opencreditos').select2();*/

                    if($(this).val() != null){
                        $("#cupon").attr('disabled', 'disabled');
                        $("#procesarCupon").css('cursor', 'no-drop');
                        $("#procesarCupon").attr('disabled', 'disabled');
                        opencreditos_aplicado=$(this).val();
                    }
                    else{
                        $("#cupon").removeAttr('disabled');
                        $("#procesarCupon").removeAttr('disabled');
                        $("#procesarCupon").css('cursor', 'pointer');
                    }
                    let cambio_origen = $("#cambio_origen").val();


                    if(cambio_origen > 0){
                        $("#old-tasa-venta").css('display','none');
                        $("#old-tasa-compra").css('display','none');
                        if(estado_cambio==0){
                            // recalculamos el cambio destino antes de aplicar los open creditos
                            $("#cambio_destino").val(financial(cambio_origen/tasa_venta_js));
                            let cambio_destino = $("#cambio_destino").val();
                            if($(this).val() > 0)
                            {
                                $("#cambio_origen").val(financial(cambio_origen-$(this).val()));
                                let nuevo_monto_origen=financial(cambio_origen-$(this).val());

                                // actualizar tasa
                                $("#old-tasa-venta").html(parseFloat(tasa_venta_js).toFixed(3));
                                $("#old-tasa-venta").css('display','block');
                                $("#tasa-venta").html((nuevo_monto_origen/cambio_destino).toFixed(3));
                            }
                        }
                        else{
                            // recalculamos el cambio destino antes de aplicar los open creditos
                            $("#cambio_destino").val(financial(cambio_origen*tasa_compra_js));
                            let cambio_destino = $("#cambio_destino").val();
                            if($(this).val() > 0)
                            {
                                $("#cambio_origen").val(financial(cambio_origen-($(this).val()/tasa_compra+pips_compra)));
                                let nuevo_monto_origen=financial(cambio_origen-($(this).val()/tasa_compra+pips_compra));

                                // actualizar tasa
                                $("#old-tasa-compra").html(parseFloat(tasa_compra_js).toFixed(3));
                                $("#old-tasa-compra").css('display','block');
                                $("#tasa-compra").html((cambio_destino/nuevo_monto_origen).toFixed(3));
                            }
                        }
                    }
                }


            });

            $('#opencreditos').html(options_oc);

            var $select = $('#opencreditos option');
            $select.filter(function(i, e){
                if(i != 0){
                    if(($(e).val() <= ($("#cambio_destino").val() / 400) * 5) == false )
                        $(e).remove();
                }
            }).prop('selected', true);

            $("#opencreditos").trigger('change');

            $("#boton-cambiar").click(function () {
                var monto_a_cambiar=$("#cambio_origen").val();
                cantidad_enviar=monto_a_cambiar;
                if(monto_a_cambiar == 0){
                    $("#ahorra").html("Ahorra S/ 0 con OpenDolar");
                    $("#cambio_destino").val(null);
                }

                if(estado_cambio==0)
                {
                    estado_cambio=1;
                    $("#tipo_cambio").val(1);

                    if(tasa_compra_cupon_aplicado>0)
                    {
                        resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra_cupon_aplicado);
                    }
                    else
                    {
                        resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra+pips_compra);
                    }
                    resultadoBanco=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra_banco);
                    resultadoFinal = (resultadoBanco.toFixed(3)-resultado.toFixed(3));

                    $("#cambio_destino").val(financial(resultado));

                    $("#text-moneda1").html('USD');
                    $("#text-moneda2").html('PEN');

                    $("#ban1").attr('src','/assets-web/images/estados-unidos.jpg');
                    $("#ban2").attr('src','/assets-web/images/peru.jpg');

                    $("#titulo").html('Cambiando dolares a soles');

                    // verificando y calculando para los oc
                    if($("#cambio_origen").val() >= 400 && $("#cambio_origen").val() / 400 > 0){
                        $("#opencreditos").removeAttr('disabled');

                        $('#opencreditos').html(options_oc);

                        var $select = $('#opencreditos option');
                        $select.filter(function(i, e){
                            if(i != 0){
                                if(($(e).val() <= ($ ("#cambio_origen").val() / 400) * 5) == false )
                                    $(e).remove();
                            }
                        }).prop('selected', true);

                        $("#opencreditos").trigger('change');
                    }
                    else $("#opencreditos").attr('disabled', 'disabled');


                    if($("#opencreditos option[value='"+opencreditos_aplicado+"']").length)
                        $("#opencreditos").val(opencreditos_aplicado).trigger('change');

                    if(tasa_compra_cupon_aplicado>0 || tasa_venta_cupon_aplicado>0)
                    {
                        // bloqueamos el uso de opencreditos
                        $("#opencreditos").attr('disabled', 'disabled');
                    }

                    if(isNaN(resultadoFinal)) {
                        $("#ahorra").html("Ahorra S/ 0 con OpenDolar");
                    }else{
                        $("#ahorra").html("Ahorra S/ "+Math.abs(resultadoFinal.toFixed(2))+" con OpenDolar");
                    }

                }
                else
                {
                    estado_cambio=0;
                    $("#tipo_cambio").val(0);
                    if(tasa_venta_cupon_aplicado>0)
                    {
                        resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta_cupon_aplicado);
                    }
                    else
                    {
                        resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta+pips_venta);
                    }
                    resultadoBanco=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta_banco);
                    resultadoFinal = (resultadoBanco.toFixed(2)-resultado.toFixed(2))*parseFloat(tasa_venta_banco);

                    $("#cambio_destino").val(financial(resultado));

                    // verificando y calculando para los oc
                    if(resultado >= 400 && resultado / 400 >= 0){
                        $("#opencreditos").removeAttr('disabled');

                        $('#opencreditos').html(options_oc);

                        var $select = $('#opencreditos option');
                        $select.filter(function(i, e){
                            if(i != 0){
                                if(($(e).val() <= ($("#cambio_destino").val() / 400) * 5) == false )
                                    $(e).remove();
                            }
                        }).prop('selected', true);

                        $("#opencreditos").trigger('change');
                    }
                    else $("#opencreditos").attr('disabled', 'disabled');

                    if($("#opencreditos option[value='"+opencreditos_aplicado+"']").length)
                        $("#opencreditos").val(opencreditos_aplicado).trigger('change');

                    if(tasa_compra_cupon_aplicado>0 || tasa_venta_cupon_aplicado>0)
                    {
                        // bloqueamos el uso de opencreditos
                        $("#opencreditos").attr('disabled', 'disabled');
                    }


                    $("#text-moneda1").html('PEN');
                    $("#text-moneda2").html('USD');

                    $("#ban1").attr('src','/assets-web/images/peru.jpg');
                    $("#ban2").attr('src','/assets-web/images/estados-unidos.jpg');
                    $("#titulo").html('Cambiando soles a dolares');

                    if(isNaN(resultadoFinal)) {
                        $("#ahorra").html("Ahorra S/ 0 con OpenDolar");
                    }else{
                        $("#ahorra").html("Ahorra S/ "+Math.abs(resultadoFinal.toFixed(2))+" con OpenDolar");
                    }

                }

                cambiosCuentas();
            });

                    @if(isset($monto_envio))

            var monto_a_cambiar=$("#cambio_origen").val();
            cantidad_enviar=monto_a_cambiar;
            if(monto_a_cambiar == 0){
                $("#ahorra").html("Ahorra S/ 0 con OpenDolar");
                $("#cambio_destino").val(null);
            }

            if(tasa_compra_cupon_aplicado>0 || tasa_venta_cupon_aplicado>0)
            {
                // bloqueamos el uso de opencreditos
                $("#opencreditos").attr('disabled', 'disabled');
            }

            if(monto_a_cambiar > 0){

                if(estado_cambio==0)
                {

                    if(tasa_venta_cupon_aplicado>0)
                    {
                        resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta_cupon_aplicado);
                    }
                    else
                    {
                        resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta+pips_venta);
                    }

                    resultadoBanco=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta_banco);
                    resultadoFinal = (resultadoBanco.toFixed(2)-resultado.toFixed(2))*tasa_venta_banco;

                    // verificando y calculando para los oc
                    if(resultado >= 400 && resultado / 400 >= 0){
                        $("#opencreditos").removeAttr('disabled');

                        $('#opencreditos').html(options_oc);

                        var $select = $('#opencreditos option');
                        $select.filter(function(i, e){
                            if(i != 0){
                                if(($(e).val() <= (resultado / 400) * 5) == false )
                                    $(e).remove();
                            }
                        }).prop('selected', true);

                        $("#opencreditos").trigger('change');
                    }
                    else $("#opencreditos").attr('disabled', 'disabled');

                    $("#cambio_destino").val(financial(resultado));
                    $("#ahorra").html("Ahorra S/ "+Math.abs(resultadoFinal.toFixed(2))+" con OpenDolar");
                }
                else
                {

                    if(tasa_compra_cupon_aplicado>0)
                    {
                        resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra_cupon_aplicado);
                    }
                    else
                    {
                        resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra+pips_compra);
                    }

                    resultadoBanco=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra_banco);
                    resultadoFinal = (resultadoBanco.toFixed(3)-resultado.toFixed(3));

                    if($("#cambio_origen").val() >= 400 && $("#cambio_origen").val() / 400 > 0){
                        $("#opencreditos").removeAttr('disabled');

                        $('#opencreditos').html(options_oc);

                        var $select = $('#opencreditos option');
                        $select.filter(function(i, e){
                            if(i != 0){
                                if(($(e).val() <= ($("#cambio_origen").val() / 400) * 5) == false )
                                    $(e).remove();
                            }
                        }).prop('selected', true);

                        $("#opencreditos").trigger('change');
                    }
                    else $("#opencreditos").attr('disabled', 'disabled');

                    $("#cambio_destino").val(financial(resultado));
                    $("#ahorra").html("Ahorra S/ "+Math.abs(resultadoFinal.toFixed(2))+" con OpenDolar");
                }
            }

            @endif

            $("#cambio_origen").on('keyup', function (e) {
                var monto_a_cambiar=$("#cambio_origen").val();
                cantidad_enviar=monto_a_cambiar;
                if(monto_a_cambiar == 0){
                    $("#ahorra").html("Ahorra S/ 0 con OpenDolar");
                    $("#cambio_destino").val(null);
                }

                if(tasa_compra_cupon_aplicado>0 || tasa_venta_cupon_aplicado>0)
                {
                    // bloqueamos el uso de opencreditos
                    $("#opencreditos").attr('disabled', 'disabled');
                }

                if(monto_a_cambiar > 0){

                    if(estado_cambio==0)
                    {

                        if(tasa_venta_cupon_aplicado>0)
                        {
                            resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta_cupon_aplicado);
                        }
                        else
                        {
                            resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta+pips_venta);
                        }

                        resultadoBanco=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta_banco);
                        resultadoFinal = (resultadoBanco.toFixed(2)-resultado.toFixed(2))*tasa_venta_banco;

                        // verificando y calculando para los oc
                        if(resultado >= 400 && resultado / 400 >= 0){
                            $("#opencreditos").removeAttr('disabled');

                            $('#opencreditos').html(options_oc);

                            var $select = $('#opencreditos option');
                            $select.filter(function(i, e){
                                if(i != 0){
                                    if(($(e).val() <= (resultado / 400) * 5) == false )
                                        $(e).remove();
                                }
                            }).prop('selected', true);

                            $("#opencreditos").trigger('change');
                        }
                        else $("#opencreditos").attr('disabled', 'disabled');

                        $("#cambio_destino").val(financial(resultado));
                        $("#ahorra").html("Ahorra S/ "+Math.abs(resultadoFinal.toFixed(2))+" con OpenDolar");
                    }
                    else
                    {

                        if(tasa_compra_cupon_aplicado>0)
                        {
                            resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra_cupon_aplicado);
                        }
                        else
                        {
                            resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra+pips_compra);
                        }

                        resultadoBanco=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra_banco);
                        resultadoFinal = (resultadoBanco.toFixed(3)-resultado.toFixed(3));

                        if($("#cambio_origen").val() >= 400 && $("#cambio_origen").val() / 400 > 0){
                            $("#opencreditos").removeAttr('disabled');

                            $('#opencreditos').html(options_oc);

                            var $select = $('#opencreditos option');
                            $select.filter(function(i, e){
                                if(i != 0){
                                    if(($(e).val() <= ($("#cambio_origen").val() / 400) * 5) == false )
                                        $(e).remove();
                                }
                            }).prop('selected', true);

                            $("#opencreditos").trigger('change');
                        }
                        else $("#opencreditos").attr('disabled', 'disabled');

                        $("#cambio_destino").val(financial(resultado));
                        $("#ahorra").html("Ahorra S/ "+Math.abs(resultadoFinal.toFixed(2))+" con OpenDolar");
                    }
                }
            });

            $("#cambio_destino").on('keyup', function () {
                var monto_a_cambiar=$("#cambio_destino").val();
                cantidad_enviar=monto_a_cambiar;

                if(monto_a_cambiar == 0){
                    $("#ahorra").html("Ahorra S/ 0 con OpenDolar");
                    $("#cambio_destino").val(null);
                }

                if(tasa_compra_cupon_aplicado>0 || tasa_venta_cupon_aplicado>0)
                {
                    // bloqueamos el uso de opencreditos
                    $("#opencreditos").attr('disabled', 'disabled');
                }

                if(monto_a_cambiar > 0){
                    if(estado_cambio==0)
                    {
                        if(tasa_venta_cupon_aplicado>0)
                        {
                            resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_venta_cupon_aplicado);
                        }
                        else
                        {
                            resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_venta+pips_venta);
                        }

                        resultadoBanco=parseFloat(monto_a_cambiar)*parseFloat(tasa_venta_banco);
                        resultadoFinal = (resultadoBanco.toFixed(3)-resultado.toFixed(3));

                        // verificando y calculando para los oc
                        if($("#cambio_destino").val() >= 400 && $("#cambio_destino").val() / 400 >= 0){
                            $("#opencreditos").removeAttr('disabled');

                            $('#opencreditos').html(options_oc);

                            var $select = $('#opencreditos option');
                            $select.filter(function(i, e){
                                if(i != 0){
                                    if(($(e).val() <= ($("#cambio_destino").val() / 400) * 5) == false )
                                        $(e).remove();

                                }
                            }).prop('selected', true);

                            $("#opencreditos").trigger('change');
                        }
                        else $("#opencreditos").attr('disabled', 'disabled');


                        $("#ahorra").html("Ahorra S/ "+Math.abs(resultadoFinal.toFixed(2))+" con OpenDolar");
                        $("#cambio_origen").val(financial(resultado));
                    }
                    else
                    {
                        if(tasa_compra_cupon_aplicado>0)
                        {
                            resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_compra_cupon_aplicado);
                        }
                        else
                        {
                            resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_compra+pips_compra);
                        }

                        resultadoBanco=parseFloat(monto_a_cambiar)/parseFloat(tasa_compra_banco);
                        resultadoFinal = (resultadoBanco.toFixed(2)-resultado.toFixed(2))*parseFloat(tasa_compra_banco);

                        if(resultado >= 400 && resultado / 400 >= 0){
                            $("#opencreditos").removeAttr('disabled');

                            $('#opencreditos').html(options_oc);

                            var $select = $('#opencreditos option');
                            $select.filter(function(i, e){
                                if(i != 0){
                                    if(($(e).val() <= (resultado / 400) * 5) == false )
                                        $(e).remove();
                                }
                            }).prop('selected', true);

                            $("#opencreditos").trigger('change');
                        }
                        else $("#opencreditos").attr('disabled', 'disabled');

                        $("#cambio_origen").val(financial(resultado));
                        $("#ahorra").html("Ahorra S/ "+Math.abs(resultadoFinal.toFixed(2))+" con OpenDolar");

                    }
                }
            });


            // Validar modal par abrirlo
            $("#disparar-modal").click(function () {

                var cambio_origen=$("#cambio_origen").val();
                var cambio_destino=$("#cambio_destino").val();

                if(cambio_origen>0 && cambio_destino>0)
                {
                    $("#exampleModal").modal('show');
                }
                else
                {
                    toastr.warning("Debe ingresar una cantidad a cambiar!");
                }
            });

            // 0 - UNA SOLA CUENTA

            $("#una_cuenta").click(function () {
                $("#exampleModal").modal('hide');
                $("#tipo_cuentas").val(0);
                $("#formularioProceso").submit();
            });

            // 1 - VARIAS CUENTAS

            $("#varias_cuentas").click(function () {
                $("#exampleModal").modal('hide');
                $("#tipo_cuentas").val(1);
                $("#formularioProceso").submit();
            });

            var estado_cupon=0;
            var monto_envio_old=0;
            var monto_recibe_old=0;

            $("#procesarCupon").click(function (e) {

                if($("#opencreditos").val() == null){

                    if($("#cambio_origen").val()>0)
                    {
                        var cupon = $("#cupon").val();
                        $("#old-tasa-venta").css('display','none');
                        $("#old-tasa-compra").css('display','none');
                        if(cupon.length>2)
                        {

                            if($("#cambio_origen").val()>0)
                            {
                                if(estado_cupon==0)
                                {
                                    estado_cupon=1;
                                    $("#icono-cupon-1").hide(300);
                                    $("#icono-cupon-2").show(300);
                                    $.ajax({
                                        url: "{{ route('procesar-cupon') }}?cupon=" + cupon,
                                        method: 'GET',
                                        success: function (data) {
                                            if(data.resultado.status == 200){

                                                $("#opencreditos").attr('disabled', 'disabled');

                                                if(estado_cambio == 0){
                                                    toastr.success("Su cupon ha sido aplicado, su monto a enviar ha sido actualizado!");
                                                    let totalAp = parseFloat(data.resultado.cupon.valor_soles);


                                                    $("#old-tasa-venta").html(parseFloat(tasa_venta_js).toFixed(3));
                                                    $("#old-tasa-venta").css('display','block');
                                                    $("#tasa-venta").html((parseFloat(tasa_venta_js)-totalAp).toFixed(3));

                                                    tasa_venta_cupon_aplicado=(parseFloat(tasa_venta_js)-totalAp).toFixed(3);

                                                    let cambio = $("#cambio_origen").val();
                                                    let recibir = $("#cambio_destino").val();
                                                    $("#cambio_origen").val(financial(recibir*tasa_venta_cupon_aplicado));
                                                    monto_envio_old=cambio;
                                                    monto_recibe_old=recibir;
                                                    var monto_ahorro_total_cupon=cambio-(recibir*tasa_venta_cupon_aplicado);
                                                    $("#pip-mejora").html('Usando el cupón <b>'+cupon+'</b> tienes '+totalAp.toFixed(3)+' pips de mejora.');
                                                    $("#pip-calculo").html('Estás enviando <b>'+monto_ahorro_total_cupon.toFixed(2)+' soles</b> menos.');
                                                    $("#pip-mejora").fadeIn();
                                                    $("#pip-calculo").fadeIn();
                                                }
                                                if(estado_cambio == 1){
                                                    toastr.success("Su cupon ha sido aplicado, su monto a recibir ha sido actualizado!");
                                                    let totalAp = parseFloat(data.resultado.cupon.valor_soles)/parseFloat('{{number_format($tasa->tasa_compra,3)}}');

                                                    $("#old-tasa-compra").html(parseFloat(tasa_compra_js).toFixed(3));
                                                    $("#old-tasa-compra").css('display','block');
                                                    $("#tasa-compra").html((parseFloat(tasa_compra_js)+totalAp).toFixed(3));

                                                    tasa_compra_cupon_aplicado=(parseFloat(tasa_compra_js)+totalAp).toFixed(3);

                                                    let cambio = $("#cambio_origen").val();
                                                    let recibir = $("#cambio_destino").val();
                                                    monto_envio_old=cambio;
                                                    monto_recibe_old=recibir;
                                                    var nueva_monto_recibir_total=cambio*tasa_compra_cupon_aplicado;

                                                    $("#cambio_destino").val(financial(nueva_monto_recibir_total));
                                                    var monto_ahorro_total_cupon=(cambio*tasa_compra_cupon_aplicado)-recibir;
                                                    $("#pip-mejora").html('Usando el cupón <b>'+cupon+'</b> tienes '+totalAp.toFixed(3)+' pips de mejora.');
                                                    $("#pip-calculo").html('Estás recibiendo <b>'+monto_ahorro_total_cupon.toFixed(2)+' soles</b> de más.');
                                                    $("#pip-mejora").fadeIn();
                                                    $("#pip-calculo").fadeIn();
                                                }

                                            }
                                            if(data.resultado.status == 201){
                                                toastr.warning("El cupon introducido ha caducado!");
                                            }
                                            if(data.resultado.status == 300){
                                                toastr.warning("Cupon incorrecto!");
                                            }
                                        },
                                        error:function (error) {
                                            console.log(error);
                                            toastr.error(error.message);
                                        }
                                    });
                                }
                                else
                                {
                                    estado_cupon=0;
                                    $("#cupon").val('');
                                    $("#cambio_origen").val(financial(monto_envio_old));
                                    $("#cambio_destino").val(financial(monto_recibe_old));
                                    $("#pip-mejora").fadeOut();
                                    $("#pip-calculo").fadeOut();

                                    $("#icono-cupon-2").hide(300);
                                    $("#icono-cupon-1").show(300);
                                    $("#opencreditos").attr('disabled', false);
                                    toastr.success("Cupon removido correctamente!");
                                }
                            }

                        }
                        else
                        {
                            toastr.warning("Ingrese un Cupon!");
                        }
                    }
                    else
                    {
                        toastr.warning("Ingre el monto a cambiar!");
                    }


                }else e.preventDefault();
            });

            $("#skip-div").click(() => {
                $("#div-suma").remove();
                $("#row-principal").addClass("justify-content-center");
                $("#row-principal").removeClass("justify-content-end");
            });
        });

        function check(e) {
            tecla = (document.all) ? e.keyCode : e.which;

            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            // Patrón de entrada, en este caso solo acepta numeros y letras
            // [*0-9.] Acepta numeros y decimales en vez de coma con punto
            patron = /[*0-9.]/;
            tecla_final = String.fromCharCode(tecla);
            return patron.test(tecla_final);
        }

    </script>
@endsection