@extends('layouts.index_app')
@section('content')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgb(198 208 93 / 55%);
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 16px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 50%;
        }

        input:checked + .slider {
            background-color: #C6D05D;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Cupones</h1>

        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
            <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                <div class="col-sm-7 col-md-6 col-lg-5 text-center text-sm-start">

                </div>
                @canany('cupones_agregar')
                    <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                        <button class="btn btn-login PoppinsMedium" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar Cupon</button>
                    </div>
                @endcanany
            </div>

            <div class="div-scrolll" id="div-tabla-cupones">
                <table class="table table-border-cero" id="cupones">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Código</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Fecha Caducidad</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Valor en Soles</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Valor en Pips</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Estado</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                    @if(count($cupones)>0)
                        @foreach($cupones as $cupon)
                            <tr class="tr-border-top">
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$cupon->codigo}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{date('d-m-Y',strtotime($cupon->fecha_caducidad))}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$cupon->valor_soles}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$cupon->valor_pips}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium" id="estado-{{ $cupon->id }}">
                                    {{$cupon->estado()}}
                                </th>

                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    @canany('cupones_eliminar')
                                        <label class="switch">
                                            @if($cupon->estado == 0)
                                                <input type="checkbox" class="cambiarEstado" value="{{ $cupon->id }}">
                                            @else
                                                <input type="checkbox" class="cambiarEstado" value="{{ $cupon->id }}" checked>
                                            @endif
                                            <span class="slider"></span>
                                        </label>
                                    @endcanany
                                </th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{ $cupones->links() }}
            </div>
        </div>
    </div>

    @canany('cupones_agregar')
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">

                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Agregar nuevo Cupon</h1>

                            <form method="post" action="{{ route('registrar-cupon') }}">
                                @csrf
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="text" name="codigo" class="input-login PoppinsRegular" placeholder="Ingrese Cupon">
                                        </div>
                                    </div>
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="date" name="fecha_caducidad" class="input-login PoppinsRegular" placeholder="Ingrese Fecha de Caducidad">
                                            
                                        </div>
                                    </div>
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="valor_soles" class="input-login PoppinsRegular" placeholder="Ingrese el valor en soles" id="valor_soles">
                                        </div>
                                    </div>
                                    <div class="col-12 px-lg-5">
                                        <div class="div-form-login">
                                            <input readonly type="text" name="valor_pips" class="input-login PoppinsRegular" title="Valor en pips" id="valor_pips" placeholder="Valor en pips">
                                        </div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            //$('#cupones').DataTable({ "bSort" : false });
            $("#valor_soles").on('keyup change',function(){
                let mult = $(this).val()*10000;
                $("#valor_pips").val(mult.toFixed(2));
            });

            $(".cambiarEstado").click(function () {
                let activate = $(this).is(':checked');
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('cambiar-estado-cupon') }}?id=" + id,
                    method: 'GET',
                    success: function (data) {

                        if(activate)
                        {
                            $("#estado-"+id).html('Activo');
                            toastr.success("Cupon activado!");
                        }
                        else
                        {
                            $("#estado-"+id).html('Desactivado');
                            toastr.success("Cupon desactivado!");
                        }

                    },
                    error:function (error) {
                        console.log(error);
                        toastr.error(error.message);
                    }
                });
            });

        });


    </script>

@endsection
