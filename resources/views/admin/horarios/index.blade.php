@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Horario de Atención</h1>

        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
            <div class="div-scrolll">
                <table class="table table-border-cero" id="horarios">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Día</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Hora Inicio</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Hora Cierre</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                    @if(count($horarios)>0)
                        @foreach($horarios as $horario)
                            <tr class="tr-border-top">
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$horario->nombredia()}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$horario->hora_inicio}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$horario->hora_cierre}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium" style="display: flex;align-items: center;justify-content: center;">
                                    @canany('horario_atencion_editar')
                                        <a href="#" class="btn px-2 btn-icono-admi btn-with-iconos" style="text-decoration: none;display: flex;align-items: center;justify-content: center;" onclick="modalEdit('{{ $horario }}', '{{$horario->nombredia()}}')">
                                            <i class="fa-solid fa-pencil"></i>
                                        </a>
                                    @endcanany
                                </th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @canany('horario_atencion_editar')
        <div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Editar Horario&nbsp;<b id="dia"></b></h1>

                            <form method="post" action="{{ route('editar-horario') }}">
                                @csrf
                                <input type="hidden" name="id" id="id">
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="time" id="hora_inicio" name="hora_inicio" class="input-login PoppinsRegular"  placeholder="Ingrese Hora de Inicio" required>
                                        </div>
                                    </div>

                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="time" id="hora_cierre" name="hora_cierre" class="input-login PoppinsRegular"  placeholder="Ingrese Hora de Cierre" required>
                                        </div>
                                    </div>


                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Editar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            //$('#horarios').DataTable({ "bSort" : false });
        } );

        var modalEdit = (obj, dia) => {
            var myModal = new bootstrap.Modal(document.getElementById('modalEdit'));
            var op = JSON.parse(obj);

            $("#id").val(op.id);
            $("#hora_inicio").val(op.hora_inicio);
            $("#hora_cierre").val(op.hora_cierre);
            $("#dia").html(dia);
            myModal.show();

        };

    </script>

@endsection
