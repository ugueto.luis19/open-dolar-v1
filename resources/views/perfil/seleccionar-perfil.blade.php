@extends('layouts.perfil')
@section('title','Seleccionar Perfil')
@section('content')

	<div class="conten_panel mt-3 mt-lg-0">
		<div class="div-conten text-center">
			<img class="icon-perfil mb-3" style="width:95px;" src="/assets-web/img/check.png" alt="">
			<h1 class="PoppinsBold text-center mb-0" >Hola {{ $perfiles[0]->nombres }}</h1>
			<h1 class="titulo-conten text-center mb-0" >¿Qué perfil deseas usar hoy?</h1>
			<div class="row mx-0 justify-content-center mt-5">
				@foreach($perfiles as $perfil)
					<div class="col-lg-7 col-xl-7">
					<a href="{{ url('perfil/'.$perfil->codigo_asignado) }}" class="link-nombre" style="text-decoration: none;">
						<div class="d-flex flex-wrap justify-content-center align-items-center mb-3">
							<div class="div-avatar">
								@if(!empty($perfil->imagen_perfil) || $perfil->imagen_perfil != null)
									<img src="/img/perfiles/{{ $perfil->imagen_perfil }}" class="div-avatar">
			                    @else
			                    	<img src="/assets-web/img/Grupo 380.png" class="div-avatar">
			                    @endif
							</div>
							<div class="div-nombe-perfil text-center">
								<p class="PoppinsRegular my-0 p-nombre-perfil">
									@if($perfil->tipo==1)
										{{ $perfil->nombres }} {{ $perfil->apellido_paterno }}
									@else
										{{ $perfil->nombre_juridico }}
									@endif
								</p>
							</div>
						</div>
						</a>
					</div>
				@endforeach
			</div>

			@if(Auth::user()->perfiles_juridicos()==true)
			<div class="row">
				<div class="col-12 mt-5 d-flex justify-content-center">
					<a href="{{ route('persona-juridica') }}">
						<button type="submit" class="btn btn-login PoppinsMedium px-5">Agregar perfil</button>
					</a>
				</div>
			</div>
			@endif
		</div>
		
	</div>
@endsection