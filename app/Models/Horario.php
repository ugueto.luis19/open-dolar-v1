<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    use HasFactory;

    protected $table = 'horarios';
    protected $fillable = [
        'hora_inicio',
        'hora_cierre',
        'dia',
    ];

    public function nombreDia()
    {
        $horarios = Horario::where('id',$this->id)->first();
        $dia = '';
        switch($horarios->dia){
            case '0': $dia = 'Domingo'; break;
            case '1': $dia = 'Lunes'; break;
            case '2': $dia = 'Martes'; break;
            case '3': $dia = 'Miércoles'; break;
            case '4': $dia = 'Jueves'; break;
            case '5': $dia = 'Viernes';  break;
            case '6':$dia = 'Sábado'; break;
        }

        return $dia;
    }

    public function diaActual()
    {
        $horario = Horario::where('dia', date('w'))->first();

        return $horario;
    }

    public function diaSiguiente()
    {
        $diaSiguiente = date('w')+1;

        if($diaSiguiente == 7)
            $diaSiguiente = 0;

        $horario = Horario::where('dia', $diaSiguiente)->first();

        return $horario;
    }
}
