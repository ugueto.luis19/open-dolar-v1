<div class="div-scrolll">
                <table class="table table-border-cero" id="instituciones">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Nombre</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Estado</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                    @if(count($instituciones)>0)
                        @foreach($instituciones as $inst)
                            <tr class="tr-border-top">
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$inst->nombre}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$inst->estado()}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium d-flex justify-content-between">
                                    @canany('instituciones_editar')
                                        <a title="Editar Institución" href="#" class="link-descarga-table btn-with-iconos" style="text-decoration: none;" onclick="modalEdit('{{ $inst }}')">
                                            <i class="fa-solid fa-pencil"></i>
                                        </a>
                                    @endcanany

                                    @canany('instituciones_eliminar')
                                        @if($inst->estado == 1)
                                            <a title="Eliminar Institución" href="#" onclick="eliminarInstitucion(this, '{{ $inst->id }}', '{{route("eliminar-institucion",["id"=>$inst->id])}}');" style="text-decoration: none;" id="eliminar-institucion-{{ $inst->id }}">
                                                <button type="button" class="btn btn-borrar btn-with-iconos" >
                                                    <i class="fa-solid fa-trash-can" ></i>
                                                </button>
                                            </a>
                                        @else
                                            <a title="Activar Institución" href="#" onclick="activarInstitucion(this, '{{ $inst->id }}', '{{route("activar-institucion",["id"=>$inst->id])}}');" style="text-decoration: none;" id="activar-institucion-{{ $inst->id }}">
                                                <button type="button" class="btn btn-borrar btn-with-iconos" >
                                                    <i class="fa-solid fa-check"></i>
                                                </button>
                                            </a>
                                        @endif
                                    @endcanany
                                </th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{$instituciones->links()}}
            </div>