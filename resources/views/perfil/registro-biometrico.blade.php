@extends('layouts.index_app')

@section('content')
	<div class="conten_panel_administrador mt-3 mt-lg-0">
		<div class="div-conten pb-xl-3">
			<h1 class="PoppinsBold titulo-conten text-center mb-0">Verificación biometrica</h1>
			<div class="row mx-0 justify-content-center mt-3">
				<div class="col-10 text-center">
					<p class="PoppinsRegular p-verificacion-subtitulo">Tenemos las herramientas para protege tu identidad de riesgo de fraude o suplantación, con solo 2 simples pasos:</p>
				</div>
				<div class="col-sm-10 col-md-9 col-xl-3">
					<div class="div-circulo-biometrica">
						<img src="{{asset('/assets-web/img/DNI-open-dolar.png')}}" class="img-verificacion-biometrica">
					</div>
					<p class="PoppinsRegular p-verificacion-descripcion text-center mt-2">Sube una foto de tu documento por ambos lados, tu documento debe estar en buen estado.</p>
				</div>
				<div class="col-sm-10 col-md-9 col-xl-3 mt-5 mt-lg-0">
					<div class="div-circulo-biometrica">
						<img src="{{asset('/assets-web/img/selfie-opem-dolar.png')}}" class="img-verificacion-biometrica">
					</div>
					<p class="PoppinsRegular p-verificacion-descripcion text-center mt-2">Toma una selfie de tu rostro, procura estar en un lugar con buena iluminación.</p>
				</div>
				<div class="col-12 text-center">
					<a href="{{ route('biometrico-1') }}" class="btn btn-login PoppinsMedium px-5">¡Empecemos!</a>
				</div>
				<div class="col-12 text-center my-3">
					<a href="{{ route('dashboard') }}" class="PoppinsRegular link-omitir">Omitir por ahora></a>
				</div>
			</div>
		</div>
	</div>
@endsection