@if($operacion->tipo== 0)
    <!-- 1 Select cuentas en Dolares -->
    <div class="col-8 mt-3">
        <div class="div-form-login">
            <select class="select-fomr" id="cuenta_destino_usd_3" name="id_cuenta_destino[]" required>
                <option value="" selected>Cuenta de Destino</option>
                @if(count($cuentas_bancarias)>0)
                    @foreach($cuentas_bancarias as $cuentausd)
                        @if($cuentausd->moneda->nombre == 'Dolares')
                            <option value="{{$cuentausd->id}}" @if(is_object($cuenta)) @if($cuenta->id==$cuentausd->id) selected @endif @endif  >{{$cuentausd->banco->nombre}} ({{$cuentausd->nro_cuenta}})</option>
                        @endif
                    @endforeach
                @endif
            </select>
            <i class="fa fa-angle-down icon-select"></i>
        </div>
    </div>
    <div class="col-4 mt-3">
        <input type="number" name="monto_banco[]" onkeyup="verificarMonto();" min="0" step="any" id="monto_3" class="input-login PoppinsRegular input-padding-right montos" placeholder="Monto">
    </div>
@else
    <!-- 1 Select cuentas en Soles -->
    <div class="col-8 mt-3">
        <div class="div-form-login">
            <select class="select-fomr" id="cuenta_destino_soles_3" name="id_cuenta_destino[]" required>
                <option value="" selected>Cuenta de Destino</option>
                @if(count($cuentas_bancarias)>0)
                    @foreach($cuentas_bancarias as $cuentapen)
                        @if($cuentapen->moneda->nombre == 'Soles')
                            <option value="{{$cuentapen->id}}" @if(is_object($cuenta)) @if($cuenta->id==$cuentapen->id) selected @endif @endif >{{$cuentapen->banco->nombre}} ({{$cuentapen->nro_cuenta}})</option>
                        @endif
                    @endforeach
                @endif
            </select>
            <i class="fa fa-angle-down icon-select"></i>
        </div>
    </div>
    <div class="col-4 mt-3">
        <input type="number" name="monto_banco[]" min="0" step="any" id="monto_3" class="input-login PoppinsRegular input-padding-right" placeholder="Monto">
    </div>
@endif
<script>
    $("select").select2();
</script>