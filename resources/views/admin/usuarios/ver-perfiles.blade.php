@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">
        <h3 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Perfiles del usuario: {{$perfiles[0]->usuario->email}}</h3>
        <br>
        <br>
        <a href="{{route('clientes')}}" style="text-decoration: none;">
            <p class="PoppinsRegular p-color-ultimas my-0 ms-3">< Regresar</p>
        </a>
        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
            <div class="div-scrolll">
                <table class="table table-border-cero" id="perfiles">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Nombres y Apellidos</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Tipo de Documento</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Nro de Documento</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Codigo Asignado</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Tipo</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Ruc</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Ocupación</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Verificación</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                    @if(count($perfiles)>0)
                        @foreach($perfiles as $perfil)
                            <tr class="tr-border-top">
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    {{$perfil->nombres}} {{$perfil->apellido_paterno}} {{$perfil->apellido_materno}}
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    @if($perfil->tipo_doc=='1')
                                        DNI
                                    @elseif($perfil->tipo_doc=='2')
                                        Pasaporte
                                    @elseif($perfil->tipo_doc=='3')
                                        CE
                                    @elseif($perfil->tipo_doc=='4')
                                        PTP
                                    @endif
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    {{$perfil->nro_doc}}
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    {{$perfil->codigo_asignado}}
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    @if($perfil->tipo=='1')
                                        Natural
                                    @elseif($perfil->tipo=='2')
                                        Juridico
                                    @endif
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    {{$perfil->ruc}}
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    {{$perfil->ocupacion->nombre}}
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    <?php

                                    $estado = 'Sin Verificar';

                                    if(is_object($perfil->ultima_verificacion())){
                                        switch($perfil->ultima_verificacion()->estado){
                                            case '0':
                                                $estado = "Por Verificar";
                                                break;
                                            case '1':
                                                $estado = "Verificado";
                                                break;
                                            case '2':
                                                $estado = "Error en la verificación";
                                                break;
                                        }
                                    }
                                    ?>
                                    {{ $estado }}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    @canany('clientes_ver_historial')
                                        <a class="btn btn-icono-admi px-2" style="text-decoration: none;" href="{{route('ver-informacion-completa',['id_perfil'=>$perfil->id])}}">
                                            <img src="{{ asset('assets-web/img/icon-tornillo.png') }}" title="Ver Información Completa">
                                        </a>
                                    @endcanany
                                </th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{$perfiles->links()}}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            //$('#perfiles').DataTable();
        } );
    </script>

@endsection