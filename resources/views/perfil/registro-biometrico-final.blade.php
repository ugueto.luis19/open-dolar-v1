@extends('layouts.index_app')

@section('content')
	<div class="conten_panel_administrador mt-3 mt-lg-0">
		<div class="div-conten div-conten-padding-left-right div-conten-heigth-final">
			<img src="assets-web/img/Enmascarar grupo 7.png" class="img-absolute-biometrico">
			<div class="row mx-0 justify-content-end mt-5 mt-sm-0">
				<div class="col-sm-6 mb-3 text-center text-sm-start">
					<h1 class="PoppinsBold titulo-conten mb-0">¡Éxito!</h1>
					<p class="PoppinsRegular p-verificacion-descripcion">Tu identidad ha sido confirmada exitosamente, ya puedes realizar tus operaciones tranquilo.</p>
					<a href="{{ route('mi-perfil') }}" class="btn btn-login btn-file-top PoppinsMedium px-5">Terminar</a>
				</div>
			</div>
		</div>
	</div>
@endsection