@extends('layouts.perfil')
@section('title','Seleccionar Tipo de Cuenta')
@section('content')

	<div class="conten_panel mt-3 mt-lg-0">
		<a href="{{route('login')}}" class="btn btn-volver-azul PoppinsRegular mb-2">&lt; Volver</a>
		<div class="div-conten">
			<h1 class="PoppinsBold titulo-conten text-center mb-0">¿Que tipo de cuenta tienes?</h1>
			<div class="row mx-0 justify-content-center mt-5">
				<div class="col-sm-10 col-md-9 col-xl-5">
					<a style="text-decoration: none;" href="{{ route('persona-natural') }}">
						<div class="div-circulo-cuenta">
							<img src="/assets-web/img/pnatural-opem-dolar.png" class="mx-auto d-block" style="width: 70%;">
						</div>
						<p class="text-center p-azul-cuenta PoppinsBold mt-4 text-center">Persona Natural</p>
					</a>
				</div>
				<div class="col-sm-10 col-md-9 col-xl-5 mt-5 mt-lg-0">
					<a style="text-decoration: none;" href="{{ route('persona-juridica') }}">
						<div class="div-circulo-cuenta">
							<img src="/assets-web/img/pjuridica-opem-dolar.png" class="mx-auto d-block" style="width: 70%;">
						</div>
						<p class="text-center p-azul-cuenta PoppinsBold mt-4 text-center">Persona Jurídica</p>
					</a>
				</div>
			</div>
		</div>
	</div>
@endsection