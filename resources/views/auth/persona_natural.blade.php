@extends('layouts.perfil')
@section('title','Formulario de Registro Persona Natural')
@section('content')

	<div class="conten_panel mt-3 mt-lg-0">
		<a href="{{route('seleccionar-registro')}}" class="btn btn-volver-azul PoppinsRegular mb-2">&lt; Volver</a>
		<div class="div-conten div-conten-padding-left-right" id="div-contenedor-padre">
			<h1 class="PoppinsBold titulo-conten text-center mb-0">Tu cuenta en un toque</h1>
			<form action="{{ route('registro-persona-natural') }}" method="post">
				@csrf
				<div class="row mx-0 justify-content-start mt-5">

					<input name="tipo" type="hidden" value="1">
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Tipo de Documento</label>
							<select class="select-fomr" name="tipo_doc" id="tipo_doc" required>
								<option value="" disabled selected>Debe seleccionar una Opción</option>
								<option value="1" @if(old('tipo_doc')==1) selected @endif>DNI</option>
								<option value="3" @if(old('tipo_doc')==3) selected @endif>CE</option>
								<option value="4" @if(old('tipo_doc')==4) selected @endif>PTP</option>
								<option value="2" @if(old('tipo_doc')==2) selected @endif>Pasaporte</option>
							</select>
							<i class="fa fa-angle-down icon-select"></i>
						</div>
					</div>
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Número de Documento</label>
							<input type="number" name="nro_doc" value="{{old('nro_doc')}}" onkeypress="return check(event)" id="nro_doc" class="input-login PoppinsRegular" required>
						</div>
					</div>
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Nombres</label>
							<input type="text" name="nombres" value="{{old('nombres')}}" onkeypress="return checkTexto(event)" id="nombres" class="input-login PoppinsRegular" required>
						</div>
					</div>
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Apellido Paterno</label>
							<input type="text" name="apellido_paterno" value="{{old('apellido_paterno')}}" onkeypress="return checkTexto(event)" id="apellido_paterno"  class="input-login PoppinsRegular" required>
						</div>
					</div>
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Apellido Materno</label>
							<input type="text" name="apellido_materno" value="{{old('apellido_materno')}}" onkeypress="return checkTexto(event)" id="apellido_materno" class="input-login PoppinsRegular" required>
						</div>
					</div>
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1" id="div-nacionalidad" style="display: none;">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Nacionalidad</label>
							<select name="id_nacionalidad" class="select-fomr" id="select-nacionalidad">
								<option value="" disabled selected>Debe seleccionar una Opción</option>
								@if(count($paises)>0)
									@foreach($paises as $pais)
										<option value="{{$pais->id}}" @if(old('id_nacionalidad')==$pais->id) selected @endif>{{$pais->nombre}}</option>
									@endforeach
								@endif
							</select>
							<i class="fa fa-angle-down icon-select"></i>
						</div>
					</div>
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1" id="div-fecha-nacimiento" style="display: none;">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Fecha de Nacimiento</label>
							<input type="date" name="fecha_nacimiento" value="{{old('fecha_nacimiento')}}" id="fecha-nacimiento" class="input-login PoppinsRegular">
						</div>
					</div>

					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Ocupación</label>
							<select name="id_ocupacion" required class="select-fomr">
								<option value="" disabled selected>Debe seleccionar una Opción</option>
								@if(count($ocupaciones)>0)
									@foreach($ocupaciones as $ocupacion)
										<option value="{{$ocupacion->id}}" @if(old('id_ocupacion')==$ocupacion->id) selected @endif>{{$ocupacion->nombre}}</option>
									@endforeach
								@endif
							</select>
							<i class="fa fa-angle-down icon-select"></i>
						</div>
					</div>
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Soy Publicamente Expuesto</label>
							<select class="select-fomr" name="publicamente_expuesto" id="publicamente_expuesto" required>
								<option value="" disabled selected>Debe seleccionar una Opción</option>
								<option value="1" @if(old('publicamente_expuesto')==1) selected @endif>Si</option>
								<option value="0" @if(old('publicamente_expuesto')==0) selected @endif>No</option>
							</select>
							<i class="fa fa-angle-down icon-select"></i>
						</div>
					</div>
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1" id="div-institucion" style="display: none;">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Institución</label>
							<select class="select-fomr" name="id_institucion" id="id_institucion">
								<option value="" disabled selected>Debe seleccionar una Opción</option>
								<option value="otros">Otros</option>
								@if(count($instituciones)>0)
									@foreach($instituciones as $institucion)
										<option value="{{$institucion->id}}" @if(old('id_institucion')==$institucion->id) selected @endif>{{$institucion->nombre}}</option>
									@endforeach
								@endif
							</select>
							<i class="fa fa-angle-down icon-select"></i>
						</div>
					</div>
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1" id="div-nombre-institucion" style="display: none;">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Nombre de la institución</label>
							<input type="text" name="nombre_institucion" value="{{old('nombre_institucion')}}" class="input-login PoppinsRegular">
						</div>
					</div>
					<div class="col-sm-6 col-xl-4 mb-3 px-sm-1" id="div-cargo" style="display: none;">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Cargo</label>
							<select class="select-fomr" name="id_cargo" id="id_cargo">
								<option value="" disabled selected>Debe seleccionar una Opción</option>
								@if(count($cargos)>0)
									@foreach($cargos as $cargo)
										<option value="{{$cargo->id}}" @if(old('id_cargo')==$cargo->id) selected @endif >{{$cargo->nombre}}</option>
									@endforeach
								@endif
							</select>
							<i class="fa fa-angle-down icon-select"></i>
						</div>
					</div>
					<div class="col-12 text-center">
						<button type="submit" id="boton-enviar" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural">¡Continuamos!</button>
					</div>
				</div>
				@csrf
			</form>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		var tipo_documento=0;
		$( document ).ready(function() {

			$("#tipo_doc").change(function () {
				var valor=$(this).val();
				tipo_documento=valor;
				if(valor==1)
				{
					// es DNI
					$("#nro_doc").attr('type','text');
					$("#nro_doc").attr('maxlength','8');
					$("#div-fecha-nacimiento").fadeOut();
					$("#fecha-nacimiento").removeAttr('required');
					$("#div-nacionalidad").fadeOut();
					$("#select-nacionalidad").removeAttr('required');

					// aplicamos la api del DNI
					if($("#nro_doc").val().length<=8 && $("#nro_doc").val().length>=6)
					{
						toastr.warning("Buscando DNI!");

						$("#div-contenedor-padre").css('cursor','wait');
						$.ajax({
							url: "{{ route('validar-DNI') }}?dni=" + $("#nro_doc").val(),
							method: 'GET',
							success: function (data) {
								console.log('test',data);
								$("#div-contenedor-padre").css('cursor','default');
								if(data.resultado.success){
									var nombre_completo=data.resultado.data.fullname;
									var partes_nombre_completo=nombre_completo.split(',');
									var partes_apellidos=partes_nombre_completo[0].split(' ');
									$("#nombres").val(partes_nombre_completo[1]);
									$("#apellido_paterno").val(partes_apellidos[0]);
									if(partes_apellidos.length>=1)
									{
										$("#apellido_materno").val(partes_apellidos[1]);
									}
									toastr.success("DNI Encontrado!");
									$("#boton-enviar").removeAttr('disabled');
								}else{
									toastr.warning("DNI no Encontrado!");
									$("#boton-enviar").attr('disabled','disabled');
									$("#nombres").val('');
									$("#apellido_paterno").val('');
									$("#apellido_materno").val('');
								}
							},
							error:function (error) {
								$("#div-contenedor-padre").css('cursor','default');
								console.log(error);
								toastr.error(error.message);
							}
						});
					}

				}
				else if(valor==2)
				{
					// es pasaporte
					$("#nro_doc").attr('type','text');
					$("#nro_doc").removeAttr('maxlength');
					$("#div-nacionalidad").fadeIn();
					$("#select-nacionalidad").attr('required','required');
					$("#div-fecha-nacimiento").fadeOut();
					$("#fecha-nacimiento").removeAttr('required');
					$("#boton-enviar").removeAttr('disabled');
				}
				else if(valor==3)
				{
					// Es CE
					$("#nro_doc").attr('maxlength','9');
					$("#nro_doc").attr('type','text');
					$("#div-fecha-nacimiento").fadeIn();
					$("#fecha-nacimiento").attr('required','required');
					$("#div-nacionalidad").fadeOut();
					$("#select-nacionalidad").removeAttr('required');
					$("#boton-enviar").removeAttr('disabled');
				}
				else if(valor==4)
				{
					// Es PTP
					$("#nro_doc").attr('maxlength','9');
					$("#nro_doc").attr('type','text');
					$("#div-fecha-nacimiento").fadeIn();
					$("#fecha-nacimiento").attr('required','required');
					$("#div-nacionalidad").fadeOut();
					$("#select-nacionalidad").removeAttr('required');
					$("#boton-enviar").removeAttr('disabled');
				}
				else
				{
					$("#nro_doc").attr('type','number');
					$("#nro_doc").removeAttr('maxlength');
					$("#div-fecha-nacimiento").fadeOut();
					$("#fecha-nacimiento").removeAttr('required');
					$("#div-nacionalidad").fadeOut();
					$("#select-nacionalidad").removeAttr('required');
					$("#boton-enviar").removeAttr('disabled');
				}
			});


			$("#nro_doc").focusout(function() {
				if($("#tipo_doc").val()==1)
				{
					// es DNI
					// aplicamos la api del DNI
					if($("#nro_doc").val().length<=8 && $("#nro_doc").val().length>=6)
					{
						toastr.warning("Buscando DNI!");

						$("#div-contenedor-padre").css('cursor','wait');
						$.ajax({
							url: "{{ route('validar-DNI') }}?dni=" + $("#nro_doc").val(),
							method: 'GET',
							success: function (data) {
								console.log('test',data);
								$("#div-contenedor-padre").css('cursor','default');
								if(data.resultado.success){
									var nombre_completo=data.resultado.data.fullname;
									var partes_nombre_completo=nombre_completo.split(',');
									var partes_apellidos=partes_nombre_completo[0].split(' ');
									$("#nombres").val(partes_nombre_completo[1]);
									$("#apellido_paterno").val(partes_apellidos[0]);
									if(partes_apellidos.length>=1)
									{
										$("#apellido_materno").val(partes_apellidos[1]);
									}
									toastr.success("DNI Encontrado!");
									$("#boton-enviar").removeAttr('disabled');
								}else{
									toastr.warning("DNI no Encontrado!");
									$("#boton-enviar").attr('disabled','disabled');
									$("#nombres").val('');
									$("#apellido_paterno").val('');
									$("#apellido_materno").val('');
								}
							},
							error:function (error) {
								$("#div-contenedor-padre").css('cursor','default');
								console.log(error);
								toastr.error(error.message);
							}
						});
					}
					else
					{
						toastr.warning("El DNI debe ser de 8 digitos maximo!");
					}


				}
			});

			$("#publicamente_expuesto").change(function () {
				var valor=$(this).val();

				if(valor=='0')
				{
					$("#div-institucion").fadeOut();
					$("#div-cargo").fadeOut();
				}
				else
				{
					$("#div-institucion").fadeIn();
					$("#div-cargo").fadeIn();
				}
			});

			$("#id_institucion").change(function () {
				if($(this).val()=='otros')
				{
					$("#div-nombre-institucion").fadeIn();
				}
				else
				{
					$("#div-nombre-institucion").fadeOut();
				}
			});


		});

		function check(e) {
			tecla = (document.all) ? e.keyCode : e.which;

			//Tecla de retroceso para borrar, siempre la permite
			if (tecla == 8) {
				return true;
			}

			// Patrón de entrada

			if(tipo_documento==1)
			{
				// Tipo numero
				patron = /[*0-9]/;
			}
			else if(tipo_documento==2)
			{
				// Tipo texto y numero
				patron = /[A-Za-z0-9]+/;
			}
			else if(tipo_documento==3 || tipo_documento==4)
			{
				// Tipo numero
				patron = /[*0-9]/;
			}
			else
			{
				// Tipo numero
				patron = /[*0-9]/;
			}

			tecla_final = String.fromCharCode(tecla);
			return patron.test(tecla_final);
		}
		function checkTexto(e) {
			tecla = (document.all) ? e.keyCode : e.which;

			//Tecla de retroceso para borrar, siempre la permite
			if (tecla == 8) {
				return true;
			}

			// Patrón de entrada


			// Tipo texto
			patron = /[A-Za-z]+/;

			tecla_final = String.fromCharCode(tecla);
			return patron.test(tecla_final);
		}
	</script>
@endsection

