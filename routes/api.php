<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PerfilController;
use App\Http\Controllers\Api\TasasController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/perfil/{id}',  [PerfilController::class, 'informacionPerfilApi']);
Route::get('/ultimas-tasas',  [TasasController::class, 'ultimasTasas']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
