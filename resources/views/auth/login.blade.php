@extends('layouts.index_app')
@section('content')
    <div class="conten_panel conten_panel_login d-flex align-items-center vh-100 justify-content-center">
        <div class="div-conten-login">
            <div class="d-flex justify-content-center">
                <div class="d-flex justify-content-center" style="margin-top: -120px; margin-bottom: 25px; width: 95px;  height: 95px; -moz-border-radius: 50%;  -webkit-border-radius: 50%; border-radius: 50%; background: #1F9F99;">
                    <img src="/assets-web/img/bienvenido.svg" style="    margin-left: -6px; width: 55px;">
                </div>
            </div>
			<h1 class="PoppinsBold text-center mb-0" >Bienvenido/a</h1>
			<h1 class="titulo-conten-log text-center mb-0" >Ingresar a mi cuenta</h1>
            <form method="POST" action="{{url('login')}}">
            @csrf
            <div class="row mx-0 justify-content-center mt-5 row-padding-left-right-login">
                <div class="col-12 mb-3">
                    <div class="div-form-login">
                        <label class="label-login PoppinsRegular">Correo</label>
                        <input type="email" name="email" :value="old('email')" required class="input-login PoppinsRegular">
                    </div>
                </div>
                <div class="col-12 mb-4">
                    <div class="div-form-login">
                        <label class="label-login PoppinsRegular">Contraseña</label>
                        <input type="password" name="password" required autocomplete="current-password" class="input-login PoppinsRegular">
                    </div>
                </div>
                <div class="col-12 text-center">
                    <p class="PoppinsRegular p-login-contrasena">Olvidé mi contraseña. <a href="{{ route('password.request') }}">Recuperarla aqui</a></p>
                </div>
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-login PoppinsMedium px-5">Ingresar</button>
                </div>
            </div>
        </form>
            <div class="div-footer-login text-center py-3 mt-5">
                <p class="PoppinsRegular p-blanco-cuenta">¿Todavia no tienes cuenta?</p>
                <a href="{{route('seleccionar-registro')}}" class="btn link-blanco-login PoppinsMedium">Crear cuenta</a>
            </div>
        </div>
    </div>
@endsection








