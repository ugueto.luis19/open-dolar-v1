@extends('layouts.index_app')
@section('content')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgb(198 208 93 / 55%);
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 16px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 50%;
        }

        input:checked + .slider {
            background-color: #C6D05D;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Clientes</h1>

        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
            <div class="div-scrolll">
                <table class="table table-border-cero" id="usuarios">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Nombres y Apellidos</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Correo Electrónico</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Número Celular</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Estado</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                    @if(count($usuarios)>0)
                        @foreach($usuarios as $user)
                            <tr class="tr-border-top">
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    @if(count($user->perfiles)>0)
                                        {{$user->perfiles[0]->nombres}} {{$user->perfiles[0]->apellido_paterno}} {{$user->perfiles[0]->apellido_materno}}
                                    @else
                                        <b title="No posee perfil una función automaticamente a las 12:00 AM se encargara de eliminar esta cuenta!">--</b>
                                    @endif
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    {{$user->email}}
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    {{$user->nro_celular}}
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    @canany('clientes_bloquear_acceso')
                                        <label class="switch">
                                            @if($user->estado == 0)
                                                <input type="checkbox" class="cambiarEstado" value="{{ $user->id }}">
                                            @else
                                                <input type="checkbox" class="cambiarEstado" value="{{ $user->id }}" checked>
                                            @endif
                                            <span class="slider"></span>
                                        </label>
                                    @endcanany
                                </th>
                                <th scope="row" class="text-start td-color-datos PoppinsMedium">
                                    @if(count($user->perfiles)>0)
                                        <a class="btn btn-icono-admi btn-with-iconos" style="text-decoration: none;" href="{{route('ver-perfiles',['id_usuario'=>$user->id])}}">
                                            <img src="{{ asset('assets-web/img/icon-eyes.png') }}" width="50%" title="Ver Perfiles">
                                        </a>
                                    @endif
                                </th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{$usuarios->links()}}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            //$('#usuarios').DataTable();

            $(".cambiarEstado").click(function () {
                let activate = $(this).is(':checked');
                let id = $(this).val();

                $.ajax({
                    url: "{{ route('cambiar-estado-usuario') }}?id=" + id,
                    method: 'GET',
                    success: function (data) {
                        if(activate)
                            toastr.success("Cliente activado!");
                        else
                            toastr.success("Cliente desactivado!");
                    },
                    error:function (error) {
                        console.log(error);
                        toastr.error(error.message);
                    }
                });
            });
        } );


    </script>

@endsection