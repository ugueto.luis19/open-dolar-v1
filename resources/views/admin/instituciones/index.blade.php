@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Instituciones</h1>

        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
            <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                <div class="col-sm-7 col-md-6 col-lg-5 text-center text-sm-start">
<input style="margin-left: 23px;" type="text" name="buscador" autocomplete="off" class="form-control input-operacioes PoppinsRegular" value="" id="buscar" placeholder="Buscador" required="">
                </div>

                @canany('instituciones_editar')
                    <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                        <button class="btn btn-login PoppinsMedium" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar Institución</button>
                    </div>
                @endcanany
            </div>

            <br>
            <div id="div-contenido-buscador">
                <div class="div-scrolll">
                    <table class="table table-border-cero" id="instituciones">
                        <thead>
                        <tr>
                            <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Nombre</th>
                            <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Estado</th>
                            <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Opciones</th>
                        </tr>
                        </thead>
                        <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                        @if(count($instituciones)>0)
                            @foreach($instituciones as $inst)
                                <tr class="tr-border-top">
                                    <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                        {{$inst->nombre}}
                                    </th>
                                    <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                        {{$inst->estado()}}
                                    </th>
                                    <th scope="row" class="text-center td-color-datos PoppinsMedium d-flex justify-content-between">
                                        @canany('instituciones_editar')
                                            <a title="Editar Institución" href="#" class="link-descarga-table btn-with-iconos" style="text-decoration: none;" onclick="modalEdit('{{ $inst }}')">
                                                <i class="fa-solid fa-pencil"></i>
                                            </a>
                                        @endcanany

                                        @canany('instituciones_eliminar')
                                            @if($inst->estado == 1)
                                                <a title="Eliminar Institución" href="#" onclick="eliminarInstitucion(this, '{{ $inst->id }}', '{{route("eliminar-institucion",["id"=>$inst->id])}}');" style="text-decoration: none;" id="eliminar-institucion-{{ $inst->id }}">
                                                    <button type="button" class="btn btn-borrar btn-with-iconos" >
                                                        <i class="fa-solid fa-trash-can" ></i>
                                                    </button>
                                                </a>
                                            @else
                                                <a title="Activar Institución" href="#" onclick="activarInstitucion(this, '{{ $inst->id }}', '{{route("activar-institucion",["id"=>$inst->id])}}');" style="text-decoration: none;" id="activar-institucion-{{ $inst->id }}">
                                                    <button type="button" class="btn btn-borrar btn-with-iconos" >
                                                        <i class="fa-solid fa-check"></i>
                                                    </button>
                                                </a>
                                            @endif
                                        @endcanany
                                    </th>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    {{$instituciones->links()}}
                </div>
            </div>
        </div>
    </div>

    @canany('instituciones_editar')
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Agregar nueva Institución</h1>

                            <form method="post" action="{{ route('registrar-institucion') }}">
                                @csrf
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="text" name="nombre" class="input-login PoppinsRegular" placeholder="Ingrese nombre de la institución" required>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany

    @canany('instituciones_editar')
        <div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Editar Institución</h1>
                            <form method="post" action="{{ route('editar-institucion') }}">
                                @csrf
                                <input type="hidden" name="id" id="id">
                                <div class="row mx-0 justify-content-start mt-5">

                                    <div class="col-12 px-lg-5">
                                        <div class="div-form-login">
                                            <input id="nombre" name="nombre" class="input-login PoppinsRegular"  placeholder="Ingrese nombre" required>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Editar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany

    {{ csrf_field() }}
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            //$('#instituciones').DataTable();

            $("#buscar").keyup(function(){
                if($(this).val().length>3 || $(this).val().length==0)
                {
                    var valores = {};                    
                    valores['buscar'] = $(this).val();
                    valores["_token"] = "{{ csrf_token() }}";
                    $.ajax({
                        url: '/buscador-instituciones',
                        type: 'post',
                        dataType: 'json',
                        data: valores,
                        beforeSend: function () {

                        },
                        success: function (response) {
                            $('#div-contenido-buscador').html(response.htmlview);
                        }
                    });
                }
            });
        } );

        var eliminarInstitucion = (e, id, ruta) => {
            e.preventDefault;

            toastr.warning("<br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Si</button>",'Desea eliminar la institución?',
                {
                    closeButton: true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#confirmationRevertYes").click(function(){
                            window.location.href = ruta;
                        });
                    }
                });
        };

        var activarInstitucion = (e, id, ruta) => {
            e.preventDefault;

            toastr.warning("<br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Si</button>",'Desea activar la institución?',
                {
                    closeButton: true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#confirmationRevertYes").click(function(){
                            window.location.href = ruta;
                        });
                    }
                });
        };


        var modalEdit = (obj) => {
            var myModal = new bootstrap.Modal(document.getElementById('modalEdit'));
            var op = JSON.parse(obj);

            $("#id").val(op.id);
            $("#nombre").val(op.nombre);
            myModal.show();
        };

    </script>

@endsection