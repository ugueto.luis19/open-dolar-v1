<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuentasBancariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas_bancarias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nro_cuenta',30);
            $table->string('titular');
            $table->char('estado');
            $table->unsignedInteger('id_banco');
            $table->foreign('id_banco')->references('id')->on('bancos')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->unsignedInteger('id_moneda');
            $table->foreign('id_moneda')->references('id')->on('monedas')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->unsignedInteger('id_tipo_cuenta');
            $table->foreign('id_tipo_cuenta')->references('id')->on('tipos_cuentas')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->unsignedBigInteger('id_perfil');
            $table->foreign('id_perfil')->references('id')->on('perfiles')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas_bancarias');
    }
}
