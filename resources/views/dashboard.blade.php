@extends('layouts.index_app')
@section('content')
    <!-- card de necesitas ayuda-->
    <div class="div-flotante">
        <img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 40%">
    </div>

    <div class="div-flotante-azul px-5 py-4">
        <button class="btn btn-close-ayuda">
            <i class="fa-solid fa-circle-xmark icon-close-ayuda"></i>
        </button>
        <h4 class="Arialregular titulo-ayuda"><img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 8%;">Necesitas ayuda?</h4>
        <h5 class="ArialBold mt-5 mb-2">Temas recurrentes</h5>
        <p class="Arialregular p-link-temas my-0"><a href="">Problemas con transacción</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Perfil y registro</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Privacidad y seguridad</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Cancelaciones y devoluciones</a></p>
        <h5 class="ArialBold mt-5 mb-2">Contacto</h5>
        <p class="Arialregular">
            Con el fin de ahorrar tu tiempo, recomendamos revisar las preguntas frecuentes. Si sigues sin encontrar respuesta, puedes contactarnos de la manera que mas te acomode:
        </p>
        <ul class="ps-0">
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-phone me-3"></i>
                (+51) 1 1234567
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-brands fa-whatsapp me-3"></i>
                998877665
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-envelope me-3"></i>
                confianza@opendolar.com
            </li>
            <li class="d-flex Arialregular align-items-start list-contacto">
                <i class="fa-solid fa-location-dot me-3"></i>
                Centro Empresarial El Trigal, Calle Antares 320, Of. 802-C, Torre B, Surco - Lima.
            </li>
        </ul>
        <div class="d-flex justify-content-start mt-4">
            <a href="" class="me-4">
                <i class="fa-solid fa-share-nodes icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-whatsapp icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-facebook-f icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-linkedin-in icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-instagram icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="">
                <i class="fa-solid fa-envelope icon-redes-verde" style="color: #fff !important;"></i>
            </a>
        </div>
    </div>

    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Mis operaciones</h1>
        <div class="row mx-0 mt-4 justify-content-center">
            <div class="col-sm-10 col-md-8 col-lg-6 ps-0 pe-0 pe-lg-2">
                <form action="{{route('proceso-cambio')}}" method="post">
                    <div class="card-blanco">
                        <h2 class="PoppinsBold subtitulo-card text-center">¿Cuanto deseas <span>cambiar</span> ?</h2>
                        <div class="row mx-0">
                            <div class="col-6 d-flex justify-content-between col-padding-compra col-border-right py-2">
                                <p class="my-0 PoppinsBold p-color-compra-venta">Venta</p>
                                <p class="my-0 PoppinsRegular p-color-compra-venta">{{number_format($tasa->tasa_venta,2)}}</p>
                            </div>
                            <div class="col-6 d-flex justify-content-between col-padding-compra py-2">
                                <p class="my-0 PoppinsBold p-color-compra-venta">Compra</p>
                                <p class="my-0 PoppinsRegular p-color-compra-venta">{{number_format($tasa->tasa_compra,2)}}</p>
                            </div>
                        </div>
                        <div class="row mx-0">
                            <p>La página se actualizará en <span id="minutes"></span> minutos : <span id="seconds"></span> segundos</p>
                        </div>
                        <div class="row mx-0 mt-5" style="position: relative;">
                            <button type="button" class="btn btn-cambio-visas" id="boton-cambiar">
                                <img src="{{ asset('assets-web/img/Grupo 149.png') }}">
                            </button>
                            <input type="hidden" name="tipo" id="tipo_cambio" value="0">
                            <input type="hidden" name="id_tasa" id="id_tasa" value="{{$tasa->id}}">
                            <div class="col-6">
                                <div class="div-form-login">
                                    <label class="label-login PoppinsRegular">Tengo</label>
                                    <input type="number" name="monto_enviado" step="any" id="cambio_origen" class="input-login PoppinsRegular input-padding-right" value="{{ old('monto_enviado') }}">
                                    <span class="spna-absolute-centro PoppinsRegular" id="text-moneda1">PEN</span>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="div-form-login">
                                    <label class="label-login PoppinsRegular">Recibiré</label>
                                    <input type="number" name="monto_recibido" step="any" id="cambio_destino" class="input-login PoppinsRegular input-padding-right" value="{{ old('monto_recibido') }}">
                                    <span class="spna-absolute-centro PoppinsRegular" id="text-moneda2">USD</span>
                                </div>
                            </div>
                        </div>

                        <div class="row mx-0 mt-5" id="cuentas" style="display:none;">
                            <div class="col-12 mb-2">
                                <div class="div-form-login">
                                    <select class="select-fomr" id="cuenta_origen_usd" name="id_cuenta_origen" value="{{ old('id_cuenta_origen') }}" required>
                                        <option value="" selected>Seleccionar Cuenta Origen</option>
                                        @if(count($cuentas_bancarias)>0)
                                            @foreach($cuentas_bancarias as $cuenta)
                                                @if($cuenta->moneda->nombre == 'Dolares')
                                                    <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                    <select class="select-fomr" id="cuenta_origen_soles" name="id_cuenta_origen" value="{{ old('id_cuenta_origen') }}" disabled required>
                                        <option value="" selected>Seleccionar Cuenta Origen</option>
                                        @if(count($cuentas_bancarias)>0)
                                            @foreach($cuentas_bancarias as $cuenta)
                                                @if($cuenta->moneda->nombre == 'Soles')
                                                    <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                    <i class="fa fa-angle-down icon-select"></i>
                                </div>
                            </div>
                            <div class="col-12 mb-2">
                                <div class="div-form-login">
                                    <select class="select-fomr" id="cuenta_destino_usd" name="id_cuenta_destino" value="{{ old('id_cuenta_destino') }}" required>
                                        <option value="" selected>Seleccionar Cuenta Destino</option>
                                        @if(count($cuentas_bancarias)>0)
                                            @foreach($cuentas_bancarias as $cuenta)
                                                @if($cuenta->moneda->nombre == 'Dolares')
                                                    <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                    <select class="select-fomr" id="cuenta_destino_soles" name="id_cuenta_destino" value="{{ old('id_cuenta_destino') }}" required>
                                        <option value="" selected>Seleccionar Cuenta Destino</option>
                                        @if(count($cuentas_bancarias)>0)
                                            @foreach($cuentas_bancarias as $cuenta)
                                                @if($cuenta->moneda->nombre == 'Soles')
                                                    <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                    <i class="fa fa-angle-down icon-select"></i>
                                </div>
                            </div>
                            <div class="col-6 pe-sm-1">
                                <div class="div-form-login">
                                    <input type="text" name="cupon" class="input-login PoppinsRegular" placeholder="Ingresa tu cupon" value="{{ old('cupon') }}">
                                </div>
                            </div>
                            <div class="col-6 ps-sm-1">
                                <div class="div-form-login">
                                    <input type="number" @if(Session::get('perfil')->totalOpenCredit()<1) disabled @endif name="uso_opendolar" min="0" max="{{Session::get('perfil')->totalOpenCredit()}}" class="input-login PoppinsRegular" step="any" placeholder="Cantidad de OpenCreditos" value="{{ old('uso_opendolar') }}">
                                </div>
                            </div>
                            <div class="col-12 mt-3">
                                <div class="form-check text-start">
                                    <input class="form-check-input" type="checkbox" name="declaro_cuenta_propia" required value="1" id="flexCheckDefault">
                                    <label class="form-check-label p-login-contrasena lable-size-peque" for="flexCheckDefault">
                                        Declaro que transferiré los fondos de una cuenta propia.
                                    </label>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-4">Cambiar</button>
                            </div>
                        </div>
                    </div>
                    @csrf
                </form>
            </div>
            <div class="col-sm-10 col-md-8 col-lg-6 col-heigth-porcentaje mt-4 mt-lg-0 ps-0 ps-lg-2 pe-0">
                <div class="card-blanco height-ultimas-operaciones text-center">
                    <h5 class="text-center arlrdbd mb-4">Mis últimas operaciones</h5>
                    @if(count($operaciones)>0)
                        @foreach($operaciones as $operacion)
                            <?php
                            if($operacion->tipo=='0')
                            {
                                $moneda_1='S/';
                                $moneda_2='$';
                                $monto_tasa=$operacion->tasa->tasa_compra;
                            }
                            else{
                                $moneda_1='$';
                                $moneda_2='S/';
                                $monto_tasa=$operacion->tasa->tasa_venta;
                            }
                            ?>
                            <div class="d-flex justify-content-between px-5 align-items-center mb-3">
                                <p class="PoppinsBold my-0 p-color-ultimas">{{date('d-m-Y',strtotime($operacion->fecha_operacion))}}</p>
                                <p class="PoppinsRegular my-0 p-color-ultimas">{{$moneda_1}} {{number_format($operacion->monto_enviado,2)}}<span class="mx-2">></span>{{$moneda_2}} {{number_format($operacion->monto_recibido,2)}}</p>
                                <p class="PoppinsRegular my-0 p-color-ultimas">({{number_format($monto_tasa,2)}})</p>
                            </div>
                        @endforeach
                    @endif
                    <a href="{{route('mis-operaciones')}}" class="btn btn-login btn-azul PoppinsMedium px-5 btn-padding-top-a">Ver todos</a>
                </div>
                <div class="card-blanco text-center card-azul color-blanco-texto mt-4 py-4">
                    <h4 class="PoppinsBold mt-2">Crea un alerta</h4>
                    <p class="PoppinsRegular">Configura una alarma para cuando el tipo de cambio llegara al valor requerido</p>
                    <button class="btn btn-login btn-blanco PoppinsMedium px-5 btn-padding-top-a" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        <img src="{{ asset('assets-web/img/icon-campana.png') }}" class="me-1">
                        Crear alerta
                    </button>
                </div>
                @if(date('H:i:s') < $dia->hora_inicio || date('H:i:s') >= $dia->hora_cierre)
                    <div class="mt-4 card-rojo">
                        <i class="fa-solid fa-clock"></i>
                        <p class="PoppinsRegular p-hora-atencion mb-0">
                            Estamos fuera de horario de atención, atenderemos tu solicitud con prioridad. Tu operación se realizará mañana a partir de las {{ date('H:i a', strtotime($diaSiguiente->hora_inicio)) }}.
                        </p>
                    </div>
                @endif
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body px-0 py-0" style="position: relative;">
                    <img src="{{ asset('assets-web/img/mano-reloj.png') }}" class="img-mano-absolute">
                    <div class="modal-padding-left-right-alert pt-5 pb-5">
                        <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Configurar nueva <span class="ms-3">alerta</span></h1>
                        <p class="PoppinsRegular p-login-contrasena text-center my-0">Quiero recibir un correo electrónico cuando:</p>

                        <form method="post" action="{{ route('registrar-alerta') }}">
                            @csrf
                            <div class="row mx-0 justify-content-start mt-5">
                                <div class="col-12 mb-3 px-lg-5">
                                    <div class="div-form-login">
                                        <select class="select-fomr" name="tipo" required>
                                            <option value="" disabled selected>Seleccione una Opción</option>
                                            <option value="1">Compra</option>
                                            <option value="2">Venta</option>
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-12 mb-3 px-lg-5">
                                    <div class="div-form-login">
                                        <select class="select-fomr" name="condicional" required>
                                            <option value="" disabled selected>Seleccione una Opción</option>
                                            <option value="1">Menor o Igual</option>
                                            <option value="2">Igual</option>
                                            <option value="3">Mayor o Igual</option>
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-12 px-lg-5">
                                    <div class="div-form-login">
                                        <input type="number" step="any" name="valor_deseado" class="input-login PoppinsRegular" placeholder="Ingrese el valor deseado">
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var estado_cambio=0;

        //// CONTADOR 
        let DATE_TARGET = new Date();
        DATE_TARGET.setMinutes(DATE_TARGET.getMinutes() + 5);

        let SPAN_MINUTES = document.querySelector('span#minutes');
        let SPAN_SECONDS = document.querySelector('span#seconds');

        // CALCULOS EN MILISEGUNDOS
        let MILLISECONDS_OF_A_SECOND = 1000;
        let MILLISECONDS_OF_A_MINUTE = MILLISECONDS_OF_A_SECOND * 60;
        let MILLISECONDS_OF_A_HOUR = MILLISECONDS_OF_A_MINUTE * 60;
        let MILLISECONDS_OF_A_DAY = MILLISECONDS_OF_A_HOUR * 24

        var updateCountdown = () => {
            let NOW = new Date();
            let DURATION = DATE_TARGET - NOW;
            let REMAINING_DAYS = Math.floor(DURATION / MILLISECONDS_OF_A_DAY);
            let REMAINING_HOURS = Math.floor((DURATION % MILLISECONDS_OF_A_DAY) / MILLISECONDS_OF_A_HOUR);
            let REMAINING_MINUTES = Math.floor((DURATION % MILLISECONDS_OF_A_HOUR) / MILLISECONDS_OF_A_MINUTE);
            let REMAINING_SECONDS = Math.floor((DURATION % MILLISECONDS_OF_A_MINUTE) / MILLISECONDS_OF_A_SECOND);

            if((REMAINING_MINUTES == 0 && REMAINING_SECONDS == 0) || (REMAINING_MINUTES < 0 && REMAINING_SECONDS < 0)){ // SI EL CONTADOR LLEGA A 0, REFRESCAR PÁGINA
                window.location.reload();
            }

            SPAN_MINUTES.textContent = REMAINING_MINUTES;
            SPAN_SECONDS.textContent = REMAINING_SECONDS;
        }

        updateCountdown();
        setInterval(updateCountdown, MILLISECONDS_OF_A_SECOND);  // REFRESCAR CADA SEGUNDO

        ///////////////////// FIN CONTADOR /////////////////////////////////////////////////


        /* 
            Funcion para el cambio de cuentas mediante el tipo de cambio selecionado;
            
            Estado cambio == 1 => Dolares a Soles Tasa de Venta
            Estado cambio == 0 => Soles a Dolares Tasa de Compra
        */
        var cambiosCuentas = () => {
            if(estado_cambio == 1){
                $("#cuenta_origen_usd").next(".select2-container").show();
                $("#cuenta_origen_usd").removeAttr('disabled');
                
                $("#cuenta_origen_soles").next(".select2-container").hide();
                $("#cuenta_origen_soles").attr('disabled', 'disabled');

                $("#cuenta_destino_soles").next(".select2-container").show();
                $("#cuenta_destino_soles").removeAttr('disabled');
                
                $("#cuenta_destino_usd").next(".select2-container").hide();
                $("#cuenta_destino_usd").attr('disabled', 'disabled');
            }

            if(estado_cambio == 0){
                $("#cuenta_origen_usd").next(".select2-container").hide();
                $("#cuenta_origen_usd").attr('disabled', 'disabled');

                $("#cuenta_origen_soles").next(".select2-container").show();
                $("#cuenta_origen_soles").removeAttr('disabled');

                $("#cuenta_destino_soles").next(".select2-container").hide();
                $("#cuenta_destino_soles").attr('disabled', 'disabled');
                
                $("#cuenta_destino_usd").next(".select2-container").show();
                $("#cuenta_destino_usd").removeAttr('disabled');
            }

            $("#cuentas").css('display', 'inline-flex');
        }

        $( document ).ready(function() {
            /*
            0=Soles a Dolares Tasa de Compra
            1=Dolares a Soles Tasa de Venta
            */
            cambiosCuentas();

            var tasa_venta={{$tasa->tasa_venta}};
            var tasa_compra={{$tasa->tasa_compra}};

            $("#boton-cambiar").click(function () {
                var monto_a_cambiar=$("#cambio_origen").val();
                if(estado_cambio==0)
                {
                    estado_cambio=1;
                    $("#tipo_cambio").val(1);
                    resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra);
                    $("#cambio_destino").val(resultado.toFixed(2));

                    $("#text-moneda1").html('USD');
                    $("#text-moneda2").html('PEN');
                }
                else
                {
                    estado_cambio=0;
                    $("#tipo_cambio").val(0);
                    resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta);
                    $("#cambio_destino").val(resultado.toFixed(2));

                    $("#text-moneda1").html('PEN');
                    $("#text-moneda2").html('USD');
                }

                cambiosCuentas();
            });

            $("#cambio_origen").keyup(function () {
                var monto_a_cambiar=$("#cambio_origen").val();

                if(estado_cambio==0)
                {
                    resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_compra);
                    $("#cambio_destino").val(resultado.toFixed(2));
                }
                else
                {
                    resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_venta);
                    $("#cambio_destino").val(resultado.toFixed(2));
                }
            });

            $("#cambio_destino").keyup(function () {
                var monto_a_cambiar=$("#cambio_destino").val();

                if(estado_cambio==0)
                {
                    resultado=parseFloat(monto_a_cambiar)*parseFloat(tasa_compra);
                    $("#cambio_origen").val(resultado.toFixed(2));
                }
                else
                {
                    resultado=parseFloat(monto_a_cambiar)/parseFloat(tasa_venta);
                    $("#cambio_origen").val(resultado.toFixed(2));
                }
            });
        });
    </script>

@endsection