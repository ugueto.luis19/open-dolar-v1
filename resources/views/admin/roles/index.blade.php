@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Roles</h1>

        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
            <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                <div class="col-sm-7 col-md-6 col-lg-5 text-center text-sm-start">
                    
                </div>
                <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                    @canany('roles_agregar')
                        <button class="btn btn-login PoppinsMedium" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar Rol</button>
                    @endcanany
                </div>
            </div>
            <br>

            <div class="div-scrolll">
                <table class="table table-border-cero" id="roles">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Nombre</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                    @if(count($roles)>0)
                        @foreach($roles as $rol)
                            <tr class="tr-border-top">
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$rol->name}}
                                </th>
                        
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    @canany('roles_editar')
                                        <a title="Editar Rol" href="#" class="link-descarga-table p-2" style="text-decoration: none;" onclick="modalEdit('{{ $rol }}')">
                                            <i class="fa-solid fa-pencil"></i>
                                        </a>

                                        @if($rol->name != "Cliente")
                                            <a title="Eliminar Rol" href="#" onclick="eliminarRol(this, '{{ $rol->id }}', '{{route("eliminar-rol",["id"=>$rol->id])}}');" style="text-decoration: none;" id="eliminar-rol-{{ $rol->id }}">
                                                <button type="button" class="btn btn-borrar" >
                                                    <i class="fa-solid fa-trash-can" style="margin-left: -50px;"></i>
                                                </button>
                                            </a>
                                        @endif
                                    @endcanany
                                </th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{$roles->links()}}
            </div>
        </div>
    </div>

    @canany('roles_agregar')
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Agregar nuevo Rol</h1>

                            <form method="post" action="{{ route('registrar-rol') }}">
                                @csrf
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="text" name="name" class="input-login PoppinsRegular" placeholder="Ingrese nombre del Rol" required>
                                        </div>
                                    </div>

                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <select name="permission[]" multiple required class="input-login PoppinsRegular">
                                                <option value="" disabled>Debe seleccionar los permisos</option>
                                                <?php $seccion=""; $seccion_nuevo="";?>
                                                @if(count($permisos)>0)
                                                    @foreach($permisos as $value)
                                                        <?php $seccion_nuevo=explode('_',$value->name)[0];?>
                                                        @if ($seccion_nuevo!=$seccion)
                                                            <optgroup label="{{strtoupper($seccion_nuevo)}}">
                                                                @endif
                                                                <option value="{{$value->id}}">{{ $value->name }}</option>
                                                                <?php $seccion=$seccion_nuevo;?>
                                                                @if ($seccion_nuevo!=$seccion)
                                                            </optgroup>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>



                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div> 

                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany   


    @canany('roles_editar')
        <div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Editar rol</h1>
                            <form method="post" action="{{ route('editar-rol') }}">
                                @csrf
                                <input type="hidden" name="id" id="id">
                                <div class="row mx-0 justify-content-start mt-5">
                                    
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input id="nombre" name="name" class="input-login PoppinsRegular"  placeholder="Ingrese nombre" required>
                                        </div>
                                    </div>

                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <select name="permission[]" id="permisos" multiple required class="input-login PoppinsRegular">
                                                <option value="" disabled>Debe seleccionar los permisos</option>
                                                <?php $seccion=""; $seccion_nuevo="";?>
                                                @if(count($permisos)>0)
                                                    @foreach($permisos as $value)
                                                        <?php $seccion_nuevo=explode('_',$value->name)[0];?>
                                                        @if ($seccion_nuevo!=$seccion)
                                                            <optgroup label="{{strtoupper($seccion_nuevo)}}">
                                                                @endif
                                                                <option value="{{$value->id}}">{{ $value->name }}</option>
                                                                <?php $seccion=$seccion_nuevo;?>
                                                                @if ($seccion_nuevo!=$seccion)
                                                            </optgroup>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div> 
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Editar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            //$('#roles').DataTable();

        } );

        var eliminarRol = (e, id, ruta) => {
            e.preventDefault;

            toastr.warning("<br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Si</button>",'Desea eliminar el Rol?',
                {
                    closeButton: true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#confirmationRevertYes").click(function(){
                            window.location.href = ruta;
                        });
                    }
                });
        };

        var modalEdit = (obj) => {
            var myModal = new bootstrap.Modal(document.getElementById('modalEdit'));
            var op = JSON.parse(obj);
            var arrayPermisos = new Array();
            
            $.each(op.permisos, function(i, val){
                arrayPermisos.push(val.permission_id);
            });

            $("#id").val(op.id);
            $("#nombre").val(op.name);
            $("#permisos").val(arrayPermisos).change();
            myModal.show();
        };

    </script>

@endsection