@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Confirmar operación</h1>
        <p class="text-end mb-0 mt-4 mt-xl-0">

            <a class="link-anular PoppinsLight" href="#" onclick="anularOperacion(this, '{{ $operacion->id }}', '{{route("anular-cambio",["id"=>$operacion->id])}}');">< Anular orden</a>
        </p>
        <form action="{{route('finalizar-cambio')}}" method="post" enctype="multipart/form-data">
            <div class="row mx-0 mt-4 justify-content-center">
                <div class="col-sm-10 col-md-8 col-lg-6 ps-0 pe-0 pe-lg-2">
                    <div class="card-blanco card-blanco-padding-left-right">
                        <h5 class="arlrdbd titulo-card-margin-bottom">Detalles de la operación:</h5>

                        <input name="id_operacion" type="hidden" value="{{$operacion->id}}">

                        <div class="row mx-0 mb-3">
                            <div class="col-4">
                                <p class="PoppinsBold p-color-ultimas my-0">Estado:</p>
                            </div>
                            <div class="col-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">
                                    @if($operacion->estado=='0')
                                        Pendiente
                                    @elseif($operacion->estado=='1')
                                        En Proceso
                                    @elseif($operacion->estado=='2')
                                        Realizado
                                    @elseif($operacion->estado=='3')
                                        Devolución
                                    @elseif($operacion->estado=='4')
                                        Devuelto
                                    @elseif($operacion->estado=='5')
                                        Observados
                                    @elseif($operacion->estado=='6')
                                        Anulado
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="row mx-0 mb-3">
                            <div class="col-4">
                                <p class="PoppinsBold p-color-ultimas my-0">Fecha:</p>
                            </div>
                            <div class="col-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">{{date('d-m-Y',strtotime($operacion->fecha_operacion))}}</p>
                            </div>
                        </div>
                        <div class="row mx-0 mb-3">
                            <div class="col-4">
                                <p class="PoppinsBold p-color-ultimas my-0">Origen:</p>
                            </div>
                            <div class="col-8">
                                <p class="PoppinsRegular p-color-ultimas my-0" style="font-size: 14px;">@if(is_object($operacion->origen)){{$operacion->origen->banco->nombre}} ({{$operacion->origen->nro_cuenta}})@endif</p>
                            </div>
                        </div>
                        <div class="row mx-0 mb-3">
                            <div class="col-4">
                                <p class="PoppinsBold p-color-ultimas my-0">Destino:</p>
                            </div>
                            <div class="col-8">
                                <p class="PoppinsRegular p-color-ultimas my-0" style="font-size: 14px;">@if(is_object($operacion->destino)){{$operacion->destino->banco->nombre}} ({{$operacion->destino->nro_cuenta}})@endif</p>
                            </div>
                        </div>
                        <div class="row mx-0 mb-3">
                            <div class="col-4">
                                <p class="PoppinsBold p-color-ultimas my-0">Cupon:</p>
                            </div>
                            <div class="col-7">
                                @if(!is_null($operacion->id_cupon))
                                    <p class="PoppinsRegular p-color-ultimas my-0">{{ $operacion->cupon->codigo }}</p>
                                @else
                                    <p class="PoppinsRegular p-color-ultimas my-0"></p>
                                @endif
                            </div>
                        </div>

                        <?php

                        if($operacion->tipo=='0')
                        {
                            $moneda_1='S/';
                            $moneda_2='$';
                            if(is_object($operacion->cupon))
                            {
                                $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta-$operacion->cupon->valor_soles;
                            }
                            else
                            {
                                $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta;
                            }
                        }
                        else{
                            $moneda_1='$';
                            $moneda_2='S/';
                            if(is_object($operacion->cupon))
                            {
                                $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra+($operacion->cupon->valor_soles/$operacion->tasa->tasa_compra);
                            }
                            else
                            {
                                $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra;
                            }
                        }

                        ?>

                        <div class="row mx-0 mb-3">
                            <div class="col-4">
                                <p class="PoppinsBold p-color-ultimas my-0">Monto a Enviar:</p>
                            </div>
                            <div class="col-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">{{$moneda_1}} {{number_format($operacion->monto_enviado,2)}}</p>
                            </div>
                        </div>
                        <div class="row mx-0 mb-3">
                            <div class="col-4">
                                <p class="PoppinsBold p-color-ultimas my-0">Monto a Recibir:</p>
                            </div>
                            <div class="col-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">{{$moneda_2}}
                                    @if($operacion->uso_opendolar>0 && Session::get('perfil')->totalOpenCredit()>0)
                                        <?php
                                            // Validación por si quiere vulnerar el sistema de igual forma aplico la validación  en el controlador al finalizar esta orden
                                        ?>
                                        @if($operacion->uso_opendolar<=Session::get('perfil')->totalOpenCredit())
                                            @if($operacion->tipo=='0')
                                                <?php
                                                $dinero_adicional_opencreditos_usd=$comision_config->tasa_opencredit*$operacion->uso_opendolar;
                                                ?>
                                                {{number_format($operacion->monto_recibido+$dinero_adicional_opencreditos_usd,2)}}
                                                <br>
                                                <span>Se le sumo a su monto una bonificación por el cambio de sus Opencreditos de {{$moneda_2}} {{number_format($dinero_adicional_opencreditos_usd,2)}} </span>
                                            @else
                                                <?php
                                                $dinero_adicional_opencreditos_soles=($comision_config->tasa_opencredit*$operacion->uso_opendolar)*$monto_tasa;
                                                ?>
                                                {{number_format($operacion->monto_recibido+$dinero_adicional_opencreditos_soles,2)}}
                                                <br>
                                                <span>Se le sumo a su monto una bonificación por el cambio de sus Opencreditos de {{$moneda_2}} {{number_format($dinero_adicional_opencreditos_soles,2)}} </span>
                                            @endif
                                        @else
                                            {{number_format($operacion->monto_recibido,2)}}
                                        @endif
                                    @else
                                        {{number_format($operacion->monto_recibido,2)}}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="row mx-0 mb-3">
                            <div class="col-4">
                                <p class="PoppinsBold p-color-ultimas my-0">Cambio:</p>
                            </div>
                            <div class="col-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">{{number_format($monto_tasa,3)}}</p>
                            </div>
                        </div>
                        <div class="card-rojo card-rojo-border-amarrillo">
                            <img src="/assets-web/img/icon-alert.png">
                            <p class="PoppinsRegular p-hora-atencion mb-0">
                                Antes de confirmar la operación, por favor revisa con atención los detalles.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10 col-md-8 col-lg-6 col-heigth-porcentaje mt-4 mt-lg-0 ps-0 ps-lg-2 pe-0">
                    <div class="card-blanco card-blanco-padding-left-right height-ultimas-operaciones">
                        <h5 class="arlrdbd mb-4 titulo-card-margin-bottom">Comprobante</h5>
                        <p class="PoppinsRegular p-color-ultimas">Para finalizar la transacción, ingrese el numero de operación o adjunte conprobante</p>
                        <input type="number" name="nro_transferencia" placeholder="Ingrese número de operación" required class="input-cupon PoppinsRegular">
                        <span class="btn btn-file-adjuntar PoppinsMedium mt-3">
						<img src="/assets-web/img/icon-imagen.png" class="me-2"> Adjuntar vaucher o captura
						<input type="file" name="imagen_comprobante" required id="imageFile1">
                            <label id="reportar_imagen"></label>
					</span>
                    </div>
                    <div class="card-blanco card-blanco-padding-left-right mt-4 text-end">
                        <div class="d-flex align-items-center justify-content-between mt-4">
                            <img src="/assets-web/img/icon-reloj.png" class="img-reloj-amarillo me-3">
                            <p class="PoppinsRegular p-hora-atencion mb-0 text-start">Recuerda que el tipo de cambio es dinámico y por eso solo podemos guardartelo por 20 minutos. Tu operacion inicio a las {{date('H:i:s',strtotime($operacion->fecha_operacion))}}.</p>
                        </div>
                        <p class="PoppinsBold p-color-ultimas p-hora-span mt-3 text-start">
                            Hora de vencimiento:<br>

                            <?php
                            $hora_vencimiento= date('Y-m-d H:i:s',strtotime($operacion->fecha_operacion.' +20 minute'));
                            ?>

                            <span>{{date('H:i:s',strtotime($hora_vencimiento))}}</span>
                        </p>
                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-4">Enviar</button>
                    </div>
                </div>
            </div>
            @csrf
        </form>

    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {

            $("#imageFile1").change(function () {
                $("#reportar_imagen").html('Archivo Cargado Correctamente!');
            });

        });

        var anularOperacion = (e, id, ruta) => {
            e.preventDefault;

            toastr.warning("<br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Si</button>",'Desea anular la operación?',
                {
                  closeButton: true,
                  allowHtml: true,
                  onShown: function (toast) {
                        $("#confirmationRevertYes").click(function(){
                            window.location.href = ruta;
                        });
                    }
                });
        };
    </script>
@endsection