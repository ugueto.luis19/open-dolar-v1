<?php

namespace App\Http\Controllers;

use App\Models\Cargo;
use App\Models\Institucion;
use App\Models\Ocupacion;
use App\Models\Pais;
use App\Models\ActividadEconomica;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Perfil;
use App\Models\Verificacion;
use Redirect;
use Session;
use Illuminate\Support\Facades\Hash;

class PerfilController extends Controller
{
    public function informacionPerfilApi($id)
    {
        $perfil=Perfil::where('id',$id)
            ->with('usuario')
            ->with('ocupacion')
            ->with('institucion')
            ->first();

        $perfil->imagen_perfil='http://sistema.opendollar.dhdinc.org/img/perfiles/'.$perfil->imagen_perfil;

        return response()->json([
            'estado' => 'success',
            'message' => 'Informacion del perfil solicitado!',
            'perfil' => $perfil

        ],201);
    }

    public function miPerfil()
    {
        $menu=4;
        $id_perfil=session('id_perfil');
        $perfil=Perfil::find($id_perfil);
        $verificacion = Verificacion::where('id_perfil', session('id_perfil'))->orderBy('created_at', 'desc')->first();

        return view('perfil.info-perfil',compact('menu','perfil', 'verificacion'));
    }

    public function editarPerfil($codigo)
    {
        $menu=4;
        
        $perfil = Perfil::where('codigo_asignado', $codigo)->first();

        if(Session::get('perfil')->codigo_asignado != $codigo){
            $notification = array(
                'message' => 'Ha ocurrido un error al editar perfil.',
                'alert-type' => 'error'
            );

            return Redirect::back()->with($notification);
        }

        $ocupaciones = Ocupacion::all();
        $paises = Pais::all();
        $instituciones=Institucion::where('estado','1')->get();
        $actividad_economica=ActividadEconomica::all();
        $cargos=Cargo::all();
        return view('perfil.edit-perfil',compact('cargos','menu','perfil','ocupaciones','instituciones','paises', 'actividad_economica'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nro_celular' => 'required',
            'nombres' => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
            'tipo_doc' => 'required',
            'nro_doc' => 'required',
            'id_ocupacion' => 'required',
            'publicamente_expuesto' => 'required'
        ]);

        $path = public_path().'/img/perfiles/';
        $data = $request->all();

        if($request->id_institucion=='otros')
        {
            $data['id_institucion']=null;
        }
        else{
            $data['nombre_institucion']=null;
        }


        if($request->hasFile('imagen_perfil')){
            $file = $request->file('imagen_perfil');
            $filenameWithExt = $file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $fileNameToStore = time() . '.' . $extension;
            $data["imagen_perfil"] = $fileNameToStore;
            $file->move($path, $fileNameToStore);
            Session::put('imagen_perfil', $data["imagen_perfil"]);
        }

        $update_perfil = Perfil::find(session('id_perfil'));
        $update_perfil->update($data);

        $notification = array(
            'message' => 'El Perfil se actualizo correctamente.',
            'alert-type' => 'success'
        );

        return redirect()->route('mi-perfil')->with($notification);
    }


    public function seleccionarPerfil($codigo)
    {  
        $perfil = Perfil::where('id_cuenta', Auth::user()->id)->where('codigo_asignado', $codigo)->get();

        if($perfil->count() > 0){ // VERIFICANDO SI EL ID DEL PERFIL INGRESADO COINCIDE CON LA CUENTA QUE INICIO SESIÓN

            Session::put('perfil', $perfil->first());
            Session::put('id_perfil', $perfil->first()->id);
            Session::put('imagen_perfil', $perfil->first()->imagen_perfil);
            Session::put('tipo', $perfil->first()->tipo);

            if($perfil->first()->tipo==1)
            {
                Session::put('nombres', $perfil->first()->nombres);
                Session::put('apellido_paterno', $perfil->first()->apellido_paterno);
            }
            else
            {
                Session::put('nombre_juridico', $perfil->first()->nombre_juridico);
            }

            $notification = array(
                'message' => 'Ha iniciado sesión con su perfil correctamente.',
                'alert-type' => 'success'
            );

            return redirect()->route('dashboard')->with($notification);
        }
        else{
            $notification = array(
                'message' => 'El perfil no coincide con sus registros.',
                'alert-type' => 'warning'
            );

            return redirect()->route('dashboard')->with($notification);
        }
            
    }




    public function cambiarContrasena(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:8',
            'password_confirm' => 'required|min:8|required|same:password',
        ]);

        $request['password']=Hash::make($request->password);
        

        $user = User::find(Auth::user()->id);
        $user->update($request->all());

        $notification = array(
            'message' => 'Contraseña actualizada correctamente.',
            'alert-type' => 'success'
        );

        return redirect()->route('mi-perfil')->with($notification);
    }
}
