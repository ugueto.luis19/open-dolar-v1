<?php

namespace App\Http\Controllers;

use App\Models\Banco;
use App\Models\CuentaBancaria;
use App\Models\Moneda;
use App\Models\Operacion;
use App\Models\Perfil;
use App\Models\TiposCuenta;
use Illuminate\Http\Request;
use Session;
use Redirect;
use View;
class CuentaController extends Controller
{
    //
    public function misCuentas(){
        $menu=3;
        $bancos=Banco::all();
        $monedas=Moneda::all();
        $tipos_cuentas=TiposCuenta::all();
        $cuentas_bancarias=CuentaBancaria::where('estado',1)->where('id_perfil',Session::get('id_perfil'))->paginate(5);
        return view('cuentas.mis-cuentas',compact('menu','bancos','monedas','tipos_cuentas','cuentas_bancarias'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nro_cuenta' => 'required',
            'titular' => 'required',
            'id_banco' => 'required',
            'id_moneda' => 'required',
            'id_tipo_cuenta' => 'required',
            'propiedad' => 'required',
            'tipo_doc' => 'required',
            'nro_doc' => 'required',
        ]);
        $request['id_perfil']=Session::get('id_perfil');
        $request['estado']=1;
        $cuenta = new CuentaBancaria($request->all());
        $cuenta->save();

        $notification = array(
            'message' => 'La Cuenta Bancaria se registro correctamente.',
            'alert-type' => 'success'
        );

        return redirect()->route('mis-cuentas')->with($notification);
    }

    public function storeAjax(Request $request)
    {
        if($request->ajax()) {

            if ($request->nro_cuenta != null) {
                $this->validate($request, [
                    'nro_cuenta' => 'required',
                    'titular' => 'required',
                    'id_banco' => 'required',
                    'id_moneda' => 'required',
                    'id_tipo_cuenta' => 'required',
                    'propiedad' => 'required',
                    'tipo_doc' => 'required',
                    'nro_doc' => 'required',
                ]);
                $request['id_perfil'] = Session::get('id_perfil');
                $request['estado'] = 1;
                $cuenta = new CuentaBancaria($request->all());
                $cuenta->save();
            }
            else
            {
                $cuenta=null;
            }


            $operacion=Operacion::find($request->id_operacion);
            $cuentas_bancarias=CuentaBancaria::where('id_perfil',Session::get('id_perfil'))->where('estado','1')->get();

            $view = View::make('partes.select-cuentas')
                ->with('cuenta', $cuenta)
                ->with('operacion', $operacion)
                ->with('cuentas_bancarias', $cuentas_bancarias)
                ->render();

            return response()->json(['status' => true, 'cuenta'=>$cuenta, 'htmlview' => $view]);
        }
    }

    public function anularMasivomodal(Request $request)
    {
        if($request->ajax()) {

            $perfil=Perfil::find(Session::get('id_perfil'));

            $view = View::make('partes.eliminar-cuentas-modal-contenido')
                ->with('array_cuentas', $request->array_cuentas)
                ->with('perfil', $perfil)
                ->render();

            return response()->json(['status' => true, 'htmlview' => $view]);
        }
    }

    public function anularMasivo(Request $request)
    {
        if($request->ajax()) {

            if(count($request->array_cuentas)>0)
            {
                foreach ($request->array_cuentas as $array_cuentas)
                {
                    $cuenta = CuentaBancaria::where('id_perfil', Session::get('id_perfil'))->where('id', $array_cuentas)->get();
                    if($cuenta->count() > 0){
                        $cuenta=CuentaBancaria::find($array_cuentas);
                        $cuenta->estado=0;
                        $cuenta->save();
                    }
                }
            }
            return response()->json(['status' => true, 'estado' => 1]);
        }
    }

    public function buscarCuentas(Request $request)
    {
        if($request->ajax()) {
            $cuentas_bancarias=CuentaBancaria::where('estado',1)->where('id_perfil',Session::get('id_perfil'))->where(function ($query) use ($request) {
                $query->where('nro_cuenta', 'like', '%' . $request->buscar . '%')
                      ->orWhere('titular', 'like', '%' . $request->buscar . '%');
            })->paginate(5);

            $view = View::make('partes.cuentas_bancarias')
                ->with('cuentas_bancarias', $cuentas_bancarias)
                ->render();

            return response()->json(['status' => true, 'htmlview' => $view]);
        }
    }

}
