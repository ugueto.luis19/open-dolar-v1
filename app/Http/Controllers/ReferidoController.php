<?php

namespace App\Http\Controllers;

use App\Models\Comision;
use App\Models\ComisionReferidoConfig;
use App\Models\Perfil;
use Illuminate\Http\Request;
use App\Models\Operacion;
use Session;

class ReferidoController extends Controller
{
    //
    public function indexReferido()
    {
        $menu=6;
        $perfil=Perfil::find(Session::get('id_perfil'));
        $comisiones = Comision::orderBy('id','desc')
                ->where('id_perfil_padre',Session::get('id_perfil'))
                ->take(5)
                ->get();
        $comision_config=ComisionReferidoConfig::orderBy('id', 'desc')->first();
        return view('referidos.index',compact('menu', 'comisiones','perfil','comision_config'));
    }

    public function historialReferidos()
    {
        $menu=7;

        $comisiones = Comision::orderBy('id','desc')
            ->where('id_perfil_padre',Session::get('id_perfil'))
            ->paginate(6);

        return view('referidos.mi-historial-referidos',compact('menu','comisiones'));
    }

}
