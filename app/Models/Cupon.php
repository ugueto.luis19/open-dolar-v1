<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cupon extends Model
{
    use HasFactory;
    protected $table = 'cupones';
    protected $fillable = [
        'codigo',
        'fecha_caducidad',
        'valor_soles',
        'valor_pips',
        'estado',
    ];

    public function estado(){
        $estado = "";

        switch($this->estado){
            case 1: $estado = "Activo"; break;
            case 0: $estado = "Desactivado"; break;
        }

        return $estado;
    }


}
