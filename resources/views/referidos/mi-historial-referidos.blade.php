@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Mi Historial Referidos</h1>
        <div class="row">

            <div class="col-12 col-sm-3 text-end px-0 mt-4">
                <input style="margin-left: 23px;" type="text" name="buscador" autocomplete="off" class="form-control input-operacioes PoppinsRegular" value="" id="buscar" placeholder="Buscador" required="">
            </div>


        </div>
        <div id="div-contenido-buscador" class="mt-3">
            @if(count($comisiones)>0)
                @foreach($comisiones as $comision)
                    <div class="row">
                        <div class="col-md-9 col-lg-7 col-xl-7">
                            <div class="card card-c" style="margin: 11px 11px 11px 11px;padding: 0;border-radius: 11px;">
                                <div class="card-body" style="padding: 1rem 1rem !important;">
                                    <div class="row align-items-center no-gutters">
                                        <div class="col-3">
                                            <div class="fuente6-card"><span>Fecha</span></div><span class="contenido-cuentas"> {{date('d-m-Y',strtotime($comision->operacion->fecha_operacion))}}</span>
                                        </div>
                                        <div class="col-5">
                                            <div class="fuente6-card"><span>Nombre</span></div><span class="contenido-cuentas">{{$comision->perfilHijo->nombres}} {{$comision->perfilHijo->apellido_paterno}} {{$comision->perfilHijo->apellido_materno}}</span>
                                        </div>
                                        <div class="col-3">
                                            <div class="fuente6-card"><span>Me otorgó</span ></div><span class="contenido-cuentas">{{number_format($comision->monto,0)}} OpenCreditos</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="mt-3">
                {{ $comisiones->links() }}
            </div>
        </div>

       
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#mis-ref').DataTable();
        } );
    </script>

@endsection