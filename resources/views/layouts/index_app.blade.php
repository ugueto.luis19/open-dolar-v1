<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
	<meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
	<title>Open Dolar</title>
	<link rel="icon" type="image/png" href="{{asset('favicon-open-dolar.png')}}" sizes="16x16">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/styles.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/font-awesome.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugin-telf/css/intlTelInput.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/select2.min.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets-web/css/jquery.dataTables.min.css') }}"/>
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>
<body @if(isset($menu)==true) @if($menu==0) class="d-flex flex-wrap" @endif @else class="d-flex flex-wrap" @endif>

	@if(Auth::check() && Auth::user()->hasRole('Cliente') && $menu!=0)
		{{--Menu logeados--}}
		<div class="nav-header-administrador text-start text-xl-center py-0 py-xl-5 d-flex justify-content-center flex-wrap">
			<div class="div-header-nav px-4 px-md-5 px-xl-0 py-3 py-xl-0">
				<img src="{{ asset('/assets-web/img/logo.svg') }}" class="logo-general">
				<button class="btn btn-menu">
					<i class="fa fa-bars" id="iconNav"></i>
				</button>
			</div>
			<div class="div-nav-conten">
				<div class="div-perfil-header text-center">
					@if(!empty(\Session::get('imagen_perfil') ) || \Session::get('imagen_perfil') != null)
	                    <img class="img-perfil-header" src="/img/perfiles/{{\Session::get('imagen_perfil') }}">
	                @else
	                    <img src="/assets-web/img/Grupo 380.png" class="img-perfil-header">
	                @endif
					<h2 class="ArialBold titulo-nombre-header mt-2">
						@if(Session::get('tipo')==1)
							{{ Session::get('nombres') }} {{ Session::get('apellido_paterno') }}
						@else
							{{ Session::get('nombre_juridico') }}
						@endif
					</h2>
				</div>
				<ul style="list-style: none;" class="text-start ul-nav">
					<li class="list-nav">
						<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=1) class="img-oculta-nav" @endif >
						<a href="{{route('dashboard')}}" class="link-nav PoppinsRegular @if($menu==1) link-activo @endif ">Cambiar</a>
					</li>
					<li class="list-nav">
						<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=2) class="img-oculta-nav" @endif >
						<a href="{{route('mis-operaciones')}}" class="link-nav PoppinsRegular @if($menu==2) link-activo @endif ">Mis ordenes</a>
					</li>
					<li class="list-nav">
						<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=3) class="img-oculta-nav" @endif >
						<a href="{{route('mis-cuentas')}}" class="link-nav PoppinsRegular @if($menu==3) link-activo @endif">Cuentas bancarias</a>
					</li>
					<li class="list-nav">
						<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=4) class="img-oculta-nav" @endif >
						<a href="{{route('mi-perfil')}}" class="link-nav PoppinsRegular @if($menu==4) link-activo @endif">Perfil</a>
					</li>
					<li class="list-nav">
						<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=5) class="img-oculta-nav" @endif >
						<a href="{{route('alertas')}}" class="link-nav PoppinsRegular @if($menu==5) link-activo @endif">Alertas</a>
					</li>
					<li class="list-nav">
						<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=6) class="img-oculta-nav" @endif >
						<a href="{{route('referido')}}" class="link-nav PoppinsRegular @if($menu==6) link-activo @endif">Recomienda y gana</a>
					</li>
					<li class="list-nav">
						<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=7) class="img-oculta-nav" @endif >
						<a href="{{route('historial-referidos')}}" class="link-nav PoppinsRegular @if($menu==7) link-activo @endif">Historial Referidos</a>
					</li>
                    <li class="list-nav">
                        <a href="{{route('logout')}}" class="link-nav PoppinsRegular">Salir</a>
                    </li>
				</ul>
				<div class="div-footer-nav text-start">
					<svg class="icono-monedas" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23.542 23.542" style="height: 20px;margin-top: -5px; position: absolute; top: 3px;">
						<path style="fill:#ffffff !important" id="Icon_awesome-coins" data-name="Icon awesome-coins" d="M0,18.636V20.6c0,1.623,3.954,2.943,8.828,2.943s8.828-1.32,8.828-2.943V18.636c-1.9,1.338-5.371,1.963-8.828,1.963S1.9,19.974,0,18.636ZM14.714,5.886c4.874,0,8.828-1.32,8.828-2.943S19.588,0,14.714,0,5.886,1.32,5.886,2.943,9.84,5.886,14.714,5.886ZM0,13.813v2.373c0,1.623,3.954,2.943,8.828,2.943s8.828-1.32,8.828-2.943V13.813c-1.9,1.563-5.375,2.373-8.828,2.373S1.9,15.376,0,13.813Zm19.128.506c2.635-.51,4.414-1.458,4.414-2.547V9.808a11.292,11.292,0,0,1-4.414,1.586ZM8.828,7.357C3.954,7.357,0,9,0,11.035s3.954,3.678,8.828,3.678,8.828-1.646,8.828-3.678S13.7,7.357,8.828,7.357ZM18.912,9.946c2.759-.5,4.63-1.471,4.63-2.589V5.394c-1.632,1.154-4.437,1.775-7.389,1.922A5.149,5.149,0,0,1,18.912,9.946Z" fill="#fff"></path>
					</svg>
					<p class="PoppinsRegular" style="font-size: 16px;">Mis OpenCreditos: {{Session::get('perfil')->totalOpenCredit()}}</p>
					<a href="{{route('dashboard')}}" class="PoppinsRegular">Usar mis OpenDolar</a>
				</div>
			</div>
		</div>
	@elseif(Auth::check() && Auth::user()->hasRol->rol->id != 1 && isset($menu))
		<div class="nav-header-administrador text-start text-xl-center py-0 py-xl-5 d-flex justify-content-center flex-wrap">
			<div class="div-header-nav px-4 px-md-5 px-xl-0 py-3 py-xl-0">
				<img src="{{ asset('/assets-web/img/logo.svg') }}" class="logo-general">
				<button class="btn btn-menu">
					<i class="fa fa-bars" id="iconNav"></i>
				</button>
			</div>
			<div class="div-nav-conten">
				<div class="div-perfil-header text-center">
					
	                <img src="/assets-web/img/Grupo 380.png" class="img-perfil-header">

					<h2 class="ArialBold titulo-nombre-header mt-2">{{ Auth::user()->hasRol->rol->name }}</h2>
				</div>
				<ul style="list-style: none;" class="text-start ul-nav">
					@canany('tasas_cambio_listado')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=7) class="img-oculta-nav" @endif >
							<a href="{{ route('tasas-cambio') }}" class="link-nav PoppinsRegular @if($menu==7) link-activo @endif ">Tasas de Cambio</a>
						</li>
					@endcanany

					@canany('cupones_listado')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=8) class="img-oculta-nav" @endif >
							<a href="{{ route('cupones') }}" class="link-nav PoppinsRegular @if($menu==8) link-activo @endif ">Cupones</a>
						</li>
					@endcanany

					@canany('horario_atencion_ver')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=1) class="img-oculta-nav" @endif >
							<a href="{{ route('horarios') }}" class="link-nav PoppinsRegular @if($menu==1) link-activo @endif ">Horario de Atención</a>
						</li>
					@endcanany

					@canany('operaciones_listado')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=2) class="img-oculta-nav" @endif >
							<a href="{{ route('operaciones') }}" class="link-nav PoppinsRegular @if($menu==2) link-activo @endif ">Operaciones</a>
						</li>
					@endcanany

					@canany('verificaciones_listado')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=3) class="img-oculta-nav" @endif >
							<a href="{{ route('verificaciones') }}" class="link-nav PoppinsRegular @if($menu==3) link-activo @endif">Verificaciones</a>
						</li>
					@endcanany

					@canany('configuracion_opencreditos_listado')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=5) class="img-oculta-nav" @endif >
							<a href="{{ route('comisiones-config') }}" class="link-nav PoppinsRegular @if($menu==5) link-activo @endif">Configuración OpenCreditos</a>
						</li>
					@endcanany

					@canany('instituciones_listado')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=6) class="img-oculta-nav" @endif >
							<a href="{{ route('instituciones') }}" class="link-nav PoppinsRegular @if($menu==6) link-activo @endif">Instituciones PTE</a>
						</li>
					@endcanany

					@canany('clientes_listado')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=4) class="img-oculta-nav" @endif >
							<a href="{{ route('clientes') }}" class="link-nav PoppinsRegular @if($menu==4) link-activo @endif">Clientes</a>
						</li>
					@endcanany

					@canany('usuarios_listado')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=10) class="img-oculta-nav" @endif >
							<a href="{{ route('usuarios') }}" class="link-nav PoppinsRegular @if($menu==10) link-activo @endif">Usuarios</a>
						</li>
					@endcanany

					@canany('roles_listado')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=9) class="img-oculta-nav" @endif >
							<a href="{{ route('roles') }}" class="link-nav PoppinsRegular @if($menu==9) link-activo @endif">Roles</a>
						</li>
					@endcanany

					@canany('cuentas_bancarias_ad_listado')
						<li class="list-nav">
							<img src="{{ asset('/assets-web/img/Grupo 22.png') }}" @if($menu!=11) class="img-oculta-nav" @endif >
							<a href="{{ route('cuentas_bancarias_admin') }}" class="link-nav PoppinsRegular @if($menu==11) link-activo @endif">Cuentas Bancarias Admin</a>
						</li>
					@endcanany

                    <li class="list-nav">
                        <a href="{{route('logout')}}" class="link-nav PoppinsRegular">Salir</a>
                    </li>
				</ul>
			</div>
		</div>
	@else
		{{--Menu no logeados--}}
		<div class="nav-header py-3 py-lg-0 px-5 px-lg-0">
			<img src="{{ asset('/assets-web/img/Grupo 19.png') }}" class="logo-open">
		</div>
	@endif

	@yield('content')
	@if(Auth::check())
	<div class="ht-ctc ht-ctc-chat ctc-analytics ctc_wp_desktop style-7_1 ht_ctc_animation no-animations" id="ht-ctc-chat" style="position: fixed; bottom: 15px; right: 15px; cursor: pointer; z-index: 99999999;">
        <div class="ht_ctc_style ht_ctc_chat_style">
                <style id="ht-ctc-s7_1">

    .ctc_s_7_1 {
    width: 50px;
    height: 50px;

    -webkit-transition: width 0.3s, display 0.3s ;
    -moz-transition: width 0.3s, display 0.3s ;
    -o-transition: width 0.3s, display 0.3s ;
    transition: width 0.3s, display 0.3s ;

}

      
    .ctc_s_7_1:hover {
		width: 183px;
    padding-left: 22px;
    }
     .ctc_s_7_1:hover svg {
        visibility: hidden;
    }

    .ctc_s_7_1 p {
       display: none;
       white-space: nowrap;
    }
    .ctc_s_7_1:hover p {
       display: block;
    }

</style>

    <div class="ctc_s_7_1 ctc-analytics" style="display:flex;justify-content:center;align-items:center; background-color: #25D366; border-radius:25px;">
        <p class="ctc_s_7_1_cta ctc-analytics ctc_cta ht-ctc-cta  ht-ctc-cta-hover ctc_cta_stick " style="order: 0; color: rgb(255, 255, 255); padding-left: 21px; margin: 0px 10px; border-radius: 25px;">¿Necesitas ayuda?</p>
        <div class="ctc_s_7_icon_padding ctc-analytics " style="padding: 12px;border-radius: 25px; ">
            <svg style="pointer-events:none; display:block; height:20px; width:20px;" height="20px" version="1.1" viewBox="0 0 509 512" width="20px">
        <desc></desc><defs></defs>
        <g fill="none" fill-rule="evenodd" id="Page-1" stroke="none" stroke-width="1">
            <path d="M259.253137,0.00180389396 C121.502859,0.00180389396 9.83730687,111.662896 9.83730687,249.413175 C9.83730687,296.530232 22.9142299,340.597122 45.6254897,378.191325 L0.613226597,512.001804 L138.700183,467.787757 C174.430395,487.549184 215.522926,498.811168 259.253137,498.811168 C396.994498,498.811168 508.660049,387.154535 508.660049,249.415405 C508.662279,111.662896 396.996727,0.00180389396 259.253137,0.00180389396 L259.253137,0.00180389396 Z M259.253137,459.089875 C216.65782,459.089875 176.998957,446.313956 143.886359,424.41206 L63.3044195,450.21808 L89.4939401,372.345171 C64.3924908,337.776609 49.5608297,295.299463 49.5608297,249.406486 C49.5608297,133.783298 143.627719,39.7186378 259.253137,39.7186378 C374.871867,39.7186378 468.940986,133.783298 468.940986,249.406486 C468.940986,365.025215 374.874096,459.089875 259.253137,459.089875 Z M200.755924,146.247066 C196.715791,136.510165 193.62103,136.180176 187.380228,135.883632 C185.239759,135.781068 182.918689,135.682963 180.379113,135.682963 C172.338979,135.682963 164.002301,138.050856 158.97889,143.19021 C152.865178,149.44439 137.578667,164.09322 137.578667,194.171258 C137.578667,224.253755 159.487251,253.321759 162.539648,257.402027 C165.600963,261.477835 205.268745,324.111057 266.985579,349.682963 C315.157262,369.636141 329.460495,367.859106 340.450462,365.455539 C356.441543,361.9639 376.521811,350.186865 381.616571,335.917077 C386.711331,321.63837 386.711331,309.399797 385.184018,306.857991 C383.654475,304.305037 379.578667,302.782183 373.464955,299.716408 C367.351242,296.659552 337.288812,281.870254 331.68569,279.83458 C326.080339,277.796676 320.898622,278.418749 316.5887,284.378615 C310.639982,292.612729 304.918689,301.074268 300.180674,306.09099 C296.46161,310.02856 290.477218,310.577055 285.331175,308.389764 C278.564174,305.506821 259.516237,298.869139 236.160607,278.048627 C217.988923,261.847958 205.716906,241.83458 202.149458,235.711949 C198.582011,229.598236 201.835077,225.948292 204.584241,222.621648 C207.719135,218.824546 210.610997,216.097679 213.667853,212.532462 C216.724709,208.960555 218.432625,207.05866 220.470529,202.973933 C222.508433,198.898125 221.137195,194.690767 219.607652,191.629452 C218.07588,188.568136 205.835077,158.494558 200.755924,146.247066 Z" fill="#ffffff" id="htwaicon-chat"></path>
        </g>
        </svg>    
    </div>
    </div>                
</div>
</div>
@endif

<script src="{{ asset('assets-web/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets-web/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets-web/js/font-awesome.js') }}"></script>
<script src="{{ asset('assets-web/js/toastr.min.js') }}"></script>
<script src="{{ asset('assets-web/js/datepicker.js') }}"></script>
<script src="{{ asset('plugin-telf/js/intlTelInput.js') }}"></script>
<script src="{{ asset('assets-web/js/select2.min.js') }}"></script>
<script src="{{ asset('assets-web/js/jquery.dataTables.min.js') }}"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('assets-web/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets-web/js/datetime-moment.js') }}"></script>

<script>
	// Mostrar mensaje del error de la validacion Laravel con toastr
	@if ($errors->any())
		@foreach ($errors->all() as $error)
			toastr.error("{{ $error }}");
		@endforeach
	@endif
	@if(Session::has('message'))
		var type = "{{ Session::get('alert-type', 'info') }}";
		switch(type){
			case 'info':
				toastr.info("{{ Session::get('message') }}");
				break;

			case 'warning':
				toastr.warning("{{ Session::get('message') }}");
				break;

			case 'success':
				toastr.success("{{ Session::get('message') }}");
				break;

			case 'error':
				toastr.error("{{ Session::get('message') }}");
				break;
			default:
				toastr.info("{{ Session::get('message') }}");
			break;
		}
	@endif
	$('.date').datepicker({
		multidate: true,
		format: 'dd-mm-yyyy'
	});
</script>
@if(Auth::check())
	<script>
		var menu = 1;
		$(document).ready(function() {

			$('select').select2();

			$(".btn-menu").click(function() {
				if (menu==1) {
					$( ".div-nav-conten" ).addClass("mostrar-menu");
					$( "#iconNav" ).addClass("fa-solid fa-xmark");
					$( "#iconNav" ).removeClass("fa fa-bars");
					$( ".nav-header-administrador" ).css("background-color", "#0000007a");
					menu = menu + 1;
				}
				else
				{
					$( "#iconNav" ).addClass("fa fa-bars");
					$( "#iconNav" ).removeClass("fa-solid fa-xmark");
					$( ".div-nav-conten" ).removeClass("mostrar-menu");
					$( ".nav-header-administrador" ).css("background-color", "transparent");
					menu = 1;
				}
			});

			$( ".div-flotante" ).click(function() {
				$(".div-flotante").hide(1000);
				$(".div-flotante-azul").show(1000);
			});
			$( ".btn-close-ayuda" ).click(function() {
				$(".div-flotante").show(1000);
				$(".div-flotante-azul").hide(1000);
			});
		});
		AOS.init();
	</script>
@endif
@yield('scripts')
</body>
</html>