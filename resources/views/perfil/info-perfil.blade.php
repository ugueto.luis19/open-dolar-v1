@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Mi Perfil</h1>
        <div class="card-blanco mt-5 card-blanco-padding-left-right" style="position: relative;">
            <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                <div class="col-3 col-xxl-3 px-0">
                    <h5 class="arlrdbd my-0">Datos del perfil</h5>
                </div>

                <div class="col-8 col-xxl-8 text-end px-0 justify-content-end" style="display: flex;">
                    @if(Auth::user()->perfiles->count()>1)
                        <a style="text-decoration: none; margin-top: 15px; margin-right: 30px;" href="{{ route('elegir-perfil') }}">
                            <p class="PoppinsRegular p-color-ultimas my-0 ms-3">> Seleccionar Perfil</p>
                        </a>
                    @endif
                    @if(Auth::user()->perfiles_juridicos()==true)
                        <a style="text-decoration: none; margin-top: 15px; margin-right: 30px;" href="{{ route('persona-juridica') }}">
                            <p class="PoppinsRegular p-color-ultimas my-0 ms-3">+ Agregar nuevo Perfil</p>
                        </a>
                    @endif
                    <a style="text-decoration: none;" href="{{route('editar-perfil',['codigo'=>$perfil->codigo_asignado])}}">
                        <button type="button" class="btn btn-login PoppinsMedium">Editar perfil</button>
                    </a>
                </div>
            </div>
            <div class="row mx-0 mb-4 align-items-end mt-5 justify-content-center justify-content-md-start">
                <div class="col-9 col-sm-4 text-center text-md-start">
                    @if(!empty(\Session::get('imagen_perfil') ) || \Session::get('imagen_perfil') != null)
                        <img class="img-perfil-admi" src="/img/perfiles/{{\Session::get('imagen_perfil') }}">
                    @else
                        <img src="/assets-web/img/Grupo 380.png" class="img-perfil-admi">
                    @endif


                </div>
                <div class="col-sm-10 col-md-8 col-lg-7 mt-4 mt-md-0">
                    @if(is_object($verificacion))
                        <div class="d-flex align-items-center wrap-guella">
                            @if($verificacion->estado=='1')
                                <div class="div-huella">
                                    <i class="fa-solid fa-fingerprint icn-huella"></i>
                                    <i class="fa-solid fa-circle-check icon-check-absolute"></i>
                                </div>
                            @else
                                <div class="div-huella">
                                    <i class="fa-solid fa-fingerprint icn-huella"></i>
                                    <i class="fa-solid fa-circle-xmark icon-close-absolute"></i>
                                </div>
                            @endif
                            <p class="PoppinsRegular p-color-ultimas my-0 ms-3">Registro biometrico</p>
                            @if($verificacion->estado=='2')
                                <a class="btn btn-proteger-cuenta PoppinsRegular px-3 ms-5" href="{{ route('registro-biometrico') }}">Proteger mi cuenta</a>
                            @endif
                        </div>
                    @else
                        <div class="d-flex align-items-center mt-2 wrap-guella">
                            <div class="div-huella">
                                <i class="fa-solid fa-fingerprint icn-huella"></i>
                                <i class="fa-solid fa-circle-xmark icon-close-absolute"></i>
                            </div>
                            <p class="PoppinsRegular p-color-ultimas my-0 ms-3">Registro biometrico</p>
                            <a class="btn btn-proteger-cuenta PoppinsRegular px-3 ms-5" href="{{ route('registro-biometrico') }}">Proteger mi cuenta</a>
                        </div>
                    @endif
                </div>
            </div>

            @if($perfil->tipo=='2')
                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">RUC:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->ruc}}</p>
                    </div>
                </div>

                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Nombre Juridico:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->nombre_juridico}}</p>
                    </div>
                </div>

                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Dirección:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->direccion_juridico}}</p>
                    </div>
                </div>


            @if(is_object($perfil->actividad_economica_obj))
                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Actividad Económica:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->actividad_economica_obj->nombre}}</p>
                    </div>
                </div>
            @else
                @if($perfil->id_actividad_economica==0)
                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Actividad Económica:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->actividad_economica}}</p>
                        </div>
                    </div>
                @endif
            @endif

            @if(intval($perfil->representante)==0 && $perfil->representante!=null && $perfil->representante!='')
                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Tipo de Documento del Representante Legal:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">
                            @if($perfil->tipo_doc_repre=='1')
                                DNI
                            @elseif($perfil->tipo_doc_repre=='2')
                                Pasaporte
                            @elseif($perfil->tipo_doc_repre=='3')
                                CE
                            @elseif($perfil->tipo_doc_repre=='4')
                                PTP
                            @endif
                        </p>
                    </div>
                </div>
                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Número de Documento del Representante Legal:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">
                            {{$perfil->nro_doc_repre}}
                        </p>
                    </div>
                </div>
                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Nombre del Representante Legal:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">
                            {{$perfil->nombres_repre}}
                        </p>
                    </div>
                </div>
                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Apellido Paterno del Representante Legal:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">
                            {{$perfil->apellido_paterno_repre}}
                        </p>
                    </div>
                </div>
                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Apellido Materno del Representante Legal:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">
                            {{$perfil->apellido_materno_repre}}
                        </p>
                    </div>
                </div>
            @endif
            @endif
            <div class="row mx-0 mb-4">
                <div class="col-sm-5 col-md-4">
                    <p class="PoppinsRegular p-color-ultimas my-0">Nombres y Apellidos:</p>
                </div>
                <div class="col-sm-7">
                    <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->nombres}} {{$perfil->apellido_paterno}} {{$perfil->apellido_materno}}</p>
                </div>
            </div>
            <div class="row mx-0 mb-4">
                <div class="col-sm-5 col-md-4">
                    <p class="PoppinsRegular p-color-ultimas my-0">Tipo de Documento:</p>
                </div>
                <div class="col-sm-7">
                    <p class="PoppinsRegular p-color-ultimas my-0">
                        @if($perfil->tipo_doc=='1')
                            DNI
                        @elseif($perfil->tipo_doc=='2')
                            Pasaporte
                        @elseif($perfil->tipo_doc=='3')
                            CE
                        @elseif($perfil->tipo_doc=='4')
                            PTP
                        @endif
                    </p>
                </div>
            </div>
            <div class="row mx-0 mb-4">
                <div class="col-sm-5 col-md-4">
                    <p class="PoppinsRegular p-color-ultimas my-0">Número de Documento:</p>
                </div>
                <div class="col-sm-7">
                    <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->nro_doc}}</p>
                </div>
            </div>

            @if(is_object($perfil->nacionalidad))
                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Nacionalidad:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->nacionalidad->nombre}}</p>
                    </div>
                </div>
            @endif

            @if($perfil->fecha_nacimiento!='' && $perfil->fecha_nacimiento!=null)
                <div class="row mx-0 mb-4">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Fecha Nacimiento:</p>
                    </div>
                    <div class="col-sm-7">
                        <p class="PoppinsRegular p-color-ultimas my-0">{{date('d-m-Y',strtotime($perfil->fecha_nacimiento))}}</p>
                    </div>
                </div>
            @endif

            <div class="row mx-0 mb-4">
                <div class="col-sm-5 col-md-4">
                    <p class="PoppinsRegular p-color-ultimas my-0">Número de teléfono:</p>
                </div>
                <div class="col-sm-7">
                    <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->nro_celular}}</p>
                </div>
            </div>
            <div class="row mx-0 mb-4">
                <div class="col-sm-5 col-md-4">
                    <p class="PoppinsRegular p-color-ultimas my-0">Correo electrónico:</p>
                </div>
                <div class="col-sm-7">
                    <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->usuario->email}}</p>
                </div>
            </div>
            <div class="row mx-0 mb-4">
                <div class="col-sm-5 col-md-4">
                    <p class="PoppinsRegular p-color-ultimas my-0">Ocupación:</p>
                </div>
                <div class="col-sm-7">
                    <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->ocupacion->nombre}}</p>
                </div>
            </div>

            <div class="row mx-0 mb-4">
                <div class="col-sm-5 col-md-4">
                    <p class="PoppinsRegular p-color-ultimas my-0">Publicamente expuesto:</p>
                </div>
                <div class="col-sm-7">
                    <p class="PoppinsRegular p-color-ultimas my-0">
                        @if($perfil->publicamente_expuesto=='1')
                            Si
                        @else
                            No
                        @endif
                    </p>
                </div>
            </div>
            @if($perfil->publicamente_expuesto=='1')
                @if(is_object($perfil->institucion))
                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Institución:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">
                                {{$perfil->institucion->nombre}}
                            </p>
                        </div>
                    </div>
                @else
                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Institución:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">
                                {{$perfil->nombre_institucion}}
                            </p>
                        </div>
                    </div>
                @endif
                @if(is_object($perfil->cargo))
                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Cargo:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">
                                {{$perfil->cargo->nombre}}
                            </p>
                        </div>
                    </div>
                @endif
            @endif

        </div>
    </div>
@endsection
@section('scripts')
@endsection