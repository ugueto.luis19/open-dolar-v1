@extends('layouts.index_app')

@section('content')
	<div class="conten_panel_administrador mt-3 mt-lg-0">
		<div class="div-conten pb-xl-3 div-conten-padding-left-right">
			<h1 class="PoppinsBold titulo-conten text-center mb-0">Paso 1</h1>
			<form method="post" action="{{ route('biometrico-paso-1') }}" enctype="multipart/form-data">
				@csrf
				<div class="row mx-0 justify-content-center mt-3">
					<div class="col-10 text-center mb-3">
						<p class="PoppinsRegular p-verificacion-subtitulo">Toma buena foto de tu documento de identidad <br> de adelante y atrás y súbelo.</p>
					</div>
					<div class="col-sm-6 col-xl-4 px-xl-3">
						<p class="PoppinsRegular p-verificacion-descripcion text-center mt-2 mb-1">Parte frontal</p>
						<div class="div-file">
							<div class="div-file-imagen" id="imgpreviewer1">
								<i class="fas fa-image iconFile" id="iconFile1"></i>
							</div>
							<span class="btn btn-file PoppinsMedium">
								Seleccionar archivo
								<input type="file" name="imagen1" id="imageFile1" required accept="image/*" capture="user">
							</span>
						</div>
					</div>

					<div class="col-sm-6 col-xl-4 px-xl-3 mt-5 mt-sm-0">
						<p class="PoppinsRegular p-verificacion-descripcion text-center mt-2 mb-1">Posterior</p>
						<div class="div-file">
							<div class="div-file-imagen" id="imgpreviewer2">
								<i class="fas fa-image iconFile" id="iconFile2"></i>
							</div>
							<span class="btn btn-file PoppinsMedium">
								Seleccionar archivo
								<input type="file" name="imagen2" id="imageFile2" required accept="image/*" capture="user">
							</span>
						</div>
					</div>
					<div class="col-12 text-center mt-5">
						<button type="submit" class="btn btn-login btn-file-top PoppinsMedium px-5">¡Listo!</button>
					</div>
				
					<div class="col-12 text-center my-3">
						<a href="{{ route('dashboard') }}" class="PoppinsRegular link-omitir">Continuar mas tarde ></a>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$("#imageFile1").on("change",function(){
			var imagen = $(this)[0].files[0];
			procesarImagen(imagen, function(result){
				$("#imgpreviewer1").append("<img src= '"+ result +"'>");
				$("#iconFile1").css("display", "none");
			});
		});
		$("#imageFile2").on("change",function(){
			var imagen = $(this)[0].files[0];
			procesarImagen(imagen, function(result){
				$("#imgpreviewer2").append("<img src= '"+ result +"'>");
				$("#iconFile2").css("display", "none");
			});
		});

		var procesarImagen = function(imagen, callback){
			var fileReader = new FileReader();
			fileReader.readAsDataURL(imagen);
			fileReader.onload = function(){
				callback(fileReader.result);
			};
		};

	</script>
@endsection