@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Mis cuentas</h1>

        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative;">
            <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                <div class="col-sm-7 col-md-6 col-lg-5 text-center text-sm-start">

                </div>
                @canany('cuentas_bancarias_ad_agregar')
                    <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                        <button class="btn btn-login PoppinsMedium" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar Cuenta Bancaria Admin</button>
                    </div>
                @endcanany
            </div>
            <div class="div-scrolll">
                <table class="table table-border-cero" id="mis-cuentas">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Banco</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Moneda</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Tipo</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Nro Cuenta</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Títular</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">RUC</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Estado</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                    @if(count($cuentas_bancarias)>0)
                        @foreach($cuentas_bancarias as $cuenta_bancaria)
                            <tr class="tr-border-top">
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$cuenta_bancaria->banco->nombre}}
                                </th>
                                <td class="text-center td-color-datos PoppinsMedium">
                                    {{$cuenta_bancaria->moneda->nombre}}
                                </td>
                                <td class="text-center td-color-datos PoppinsMedium">
                                    {{$cuenta_bancaria->tipo->nombre}}
                                </td>
                                <td class="text-center td-color-datos PoppinsMedium">
                                    {{$cuenta_bancaria->nro_cuenta}}
                                </td>
                                <td class="text-center td-color-datos PoppinsMedium">
                                    {{$cuenta_bancaria->titular}}
                                </td>
                                <td class="text-center td-color-datos PoppinsMedium">
                                    {{$cuenta_bancaria->ruc}}
                                </td>

                                <td class="text-center td-color-datos PoppinsMedium">
                                    @if($cuenta_bancaria->estado=='0')
                                        <strong style="color: red;">Anulada</strong>
                                    @else
                                        <strong style="color: green;">Activa</strong>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @canany('cuentas_bancarias_ad_editar')
                                        <a href="#" class="btn-with-iconos" style="text-decoration: none;" onclick="modalEdit('{{ $cuenta_bancaria }}')">
                                            <i class="fa-solid fa-pencil" style="width: 28px;"></i>
                                        </a>
                                    @endcanany

                                    @canany('cuentas_bancarias_ad_eliminar')
                                        @if($cuenta_bancaria->estado=='0')

                                        @else
                                            <a href="#" onclick="anularCuenta(this, '{{ $cuenta_bancaria->id }}', '{{route("eliminar-cuenta-bancaria-ad",["id"=>$cuenta_bancaria->id])}}');" style="text-decoration: none;" id="anular-cuenta-{{ $cuenta_bancaria->id }}">
                                                <button type="button" class="btn btn-borrar btn-with-iconos" >
                                                    <i class="fa-solid fa-trash-can" title="Anular Cuenta"></i>
                                                </button>
                                            </a>
                                        @endif
                                    @endcanany
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{$cuentas_bancarias->links()}}
            </div>
        </div>
    </div>

    @canany('cuentas_bancarias_ad_agregar')
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0">
                        <form action="{{route('registrar-cuenta-bancaria-ad')}}" method="post">
                            <div class="div-modal-padding-left-right pt-5">
                                <h1 class="PoppinsBold titulo-conten mb-0 mt-5 text-center">Registrar Cuenta Bancaria Admin</h1>
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Moneda</label>
                                            <select class="select-fomr" name="id_moneda" required>
                                                <option value="" selected>Debe seleccionar una Opción</option>
                                                @if(count($monedas)>0)
                                                    @foreach($monedas as $moneda)
                                                        <option value="{{$moneda->id}}">{{$moneda->nombre}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Nombre del banco</label>
                                            <select class="select-fomr" name="id_banco" required>
                                                <option value="" selected>Debe seleccionar una Opción</option>
                                                @if(count($bancos)>0)
                                                    @foreach($bancos as $banco)
                                                        <option value="{{$banco->id}}">{{$banco->nombre}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Tipo de cuenta</label>
                                            <select class="select-fomr" name="id_tipo_cuenta" required>
                                                <option value="" selected>Debe seleccionar una Opción</option>
                                                @if(count($tipos_cuentas)>0)
                                                    @foreach($tipos_cuentas as $tipo_cuenta)
                                                        <option value="{{$tipo_cuenta->id}}">{{$tipo_cuenta->nombre}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Numero de cuenta</label>
                                            <input type="number" name="nro_cuenta" class="input-login PoppinsRegular" required>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Nombre del titular</label>
                                            <input type="text" name="titular" required class="input-login PoppinsRegular">
                                        </div>
                                    </div>


                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">RUC</label>
                                            <input type="number" name="ruc" required class="input-login PoppinsRegular">
                                        </div>
                                    </div>
                        
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural" style="color: #fff !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                    </div>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endcanany

    @canany('cuentas_bancarias_ad_editar')
        <div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten-modal titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Editar Cuenta Bancaria Admin</h1>                    

                            <form action="{{route('editar-cuenta-bancaria-ad')}}" method="post">
                                <input type="hidden" name="id" id="id" value="">
                                <div class="div-modal-padding-left-right pt-5">
                                    <div class="row mx-0 justify-content-start mt-5">
                                        <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                            <div class="div-form-login">
                                                <label class="label-login PoppinsRegular">Moneda</label>
                                                <select class="select-fomr" id="id_moneda" name="id_moneda" required>
                                                    <option value="" selected>Debe seleccionar una Opción</option>
                                                    @if(count($monedas)>0)
                                                        @foreach($monedas as $moneda)
                                                            <option value="{{$moneda->id}}">{{$moneda->nombre}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <i class="fa fa-angle-down icon-select"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                            <div class="div-form-login">
                                                <label class="label-login PoppinsRegular">Nombre del banco</label>
                                                <select class="select-fomr" id="id_banco" name="id_banco" required>
                                                    <option value="" selected>Debe seleccionar una Opción</option>
                                                    @if(count($bancos)>0)
                                                        @foreach($bancos as $banco)
                                                            <option value="{{$banco->id}}">{{$banco->nombre}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <i class="fa fa-angle-down icon-select"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                            <div class="div-form-login">
                                                <label class="label-login PoppinsRegular">Tipo de cuenta</label>
                                                <select class="select-fomr"  id="id_tipo_cuenta"name="id_tipo_cuenta" required>
                                                    <option value="" selected>Debe seleccionar una Opción</option>
                                                    @if(count($tipos_cuentas)>0)
                                                        @foreach($tipos_cuentas as $tipo_cuenta)
                                                            <option value="{{$tipo_cuenta->id}}">{{$tipo_cuenta->nombre}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <i class="fa fa-angle-down icon-select"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                            <div class="div-form-login">
                                                <label class="label-login PoppinsRegular">Numero de cuenta</label>
                                                <input type="number" id="nro_cuenta" name="nro_cuenta" class="input-login PoppinsRegular" required>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                            <div class="div-form-login">
                                                <label class="label-login PoppinsRegular">Nombre del titular</label>
                                                <input type="text" id="titular" name="titular" required class="input-login PoppinsRegular">
                                            </div>
                                        </div>


                                        <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                            <div class="div-form-login">
                                                <label class="label-login PoppinsRegular">RUC</label>
                                                <input type="number" id="ruc" name="ruc" required class="input-login PoppinsRegular">
                                            </div>
                                        </div>
                            
                                        <div class="col-12 text-center">
                                            <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural" style="color: #fff !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                        </div>
                                    </div>
                                </div>
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany


@endsection
@section('scripts')
    <script>

        var modalEdit = (cuenta) => {
            var cuenta = JSON.parse(cuenta);

            var myModal = new bootstrap.Modal(document.getElementById('modalEdit'));
            $("#id").val(cuenta.id);
            $("#nro_cuenta").val(cuenta.nro_cuenta);
            $("#titular").val(cuenta.titular);
            $("#ruc").val(cuenta.ruc);
            $("#id_banco").val(cuenta.id_banco).trigger('change');
            $("#id_moneda").val(cuenta.id_moneda).trigger('change');
            $("#id_tipo_cuenta").val(cuenta.id_tipo_cuenta).trigger('change');
            myModal.show();
        };


        $(document).ready(function() {
           // $('#mis-cuentas').DataTable();
        } );
        var anularCuenta = (e, id, ruta) => {
            e.preventDefault;

            toastr.warning("<br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Si</button>",'Desea anular la cuenta?',
                {
                    closeButton: true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#confirmationRevertYes").click(function(){
                            window.location.href = ruta;
                        });
                    }
                });
        };
    </script>

@endsection