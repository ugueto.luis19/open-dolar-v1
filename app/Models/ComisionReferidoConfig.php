<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComisionReferidoConfig extends Model
{
    use HasFactory;
    protected $table = 'comisiones_referidos_config';
    protected $fillable = [
        'monto_ganar',
        'monto_operacion_minimo',
        'tasa_opencredit'
    ];

}
