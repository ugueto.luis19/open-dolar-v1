@extends('layouts.index_app')

@section('content')
	<div class="conten_panel_administrador mt-3 mt-lg-0">
		<div class="div-conten div-conten-padding-left-right">
			<h1 class="PoppinsBold titulo-conten text-center mb-0">Paso 2</h1>
			<form method="post" action="{{ route('biometrico-paso-2') }}" enctype="multipart/form-data">
				@csrf
				<div class="row mx-0 justify-content-center mt-3">
					<div class="col-10 text-center mb-3">
						<p class="PoppinsRegular p-verificacion-subtitulo">Toma un video selfie, haciendo 2 círculos <br> con la nariz, siguiendo los puntos</p>
					</div>
					<div class="col-sm-6 col-xl-4 px-xl-4">
						<div class="div-file">
							<div class="div-file-imagen div-file-video" id="imgpreviewer1">
								<i class="fas fa-play-circle iconFile" id="iconFile1"></i>
							</div>
							<span class="btn btn-file PoppinsMedium">
								Seleccionar archivo
								<input type="file" name="video" id="imageFile1" required accept="video/*" capture="environment">
							</span>
						</div>
					</div>
					<div class="col-12 text-center mt-5">
						<button type="submit" class="btn btn-login btn-file-top PoppinsMedium px-5">Terminar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$("#imageFile1").on("change",function(){
			var imagen = $(this)[0].files[0];
			procesarImagen(imagen, function(result){
				
				document.getElementById("imgpreviewer1").innerHTML = "<video autoplay controls src= '"+ result +"' width='100%' height='100%'></video>";
				$("#iconFile1").css("display", "none");
			});
		});

		var procesarImagen = function(imagen, callback){
			var fileReader = new FileReader();
			fileReader.readAsDataURL(imagen);
			fileReader.onload = function(){
				callback(fileReader.result);
			};
		}
	</script>
@endsection