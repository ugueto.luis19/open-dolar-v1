@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador text-center" style="position: relative;">
        <img src="/assets-web/img/Enmascarar grupo.png" class="img-absolute-biometrico img-widht-contra img-relative-finalizado" style="z-index: -1; mix-blend-mode: multiply;" data-aos="fade-up" >
        <div class="row mx-0 mt-0 justify-content-center row-heigt-center align-items-center">
            <div class="col-sm-10 col-md-8 col-lg-8 col-xl-5 ps-0 pe-0">
                <div class="card-blanco card-padding-top mt-4 text-center">
                    <?php
                    if($operacion->tipo=='0')
                    {
                        $moneda_2='$';
                    }
                    else{
                        $moneda_2='S/';
                    }
                    ?>
                    <img src="/assets-web/img/logo-color-open-dolar.png" class="logo-confirmacion-contra">
                    <h1 class="PoppinsBold titulo-conten mb-0 mt-2 mt-sm-4 text-center">Tu transferencia ha sido enviada</h1>
                    <h5 class="PoppinsRegular h5-azul-size mt-4 text-center">
                        Estamos verificando tu transferencia, Recibirás {{$moneda_2}} {{number_format($operacion->monto_recibido,2)}} en un rango de 10 a 20 minutos.
                    </h5>
                    <p class="PoppinsRegular p-verificacion-descripcion mt-3 text-center">Si ya estamos fuera de horario de atención, tu operacion será la primera en ser atendida en la mañana.</p>
                    <div class="align-items-center justify-content-center mt-5 row mb-0">
                        <a href="{{route('mis-operaciones')}}" class="btn btn-login mt-2 PoppinsMedium px-5 btn-padding-top-a" style="width: 75%;">Mis Ordenes</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
        });
    </script>
@endsection