<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\AlertaProgramada;
use App\Models\Tasa;
use Redirect;
use Session;

class AlertaController extends Controller
{
    public function misAlertas()
    {
        $menu=5;
        $alertas = AlertaProgramada::where('id_perfil', Session::get('id_perfil'))->get();
        
        if($alertas->count() == 0){
            $alertaCompra = new AlertaProgramada();
            $alertaCompra->tipo = 1;
            $alertaCompra->condicional = 3;
            $alertaCompra->valor_deseado = 0;
            $alertaCompra->id_perfil = Session::get('id_perfil');
            $alertaCompra->save();

            $alertaVenta = new AlertaProgramada();
            $alertaVenta->tipo = 2;
            $alertaVenta->condicional = 1;
            $alertaVenta->valor_deseado = 0;
            $alertaVenta->id_perfil = Session::get('id_perfil');
            $alertaVenta->save();
        }

        $alertas = AlertaProgramada::where('id_perfil', Session::get('id_perfil'))->get();
        $tasa = Tasa::orderBy('id', 'desc')->first();

        return view('alertas.index',compact('menu', 'alertas', 'tasa'));
    }

    public function store(Request $request)
    {
        $alertas = AlertaProgramada::where('id_perfil', Session::get('id_perfil'))->get();

        // COMPRA
        $alertaCompra = AlertaProgramada::find($alertas[0]->id);
        $alertaCompra->valor_deseado = $request->valor_deseado_compra;
        if(isset($request->checkCompra)) $alertaCompra->estado = 1; else $alertaCompra->estado = 0;
        $alertaCompra->update();

        // VENTA
        $alertaVenta = AlertaProgramada::find($alertas[1]->id);
        $alertaVenta->valor_deseado = $request->valor_deseado_venta;
        if(isset($request->checkVenta)) $alertaVenta->estado = 1; else $alertaVenta->estado = 0;
        $alertaVenta->update();

        $notification = array(
            'message' => 'La alerta se ha guardado correctamente.',
            'alert-type' => 'success'
        );

        return redirect()->route('alertas')->with($notification);
    }

    public function edit(Request $request)
    {
        $alerta = AlertaProgramada::find($request->id);
        $alerta->update($request->all());

        $notification = array(
            'message' => 'La alerta se editó correctamente.',
            'alert-type' => 'success'
        );

        return redirect()->route('alertas')->with($notification);
    }

    public function eliminarAlerta($id)
    {
        $alerta = AlertaProgramada::where('id_perfil', Session::get('id_perfil'))->where('id', $id)->get();

        if($alerta->count() > 0){
            AlertaProgramada::destroy($id);

             $notification = array(
                'message' => 'Alerta eliminada correctamente.',
                'alert-type' => 'success'
            );

            return redirect()->route('alertas')->with($notification);
        }
        else{
            $notification = array(
                'message' => 'La alerta no coincide con sus registros.',
                'alert-type' => 'warning'
            );

            return redirect()->route('alertas')->with($notification);
        }

    }
}
