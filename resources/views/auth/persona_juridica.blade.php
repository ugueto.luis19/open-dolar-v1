@extends('layouts.perfil')
@section('title','Buscar RUC')
@section('content')

	<div class="conten_panel mt-3 mt-lg-0" id="div-contenedor-padre">
		@if(Auth::check())
			<a href="{{route('elegir-perfil')}}" class="btn btn-volver-azul PoppinsRegular mb-2">&lt; Volver</a>
		@else
			<a href="{{route('seleccionar-registro')}}" class="btn btn-volver-azul PoppinsRegular mb-2">&lt; Volver</a>
		@endif
		<div class="div-conten div-conten-padding-left-right">
			<h1 class="PoppinsBold titulo-conten text-center mb-0">Tu cuenta en un toque</h1>

			<form action="{{ route('continuar-juridica') }}" method="post">
				<div class="row mx-0 justify-content-center mt-5">
					<div class="col-sm-8 col-md-6 col-lg-7 col-xl-5 mb-3 px-sm-1">

						@csrf

						<input type="hidden" name="nombre_juridico" id="nombre_juridico">
						<input type="hidden" name="direccion_juridico" id="direccion_juridico">
						
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">RUC</label>
							<input type="number" name="ruc" id="ruc" onkeypress="return check(event)" maxlength="11" class="input-login PoppinsRegular">
						</div>
						<button class="btn btn-login" type="button" id="validar-ruc" style="margin-top: 10px;">
							Validar RUC
						</button>

						<div class="col-12 mb-3 mt-3 px-sm-1" id="div-actividad" style="display: none;">
							<div class="div-form-login">
								<label class="label-login PoppinsRegular">Actividad Económica</label>
								<select name="id_actividad_economica" id="id_actividad_economica" required class="select-fomr">
									<option value="" disabled selected>Debe seleccionar una Opción</option>
									<option value="0">Otros</option>
									@if(count($actividad_economica)>0)
										@foreach($actividad_economica as $actividad)
											<option value="{{$actividad->id}}" @if(old('id_actividad_economica')==$actividad->id) selected @endif>{{$actividad->nombre}}</option>
										@endforeach
									@endif
								</select>
								<i class="fa fa-angle-down icon-select"></i>
							</div>
						</div>
						<div class="col-12 mb-3 mt-3 px-sm-1" id="div-actividad-otros" style="display: none;">
							<div class="div-form-login">
								<label class="label-login PoppinsRegular">Nombre de la Actividad Económica</label>
								<input type="text" name="actividad_economica" value="{{old('actividad_economica')}}" id="actividad_economica" class="input-login PoppinsRegular">
							</div>
						</div>

						<div class="px-sm-4 mt-3">
							<p class="PoppinsBold p-azul-nombre-empresa my-0 ocultar" id="nombre" style="display: none !important;"></p>
							<p class="PoppinsBold p-azul-nombre-empresa mb-0 mt-1 ocultar" id="direccion" style="display: none !important;"></p>
							<div class="d-flex justify-content-between mt-4 align-items-center ocultar" style="display:none !important;">
								<p class="PoppinsRegular p-azul-peque-radio mb-0">Soy yo el representante legal</p>
								<div class="d-flex justify-content-between">
									<div class="d-flex align-items-center me-2">
										<input type="radio" name="representante" class="representante" value="1" required>
										<label class="PoppinsRegular label-radio ms-1">SI</label>
									</div>
									<div class="d-flex align-items-center">
										<input type="radio" name="representante" class="representante" value="0" required>
										<label class="PoppinsRegular label-radio ms-1">NO</label>
									</div>
								</div>
							</div>
						</div>
						
						<div class="mt-3" id="div-form-representante-legal" style="display: none;">

							<div class="row">
							<h1 class="PoppinsBold subtitulo-conten text-center mb-4">Datos del Representante Legal</h1>
								<div class="col-6 mb-3">
									<div class="div-form-login">
										<label class="label-login PoppinsRegular">Tipo de Documento</label>
										<select class="select-fomr" name="tipo_doc_repre" id="tipo_doc" required>
											<option value="" disabled selected>Debe seleccionar una Opción</option>
											<option value="1" @if(old('tipo_doc_repre')==1) selected @endif>DNI</option>
											<option value="3" @if(old('tipo_doc_repre')==3) selected @endif>CE</option>
											<option value="4" @if(old('tipo_doc_repre')==4) selected @endif>PTP</option>
											<option value="2" @if(old('tipo_doc_repre')==2) selected @endif>Pasaporte</option>
										</select>
										<i class="fa fa-angle-down icon-select"></i>
									</div>
								</div>
								<div class="col-6 mb-3">
									<div class="div-form-login">
										<label class="label-login PoppinsRegular">Número de Documento</label>
										<input type="number" name="nro_doc_repre" value="{{old('nro_doc_repre')}}" id="nro_doc" class="input-login PoppinsRegular">
									</div>
								</div>
								<div class="col-12 mb-3">
									<div class="div-form-login">
										<label class="label-login PoppinsRegular">Nombre del representante legal</label>
										<input type="text" name="nombres_repre" value="{{old('nombres_repre')}}" id="nombres" class="input-login PoppinsRegular">
									</div>
								</div>
								<div class="col-6 mb-3">
									<div class="div-form-login">
										<label class="label-login PoppinsRegular">Apellido Paterno</label>
										<input type="text" name="apellido_paterno_repre" value="{{old('apellido_paterno_repre')}}" id="apellido_paterno"  class="input-login PoppinsRegular">
									</div>
								</div>
								<div class="col-6 mb-3">
									<div class="div-form-login">
										<label class="label-login PoppinsRegular">Apellido Materno</label>
										<input type="text" name="apellido_materno_repre" value="{{old('apellido_materno_repre')}}" id="apellido_materno" class="input-login PoppinsRegular">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 text-center ocultar" style="display: none !important;" >
						<button type="submit" id="boton-enviar" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural">¡Continuamos!</button>
					</div>
			</div>
		</form>

		</div>
	</div>
@endsection
@section('scripts')
	<script>
		$( document ).ready(function() {

			$("#id_actividad_economica").change(function () {
				var valor=$(this).val();

				if(valor==0)
				{
					$("#div-actividad-otros").fadeIn();
					$("#actividad_economica").attr('required','required');
				}
				else
				{
					$("#div-actividad-otros").fadeOut();
					$("#actividad_economica").removeAttr('required');
				}

			});




			$("#validar-ruc").click(function () {
				var valor = $("#ruc").val();
				if(valor.length>8 && valor.length<=11)
				{
					toastr.warning("Buscando RUC!");
					$.ajax({
						url: "{{ route('validar-RUC') }}?ruc=" + valor,
						method: 'GET',
						success: function (data) {
							console.log('test',data);
							if(data.estado==1)
							{
								$("#boton-enviar").removeAttr('disabled');
								if(data.resultado.success){
									if(data.resultado.data.status=="ACTIVO")
									{
										$("#nombre").html(data.resultado.data.name);
										$("#nombre_juridico").val(data.resultado.data.name);
										$("#direccion").html(data.resultado.data.address);
										$("#direccion_juridico").val(data.resultado.data.address);
										$("#div-actividad").fadeIn();
										$(".ocultar").css("display", "block");
										$("#validar-ruc").css("display", "none");
										$("#ruc").attr("readonly", "readonly");
										toastr.success("RUC Encontrado!");
									}
									else
									{
										toastr.warning("Este Ruc no esta activo en la SUNAT!");

									}
								}else{
									toastr.warning("El RUC insertado no existe!");
								}
							}
							else
							{
								toastr.warning("El RUC insertado ya se encuentra registrado!");
								$("#boton-enviar").attr('disabled','disabled');
							}
						},
						error:function (error) {
							console.log(error);
							toastr.error(error.message);
						}
					});
				}
				else
				{
					toastr.warning("El RUC debe ser de maximo 11 digitos!");
				}
			});
			$(".representante").change(function () {
				var valor=$(this).val();
				if(valor==0)
				{
					$("#tipo_doc").attr('required','required');
					$("#nro_doc").attr('required','required');
					$("#nombres").attr('required','required');
					$("#apellido_paterno").attr('required','required');
					$("#apellido_materno").attr('required','required');
					$("#div-form-representante-legal").fadeIn();
				}
				else
				{
					$("#div-form-representante-legal").fadeOut();
					$("#tipo_doc").removeAttr('required');
					$("#nro_doc").removeAttr('required');
					$("#nombres").removeAttr('required');
					$("#apellido_paterno").removeAttr('required');
					$("#apellido_materno").removeAttr('required');

					$("#tipo_doc").val('');
					$("#nro_doc").val('');
					$("#nombres").val('');
					$("#apellido_paterno").val('');
					$("#apellido_materno").val('');
				}
			});
			$("#tipo_doc").change(function () {
				var valor=$(this).val();
				if(valor==1)
				{
					// es DNI
					$("#nro_doc").attr('maxlength','8');
					$("#div-fecha-nacimiento").fadeOut();
					$("#fecha-nacimiento").removeAttr('required');
					$("#div-nacionalidad").fadeOut();
					$("#select-nacionalidad").removeAttr('required');

					// aplicamos la api del DNI
					if($("#nro_doc").val().length<=8 && $("#nro_doc").val().length>=6)
					{
						toastr.warning("Buscando DNI!");

						$("#div-contenedor-padre").css('cursor','wait');
						$.ajax({
							url: "{{ route('validar-DNI') }}?dni=" + $("#nro_doc").val(),
							method: 'GET',
							success: function (data) {
								$("#div-contenedor-padre").css('cursor','default');
								if(data.resultado.success){
									var nombre_completo=data.resultado.data.fullname;
									var partes_nombre_completo=nombre_completo.split(',');
									var partes_apellidos=partes_nombre_completo[0].split(' ');
									$("#nombres").val(partes_nombre_completo[1]);
									$("#apellido_paterno").val(partes_apellidos[0]);
									if(partes_apellidos.length>=1)
									{
										$("#apellido_materno").val(partes_apellidos[1]);
									}
									toastr.success("DNI Encontrado!");
									$("#boton-enviar").removeAttr('disabled');
								}else{
									toastr.warning("DNI no Encontrado!");
									$("#boton-enviar").attr('disabled','disabled');
									$("#nombres").val('');
									$("#apellido_paterno").val('');
									$("#apellido_materno").val('');
								}
							},
							error:function (error) {
								$("#div-contenedor-padre").css('cursor','default');
								console.log(error);
								toastr.error(error.message);
							}
						});
					}

				}
				else if(valor==2)
				{
					// es pasaporte
					$("#nro_doc").removeAttr('maxlength');
					$("#div-nacionalidad").fadeIn();
					$("#select-nacionalidad").attr('required','required');
					$("#div-fecha-nacimiento").fadeOut();
					$("#fecha-nacimiento").removeAttr('required');
					$("#boton-enviar").removeAttr('disabled');
				}
				else if(valor==3 || valor==4)
				{
					$("#nro_doc").removeAttr('maxlength');
					$("#div-fecha-nacimiento").fadeIn();
					$("#fecha-nacimiento").attr('required','required');
					$("#div-nacionalidad").fadeOut();
					$("#select-nacionalidad").removeAttr('required');
					$("#boton-enviar").removeAttr('disabled');
				}
				else
				{
					$("#nro_doc").removeAttr('maxlength');
					$("#div-fecha-nacimiento").fadeOut();
					$("#fecha-nacimiento").removeAttr('required');
					$("#div-nacionalidad").fadeOut();
					$("#select-nacionalidad").removeAttr('required');
					$("#boton-enviar").removeAttr('disabled');
				}
			});


			$("#nro_doc").focusout(function() {
				if($("#tipo_doc").val()==1)
				{
					// es DNI
					// aplicamos la api del DNI
					if($("#nro_doc").val().length<=8 && $("#nro_doc").val().length>=6)
					{
						toastr.warning("Buscando DNI!");

						$("#div-contenedor-padre").css('cursor','wait');
						$.ajax({
							url: "{{ route('validar-DNI') }}?dni=" + $("#nro_doc").val(),
							method: 'GET',
							success: function (data) {
								console.log('test',data);
								$("#div-contenedor-padre").css('cursor','default');
								if(data.resultado.success){
									var nombre_completo=data.resultado.data.fullname;
									var partes_nombre_completo=nombre_completo.split(',');
									var partes_apellidos=partes_nombre_completo[0].split(' ');
									$("#nombres").val(partes_nombre_completo[1]);
									$("#apellido_paterno").val(partes_apellidos[0]);
									if(partes_apellidos.length>=1)
									{
										$("#apellido_materno").val(partes_apellidos[1]);
									}
									toastr.success("DNI Encontrado!");
									$("#boton-enviar").removeAttr('disabled');
								}else{
									toastr.warning("DNI no Encontrado!");
									$("#boton-enviar").attr('disabled','disabled');
									$("#nombres").val('');
									$("#apellido_paterno").val('');
									$("#apellido_materno").val('');
								}
							},
							error:function (error) {
								$("#div-contenedor-padre").css('cursor','default');
								console.log(error);
								toastr.error(error.message);
							}
						});
					}
					else
					{
						toastr.warning("El DNI debe ser de 8 digitos maximo!");
					}


				}
			});
		});
		function check(e) {
			tecla = (document.all) ? e.keyCode : e.which;

			//Tecla de retroceso para borrar, siempre la permite
			if (tecla == 8) {
				return true;
			}

			// Patrón de entrada

			patron = /[*0-9]/;

			tecla_final = String.fromCharCode(tecla);
			return patron.test(tecla_final);
		}
	</script>
@endsection
