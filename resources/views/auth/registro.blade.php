@extends('layouts.index_app')
@section('content')
	<div class="conten_panel mt-3 mt-lg-0">
		<div class="div-conten">
			<h1 class="PoppinsBold titulo-conten text-center mb-0">Tu cuenta en un toque</h1>
			<form action="{{route('registro-save')}}" method="post">
				<div class="row mx-0 justify-content-center mt-5">
					<div class="col-sm-6 col-xl-5 mb-3 px-sm-1">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Correo electrónico</label>
							<input type="email" name="email" class="input-login PoppinsRegular" value="{{old('email')}}" required>
						</div>
					</div>
					<div class="col-sm-6 col-xl-5 mb-3 px-sm-1">
						<div class="div-form-login">
							<input type="tel" name="nro_celular" onkeypress="return check(event)" class="input-login PoppinsRegular phone" value="{{old('nro_celular')}}" minlength="6" maxlength="15" required>
						</div>
					</div>
					<div class="col-sm-6 col-xl-5 mb-3 px-sm-1 text-center">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Contraseña</label>
							<input type="password" name="password" min="8" max="16" pattern="^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$" id="pass1" class="input-login PoppinsRegular" required>
							<a id="icon-mostrar" class="button-eye" onclick="mostrarPass();"><i id="eye" class="fa fa-eye"></i></a>
						</div>
					</div>
					<div class="col-sm-6 col-xl-5 mb-3 px-sm-1">
						<div class="div-form-login">
							<label class="label-login PoppinsRegular">Repite tu contraseña</label>
							<input type="password" name="password_confirm" min="8" max="16" pattern="^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$" id="pass2" class="input-login PoppinsRegular" required>
							<a id="icon-mostrar2" class="button-eye" onclick="mostrarPass2();"><i id="eye2" class="fa fa-eye"></i></a>
						</div>
					</div>
					<div class="col-12 col-lg-8 mb-3 text-center">
						<p class="text-start PoppinsRegular" style="font-size: 14px;color: #137188;">La contraseña debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.</p>
						<button type="button" class="btn btn-primary PoppinsMedium px-5" style="border-radius: 12px;" onclick="generarPass()">Generar Contraseña</button>
					</div>
					<div class="col-10 col-sm-6 col-md-5 text-center mt-4 px-0">
						<div class="text-center">
							<input class="form-check-input" type="checkbox" style="margin-top: 3px;" value="" id="flexCheckDefault" required>
							<label class="form-check-label p-login-contrasena" for="flexCheckDefault">
								Acepto los <a href="">Términos y Condiciones</a>
							</label>
						</div>
					</div>
					<div class="col-12 text-center">
						<button type="submit" class="btn btn-login PoppinsMedium px-5 btn-top-natural">¡Continuamos!</button>
					</div>
				</div>
				@csrf
			</form>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		var generarPass = () => {
			var characters = "0123456789abcdefghijkyzABCDEFGHIJKLZ";

			var pass = "";
			for (var i=0; i < 10; i++){
				pass += characters.charAt(Math.floor(Math.random()*characters.length));
			}
			// Tipo
			patron = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;

			// validamos que la contraseña generada cumpla con nuestra expresion regular
			if(patron.test(pass)==true)
			{
				$("#pass1").val(pass);
				$("#pass2").val(pass);

				$("#pass1").attr('type','text');
				estado = 1;
				$("#eye").removeClass("fa-eye");
				$("#eye").addClass("fa-eye-slash");

				$("#pass2").attr('type','text');
				estado2 = 1;
				$("#eye2").removeClass("fa-eye");
				$("#eye2").addClass("fa-eye-slash");
			}
			else
			{
				generarPass();
			}
		};

		var estado=0, estado2=0;

		var mostrarPass = () => {
			if(estado==0)
			{
				estado=1;
				$("#pass1").attr('type','text');
				$("#eye").removeClass("fa-eye");
				$("#eye").addClass("fa-eye-slash");
			}
			else
			{
				estado=0;
				$("#eye").addClass("fa-eye");
				$("#eye").removeClass("fa-eye-slash");
				$("#pass1").attr('type','password');

			}
		};

		var mostrarPass2 = () => {
			if(estado2==0)
			{
				estado2=1;
				$("#pass2").attr('type','text');
				$("#eye2").removeClass("fa-eye");
				$("#eye2").addClass("fa-eye-slash");
			}
			else
			{
				estado2=0;
				$("#eye2").addClass("fa-eye");
				$("#eye2").removeClass("fa-eye-slash");
				$("#pass2").attr('type','password');

			}
		};

		$(document).ready(function() {
			var input = document.querySelector(".phone");
			window.intlTelInput(input, {
				autoHideDialCode: false,
				nationalMode: false,
				preferredCountries: ['pe'],
				utilsScript: "/plugin-telf/js/utils.js",
			});
		});
		function check(e) {
			tecla = (document.all) ? e.keyCode : e.which;

			//Tecla de retroceso para borrar, siempre la permite
			if (tecla == 8) {
				return true;
			}

			// Patrón de entrada


			// Tipo nro
			patron = /[*0-9+]/;


			tecla_final = String.fromCharCode(tecla);
			return patron.test(tecla_final);
		}

	</script>
@endsection