<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comision extends Model
{
    use HasFactory;

    protected $table = 'comisiones';
    protected $fillable = [
        'id_perfil_padre',
        'id_perfil_hijo',
        'monto',
        'id_operacion',
    ];

    public function perfilPadre()
    {
        return $this->hasOne(Perfil::class, 'id', 'id_perfil_padre');
    }

    // Es quien le da la comision al perfil PADRE
    public function perfilHijo()
    {
        return $this->hasOne(Perfil::class, 'id', 'id_perfil_hijo');
    }

    public function operacion()
    {
        return $this->hasOne(Operacion::class, 'id', 'id_operacion');
    }

}
