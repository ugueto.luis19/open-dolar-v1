@extends('layouts.index_app')

@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Mis alertas</h1>
        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right" style="position: relative;">
            <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                <div class="col-sm-7 col-md-6 col-lg-5 text-center text-sm-start">
                    <h5 class="arlrdbd my-0">Me llegará un correo cuando</h5>
                </div>
                <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                    <button class="btn btn-login PoppinsMedium" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar alerta</button>
                </div>
            </div>

            <!-- alert hecho en columnas -->
            <div class="row mx-0 mt-5">
                @foreach($alertas as $alert)
                    <?php
                        $tipo = ''; $condicional = '';

                        switch($alert->tipo){
                            case '1':
                                $tipo = 'compra';
                            break;
                            case '2':
                                $tipo = 'venta';
                            break;
                        }

                        switch($alert->condicional){
                            case '1':
                                $condicional = 'Menor o Igual';
                            break;
                            case '2':
                                $condicional = 'Igual';
                            break;
                            case '3':
                                $condicional = 'Mayor o Igual';
                            break;
                        }

                    ?>
                        <div class="col-sm-6 col-md-4 px-0 px-sm-2 px-lg-4 mb-5">
                            <div class="div-notificacion text-center px-3">
                                <img src="/assets-web/img/icon-camapana.png" class="img-campana">
                                <p class="PoppinsRegular my-0 p-alerts">El valor de {{ $tipo }} del dólar es {{ $condicional }} a S/. {{ $alert->valor_deseado }}</p>
                                <button type="button" onclick="modalEdit('{{ $alert->id }}', '{{ $alert->tipo }}', '{{ $alert->condicional }}', '{{ $alert->valor_deseado }}')" class="btn btn-edit-alert">
                                    <i class="fa-solid fa-pencil"></i>
                                </button>
                                <button type="button" onclick="BorrarAlerta(this, '{{ $alert->id }}', '{{route("eliminarAlerta",["id"=>$alert->id])}}');" class="btn btn-close-alert">
                                    <i class="fa-solid fa-trash-can"></i>
                                </button>
                            </div>
                        </div>
                @endforeach
            </div>

        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body px-0 py-0" style="position: relative;">
                    <img src="assets-web/img/mano-reloj.png" class="img-mano-absolute">
                    <div class="modal-padding-left-right-alert pt-5 pb-5">
                        <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Configurar nueva <span class="ms-3">alerta</span></h1>
                        <p class="PoppinsRegular p-login-contrasena text-center my-0">Quiero recibir un correo electrónico cuando:</p>

                        <form method="post" action="{{ route('registrar-alerta') }}">
                            @csrf
                            <div class="row mx-0 justify-content-start mt-5">
                                <div class="col-12 mb-3 px-lg-5">
                                    <div class="div-form-login">
                                        <select class="select-fomr" name="tipo" required>
                                            <option value="" disabled selected>Seleccione una Opción</option>
                                            <option value="1">Compra</option>
                                            <option value="2">Venta</option>
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-12 mb-3 px-lg-5">
                                    <div class="div-form-login">
                                        <select class="select-fomr" name="condicional" required>
                                            <option value="" disabled selected>Seleccione una Opción</option>
                                            <option value="1">Menor o Igual</option>
                                            <option value="2">Igual</option>
                                            <option value="3">Mayor o Igual</option>
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-12 px-lg-5">
                                    <div class="div-form-login">
                                        <input type="number" step="any" name="valor_deseado" class="input-login PoppinsRegular" placeholder="Ingrese el valor deseado">
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body px-0 py-0" style="position: relative;">
                    <img src="assets-web/img/mano-reloj.png" class="img-mano-absolute">
                    <div class="modal-padding-left-right-alert pt-5 pb-5">
                        <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Editar <span class="ms-3">alerta</span></h1>
                        <p class="PoppinsRegular p-login-contrasena text-center my-0">Quiero recibir un correo electrónico cuando:</p>

                        <form method="post" action="{{ route('editar-alerta') }}">
                            @csrf
                            <input type="hidden" name="id" id="id">
                            <div class="row mx-0 justify-content-start mt-5">
                                <div class="col-12 mb-3 px-lg-5">
                                    <div class="div-form-login">
                                        <select class="select-fomr" id="tipo" name="tipo" required>
                                            <option value="" disabled selected>Seleccione una Opción</option>
                                            <option value="1">Compra</option>
                                            <option value="2">Venta</option>
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-12 mb-3 px-lg-5">
                                    <div class="div-form-login">
                                        <select class="select-fomr" id="condicional" name="condicional" required>
                                            <option value="" disabled selected>Seleccione una Opción</option>
                                            <option value="1">Menor o Igual</option>
                                            <option value="2">Igual</option>
                                            <option value="3">Mayor o Igual</option>
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-12 px-lg-5">
                                    <div class="div-form-login">
                                        <input id="valor_deseado" type="number" step="any" name="valor_deseado" class="input-login PoppinsRegular" placeholder="Ingrese el valor deseado">
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Editar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var modalEdit = (id, tipo, condicional, valor) => {
            var myModal = new bootstrap.Modal(document.getElementById('modalEdit'));

            $("#id").val(id);
            $("#tipo").val(tipo);
            $('#tipo').trigger('change');
            $("#condicional").val(condicional);
            $('#condicional').trigger('change');
            $("#valor_deseado").val(valor);

            myModal.show();

        };

        var BorrarAlerta = (e, id, ruta) => {
            e.preventDefault;

            toastr.warning("<br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Si</button>",'Desea Eliminar la alerta?',
                {
                    closeButton: true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#confirmationRevertYes").click(function(){
                            window.location.href = ruta;
                        });
                    }
                });
        };

       
    </script>
@endsection