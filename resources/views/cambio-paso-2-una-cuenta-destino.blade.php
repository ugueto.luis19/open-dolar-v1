@extends('layouts.index_app')
@section('content')
    <!-- card de necesitas ayuda-->
    <div class="div-flotante">
        <img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 40%">
    </div>

    <div class="div-flotante-azul px-5 py-4">
        <button class="btn btn-close-ayuda">
            <i class="fa-solid fa-circle-xmark icon-close-ayuda"></i>
        </button>
        <h4 class="Arialregular titulo-ayuda"><img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 8%;">Necesitas ayuda?</h4>
        <h5 class="ArialBold mt-5 mb-2">Temas recurrentes</h5>
        <p class="Arialregular p-link-temas my-0"><a href="">Problemas con transacción</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Perfil y registro</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Privacidad y seguridad</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Cancelaciones y devoluciones</a></p>
        <h5 class="ArialBold mt-5 mb-2">Contacto</h5>
        <p class="Arialregular">
            Con el fin de ahorrar tu tiempo, recomendamos revisar las preguntas frecuentes. Si sigues sin encontrar respuesta, puedes contactarnos de la manera que mas te acomode:
        </p>
        <ul class="ps-0">
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-phone me-3"></i>
                (+51) 1 1234567
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-brands fa-whatsapp me-3"></i>
                998877665
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-envelope me-3"></i>
                confianza@opendolar.com
            </li>
            <li class="d-flex Arialregular align-items-start list-contacto">
                <i class="fa-solid fa-location-dot me-3"></i>
                Centro Empresarial El Trigal, Calle Antares 320, Of. 802-C, Torre B, Surco - Lima.
            </li>
        </ul>
        <div class="d-flex justify-content-start mt-4">
            <a href="" class="me-4">
                <i class="fa-solid fa-share-nodes icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-whatsapp icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-facebook-f icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-linkedin-in icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-instagram icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="">
                <i class="fa-solid fa-envelope icon-redes-verde" style="color: #fff !important;"></i>
            </a>
        </div>
    </div>

    <div class="conten_panel_administrador">
        <h1 class="titulo-general-alerta PoppinsBold my-0 text-center text-xl-start"><span>A Cambiar</span></h1>


        <div class="row mx-0 mt-4 justify-content-center">
            <div class="col-12">
                <a href="{{route('dashboard',["id"=>$operacion->id])}}" class="btn btn-volver-azul PoppinsRegular">< Volver</a>
            </div>

            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 px-0 px-md-4 px-lg-4 px-xl-3" style="position: relative;">
                <div class="div-check">
                    <div class="div-chec-radius">
                        <i class="fa-solid fa-circle-check check-activado"></i>
                        <i class="fa-solid fa-circle-check"></i>
                        <i class="fa-solid fa-circle-check"></i>
                    </div>
                </div>
                <form action="{{route('guardar-cambio2')}}" method="post" id="formularioProceso">
                    <input type="hidden" id="id" name="id" value="{{ $operacion->id }}">
                    <div class="card-blanco card-padding-top mt-4">
                        <h2 class="PoppinsMedium subtitulo-card text-center" id="titulo">Escoje las cuentas</h2>

                        <div class="row mx-0 mt-3 justify-content-center px-0" style="background: #FEF8DD !important; border-radius: 9px; color: #137188; padding: 10px; margin-left: 45px; margin-right: 45px;">
                            <div class="col-6 col-sm-4 d-flex justify-content-center col-padding-compra align-items-center col-border-right py-3 py-sm-0 px-2">
                                <p class="my-0 p-color-compra-venta text-center">
                                    <span class="PoppinsBold">
                                        Envío
                                    </span>
                                    <br>
                                    @if($operacion->uso_opendolar!=0)
                                        <b style="text-decoration: line-through; color: red; display: block;" id="old-tasa-venta">
                                            @if($operacion->tipo== 0)
                                                PEN
                                            @else
                                                USD
                                            @endif
                                            @if($operacion->tipo== 0)
                                                {{ $operacion->monto_enviado+$operacion->uso_opendolar }}
                                            @else
                                                {{ $operacion->tasa->tasa_compra+$operacion->tasa->pips_venta*$operacion->monto_recibido }}
                                            @endif
                                        </b>
                                    @endif
                                    <span class="PoppinsRegular">
                                        @if($operacion->tipo== 0)
                                            PEN
                                        @else
                                            USD
                                        @endif
                                            {{ $operacion->monto_enviado }}
                                    </span>
                                </p>
                            </div>
                            <div class="col-6 col-sm-4 d-flex justify-content-center col-padding-compra col-border-right col-border-cero-cero py-3 py-sm-0 px-3 align-items-center">
                                <p class="my-0 p-color-compra-venta text-center"><span class="PoppinsBold">Recibiré</span> <br> <span class="PoppinsRegular">@if($operacion->tipo== 0) USD @else PEN @endif{{ $operacion->monto_recibido }}</span> </p>
                            </div>
                            <div class="col-6 col-sm-4 d-flex justify-content-center col-padding-compra py-3 py-sm-0 px-2 align-items-center">
                                <p class="my-0 p-color-compra-venta text-center"><span class="PoppinsBold">Tipo de Cambio</span> <br>
                                    @if(is_object($operacion->cupon))
                                        <b style="text-decoration: line-through; color: red; display: block;" id="old-tasa-venta">
                                            @if($operacion->tipo== 0)
                                                {{number_format($operacion->tasa->tasa_venta+$operacion->tasa->pips_venta,3)}}
                                            @else
                                                {{number_format($operacion->tasa->tasa_compra+$operacion->tasa->pips_compra,3)}}
                                            @endif
                                        </b>
                                    @endif
                                    <span class="PoppinsRegular">
                                        @if($operacion->tipo == 0)
                                            @if(is_object($operacion->cupon))
                                                {{ number_format(($operacion->tasa->tasa_venta+$operacion->tasa->pips_venta),3)-number_format(($operacion->cupon->valor_soles),3) }}
                                            @else
                                                {{number_format(($operacion->tasa->tasa_venta+$operacion->tasa->pips_venta),3)}}
                                            @endif
                                        @else
                                            @if(is_object($operacion->cupon))
                                                {{ number_format(($operacion->tasa->tasa_compra+$operacion->tasa->pips_compra),3)+number_format(($operacion->cupon->valor_soles/$operacion->tasa->tasa_compra),3) }}
                                            @else
                                                {{number_format(($operacion->tasa->tasa_compra+$operacion->tasa->pips_compra),2)}}
                                            @endif
                                        @endif
                                    </span>
                                </p>
                            </div>
                        </div>


                        <div class="col-12 mb-2 mt-4">
                            <div class="div-form-login">
                                <label><b style="color: #137188;">Escoge tu cuenta de origen</b></label>
                                @if($operacion->tipo==0)
                                    <div class="mt-2">
                                        <select class="select-fomr" id="cuenta_origen" name="id_cuenta_origen" required>
                                            <option value="" selected>Cuenta de Origen</option>
                                            @if(count($cuentas_bancarias)>0)
                                                @foreach($cuentas_bancarias as $cuenta)
                                                    @if($cuenta->moneda->nombre == 'Soles')
                                                        <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <i class="fa fa-angle-down icon-select" style="margin-top: 20px;"></i>
                                    </div>
                                @else
                                    <div class="mt-2">
                                        <select class="select-fomr" id="cuenta_origen" name="id_cuenta_origen" required>
                                            <option value="" selected>Cuenta de Origen</option>
                                            @if(count($cuentas_bancarias)>0)
                                                @foreach($cuentas_bancarias as $cuenta)
                                                    @if($cuenta->moneda->nombre == 'Dolares')
                                                        <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <i class="fa fa-angle-down icon-select" style="margin-top: 20px;"></i>
                                    </div>
                                @endif
                            </div>

                            <div class="col-12 mb-2 mt-2">
                                <div class="div-form-login">
                                    <label><b style="color: #137188;">Escoge tu cuenta de destino</b></label>
                                    @if($operacion->tipo== 0)
                                        <div class="mt-2">
                                            <select class="select-fomr" id="cuenta_destino" name="id_cuenta_destino" required>
                                                <option value="" selected>Cuenta de Destino</option>
                                                @if(count($cuentas_bancarias)>0)
                                                    @foreach($cuentas_bancarias as $cuenta)
                                                        @if($cuenta->moneda->nombre == 'Dolares')
                                                            <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select" style="top: 55px;"></i>
                                        </div>
                                    @else
                                        <div class="mt-3">
                                            <select class="select-fomr" id="cuenta_destino" name="id_cuenta_destino" required>
                                                <option value="" selected>Cuenta de Destino</option>
                                                @if(count($cuentas_bancarias)>0)
                                                    @foreach($cuentas_bancarias as $cuenta)
                                                        @if($cuenta->moneda->nombre == 'Soles')
                                                            <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select" style="top: 55px;"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-12 mb-2 mt-3">
                                <div class="div-form-login d-flex align-items-start pe-3">
                                    <input type="checkbox" id="declaro-checkbox" required class="mt-1"> <p  class="PoppinsRegular p-color-compra-venta ms-2 p-chec-declaro">Declaro que transferiré los fondos de una cuenta de origen propia o mancomunada y no haré uso en cuentas no autorizadas</p>
                                </div>
                            </div>

                            <div class="row mx-0 mt-1" >
                                <div class="col-12 text-center">
                                    <button id="boton-procesar" class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-0" type="button" style="color: #137188 !important;">Procesar</button><br>
                                </div>
                            </div>
                        </div>
                    @csrf
                </form>

            </div>
        </div>

        <div class="row mx-0 mt-4 justify-content-center px-0">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 px-0 px-md-4 px-lg-4 px-xl-3 py-3 text-center text-sm-start" >
                <div class="py-2" style="border: 1px solid #FF0000; border-radius: 5px;background-color: #FFC6C6;position: relative;">
                    <i class="fa-solid fa-map-location-dot icon-location-azul"></i>
                    <p class="PoppinsRegular p-azul-mensaje mb-0 mt-3 mt-sm-0">Si tu cuenta de destino es de provincia y distinta a BCP, el envío de dinero estará sujeto a cobro de comisión, Te sugerimos ingresar una cuenta BCP para recibir dinero a nivel nacional.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-top" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content" style="height: 700px;">
                <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body px-0 py-0" style="position: relative; background-color: #f9f9f9; border-radius:25px;">
                    <div class="row mx-0 justify-content-center mt-5">
                        <div class="col-sm-10 col-md-9 col-xl-9 px-1 mb-5" >
                            <h4 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center ">Antes de transferir:</h4>
                            <ul style="list-style: none;" class="mt-5 ps-0">
                                <li class="d-flex align-items-start PoppinsRegular list-modal-azul">
                                    <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                    Solo aceptamos transferencias bancarias y no depósitos en efectivo.
                                </li>
                                <li class="d-flex align-items-start PoppinsRegular list-modal-azul mt-3">
                                    <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                    Aceptamos transferencias desde banca por internet o desde la app de tu banco a nivel nacional.
                                </li>
                                <li class="d-flex align-items-start PoppinsRegular list-modal-azul mt-3">
                                    <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                    Si realizas una transferencia desde la ventanilla de un banco en provincia, aceptas asumir el costo de la comisión que el banco genere por la operación, la cual se descontada en tu orden de cambio.
                                </li>
                                <li class="d-flex align-items-start PoppinsRegular list-modal-azul mt-3">
                                    <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                    Si el origen de la transferencia es de otro banco distinto a BCP o Interbank tienes que considerar el tiempo que toma tu banco en hacer el envío interbancario. Una vez ingrese el dinero a nuestras cuentas, recién se empezara a procesar tu orden.
                                </li>
                                <li class="d-flex align-items-start PoppinsRegular list-modal-azul mt-3">
                                    <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                    Verifica con tu banco si tus cuentas generan comisiones adicionales por transferencias.
                                </li>
                                <li class="d-flex align-items-start PoppinsRegular list-modal-azul mt-3">
                                    <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                    Si la cuenta de destino no es de Lima y distinta a BCP, las transferencias de dinero estará sujeto al cobro de comisión que deberás asumir. Te sugerimos que si tuenes dudas nos para poder ayudarte a modificar la orden por una cuenta de destino en BCP.
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 text-center">
                            <button id="processModal" type="button" class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-5" style="color: #137188 !important;" >
                                <i class="fa-solid fa-circle-check"></i> Entendido</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
@section('scripts')
    <script>
        /*$("#volver").click(function(){
            window.history.back();
        });*/

        $( document ).ready(function() {
            $("#boton-procesar").click(function () {

                if( $('#cuenta_origen').val()!=0 && $('#cuenta_origen').val()!=null) {
                    if( $('#cuenta_destino').val()!=0 && $('#cuenta_destino').val()!=null) {
                        if( $('#declaro-checkbox').prop('checked') ) {
                            $("#exampleModal").modal('show');
                        }
                        else
                        {
                            toastr.warning("Debe aceptar los terminos!");
                        }
                    }
                    else
                    {
                        toastr.warning("Debe seleccionar una cuenta destino!");
                    }
                }
                else
                {
                    toastr.warning("Debe seleccionar una cuenta origen!");
                }


            });

            $("#processModal").click((e)=>{
                e.preventDefault;

                if( $('#cuenta_origen').val()!=0 && $('#cuenta_origen').val()!=null) {
                    if( $('#cuenta_destino').val()!=0 && $('#cuenta_destino').val()!=null) {
                        if( $('#declaro-checkbox').prop('checked') ) {
                            $("#formularioProceso").submit();
                        }
                        else
                        {
                            toastr.warning("Debe aceptar los terminos!");
                        }
                    }
                    else
                    {
                        toastr.warning("Debe seleccionar una cuenta destino!");
                    }
                }
                else
                {
                    toastr.warning("Debe seleccionar una cuenta origen!");
                }
            });
        });
    </script>
@endsection