<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    use HasFactory;
    protected $table = 'perfiles';
    protected $fillable = [
        'nro_celular',
        'nombres',
        'apellido_paterno',
        'apellido_materno',
        'tipo_doc',
        'nro_doc',
        'id_ocupacion',
        'publicamente_expuesto',
        'codigo_referido',
        'codigo_asignado',
        'id_institucion',
        'nombre_institucion',
        'id_cuenta',
        'tipo',
        'ruc',
        'nombre_juridico',
        'direccion_juridico',
        'imagen_perfil',
        'fecha_nacimiento',
        'id_nacionalidad',
        'representante',
        'tipo_doc_repre',
        'nro_doc_repre',
        'nombres_repre',
        'apellido_paterno_repre',
        'apellido_materno_repre',
        'id_actividad_economica',
        'id_cargo',
        'actividad_economica'
    ];

    public function ultima_verificacion()
    {
        $verificacion= Verificacion::where('id_perfil',$this->id)->orderBy('id','desc')->first();

        return $verificacion;
    }

    public function cuentasBancarias()
    {
        return $this->hasMany(CuentaBancaria::class, 'id_perfil', 'id');
    }

    public function alerta()
    {
        return $this->hasOne(AlertaProgramada::class, 'id_perfil', 'id');
    }

    public function nacionalidad()
    {
        return $this->hasOne(Pais::class, 'id', 'id_nacionalidad');
    }

    public function usuario()
    {
        return $this->hasOne(User::class, 'id', 'id_cuenta');
    }

    public function institucion()
    {
        return $this->hasOne(Institucion::class, 'id', 'id_institucion');
    }
    public function cargo()
    {
        return $this->hasOne(Cargo::class, 'id', 'id_cargo');
    }
    public function ocupacion()
    {
        return $this->hasOne(Ocupacion::class, 'id', 'id_ocupacion');
    }

    public function perfilPadre($cod)
    {
        return Perfil::where('codigo_asignado',$cod)->first();
    }

    public function totalOpenCredit()
    {
        $comisiones= Comision::where('id_perfil_padre',$this->id)->get();

        $open_credit=0;

        if(count($comisiones)>0)
        {
            foreach ($comisiones as $comision)
            {
                $open_credit+=$comision->monto;
            }
        }

        // Todas las operaciones que debitan los opencreditos usados segun sus estados son:
        // 1 En Proceso
        // 2 Realizado

        $todas_mis_operaciones=Operacion::where('id_perfil', $this->id)
            ->whereIn('estado', ['1', '2'])
            ->get();

        if($todas_mis_operaciones->count() > 0)
        {
            foreach ($todas_mis_operaciones as $operacion)
            {
                if($operacion->uso_opendolar>0)
                {
                    $open_credit-=$operacion->uso_opendolar;
                }
            }
        }

        return $open_credit;
    }

    public function informacionCuenta($id)
    {
        $cuenta_bancaria=CuentaBancaria::find($id);

        return $cuenta_bancaria;
    }

    public function actividad_economica_obj()
    {
        return $this->hasOne(ActividadEconomica::class, 'id', 'id_actividad_economica');
    }

}
