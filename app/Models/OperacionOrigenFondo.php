<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperacionOrigenFondo extends Model
{
    use HasFactory;
    protected $table = 'operacion_origen_fondos';
    protected $fillable = [
        'id_operacion',
        'origen',
        'descripcion'
    ];

    public function operacion()
    {
        return $this->hasOne(Tasa::class, 'id', 'id_operacion');
    }

}
