@extends('layouts.index_app')
@section('content')
    <style>
        /*CSS pagination tamaño de flechas*/
        span svg {
            width: 35px;
        }
    </style>
    <div class="conten_panel_administrador">
        <div class="row">
            <div class="col-12">
                <h1 class="titulo-general-admi PoppinsBold text-center text-xl-start" style="margin-bottom: 50px;">Mis cuentas bancarias</h1>
            </div>
            <div class="col-12 col-sm-3 text-end px-0">
                <input style="margin-left: 23px;" type="text" name="buscador" autocomplete="off" class="form-control input-operacioes PoppinsRegular" value="" id="buscar" placeholder="Buscador" required="">
            </div>

            <div class="col-12 col-sm-6 text-end px-0" >
                <button class="btn btn-login PoppinsMedium mt-5 mt-xl-0" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar cuenta</button>

                @if(count($cuentas_bancarias)>0)
                    <button style="margin-right: 22px;" class="btn btn-delete PoppinsMedium mt-5 mt-xl-0" id="eliminar-cuentas"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar cuentas</button>
                @endif

            </div>
            <div class="col-12 col-sm-3 text-end px-0">

            </div>
        </div>

        <div id="div-contenido-buscador">
            @if(count($cuentas_bancarias)>0)
                @foreach($cuentas_bancarias as $cuenta_bancaria)
                    <div class="row">
                        <div class="col-md-8 col-lg-9 col-xl-9">
                            <div class="card card-c" style="margin: 11px 11px 11px 11px;padding: 0;border-radius: 11px;">
                                <div class="card-body" style="padding: 1rem 1rem !important;">
                                    <div class="row align-items-center no-gutters">
                                        <div class="col text-center">
                                            <input type="checkbox" class="eliminar-cuentas" name="eliminar_cuentas[]" value="{{$cuenta_bancaria->id}}">
                                        </div>
                                        <div class="col-xl-2" style="text-align: center;">
                                        @if($cuenta_bancaria->banco->logo==null || $cuenta_bancaria->banco->logo=='')
                                            <span class="span-circulo-destino"></span>
                                        @else
                                            {!!$cuenta_bancaria->banco->logo!!}
                                        @endif
                                        </div>
                                        <div class="col-xl-2">
                                            <div class="fuente6-card"><span>Moneda</span></div><span class="contenido-cuentas">{{$cuenta_bancaria->moneda->nombre}}</span>
                                        </div>
                                        <div class="col-xl-2">
                                            <div class="fuente6-card"><span>Tipo&nbsp;</span></div><span class="contenido-cuentas">{{$cuenta_bancaria->tipo->nombre}}</span>
                                        </div>
                                        <div class="col-xl-3">
                                            <div class="fuente6-card"><span>Nº de cuenta&nbsp;</span ></div><span class="contenido-cuentas">{{$cuenta_bancaria->nro_cuenta}}</span>
                                        </div>
                                        <div class="col-xl-2">
                                            <div class="fuente6-card"><span>Titular&nbsp;</span></div><span class="contenido-cuentas">{{$cuenta_bancaria->titular}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="mt-3">
                {{ $cuentas_bancarias->links() }}
            </div>
        </div>

    </div>



    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body px-0 py-0">
                    <form action="{{route('registrar-cuenta')}}" method="post">
                        <div class="div-modal-padding-left-right pt-5">
                            <h1 class="PoppinsBold titulo-conten mb-0 mt-5 text-center">Registrar Cuenta Bancaria</h1>
                            <div class="row mx-0 justify-content-start mt-5">
                                <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                    <div class="div-form-login">
                                        <label class="label-login PoppinsRegular">Moneda</label>
                                        <select class="select-fomr" name="id_moneda" required>
                                            <option value="" selected>Debe seleccionar una Opción</option>
                                            @if(count($monedas)>0)
                                                @foreach($monedas as $moneda)
                                                    <option value="{{$moneda->id}}">{{$moneda->nombre}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                    <div class="div-form-login">
                                        <label class="label-login PoppinsRegular">Nombre del banco</label>
                                        <select class="select-fomr" name="id_banco" required>
                                            <option value="" selected>Debe seleccionar una Opción</option>
                                            @if(count($bancos)>0)
                                                @foreach($bancos as $banco)
                                                    <option value="{{$banco->id}}">{{$banco->nombre}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                    <div class="div-form-login">
                                        <label class="label-login PoppinsRegular">Tipo de cuenta</label>
                                        <select class="select-fomr" name="id_tipo_cuenta" required>
                                            <option value="" selected>Debe seleccionar una Opción</option>
                                            @if(count($tipos_cuentas)>0)
                                                @foreach($tipos_cuentas as $tipo_cuenta)
                                                    <option value="{{$tipo_cuenta->id}}">{{$tipo_cuenta->nombre}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                    <div class="div-form-login">
                                        <label class="label-login PoppinsRegular">Numero de cuenta</label>
                                        <input type="number" name="nro_cuenta" class="input-login PoppinsRegular" required>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                    <div class="div-form-login">
                                        <label class="label-login PoppinsRegular">Propiedad de la cuenta</label>
                                        <select name="propiedad" required class="select-fomr" id="propiedad">
                                            <option value="" selected>Debe seleccionar una Opción</option>
                                            <option value="0">Terceros</option>
                                            <option value="1">Propia</option>
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                    <div class="div-form-login">
                                        <label class="label-login PoppinsRegular">Nombre del titular</label>
                                        <input type="text" name="titular" id="nombre_titular" required class="input-login PoppinsRegular">
                                    </div>
                                </div>


                                <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                    <div class="div-form-login">
                                        <label class="label-login PoppinsRegular">Tipo de documento</label>
                                        <select name="tipo_doc" id="tipo_doc" required class="select-fomr">
                                            <option value="" selected>Debe seleccionar una Opción</option>
                                            <option value="1">DNI</option>
                                            <option value="3">CE</option>
                                            <option value="4">PTP</option>
                                            <option value="2">Pasaporte</option>
                                        </select>
                                        <i class="fa fa-angle-down icon-select"></i>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                    <div class="div-form-login">
                                        <label class="label-login PoppinsRegular">Numero de documento</label>
                                        <input type="number" id="nro_doc" name="nro_doc" required class="input-login PoppinsRegular">
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural" style="color: #137188 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                </div>
                            </div>
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body px-0 py-0" id="div-contenido">

                </div>
                <div class="col-12 text-center">
                    <button type="button" class="btn btn-cancelar PoppinsMedium px-5 mt-5 btn-top-natural" data-bs-dismiss="modal" aria-label="Close"> Cancelar</button>
                    <button type="button" id="confirmar-eliminar" class="btn btn-delete PoppinsMedium px-5 mt-5 btn-top-natural" style="color: #fff !important;"> Si, eliminar</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#mis-cuentas').DataTable();

            $("#buscar").keyup(function(){
                if($(this).val().length>3 || $(this).val().length==0)
                {
                    var valores = {};                    
                    valores['buscar'] = $(this).val();
                    valores["_token"] = "{{ csrf_token() }}";
                    $.ajax({
                        url: '/buscador-cuentas',
                        type: 'post',
                        dataType: 'json',
                        data: valores,
                        beforeSend: function () {

                        },
                        success: function (response) {
                            $('#div-contenido-buscador').html(response.htmlview);
                        }
                    });
                }
            });

            $("#propiedad").change(function () {
                if($(this).val() == 1){
                    $("#nombre_titular").val('{{ Session::get("perfil")->nombres }} {{ Session::get("perfil")->apellido_paterno }}');
                    $("#tipo_doc").val('{{ Session::get("perfil")->tipo_doc }}').trigger('change');
                    $("#nro_doc").val('{{ Session::get("perfil")->nro_doc }}');
                }else{
                    $("#nombre_titular").val(null);
                    $("#tipo_doc").val(null).trigger('change');
                    $("#nro_doc").val(null);
                }
            });

            $("#eliminar-cuentas").click(function () {
                if($('.eliminar-cuentas:checked').length>0)
                {
                    var valores = {};
                    var array_cuentas =  new Array();
                    $('.eliminar-cuentas:checked').each(function() {
                        array_cuentas.push($(this).val());
                    });
                    valores['array_cuentas'] = array_cuentas;
                    valores["_token"] = "{{ csrf_token() }}";
                    $.ajax({
                        url: '/eliminar-masivo-cuentas-modal',
                        type: 'post',
                        dataType: 'json',
                        data: valores,
                        beforeSend: function () {

                        },
                        success: function (response) {
                            $('#div-contenido').html(response.htmlview);
                            $("#exampleModal2").modal('show');
                        }
                    });
                }
                else
                {
                    toastr.warning("No se encuentra ninguna cuenta seleccionada!");
                }
            });

            $("#confirmar-eliminar").click(function () {
                var valores = {};
                var array_cuentas =  new Array();
                $('.eliminar-cuentas:checked').each(function() {
                    array_cuentas.push($(this).val());
                });
                valores['array_cuentas'] = array_cuentas;
                valores["_token"] = "{{ csrf_token() }}";
                $.ajax({
                    url: '/eliminar-masivo-cuentas',
                    type: 'post',
                    dataType: 'json',
                    data: valores,
                    beforeSend: function () {

                    },
                    success: function (response) {
                        if(response.estado==1)
                        {
                            toastr.success("Cuentas eliminadas correctamente!");
                            location.reload();
                        }
                    },
                    error: function (request, status, error) {
                        console.log(fail);
                        var errors = fail.responseJSON.errors;
                        $.each(errors,(i, item)=>{
                            toastr.error(item[0]);
                        });
                    }
                });
            });

        } );
    </script>
@endsection