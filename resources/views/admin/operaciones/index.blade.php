@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">

        <div class="row mx-0">
            <div class="col-md-4 px-0">
                <h4 class="arlrdbd">Operaciones</h4>
            </div>
            <div class="col-md-8 px-0 mt-3 mt-md-0">
                <form method="post" action="{{ route('filtrar-operaciones-admin') }}" style="display: inherit;">
                    <div class="row mx-0">
                        @csrf
                        <div class="col-sm-4 px-1 mb-3 mb-sm-0">
                            <div class="div-input-operaciones">
                                <input type="date" name="desde" autocomplete="off" class="form-control input-operacioes PoppinsRegular" value="{{ isset($desde) ? $desde : '' }}" placeholder="Fecha de Inicio" required>
                            </div>
                        </div>
                        <div class="col-sm-4 px-1 mb-3 mb-sm-0">
                            <div class="div-input-operaciones">
                                <input type="date" name="hasta" autocomplete="off" class="form-control input-operacioes PoppinsRegular" value="{{ isset($hasta) ? $hasta : '' }}" placeholder="Fecha Final" required>
                            </div>
                        </div>
                        <div class="col-sm-3 px-1 mb-3 mb-sm-0">
                            <div class="div-input-operaciones">
                                <i class="fa-solid fa-magnifying-glass"></i>
                                <input type="submit" placeholder="Buscar operacion" class="input-operacioes PoppinsRegular">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
            <div class="div-scrolll">
                <table class="table mt-4 table-border-cero" id="operaciones">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center"># Orden</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Estado</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Titular</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Fecha</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Monto</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Recibido</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Cupon</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Cambio</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">
                            Opciones
                        </th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero">
                    @if(count($operaciones)>0)
                        @foreach($operaciones as $operacion)
                            <?php
                            if($operacion->tipo=='0')
                            {
                                $moneda_1='S/';
                                $moneda_2='$';
                                if(is_object($operacion->cupon))
                                {
                                    $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta-$operacion->cupon->valor_soles;
                                }
                                else
                                {
                                    $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta;
                                }
                            }
                            else{
                                $moneda_1='$';
                                $moneda_2='S/';
                                if(is_object($operacion->cupon))
                                {
                                    $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra+($operacion->cupon->valor_soles/$operacion->tasa->tasa_compra);
                                }
                                else
                                {
                                    $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra;
                                }
                            }

                            switch($operacion->estado){
                                case '0':
                                    $o_alerta = "o_pendientes";
                                    $class_alerta = "pendientes";
                                    $spanEstado = "Pendiente";
                                    $nombre_icono = '<svg xmlns="http://www.w3.org/2000/svg" class="pendientes_svg" width="25" height="25" viewBox="0 0 42 42">
                                                          <g id="Grupo_574" data-name="Grupo 574" transform="translate(-946 -158)">
                                                            <circle id="Elipse_35" data-name="Elipse 35" cx="21" cy="21" r="21" transform="translate(946 158)" fill="#fff"/>
                                                            <path id="Icon_open-clock" data-name="Icon open-clock" d="M14.458,0A14.458,14.458,0,1,0,28.915,14.458,14.5,14.5,0,0,0,14.458,0Zm0,3.614A10.843,10.843,0,1,1,3.614,14.458,10.829,10.829,0,0,1,14.458,3.614ZM12.651,7.229v8.024l.578.47,1.807,1.807L16.265,18.9l2.6-2.6-1.373-1.229-1.229-1.229V7.3H12.651Z" transform="translate(952.542 164.542)" fill="#297bfc"/>
                                                          </g>
                                                        </svg>';
                                    break;
                                case '1':
                                    $o_alerta = "o_proceso";
                                    $class_alerta = "proceso";
                                    $spanEstado = "En Proceso";
                                    $nombre_icono = '<svg xmlns="http://www.w3.org/2000/svg" class="proceso_svg" width="25" height="25" viewBox="0 0 42 42">
                                                          <g id="Grupo_573" data-name="Grupo 573" transform="translate(-1162 -158)">
                                                            <circle id="Elipse_34" data-name="Elipse 34" cx="21" cy="21" r="21" transform="translate(1162 158)" fill="#fff"/>
                                                            <path id="Icon_ionic-ios-cog" data-name="Icon ionic-ios-cog" d="M32.126,17.03l-1.835-.3a.559.559,0,0,1-.464-.478c-.035-.225-.07-.45-.12-.668a.567.567,0,0,1,.274-.6l1.624-.9a.564.564,0,0,0,.274-.7l-.281-.773a.559.559,0,0,0-.661-.352l-1.821.352a.566.566,0,0,1-.6-.288q-.158-.3-.337-.591a.572.572,0,0,1,.049-.668l1.216-1.4a.559.559,0,0,0,.021-.745l-.527-.633a.555.555,0,0,0-.738-.105l-1.6.956a.562.562,0,0,1-.661-.063c-.169-.148-.345-.3-.52-.436A.561.561,0,0,1,25.242,8L25.9,6.265a.563.563,0,0,0-.232-.71l-.717-.415a.561.561,0,0,0-.731.155L23.055,6.757a.5.5,0,0,1-.6.176s-.394-.162-.689-.26a.559.559,0,0,1-.387-.541l.028-1.856a.566.566,0,0,0-.464-.591l-.816-.141a.568.568,0,0,0-.64.394l-.6,1.758a.562.562,0,0,1-.548.38c-.112,0-.232-.007-.345-.007s-.232,0-.345.007A.568.568,0,0,1,17.1,5.7l-.6-1.758a.568.568,0,0,0-.64-.394l-.816.141a.566.566,0,0,0-.464.591L14.6,6.131a.563.563,0,0,1-.387.541c-.162.063-.513.2-.682.26a.56.56,0,0,1-.619-.2L11.756,5.3a.561.561,0,0,0-.731-.155l-.717.415a.556.556,0,0,0-.232.71l.661,1.737a.566.566,0,0,1-.176.64c-.176.141-.352.288-.52.436a.562.562,0,0,1-.661.063L7.8,8.177a.563.563,0,0,0-.738.105l-.527.633a.559.559,0,0,0,.021.745l1.216,1.4a.56.56,0,0,1,.049.668q-.179.285-.338.591a.571.571,0,0,1-.6.288l-1.821-.352a.566.566,0,0,0-.661.352l-.281.773a.564.564,0,0,0,.274.7l1.624.9a.557.557,0,0,1,.274.6c-.042.225-.084.443-.12.668a.567.567,0,0,1-.464.478l-1.835.3a.568.568,0,0,0-.5.555V18.4a.552.552,0,0,0,.5.555l1.835.3a.559.559,0,0,1,.464.478c.035.225.07.45.12.668a.567.567,0,0,1-.274.6l-1.624.9a.564.564,0,0,0-.274.7l.281.773a.559.559,0,0,0,.661.352l1.821-.352a.566.566,0,0,1,.6.288q.158.3.338.591a.572.572,0,0,1-.049.668l-1.216,1.4a.559.559,0,0,0-.021.745l.527.633A.555.555,0,0,0,7.8,27.8l1.6-.956a.562.562,0,0,1,.661.063c.169.148.345.3.52.436a.561.561,0,0,1,.176.64L10.1,29.721a.563.563,0,0,0,.232.71l.717.415a.561.561,0,0,0,.731-.155l1.181-1.448a.516.516,0,0,1,.577-.183c.239.1.4.155.7.253a.559.559,0,0,1,.387.541l-.028,1.856a.566.566,0,0,0,.464.591l.816.141a.568.568,0,0,0,.64-.394l.6-1.758a.562.562,0,0,1,.548-.38c.112,0,.232.007.345.007s.232,0,.345-.007a.568.568,0,0,1,.548.38l.6,1.758a.568.568,0,0,0,.64.394l.816-.141a.566.566,0,0,0,.464-.591L21.4,29.855a.559.559,0,0,1,.387-.541c.3-.1.492-.176.675-.246a.49.49,0,0,1,.584.148l1.2,1.47a.561.561,0,0,0,.731.155l.717-.415a.556.556,0,0,0,.232-.71l-.661-1.737a.566.566,0,0,1,.176-.64c.176-.141.352-.288.52-.436a.562.562,0,0,1,.661-.063l1.6.956a.563.563,0,0,0,.738-.105l.527-.633a.559.559,0,0,0-.021-.745l-1.216-1.4a.56.56,0,0,1-.049-.668q.179-.285.337-.591a.571.571,0,0,1,.6-.288l1.821.352a.566.566,0,0,0,.661-.352l.281-.773a.564.564,0,0,0-.274-.7L30,21a.557.557,0,0,1-.274-.6c.042-.225.084-.443.12-.668a.567.567,0,0,1,.464-.478l1.835-.3a.568.568,0,0,0,.5-.555v-.823A.578.578,0,0,0,32.126,17.03ZM12.783,25.095a1.133,1.133,0,0,1-1.758.253A10.135,10.135,0,0,1,11,10.68a1.129,1.129,0,0,1,1.758.246L16.7,17.719a.573.573,0,0,1,0,.563ZM27.844,20.37a10.136,10.136,0,0,1-12.7,7.348,1.122,1.122,0,0,1-.654-1.638l3.916-6.813a.558.558,0,0,1,.485-.281h7.854A1.123,1.123,0,0,1,27.844,20.37Zm-1.09-3.354H18.893a.55.55,0,0,1-.485-.281l-3.945-6.8A1.128,1.128,0,0,1,15.11,8.29,10.148,10.148,0,0,1,27.844,15.63,1.122,1.122,0,0,1,26.754,17.016Z" transform="translate(1164.993 161.007)" fill="#328397"/>
                                                          </g>
                                                        </svg>';
                                    break;
                                case '2':
                                    $o_alerta = "o_realizada";
                                    $class_alerta = "realizada";
                                    $spanEstado = "Realizado";
                                    $nombre_icono = '<svg xmlns="http://www.w3.org/2000/svg" class="realizada_svg" width="25" height="25" viewBox="0 0 42.221 42.221">
                                                          <path id="Icon_awesome-check-circle" data-name="Icon awesome-check-circle" d="M42.783,21.673A21.11,21.11,0,1,1,21.673.563,21.11,21.11,0,0,1,42.783,21.673ZM19.231,32.851,34.894,17.188a1.362,1.362,0,0,0,0-1.926l-1.926-1.926a1.362,1.362,0,0,0-1.926,0L18.268,26.109,12.3,20.146a1.362,1.362,0,0,0-1.926,0L8.452,22.072a1.362,1.362,0,0,0,0,1.926L17.3,32.85A1.362,1.362,0,0,0,19.231,32.851Z" transform="translate(-0.563 -0.563)" fill="#f9fafb"/>
                                                        </svg>';
                                    break;
                                case '3':
                                    $o_alerta = "o_devolucion";
                                    $class_alerta = "devolucion";
                                    $spanEstado = "Devolución";
                                    $nombre_icono = '<svg xmlns="http://www.w3.org/2000/svg" class="devolucion_svg" width="25" height="25" viewBox="0 0 42 42">
                                                          <g id="Grupo_575" data-name="Grupo 575" transform="translate(-936 -158)">
                                                            <g id="Grupo_572" data-name="Grupo 572" transform="translate(-775)">
                                                              <circle id="Elipse_33" data-name="Elipse 33" cx="21" cy="21" r="21" transform="translate(1711 158)" fill="#fff"/>
                                                              <path id="Icon_material-settings-backup-restore" data-name="Icon material-settings-backup-restore" d="M16.134,4.5c-6.682,0-12.1,5.689-12.1,12.706H0l5.378,5.647,5.378-5.647H6.722a9.649,9.649,0,0,1,9.411-9.882,9.649,9.649,0,0,1,9.411,9.882,9.649,9.649,0,0,1-9.411,9.882,9.159,9.159,0,0,1-5.459-1.835L8.766,27.286a11.749,11.749,0,0,0,7.368,2.626c6.682,0,12.1-5.689,12.1-12.706S22.816,4.5,16.134,4.5Z" transform="translate(1716.413 161.794)" fill="#dc3545" stroke="#dc3545" stroke-width="1"/>
                                                            </g>
                                                            <path id="Icon_open-clock" data-name="Icon open-clock" d="M12.651,7.229v7.247l.522.424,1.632,1.632,1.11,1.241,2.35-2.35-1.241-1.11-1.11-1.11V7.294H12.651Z" transform="translate(943.35 166.499)" fill="#dc3545"/>
                                                          </g>
                                                        </svg>
                                                        ';
                                    break;
                                case '4':
                                    $o_alerta = "o_devuelta";
                                    $class_alerta = "devuelta";
                                    $spanEstado = "Devuelto";
                                    $nombre_icono = '<svg xmlns="http://www.w3.org/2000/svg" class="devuelta_svg" width="25" height="25" viewBox="0 0 42 42">
                                                          <g id="Grupo_570" data-name="Grupo 570" transform="translate(-1711 -158)">
                                                            <circle id="Elipse_33" data-name="Elipse 33" cx="21" cy="21" r="21" transform="translate(1711 158)" fill="#fff"/>
                                                            <path id="Icon_material-settings-backup-restore" data-name="Icon material-settings-backup-restore" d="M16.134,4.5c-6.682,0-12.1,5.689-12.1,12.706H0l5.378,5.647,5.378-5.647H6.722a9.649,9.649,0,0,1,9.411-9.882,9.649,9.649,0,0,1,9.411,9.882,9.649,9.649,0,0,1-9.411,9.882,9.159,9.159,0,0,1-5.459-1.835L8.766,27.286a11.749,11.749,0,0,0,7.368,2.626c6.682,0,12.1-5.689,12.1-12.706S22.816,4.5,16.134,4.5Z" transform="translate(1716.413 161.794)" fill="#30a2b8" stroke="#30a2b8" stroke-width="1"/>
                                                          </g>
                                                        </svg>
                                                        ';
                                    break;
                                case '5':
                                    $o_alerta = "o_observado";
                                    $class_alerta = "observado";
                                    $spanEstado = "Observados";
                                    $nombre_icono = '<svg xmlns="http://www.w3.org/2000/svg" class="observado_svg" width="25" height="25" viewBox="0 0 42.221 42.221">
                                                          <g id="Grupo_569" data-name="Grupo 569" transform="translate(-1532.375 -281.375)">
                                                            <path id="Icon_awesome-check-circle" data-name="Icon awesome-check-circle" d="M42.783,21.673A21.11,21.11,0,1,1,21.673.563,21.11,21.11,0,0,1,42.783,21.673Z" transform="translate(1531.813 280.813)" fill="#f9fafb"/>
                                                            <rect id="Rectángulo_356" data-name="Rectángulo 356" width="6" height="22" rx="1" transform="translate(1550 288)" fill="#e2b33b"/>
                                                            <rect id="Rectángulo_357" data-name="Rectángulo 357" width="6" height="6" rx="1" transform="translate(1550 311)" fill="#e2b33b"/>
                                                          </g>
                                                        </svg>
                                                        ';
                                    break;
                                case '6':
                                    $o_alerta = "o_anulado";
                                    $class_alerta = "anulado";
                                    $spanEstado = "Anulado";
                                    $nombre_icono = '<svg xmlns="http://www.w3.org/2000/svg" class="anulado_svg" width="25" height="25" viewBox="0 0 42 42">
                                                          <path id="Icon_material-cancel" data-name="Icon material-cancel" d="M24,3A21,21,0,1,0,45,24,20.981,20.981,0,0,0,24,3ZM34.5,31.539,31.539,34.5,24,26.961,16.461,34.5,13.5,31.539,21.039,24,13.5,16.461,16.461,13.5,24,21.039,31.539,13.5,34.5,16.461,26.961,24Z" transform="translate(-3 -3)" fill="#f9fafb"/>
                                                        </svg>
                                                        ';
                                    break;
                                case '7':
                                    $o_alerta = "o_verificado";
                                    $class_alerta = "verificado";
                                    $spanEstado = "Verificado";
                                    $nombre_icono = '<svg xmlns="http://www.w3.org/2000/svg" class="realizada_svg" width="25" height="25" viewBox="0 0 42.221 42.221">
                                                          <path id="Icon_awesome-check-circle" data-name="Icon awesome-check-circle" d="M42.783,21.673A21.11,21.11,0,1,1,21.673.563,21.11,21.11,0,0,1,42.783,21.673ZM19.231,32.851,34.894,17.188a1.362,1.362,0,0,0,0-1.926l-1.926-1.926a1.362,1.362,0,0,0-1.926,0L18.268,26.109,12.3,20.146a1.362,1.362,0,0,0-1.926,0L8.452,22.072a1.362,1.362,0,0,0,0,1.926L17.3,32.85A1.362,1.362,0,0,0,19.231,32.851Z" transform="translate(-0.563 -0.563)" fill="#f9fafb"/>
                                                        </svg>';
                                    break;
                                default:
                                    $o_alerta = null;
                                    $class_alerta = null;
                                    $spanEstado = null;
                                    $nombre_icono = null;
                                    break;
                            }


                            $operacion->spanEstado=$spanEstado;


                            ?>
                            <tr class="tr-border-top operaciones {{ $o_alerta }}">
                                <th scope="row" class="text-start">
                                    {{$operacion->id}}
                                </th>
                                <th scope="row" class="text-start">
                                    <span class="PoppinsMedium span-radius-datos {{ $class_alerta }}">{!! $nombre_icono !!} {{ $spanEstado }}</span>
                                </th>
                                <th scope="row" class="text-start">
                                    {{$operacion->perfil->nombres}} {{$operacion->perfil->apellido_paterno}} {{$operacion->perfil->apellido_materno}}
                                </th>
                                <td class="text-start td-color-datos PoppinsMedium">
                                    {{date('d-m-Y H:i:s',strtotime($operacion->fecha_operacion))}}
                                </td>
                                <td class="text-end td-color-datos PoppinsMedium">
                                    {{$moneda_1}} {{number_format($operacion->monto_enviado,2)}}
                                </td>
                                <td class="text-end td-color-datos PoppinsMedium">{{$moneda_2}} {{number_format($operacion->monto_recibido,2)}}</td>
                                <td class="text-end td-color-datos PoppinsMedium">
                                    @if(is_object($operacion->cupon))
                                        {{$operacion->cupon->codigo}}
                                    @else
                                        --
                                    @endif
                                </td>
                                <td class="text-end td-color-datos PoppinsMedium">({{number_format($monto_tasa,3)}})</td>
                                <td class="text-start d-flex justify-content-around">
                                    @if($spanEstado=='Realizado')
                                        <a href="{{$operacion->url_factura}}" target="_blank" class="p-2 bnt btn-icono-admi">
                                            <img src="{{ asset('assets-web/img/icon-descarga.png') }}" style="width: 20px;">
                                        </a>
                                    @endif

                                    @canany('operaciones_cambio_estado')
                                        <a href="#" class="px-1 btn btn-icono-admi abrirmodal" data-id="{{$operacion->id}}" style="text-decoration: none;">
                                            <i class="fa-solid fa-pencil" style="width: 28px;"></i>
                                        </a>
                                    @endcanany
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
                {{ $operaciones->links() }}
            </div>
        </div>
    </div>

    @canany('operaciones_cambio_estado')
        <div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten-modal titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Editar Operación</h1>
                            <br>
                            <div class="text-center">
                                <a href="#" id="comprobante" target="_blank">Ver Comprobante</a>
                                <br>
                            </div>

                            <form method="post" action="{{ route('editar-operacion') }}">
                            @csrf
                            <div class="col-12">
                                <div class="row" id="div-informacion-operacion">

                                </div>
                            </div>

                                <input type="hidden" name="id" id="id">
                                <div class="row mx-0 justify-content-start mt-5" id="div-editar-operaciones">
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <select class="select-fomr" id="estado" name="estado" required>
                                                <option value="" disabled selected>Seleccione una Opción</option>
                                                <option value="0">Pendiente</option>
                                                <option value="1">En Proceso</option>
                                                <option value="7">Verificada</option>
                                                <option value="2">Realizado</option>
                                                <option value="3">Devolución</option>
                                                <option value="4">Devuelto</option>
                                                <option value="5">Observados</option>
                                                <option value="6">Anulado</option>
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>

                                    <div class="col-12 px-lg-5" id="div_observacion" style="display: none;">
                                        <div class="div-form-login">
                                            <textarea id="observacion" name="observacion" class="input-login PoppinsRegular"  placeholder="Ingrese observación" style="height: 200px"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Actualizar Estado</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            /*$('#operaciones').DataTable( {
                "order": [[ 0, "desc" ]]
            } );*/
            $('#operaciones tbody').on('click', '.abrirmodal', function (e) {
               var id_operacion=$(this).data('id');
                $.ajax({
                    url:  '/detalle-operacion-ajax/'+id_operacion,
                    type: 'get',
                    dataType:  'json',
                    success: function(response) {
                        if(response.status){
                            var op = response.operacion;

                            if(op.estado==2)
                            {
                                $("#div-editar-operaciones").fadeOut();
                            }
                            else
                            {
                                $("#div-editar-operaciones").fadeIn();
                            }

                            $("#estado").val(op.estado).trigger('change');
                            if(op.estado == 5 || op.estado == 6)
                            {
                                $("#observacion").html(op.observacion);
                            }
                            $("#div-informacion-operacion").html(response.htmlview);
                            $("h2#titulo").remove();
                            $("#id").val(op.id);
                            if(op.url_imagen!='' && op.url_imagen!=null)
                            {
                                $("#comprobante").attr('href', '/img/operaciones/'+url_imagen);
                                $("#comprobante").fadeIn();
                            }
                            else
                            {
                                $("#comprobante").fadeOut();
                            }
                            $("#modalEdit").modal("show");
                        }
                    },
                    error: function (request, status, error) {
                        $("#modalEdit").modal("hide");
                        console.log(request);
                        var errors = fail.responseJSON.errors;
                        $.each(errors,(i, item)=>{
                            toastr.error(item[0]);
                        });
                    }
                });
            });
            $("#estado").change(function(){
                if($(this).val() == 2){
                    $(".referencias").css('display', 'block');
                    $(".input-ref").removeAttr('disabled');
                    $(".input-ref").attr('required','required');
                }
                if($(this).val() == 5 || $(this).val() == 6){
                    $("#div_observacion").css('display', 'block');
                    $("#observacion").attr('required', true);
                }else{
                    $("#div_observacion").css('display', 'none');
                    $("#observacion").removeAttr('required');
                    $("#observacion").val(null);
                }
            });
        } );
    </script>
@endsection