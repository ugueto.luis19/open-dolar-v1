<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Institucion extends Model
{
    use HasFactory;
    protected $table = 'instituciones';
    protected $fillable = [
        'nombre',
        'estado',
    ];

    public function estado(){
        $estado = "";

        switch($this->estado){
            case 1: $estado = "Activo"; break;
            case 0: $estado = "Eliminado"; break;
        }

        return $estado;
    }
}
