@extends('layouts.index_app')

@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-alerta PoppinsBold my-0 text-center text-xl-start">Mis <span>alertas</span></h1>
        <div class="row mx-0 mt-4 justify-content-center">
            <div class="col-sm-10 col-md-8 col-lg-8 col-xl-6 ps-0 pe-0 pe-lg-2">
                <img src="/assets-web/img/icon-camapana.png" class="img-campana-alerta mb-2">
                <form action="{{route('registrar-alerta')}}" method="post">
                    <div class="card-blanco" style="padding-top: 65px;">
                        <h3 class="titulo-general-admi PoppinsBold my-0 text-center">Cambiando soles a dolares</h3><br>
                        <div class="row px-0" style="background: #FEF8DD !important; color: #137188; border-radius: 9px; padding: 10px; margin-left: 45px; margin-right: 45px;">
                            <div class="col-6 d-flex justify-content-between col-padding-compra col-border-right py-2 pe-2" style="display: flex; align-items: center;" id="div-compra">
                                <p class="my-0 PoppinsBold p-color-compra-venta">Venta</p>
                                <div class="my-0 PoppinsRegular p-color-compra-venta">
                                    <b id="tasa-venta">@if(is_object($tasa)){{number_format(($tasa->tasa_venta+$tasa->pips_venta),3)}}@endif</b>
                                </div>
                            </div>
                            <div class="col-6 d-flex justify-content-between col-padding-compra  py-2 ps-3" style="display: flex; align-items: center;" id="div-venta">
                                <p class="my-0 PoppinsBold p-color-compra-venta">Compra</p>
                                <div class="my-0 PoppinsRegular p-color-compra-venta">
                                    <b id="tasa-compra">@if(is_object($tasa)){{number_format(($tasa->tasa_compra+$tasa->pips_compra),3)}}@endif</b>
                                </div>
                            </div>
                        </div>
                        @csrf
                        <div class="row mx-0 justify-content-start mt-5">
                            <div class="col-12 mb-3 px-lg-5">
                                <div class="div-form-login" style="color: #137188;">
                                    <p class="PoppinsRegular" style="font-size: 14px;">Notificarme cuando el valor de <b>compra de dolar</b> es mayor a</p>
                                    @if($alertas[0]->tipo == 1)
                                        <input id="valor_deseado" type="number" step="any" name="valor_deseado_compra" class="input-login PoppinsRegular" placeholder="Ingrese el valor deseado" min="0" value="{{ $alertas[0]->valor_deseado }}">
                                        <label class="switch float" required>
                                            @if($alertas[0]->estado == 1)
                                                <input type="checkbox" checked name="checkCompra">
                                            @else
                                                <input type="checkbox" name="checkCompra">
                                            @endif
                                            <span class="slider"></span>
                                        </label>
                                    @endif
                                </div>
                            </div>
                            <div class="col-12 px-lg-5">
                                <div class="div-form-login" style="color: #137188;font-size: 14px">
                                    <p class="PoppinsRegular">Notificarme cuando el valor de <b>venta de dolar</b> es menor a</p>
                                    @if($alertas[1]->tipo == 2)
                                        <input id="valor_deseado" type="number" step="any" name="valor_deseado_venta" class="input-login PoppinsRegular" placeholder="Ingrese el valor deseado" min="0" value="{{ $alertas[1]->valor_deseado }}">
                                        <label class="switch float" required>
                                            @if($alertas[1]->estado == 1)
                                                <input type="checkbox" checked name="checkVenta">
                                            @else
                                                <input type="checkbox" name="checkVenta">
                                            @endif
                                            <span class="slider"></span>
                                        </label>
                                    @endif
                                </div>
                            </div>
                            <div class="col-12 px-lg-5">
                                <br>
                                <div class="div-form-login PoppinsRegular pe-0" style="font-size: 14px;">
                                    <p style="color: #137188;" class="d-flex"><span style="margin-right: 10px;"><b style="color:#C6D05D !important;">Compra</b></span>Tu transfieres dolares y nosotros te devolvemos soles</p>
                                    <p style="margin-top: 15px; color: #137188;" class="d-flex"><span style="margin-right: 24px;"><b style="color:#C6D05D !important;">Venta</b></span>Tu transfieres soles y nosotros te devolvemos dolares</p>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-0" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>

$( document ).ready(function() {
    if(!$('input:checkbox[name=checkCompra]:checked').val()){
        $('.slider').first().css('background-color' , '#cacbc7'); 
        } 
    
    if(!$('input:checkbox[name=checkVenta]:checked').val()){
        $('.slider').last().css('background-color' , '#cacbc7'); 
        } 
}); 
        
        


    function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

    



    $(".slider").click(function() {
        if (rgb2hex($(this).css('background-color'))=="#cacbc7"){
            console.log("si");
            $(this).css('background-color' , '#c6d05d');
        }else { $(this).css('background-color' , '#cacbc7'); }
                });
</script>
@endsection
