@extends('layouts.index_app')
@section('content')
    <!-- card de necesitas ayuda-->
    <div class="div-flotante">
        <img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 40%">
    </div>

    <div class="div-flotante-azul px-5 py-4">
        <button class="btn btn-close-ayuda">
            <i class="fa-solid fa-circle-xmark icon-close-ayuda"></i>
        </button>
        <h4 class="Arialregular titulo-ayuda"><img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 8%;">Necesitas ayuda?</h4>
        <h5 class="ArialBold mt-5 mb-2">Temas recurrentes</h5>
        <p class="Arialregular p-link-temas my-0"><a href="">Problemas con transacción</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Perfil y registro</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Privacidad y seguridad</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Cancelaciones y devoluciones</a></p>
        <h5 class="ArialBold mt-5 mb-2">Contacto</h5>
        <p class="Arialregular">
            Con el fin de ahorrar tu tiempo, recomendamos revisar las preguntas frecuentes. Si sigues sin encontrar respuesta, puedes contactarnos de la manera que mas te acomode:
        </p>
        <ul class="ps-0">
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-phone me-3"></i>
                (+51) 1 1234567
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-brands fa-whatsapp me-3"></i>
                998877665
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-envelope me-3"></i>
                confianza@opendolar.com
            </li>
            <li class="d-flex Arialregular align-items-start list-contacto">
                <i class="fa-solid fa-location-dot me-3"></i>
                Centro Empresarial El Trigal, Calle Antares 320, Of. 802-C, Torre B, Surco - Lima.
            </li>
        </ul>
        <div class="d-flex justify-content-start mt-4">
            <a href="" class="me-4">
                <i class="fa-solid fa-share-nodes icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-whatsapp icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-facebook-f icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-linkedin-in icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-instagram icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="">
                <i class="fa-solid fa-envelope icon-redes-verde" style="color: #fff !important;"></i>
            </a>
        </div>
    </div>

    <div class="conten_panel_administrador">
        <h1 class="titulo-general-alerta PoppinsBold my-0 text-center text-xl-start"><span>A Cambiar</span></h1>


        <div class="row mx-0 mt-4 justify-content-center">

            <div class="col-12">
                <a href="{{route('dashboard',["id"=>$operacion->id])}}" class="btn btn-volver-azul PoppinsRegular">< Volver</a>
            </div>
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 px-0 px-md-4 px-lg-4 px-xl-3" style="position: relative;">
                <div class="div-check">
                    <div class="div-chec-radius">
                        <i class="fa-solid fa-circle-check check-activado"></i>
                        <i class="fa-solid fa-circle-check"></i>
                        <i class="fa-solid fa-circle-check"></i>
                    </div>
                </div>
                <form action="{{route('guardar-cambio2-multicuentas')}}" method="post" id="formularioProceso">
                    <input type="hidden" id="id" name="id" value="{{ $operacion->id }}">
                    <div class="card-blanco card-padding-top mt-4">
                        <h2 class="PoppinsMedium subtitulo-card text-center" id="titulo">Escoje las cuentas</h2>


                        <div class="row mx-0 px-0" style="background: #FEF8DD !important; color: #137188; border-radius: 9px; padding: 10px; margin-left: 45px; margin-right: 45px;">
                            <div class="col-6 col-sm-4 d-flex justify-content-center col-padding-compra align-items-center col-border-right py-3 py-sm-0 px-2" style="align-items: center;">
                                <p class="my-0 p-color-compra-venta text-center">
                                    <span class="PoppinsBold">
                                        Envío
                                    </span>
                                    <br>
                                    @if($operacion->uso_opendolar!=0)
                                        <b style="text-decoration: line-through; color: red; display: block;" id="old-tasa-venta">
                                            @if($operacion->tipo== 0)
                                                PEN
                                            @else
                                                USD
                                            @endif
                                            @if($operacion->tipo== 0)
                                                {{ $operacion->monto_enviado+$operacion->uso_opendolar }}
                                            @else
                                                {{ $operacion->tasa->tasa_compra+$operacion->tasa->pips_compra*$operacion->monto_recibido }}
                                            @endif
                                        </b>
                                    @endif
                                    <span class="PoppinsRegular">
                                        @if($operacion->tipo== 0)
                                            PEN
                                        @else
                                            USD
                                        @endif
                                            {{ $operacion->monto_enviado }}
                                    </span>
                                </p>
                            </div>
                            <div class="col-6 col-sm-4 d-flex justify-content-center col-padding-compra col-border-right col-border-cero-cero py-3 py-sm-0 px-3 align-items-center">
                                <p class="my-0 p-color-compra-venta text-center"><span class="PoppinsBold">Recibiré</span> <br> <span class="PoppinsRegular">@if($operacion->tipo== 0) USD @else PEN @endif{{ $operacion->monto_recibido }}</span> </p>
                            </div>
                            <div class="col-6 col-sm-4 d-flex justify-content-center col-padding-compra py-3 py-sm-0 px-2 align-items-center">
                                <p class="my-0 p-color-compra-venta text-center"> <span class="PoppinsBold">Tipo de Cambio</span> <br>
                                    @if(is_object($operacion->cupon))
                                        <b style="text-decoration: line-through; color: red; display: block;" id="old-tasa-venta">
                                            @if($operacion->tipo== 0)
                                                {{number_format($operacion->tasa->tasa_venta+$operacion->tasa->pips_venta,3)}}
                                            @else
                                                {{number_format($operacion->tasa->tasa_compra+$operacion->tasa->pips_compra,3)}}
                                            @endif
                                        </b>
                                    @endif
                                    <span class="PoppinsRegular">
                                        @if($operacion->tipo == 0)
                                            @if(is_object($operacion->cupon))
                                                {{ number_format(($operacion->tasa->tasa_venta+$operacion->tasa->pips_venta),3)-number_format(($operacion->cupon->valor_soles),3) }}
                                            @else
                                                {{number_format(($operacion->tasa->tasa_venta+$operacion->tasa->pips_venta),3)}}
                                            @endif
                                        @else
                                            @if(is_object($operacion->cupon))
                                                {{ number_format(($operacion->tasa->tasa_compra+$operacion->tasa->pips_compra),3)+number_format(($operacion->cupon->valor_soles/$operacion->tasa->tasa_compra),3) }}
                                            @else
                                                {{number_format(($operacion->tasa->tasa_compra+$operacion->tasa->pips_compra),2)}}
                                            @endif
                                        @endif
                                    </span>
                                </p>
                            </div>
                        </div>


                        <div class="col-12 mb-2 mt-3">
                            <div class="div-scroll pe-3">
                                <div class="div-form-login">
                                    <label><b style="color: #137188;">Origen</b></label>
                                    @if($operacion->tipo==0)
                                        <div class="mt-3">
                                            <div class="div-form-login">
                                                <select class="select-fomr" id="cuenta_origen_soles" name="id_cuenta_origen" required>
                                                    <option value="" selected>Cuenta de Origen</option>
                                                    @if(count($cuentas_bancarias)>0)
                                                        @foreach($cuentas_bancarias as $cuenta)
                                                            @if($cuenta->moneda->nombre == 'Soles')
                                                                <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <i class="fa fa-angle-down icon-select"></i>
                                            </div>
                                        </div>
                                    @else
                                        <div class="mt-3">
                                            <div class="div-form-login">
                                                <select class="select-fomr" id="cuenta_origen_usd" name="id_cuenta_origen" required>
                                                    <option value="" selected>Cuenta de Origen</option>
                                                    @if(count($cuentas_bancarias)>0)
                                                        @foreach($cuentas_bancarias as $cuenta)
                                                            @if($cuenta->moneda->nombre == 'Dolares')
                                                                <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <i class="fa fa-angle-down icon-select"></i>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-12 mb-2 mt-2">
                                    <div class="div-form-login">
                                        <label><b style="color: #137188;">Destino</b></label>
                                        <div class="row">
                                            <div class="col-12">

                                                <div class="row" id="listado-cuentas-bancarias">
                                                @if($operacion->tipo== 0)
                                                    <!-- 2 Select cuentas en Dolares -->
                                                        <div class="col-8 mt-3">
                                                            <div class="div-form-login">
                                                                <select class="select-fomr" id="cuenta_destino_usd_1" name="id_cuenta_destino[]" required>
                                                                    <option value="" selected>Cuenta de Destino</option>
                                                                    @if(count($cuentas_bancarias)>0)
                                                                        @foreach($cuentas_bancarias as $cuenta)
                                                                            @if($cuenta->moneda->nombre == 'Dolares')
                                                                                <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                                <i class="fa fa-angle-down icon-select"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 mt-3">
                                                            <input type="number" name="monto_banco[]" min="0" onkeyup="verificarMonto();" step="any" id="monto_1" class="input-login PoppinsRegular" placeholder="Monto">
                                                        </div>
                                                        <div class="col-8 mt-3">
                                                            <div class="div-form-login">
                                                                <select class="select-fomr" id="cuenta_destino_usd_2" name="id_cuenta_destino[]" required>
                                                                    <option value="" selected>Cuenta de Destino</option>
                                                                    @if(count($cuentas_bancarias)>0)
                                                                        @foreach($cuentas_bancarias as $cuenta)
                                                                            @if($cuenta->moneda->nombre == 'Dolares')
                                                                                <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                                <i class="fa fa-angle-down icon-select"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 mt-3">
                                                            <input type="number" name="monto_banco[]" onkeyup="verificarMonto();" min="0" step="any" id="monto_2" class="input-login PoppinsRegular" placeholder="Monto">
                                                        </div>
                                                @else
                                                    <!-- 2 Select cuentas en Soles -->

                                                        <div class="col-8 mt-3">
                                                            <div class="div-form-login">
                                                                <select class="select-fomr" id="cuenta_destino_soles_1" name="id_cuenta_destino[]" required>
                                                                    <option value="" selected>Cuenta de Destino</option>
                                                                    @if(count($cuentas_bancarias)>0)
                                                                        @foreach($cuentas_bancarias as $cuenta)
                                                                            @if($cuenta->moneda->nombre == 'Soles')
                                                                                <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                                <i class="fa fa-angle-down icon-select"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 mt-3">
                                                            <input type="number" name="monto_banco[]" min="0" step="any" id="monto_1" class="input-login PoppinsRegular" placeholder="Monto">
                                                        </div>
                                                        <div class="col-8 mt-3">
                                                            <div class="div-form-login">
                                                                <select class="select-fomr" id="cuenta_destino_soles_2" name="id_cuenta_destino[]" required>
                                                                    <option value="" selected>Cuenta de Destino</option>
                                                                    @if(count($cuentas_bancarias)>0)
                                                                        @foreach($cuentas_bancarias as $cuenta)
                                                                            @if($cuenta->moneda->nombre == 'Soles')
                                                                                <option value="{{$cuenta->id}}">{{$cuenta->banco->nombre}} ({{$cuenta->nro_cuenta}})</option>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                                <i class="fa fa-angle-down icon-select"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 mt-3">
                                                            <input type="number" name="monto_banco[]" onkeyup="verificarMonto();" min="0" step="any" id="monto_2" class="input-login PoppinsRegular" placeholder="Monto">
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div id="agregar_cuentas" class="d-flex justify-content-between align-items-center">
                                    <span style="color: #137188; cursor: pointer;" data-bs-toggle="modal" data-bs-target="#exampleModal">+ Agregar Cuenta</span> <label id="contador_suma" class="PoppinsMedium" style="color: #137188;"></label>
                                </div>

                                <div class="col-12 mb-2 mx-4 mt-3">
                                    <div class="div-form-login" style="width: 89%;">
                                        <input type="checkbox" id="checkboxDeclarar" required>
                                        <p style="display: inline; color: #137188;">
                                            Declaro que transferiré los fondos de una cuenta de origen propia o mancomunada y no haré uso en cuentas no autorizadas
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row mx-0 mt-1" >
                                <div class="col-12 text-center">
                                    <button type="button" disabled id="buttonProcesar" class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-0" style="color: #137188 !important;">Procesar</button><br>
                                </div>
                            </div>
                        </div>
                    @csrf
                </form>

            </div>
        </div>

        <div class="row mx-0 mt-4 justify-content-center px-0">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 px-0 px-md-4 px-lg-4 px-xl-3 py-3 text-center text-sm-start" >
                <div class="py-2" style="border: 1px solid #FF0000; border-radius: 5px;background-color: #FFC6C6;position: relative;">
                     <i class="fa-solid fa-map-location-dot icon-location-azul"></i>
                    <p class="PoppinsRegular p-azul-mensaje mb-0 mt-3 mt-sm-0">Si tu cuenta de destino es de provincia y distinta a BCP, el envío de dinero estará sujeto a cobro de comisión, Te sugerimos ingresar una cuenta BCP para recibir dinero a nivel nacional.</p>
                </div>
            </div>
        </div>


        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0">
                        <form method="post" id="form-new-cuenta-bancaria">
                            <div class="div-modal-padding-left-right pt-5">
                                <h1 class="PoppinsBold titulo-conten mb-0 mt-5 text-center">Registrar Cuenta Bancaria</h1>
                                <div class="row mx-0 justify-content-start mt-5">
                                    <input name="id_operacion" id="id-operacion" type="hidden" value="{{$operacion->id}}">
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Moneda</label>
                                            <select class="select-fomr" name="id_moneda" id="id_moneda" >
                                                <option value="" selected>Debe seleccionar una Opción</option>
                                                @if(count($monedas)>0)
                                                    @foreach($monedas as $moneda)
                                                        <option value="{{$moneda->id}}">{{$moneda->nombre}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Nombre del banco</label>
                                            <select class="select-fomr" name="id_banco" id="id_banco" >
                                                <option value="" selected>Debe seleccionar una Opción</option>
                                                @if(count($bancos)>0)
                                                    @foreach($bancos as $banco)
                                                        <option value="{{$banco->id}}">{{$banco->nombre}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Tipo de cuenta</label>
                                            <select class="select-fomr" name="id_tipo_cuenta" id="id_tipo_cuenta" >
                                                <option value="" selected>Debe seleccionar una Opción</option>
                                                @if(count($tipos_cuentas)>0)
                                                    @foreach($tipos_cuentas as $tipo_cuenta)
                                                        <option value="{{$tipo_cuenta->id}}">{{$tipo_cuenta->nombre}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Numero de cuenta</label>
                                            <input type="number" name="nro_cuenta" class="input-login PoppinsRegular">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Propiedad de la cuenta</label>
                                            <select name="propiedad" id="propiedad"  class="select-fomr">
                                                <option value="" selected>Debe seleccionar una Opción</option>
                                                <option value="0">Terceros</option>
                                                <option value="1">Propia</option>
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Nombre del titular</label>
                                            <input type="text" name="titular"  class="input-login PoppinsRegular">
                                        </div>
                                    </div>


                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Tipo de documento</label>
                                            <select name="tipo_doc" id="tipo_doc"  class="select-fomr">
                                                <option value="" selected>Debe seleccionar una Opción</option>
                                                <option value="1">DNI</option>
                                                <option value="2">Pasaporte</option>
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-6 mb-3 px-sm-1">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Numero de documento</label>
                                            <input type="number" name="nro_doc"  class="input-login PoppinsRegular">
                                        </div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural" style="color: #fff !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                        <button type="submit" class="btn btn-cuenta-2 PoppinsMedium px-5 mt-5 btn-top-natural" style="color: #fff !important;"><i class="fa-solid fa-circle-check"></i> Ya se encuentra registrada!</button>
                                    </div>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade modal-top" id="modalSave" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="height: 700px;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative; background-color: #f9f9f9; border-radius:25px;">
                        <div class="row mx-0 justify-content-center mt-5">
                            <div class="col-sm-10 col-md-9 col-xl-9 px-1 mb-5" >
                                <h4 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center ">Antes de transferir:</h4>
                                <ul style="list-style: none;" class="mt-5 ps-0">
                                    <li class="d-flex align-items-start PoppinsRegular list-modal-azul">
                                        <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                        Solo aceptamos transferencias bancarias y no depósitos en efectivo.
                                    </li>
                                    <li class="d-flex align-items-start PoppinsRegular list-modal-azul mt-3">
                                        <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                        Aceptamos transferencias desde banca por internet o desde la app de tu banco a nivel nacional.
                                    </li>
                                    <li class="d-flex align-items-start PoppinsRegular list-modal-azul mt-3">
                                        <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                        Si realizas una transferencia desde la ventanilla de un banco en provincia, aceptas asumir el costo de la comisión que el banco genere por la operación, la cual se descontada en tu orden de cambio.
                                    </li>
                                    <li class="d-flex align-items-start PoppinsRegular list-modal-azul mt-3">
                                        <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                        Si el origen de la transferencia es de otro banco distinto a BCP o Interbank tienes que considerar el tiempo que toma tu banco en hacer el envío interbancario. Una vez ingrese el dinero a nuestras cuentas, recién se empezara a procesar tu orden.
                                    </li>
                                    <li class="d-flex align-items-start PoppinsRegular list-modal-azul mt-3">
                                        <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                        Verifica con tu banco si tus cuentas generan comisiones adicionales por transferencias.
                                    </li>
                                    <li class="d-flex align-items-start PoppinsRegular list-modal-azul mt-3">
                                        <img src="{{ asset('assets-web/img/Grupo 425.png') }}">
                                        Si la cuenta de destino no es de Lima y distinta a BCP, las transferencias de dinero estará sujeto al cobro de comisión que deberás asumir. Te sugerimos que si tuenes dudas nos para poder ayudarte a modificar la orden por una cuenta de destino en BCP.
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 text-center">
                                <button class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-5" type="button" id="processModal" style="color: #137188 !important;" >
                                    <i class="fa-solid fa-circle-check"></i> Entendido</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('scripts')
            <script>
                        @if(is_object($operacion))
                var monto_a_recibir = {{ $operacion->monto_recibido }};
                        @else
                var monto_a_recibir = 0;
                        @endif

                var verificarMonto = () => {
                        let suma = 0;
                        let values = $("input[name='monto_banco[]']")
                            .map(function(){
                                suma+=parseFloat($(this).val());
                                return $(this).val();
                            }).get();

                        if(isNaN(suma))
                        {
                            suma=$("#monto_1").val();
                        }

                        if(parseFloat(suma).toFixed(2)==parseFloat(monto_a_recibir))
                        {
                            $("#buttonProcesar").removeAttr('disabled');
                            $("#contador_suma").html(null);
                        }
                        else
                        {
                            $("#buttonProcesar").attr('disabled', 'disabled');
                        }

                        if(parseFloat(suma).toFixed(2)>parseFloat(monto_a_recibir)){
                            toastr.warning("Se ha excedido con los montos a enviar!");
                            $("#contador_suma").html(null);
                        }
                        if(parseFloat(suma).toFixed(2)<parseFloat(monto_a_recibir)){
                            $("#contador_suma").html("Saldo restante: "+(parseFloat(parseFloat(monto_a_recibir)-parseFloat(suma).toFixed(2)).toFixed(2)));
                        }


                    };

                /* $("#volver").click(function(){
                     window.history.back();
                 });*/

                $( document ).ready(function() {
                    $("#form-new-cuenta-bancaria").on('submit',function(e){
                        e.preventDefault();
                        var data = new FormData(document.getElementById('form-new-cuenta-bancaria'));

                        $.ajax({
                            url:'{{route("registrar-cuenta-ajax")}}',
                            type: "POST",
                            dataType: "JSON",
                            processData: false,
                            contentType: false,
                            cache:false,
                            data: data
                        })
                            .done((res)=>{
                                console.log(res);
                                if(res.status){

                                    if(res.cuenta==null)
                                    {
                                        toastr.success("Select agregado correctamente!");
                                    }
                                    else
                                    {
                                        toastr.success("Cuenta agregada correctamente!");
                                    }

                                    $("#exampleModal").modal("hide");
                                    $("#listado-cuentas-bancarias").append(res.htmlview);

                                    $("#form-new-cuenta-bancaria")[0].reset();

                                    $("#id_moneda").val('').trigger('change');
                                    $("#id_banco").val('').trigger('change');
                                    $("#id_tipo_cuenta").val('').trigger('change');
                                    $("#propiedad").val('').trigger('change');
                                    $("#tipo_doc").val('').trigger('change');
                                    $("#id_moneda").val('').trigger('change');

                                    $("#id-operacion").val(res.operacion.id);
                                }
                            })
                            .fail((fail)=>{
                                $("#exampleModal").modal("hide");
                                console.log(fail);
                                var errors = fail.responseJSON.errors;
                                $.each(errors,(i, item)=>{
                                    toastr.error(item[0]);
                                });
                            })
                    });

                    $("#buttonProcesar").click((e)=>{
                        e.preventDefault;

                        let destinoEmpty = true;

                        let valuesDestino = $("select[name='id_cuenta_destino[]']")
                            .map(function(){
                                if($(this).val() == "") destinoEmpty = false;
                                return $(this).val();
                            }).get();

                        let valuesMonto = $("input[name='monto_banco[]']")
                            .map(function(){
                                if($(this).val() == "") destinoEmpty = false;
                                return $(this).val();
                            }).get();

                        if($("#cuenta_origen_soles").val() != "" && destinoEmpty && $("#checkboxDeclarar").is(':checked')    )
                        {
                            $("#modalSave").modal('show');
                        }
                        else
                        {
                            if($("#checkboxDeclarar").is(':checked'))
                            {
                                toastr.warning('Por favor complete los datos solicitados!');
                            }
                            else
                            {
                                toastr.warning('Por favor acepte los terminos y condiciones!');
                            }

                        }
                    });

                    $("#processModal").click((e)=>{
                        e.preventDefault;
                        $("#formularioProceso").submit();
                    });


                });
            </script>
@endsection