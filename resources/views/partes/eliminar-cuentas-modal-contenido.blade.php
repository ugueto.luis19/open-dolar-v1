<div class="div-modal-padding-left-right pt-5">
    <h1 class="PoppinsBold titulo-conten mb-0 mt-5 text-center">Eliminar Cuenta</h1>
    <div class="row mx-0 justify-content-start mt-5">

        @if(count($array_cuentas)>0)
            @foreach($array_cuentas as $array_cuenta)
                <p class="text-center">Esta seguro de eliminar la cuenta <b>{{$perfil->informacionCuenta($array_cuenta)->nro_cuenta}}</b> del banco <b>{{$perfil->informacionCuenta($array_cuenta)->banco->nombre}}</b>.</p>
            @endforeach
        @endif

    </div>
</div>