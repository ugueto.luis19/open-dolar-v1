<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\CambioController;
use App\Http\Controllers\CuentaController;
use App\Http\Controllers\PerfilController;
use App\Http\Controllers\SoporteController;
use App\Http\Controllers\AlertaController;
use App\Http\Controllers\ReferidoController;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/config-cache', function() {      $exitCode = Artisan::call('config:cache');      return '<h1>Clear Config cleared</h1>';  });

Route::get('/limpiar-perfiles',  [SoporteController::class, 'limpiarPerfiles']);
Route::get('/anular-operaciones',  [SoporteController::class, 'anularOperaciones']);

Route::get('get-time', function(){
    return response()->json(['date' => date('Y-m-d H:i:s')]);
})->name('get-time');

// Grupo de rutas protegidas por el Auth
Route::middleware(['auth'])->group(function () {

    Route::get('/dashboard/{id?}',  [CambioController::class, 'dashboard'])->name('dashboard');

    Route::get('/dashboard-wp/{tipo}/{monto_envio}',  [CambioController::class, 'dashboardWP'])->name('dashboard-wp');

    Route::get('/procesar-cupon',  [CambioController::class, 'procesarCupon'])->name('procesar-cupon');
    Route::post('/proceso-cambio',  [CambioController::class, 'procesoCambio'])->name('proceso-cambio');
    Route::post('/proceso-cambio-1',  [CambioController::class, 'procesoCambio1'])->name('proceso-cambio1');
    Route::get('/proceso-cambio-2/{id}',  [CambioController::class, 'procesoCambio2'])->name('proceso-cambio2');
    Route::post('/guardar-cambio-2',  [CambioController::class, 'guardarCambio2UnaCuenta'])->name('guardar-cambio2');
    Route::post('/guardar-cambio-2-multi-cuentas',  [CambioController::class, 'guardarCambio2VariasCuentas'])->name('guardar-cambio2-multicuentas');
    Route::get('/proceso-cambio-3/{id}',  [CambioController::class, 'procesoCambio3'])->name('proceso-cambio3');

    Route::post('/continuar-cambio3',  [CambioController::class, 'continuarCambio3'])->name('continuar-cambio3');

    Route::get('/proceso-cambio-3-confirmacion/{id}',  [CambioController::class, 'procesoCambio3Confirmacion'])->name('proceso-cambio3-confirmar');
    Route::post('registrar-origen-fondos-ajax',  [CambioController::class, 'registrarOrigenFondos'])->name('registrar-origen-fondos-ajax');

    Route::get('/detalle-operacion-ajax/{id}',  [CambioController::class, 'detalleOperacionAjax'])->name('detalle-operacion-ajax');
    Route::get('/detalle-operacion-ajax-v2/{id}',  [CambioController::class, 'detalleOperacionAjaxv2'])->name('detalle-operacion-ajax-v2');
    Route::get('/listado-operaciones-filtro/{estado}',  [CambioController::class, 'listadoOperacionesFiltro'])->name('listado-operaciones-filtro');

    Route::post('mis-operaciones',  [CambioController::class, 'filtrarOperaciones'])->name('filtrar-operaciones');

    //Route::get('/confirmar-cambio/{id}',  [CambioController::class, 'confirmarCambio'])->name('confirmar-cambio');
    Route::post('/finalizar-cambio',  [CambioController::class, 'finalizarCambio'])->name('finalizar-cambio');
    Route::get('/continuar-pendiente/{id}',  [CambioController::class, 'continuarPendiente'])->name('continuar-pendiente');    

    Route::get('anular-cambio/{id}',  [CambioController::class, 'anular'])->name('anular-cambio');

    Route::get('/mis-ordenes',  [CambioController::class, 'misOperaciones'])->name('mis-operaciones');
    Route::get('/mis-cuentas',  [CuentaController::class, 'misCuentas'])->name('mis-cuentas');
    Route::post('registrar-cuenta',  [CuentaController::class, 'store'])->name('registrar-cuenta');
    Route::post('registrar-cuenta-ajax',  [CuentaController::class, 'storeAjax'])->name('registrar-cuenta-ajax');


    Route::post('eliminar-masivo-cuentas-modal',  [CuentaController::class, 'anularMasivomodal'])->name('eliminar-masivo-cuentas-modal');
    Route::post('eliminar-masivo-cuentas',  [CuentaController::class, 'anularMasivo'])->name('eliminar-masivo-cuentas');
    Route::post('buscador-cuentas',  [CuentaController::class, 'buscarCuentas'])->name('buscador-cuentas');
    Route::post('buscador-instituciones',  [AdminController::class, 'buscarInstitucion'])->name('buscador-instituciones');

    Route::get('/mi-perfil',  [PerfilController::class, 'miPerfil'])->name('mi-perfil');

    Route::get('/elegir-perfil',  [CambioController::class, 'elegirPerfil'])->name('elegir-perfil');


    Route::get('/perfil/{codigo}',  [PerfilController::class, 'seleccionarPerfil'])->name('seleccionar-perfil');
    Route::get('/editar-perfil/{codigo}',  [PerfilController::class, 'editarPerfil'])->name('editar-perfil');
    Route::post('actualizar-perfil',  [PerfilController::class, 'store'])->name('actualizar-perfil');
    Route::post('cambiar-contrasena',  [PerfilController::class, 'cambiarContrasena'])->name('cambiar-contrasena');

    Route::get('/alertas',  [AlertaController::class, 'misAlertas'])->name('alertas');
    Route::get('/referido',  [ReferidoController::class, 'indexReferido'])->name('referido');
    Route::get('/historial-referidos',  [ReferidoController::class, 'historialReferidos'])->name('historial-referidos');



    Route::post('registrar-alerta',  [AlertaController::class, 'store'])->name('registrar-alerta');
    Route::post('editar-alerta',  [AlertaController::class, 'edit'])->name('editar-alerta');
    Route::get('eliminar-alerta/{id}',  [AlertaController::class, 'eliminarAlerta'])->name('eliminarAlerta');

    Route::get('/registro-biometrico',  [SoporteController::class, 'registroBiometrico'])->name('registro-biometrico');
    Route::get('/registro-biometrico-paso-1',  [SoporteController::class, 'registroBiometrico1'])->name('biometrico-1');
    Route::get('/registro-biometrico-paso-2',  [SoporteController::class, 'registroBiometrico2'])->name('biometrico-2');
    Route::get('/biometrico-final',  [SoporteController::class, 'biometricoFinal'])->name('biometricoFinal');

    Route::post('biometrico-paso-1',  [SoporteController::class, 'biometricoPaso1'])->name('biometrico-paso-1');
    Route::post('biometrico-paso-2',  [SoporteController::class, 'biometricoPaso2'])->name('biometrico-paso-2');

    Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function(){
        Route::get('horarios', [AdminController::class, 'getHorarios'])->name('horarios');
        Route::post('editar-horario',  [AdminController::class, 'editarHorario'])->name('editar-horario');

        Route::get('clientes', [AdminController::class, 'getClientes'])->name('clientes');
        Route::get('ver-perfiles/{id_usuario}', [AdminController::class, 'verPerfiles'])->name('ver-perfiles');
        Route::get('ver-informacion-completa/{id_perfil}', [AdminController::class, 'verInformacionCompleta'])->name('ver-informacion-completa');

        Route::get('verificaciones', [AdminController::class, 'getVerificaciones'])->name('verificaciones');
        Route::get('operaciones', [AdminController::class, 'getOperaciones'])->name('operaciones');
        Route::get('instituciones', [AdminController::class, 'getInstituciones'])->name('instituciones');
        Route::get('tasas-cambio', [AdminController::class, 'getTasasCambio'])->name('tasas-cambio');

        Route::post('registrar-tasa',  [AdminController::class, 'registrarTasaCambio'])->name('registrar-tasa');
        Route::post('registrar-tasa-cambistas',  [AdminController::class, 'registrarTasaCambioCambistas'])->name('registrar-tasa-cambistas');
        Route::post('registrar-tasa-bancos',  [AdminController::class, 'registrarTasaCambioBancos'])->name('registrar-tasa-bancos');

        Route::post('registrar-institucion',  [AdminController::class, 'registrarInstitucion'])->name('registrar-institucion');
        Route::get('eliminar-institucion/{id}',  [AdminController::class, 'eliminarInstitucion'])->name('eliminar-institucion');
        Route::get('activar-institucion/{id}',  [AdminController::class, 'activarInstitucion'])->name('activar-institucion');
        Route::post('editar-institucion',  [AdminController::class, 'editarInstitucion'])->name('editar-institucion');

        Route::get('cambiar-estado-verificacion',  [AdminController::class, 'cambiarEstadoVerificacion'])->name('cambiar-estado-verificacion');

        Route::get('anular-verificacion/{id}',  [AdminController::class, 'anularVerificacion'])->name('anular-verificacion');
        Route::get('procesar-verificacion/{id}',  [AdminController::class, 'procesarVerificacion'])->name('procesar-verificacion');
        Route::post('editar-operacion',  [AdminController::class, 'editarOperacion'])->name('editar-operacion');

        Route::get('comisiones-config', [AdminController::class, 'getComisionesConfig'])->name('comisiones-config');
        Route::post('registrar-comision-config',  [AdminController::class, 'registrarComisionConfig'])->name('registrar-comision-config');

        Route::post('operaciones',  [AdminController::class, 'filtrarOperaciones'])->name('filtrar-operaciones-admin');
        Route::post('tasas-filtro',  [AdminController::class, 'filtrarTasas'])->name('filtrar-tasas-admin');

        Route::get('cupones', [AdminController::class, 'getCupones'])->name('cupones');
        Route::post('registrar-cupon',  [AdminController::class, 'registrarCupon'])->name('registrar-cupon');
        Route::get('/cambiar-estado-cupon',  [AdminController::class, 'cambiarEstadoCupon'])->name('cambiar-estado-cupon');
        Route::get('eliminar-cupon/{id}',  [AdminController::class, 'eliminarCupon'])->name('eliminar-cupon');
        Route::get('activar-cupon/{id}',  [AdminController::class, 'activarCupon'])->name('activar-cupon');

        Route::get('/cambiar-estado-usuario',  [AdminController::class, 'cambiarEstadoUsuario'])->name('cambiar-estado-usuario');

        Route::get('roles',  [AdminController::class, 'getRoles'])->name('roles');
        Route::post('registrar-rol',  [AdminController::class, 'registrarRol'])->name('registrar-rol');
        Route::post('editar-rol',  [AdminController::class, 'editarRol'])->name('editar-rol');
        Route::get('eliminar-rol/{id}',  [AdminController::class, 'eliminarRol'])->name('eliminar-rol');

        Route::get('usuarios',  [AdminController::class, 'getUsuarios'])->name('usuarios');
        Route::post('registrar-usuario',  [AdminController::class, 'registrarUsuario'])->name('registrar-usuario');
        Route::post('editar-usuario',  [AdminController::class, 'editarUsuario'])->name('editar-usuario');
        Route::get('eliminar-usuario/{id}',  [AdminController::class, 'eliminarUsuario'])->name('eliminar-usuario');

        Route::get('cuentas_bancarias_admin',  [AdminController::class, 'getCuentasBancariasAdmin'])->name('cuentas_bancarias_admin');
        Route::post('registrar-cuenta-bancaria-ad',  [AdminController::class, 'registrarCuentasBancariasAdmin'])->name('registrar-cuenta-bancaria-ad');
        Route::post('editar-cuenta-bancaria-ad',  [AdminController::class, 'editarCuentasBancariasAdmin'])->name('editar-cuenta-bancaria-ad');
        Route::get('eliminar-cuenta-bancaria-ad/{id}',  [AdminController::class, 'eliminarCuentasBancariasAdmin'])->name('eliminar-cuenta-bancaria-ad');
    }); 

});


require __DIR__.'/auth.php';
Route::get('/seleccionar-tipo-de-perfil',  [UsuarioController::class, 'seleccionarRegistro'])->name('seleccionar-registro');
Route::get('persona-natural',  [UsuarioController::class, 'formPersonaNatural'])->name('persona-natural');
Route::post('registro-persona-natural',  [UsuarioController::class, 'formSavePersonaNatural'])->name('registro-persona-natural');


Route::get('/registro', [UsuarioController::class, 'registro'])->name('registro');
Route::post('guardar-registro',  [UsuarioController::class, 'guardarRegistro'])->name('registro-save');

Route::get('persona-juridica',  [UsuarioController::class, 'personaJuridica'])->name('persona-juridica');
Route::post('continuar-persona-juridica',  [UsuarioController::class, 'continuarPersonaJuridica'])->name('continuar-juridica');

Route::get('continuar-persona-juridica-datos-perfil',  [UsuarioController::class, 'continuarPersonaJuridicaView'])->name('continuar-juridica-view');

Route::post('registro-persona-juridica',  [UsuarioController::class, 'saveRucPersonaJuridica'])->name('registro-persona-juridica');
Route::get('/validar-RUC',  [UsuarioController::class, 'apiRUC'])->name('validar-RUC');
Route::get('/validar-DNI',  [UsuarioController::class, 'apiDNI'])->name('validar-DNI');
Route::get('registro-referido', [UsuarioController::class, 'registroReferido'])->name('registro-referido');



