@extends('layouts.index_app')
@section('content')
    <style>
        .select2-container--default .select2-selection--single{
            background-color:  #F5F8FA !important;
            border: 0px !important;
        }
    </style>

    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Mi perfil</h1>

        <form method="post" action="{{ route('actualizar-perfil') }}" enctype="multipart/form-data">
            <div class="card-blanco mt-5 card-blanco-padding-left-right" style="position: relative;">
                <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                    <div class="col-sm-4 text-center text-sm-start">
                        <h5 class="arlrdbd my-0">Datos del perfil</h5>
                    </div>
                    <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                        <button type="submit" class="btn btn-login PoppinsMedium">Guardar</button>
                    </div>
                </div>
                <div class="row mx-0 mb-5 mt-5">
                    <div class="col-sm-5 col-md-4">
                        <p class="PoppinsRegular p-color-ultimas my-0">Foto de perfil (200 x 200)</p>
                    </div>
                    <div class="col-sm-7 mt-4 mt-sm-0">
                        <div class="div-edit-perfil">
                            <div class="div-img-edit-perfil">
                                @if(!empty(\Session::get('imagen_perfil') ))
                                    <img id="img-perfil" src="/img/perfiles/{{\Session::get('imagen_perfil') }}" style="width: 183px; height: 183px;">
                                @else
                                    <img id="img-perfil" src="" style="width: 183px; height: 183px; display: none;">
                                @endif
                            </div>

                            <span class="btn btn-edit-perfil PoppinsMedium">
							<i class="fa-solid fa-pencil"></i>
							<input type="file" name="imagen_perfil" id="imagen_perfil">
						</span>


                            <a href="#" onclick="deleteImage()">
                                <span class="btn btn-edit-cancel-perfil PoppinsMedium">
                            <i class="fa-solid fa-xmark"></i>
                            </span>
                            </a>

                        </div>
                    </div>
                </div>
                <div class="row mx-0 mb-4 align-items-center">
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Nombres y apellidos:</p>
                    </div>
                    <div class="col-md-8">
                        <div class="row mx-0 mb-4">
                            <div class="col-12 ps-0 pe-0 pe-sm-2">
                                <input type="text" name="nombres" value="{{$perfil->nombres}}" class="input-cupon PoppinsRegular" required>
                            </div>
                        </div>

                        <div class="row mx-0">
                            <div class="col-6 ps-0 pe-0 pe-sm-2">
                                <input type="text" name="apellido_paterno" value="{{$perfil->apellido_paterno}}" class="input-cupon PoppinsRegular" required>
                            </div>
                            <div class="col-6 ps-0 ps-sm-2 pe-0">
                                <input type="text" name="apellido_materno" value="{{$perfil->apellido_materno}}" class="input-cupon PoppinsRegular" required>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row mx-0 mb-4 align-items-center">
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Tipo de documento:</p>
                    </div>
                    <div class="col-md-8" style="position: relative;">
                        <select class="select-fomr" name="tipo_doc" required>
                            <option value="" disabled selected>Debe seleccionar una Opción</option>
                            <option value="1" @if($perfil->tipo_doc=='1') selected @endif>DNI</option>
                            <option value="2" @if($perfil->tipo_doc=='2') selected @endif>Pasaporte</option>
                            <option value="1" @if($perfil->tipo_doc=='3') selected @endif>CE</option>
                            <option value="2" @if($perfil->tipo_doc=='4') selected @endif>PTP</option>
                        </select>
                        <i class="fa fa-angle-down icon-select"></i>
                    </div>
                </div>
                <div class="row mx-0 mb-4 align-items-center">
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Número de Documento:</p>
                    </div>
                    <div class="col-md-8">
                        <input type="number" name="nro_doc" required value="{{$perfil->nro_doc}}" class="input-cupon PoppinsRegular">
                    </div>
                </div>

                <div class="row mx-0 mb-4 align-items-center">
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Nacionalidad:</p>
                    </div>
                    <div class="col-md-8">
                        <select name="id_nacionalidad" class="select-fomr" id="select-nacionalidad">
                            <option value="" disabled selected>Debe seleccionar una Opción</option>
                            @if(count($paises)>0)
                                @foreach($paises as $pais)
                                    <option value="{{$pais->id}}" @if($perfil->id_nacionalidad==$pais->id) selected @endif >{{$pais->nombre}}</option>
                                @endforeach
                            @endif
                        </select>
                        <i class="fa fa-angle-down icon-select"></i>
                    </div>
                </div>

                <div class="row mx-0 mb-4 align-items-center">
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Fecha de Nacimiento:</p>
                    </div>
                    <div class="col-md-8 div-form-login">
                        <input type="date" name="fecha_nacimiento" value="{{$perfil->fecha_nacimiento}}" id="fecha-nacimiento" class="input-login PoppinsRegular">
                    </div>
                </div>

                <div class="row mx-0 mb-4 align-items-center">
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Número de teléfono:</p>
                    </div>
                    <div class="col-md-8">
                        <input type="text" name="nro_celular" required value="{{$perfil->nro_celular}}" class="input-cupon PoppinsRegular">
                    </div>
                </div>
                <div class="row mx-0 mb-4 align-items-center">
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Correo electrónico:</p>
                    </div>
                    <div class="col-md-8">
                        <input type="text" name="email" readonly value="{{$perfil->usuario->email}}" class="input-cupon PoppinsRegular">
                    </div>
                </div>
                <div class="row mx-0 mb-4 align-items-center">
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Ocupación:</p>
                    </div>
                    <div class="col-md-8" style="position: relative;">
                        <select name="id_ocupacion" required class="select-fomr">
                            <option value="" disabled selected>Debe seleccionar una Opción</option>
                            @if(count($ocupaciones)>0)
                                @foreach($ocupaciones as $ocupacion)
                                    <option value="{{$ocupacion->id}}" @if($perfil->id_ocupacion==$ocupacion->id) selected @endif >{{$ocupacion->nombre}}</option>
                                @endforeach
                            @endif
                        </select>
                        <i class="fa fa-angle-down icon-select"></i>
                    </div>
                </div>

                @if($perfil->tipo=='2')
                    <div class="row mx-0 mb-4 align-items-center">
                        <div class="col-md-4 mb-2 mb-md-0">
                            <p class="PoppinsRegular p-color-ultimas my-0">Actividad Económica:</p>
                        </div>
                        <div class="col-md-8" style="position: relative;">
                            <select name="id_actividad_economica" id="id_actividad_economica" required class="select-fomr">
                                <option value="" disabled selected>Debe seleccionar una Opción</option>
                                <option value="0" @if($perfil->id_actividad_economica==0) selected @endif>Otros</option>
                                @if(count($actividad_economica)>0)
                                    @foreach($actividad_economica as $actividad)
                                        <option value="{{$actividad->id}}" @if($perfil->id_actividad_economica==$actividad->id) selected @endif >{{$actividad->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <i class="fa fa-angle-down icon-select"></i>
                        </div>
                    </div>
                    <div class="row mx-0 mb-4 align-items-center" id="div-actividad-otros" @if($perfil->id_actividad_economica==0) style="display: flex;" @else style="display: none;" @endif >
                        <div class="col-md-4 mb-2 mb-md-0">
                            <p class="PoppinsRegular p-color-ultimas my-0">Nombre de la Actividad Económica:</p>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="actividad_economica" value="{{$perfil->actividad_economica}}" id="actividad_economica" class="input-cupon PoppinsRegular">
                        </div>
                    </div>
                @endif

                <div class="row mx-0 mb-5 align-items-center">
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Publicamente expuesto:</p>
                    </div>
                    <div class="col-md-8" style="position: relative;">
                        <select class="select-fomr" name="publicamente_expuesto" id="publicamente_expuesto" required>
                            <option value="" disabled selected>Debe seleccionar una Opción</option>
                            <option value="1" @if($perfil->publicamente_expuesto=='1') selected @endif>Si</option>
                            <option value="0" @if($perfil->publicamente_expuesto=='0') selected @endif>No</option>
                        </select>
                        <i class="fa fa-angle-down icon-select"></i>
                    </div>
                </div>
                <div class="row mx-0 mb-5 align-items-center" id="div-institucion" @if($perfil->publicamente_expuesto=='0') style="display: none;" @endif >
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Institución:</p>
                    </div>
                    <div class="col-md-8" style="position: relative;">
                        <select class="select-fomr" name="id_institucion" id="id_institucion">
                            <option value="" disabled selected>Debe seleccionar una Opción</option>
                            <option value="otros" @if($perfil->id_institucion==null) selected @endif>Otros</option>
                            @if(count($instituciones)>0)
                                @foreach($instituciones as $institucion)
                                    <option value="{{$institucion->id}}" @if($perfil->id_institucion==$institucion->id) selected @endif >{{$institucion->nombre}}</option>
                                @endforeach
                            @endif
                        </select>
                        <i class="fa fa-angle-down icon-select"></i>
                    </div>
                </div>

                @if(is_null($perfil->id_institucion) && !is_null($perfil->nombre_institucion))
                <div class="row mx-0 mb-5 align-items-center" id="div-nombre-institucion">
                @else
                    <div class="row mx-0 mb-5 align-items-center" id="div-nombre-institucion" style="display: none;">
                @endif
                        <div class="col-md-4 mb-2 mb-md-0">
                            <p class="PoppinsRegular p-color-ultimas my-0">Nombre de la Institución:</p>
                        </div>
                        <div class="col-md-8" style="position: relative;">
                        <input type="text" name="nombre_institucion" value="{{$perfil->nombre_institucion}}" class="input-login PoppinsRegular">
                    </div>
                </div>

                <div class="row mx-0 mb-5 align-items-center" id="div-cargo" @if($perfil->publicamente_expuesto=='0') style="display: none;" @endif >
                    <div class="col-md-4 mb-2 mb-md-0">
                        <p class="PoppinsRegular p-color-ultimas my-0">Cargo:</p>
                    </div>
                    <div class="col-md-8" style="position: relative;">
                        <select class="select-fomr" name="id_cargo">
                            <option value="" disabled selected>Debe seleccionar una Opción</option>
                            @if(count($cargos)>0)
                                @foreach($cargos as $cargo)
                                    <option value="{{$cargo->id}}" @if($perfil->id_cargo==$cargo->id) selected @endif >{{$cargo->nombre}}</option>
                                @endforeach
                            @endif
                        </select>
                        <i class="fa fa-angle-down icon-select"></i>
                    </div>
                </div>

                <div class="row mx-0  align-items-center row-border-top pb-3 pt-4">
                    <div class="col-sm-4 text-center text-sm-start">
                        <h5 class="arlrdbd my-0">Cambio de contraseña</h5>
                    </div>
                    <div class="col-sm-8 text-center text-sm-start mt-3 mt-sm-0">
                        <button class="btn btn-login PoppinsMedium" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Cambiar contraseña</button>
                    </div>
                </div>
            </div>
            @csrf
        </form>


    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body px-0 py-0">
                    <div class="modal-padding-left-right pt-5 pb-5">
                        <h1 class="PoppinsBold titulo-conten mb-0 mt-5 text-center">Ingresar nueva contraseña</h1>
                        <div class="row mx-0 justify-content-start mt-5">
                            <form method="post" action="{{ route('cambiar-contrasena') }}">
                                @csrf
                                <div class="col-12 mb-3 px-sm-1">
                                    <div class="div-form-login">
                                        <label class="label-login PoppinsRegular">Contraseña nueva</label>
                                        <input type="password" id="pass1" name="password" class="input-login PoppinsRegular" required>
                                        <a id="icon-mostrar" class="button-eye" onclick="mostrarPass();"><i id="eye" class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class="col-12 mb-3 px-sm-1">
                                    <div class="div-form-login">
                                        <label class="label-login PoppinsRegular">Repetir contraseña nueva</label>
                                        <input type="password" name="password_confirm" id="pass2" class="input-login PoppinsRegular" required>
                                        <a id="icon-mostrar2" class="button-eye" onclick="mostrarPass2();"><i id="eye2" class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #5A5A5A !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>

        var estado=0, estado2=0;

        var mostrarPass = () => {
            if(estado==0)
            {
                estado=1;
                $("#pass1").attr('type','text');
                $("#eye").removeClass("fa-eye");
                $("#eye").addClass("fa-eye-slash");
            }
            else
            {
                estado=0;
                $("#eye").addClass("fa-eye");
                $("#eye").removeClass("fa-eye-slash");
                $("#pass1").attr('type','password');

            }
        };

        var mostrarPass2 = () => {
            if(estado2==0)
            {
                estado2=1;
                $("#pass2").attr('type','text');
                $("#eye2").removeClass("fa-eye");
                $("#eye2").addClass("fa-eye-slash");
            }
            else
            {
                estado2=0;
                $("#eye2").addClass("fa-eye");
                $("#eye2").removeClass("fa-eye-slash");
                $("#pass2").attr('type','password');

            }
        };

        var deleteImage = () => {
            $("#img-perfil").attr("src","");

            $("#img-perfil").css("display","none");

            var input = $("#imagen_perfil");

            input.val('').clone(true);


        };

        $( document ).ready(function() {
            $("#id_actividad_economica").change(function () {
                var valor=$(this).val();

                if(valor==0)
                {
                    $("#div-actividad-otros").fadeIn();
                    $("#actividad_economica").attr('required','required');
                }
                else
                {
                    $("#div-actividad-otros").fadeOut();
                    $("#actividad_economica").removeAttr('required');
                }

            });
            console.log($("#id_institucion").val());

            $('#imagen_perfil').change(function(e){
                var img = ' ';
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.onload = function (event) {
                    var img = new Image();
                    img.src = event.target.result;
                    img.onload = function() {
                        img=event.target.result;
                        $("#img-perfil").css("display","block");
                        $("#img-perfil").attr("src",img);

                    }
                }
            });

            $("#publicamente_expuesto").change(function () {
                var valor=$(this).val();

                if(valor=='0')
                {
                    $("#div-institucion").fadeOut();
                    $("#div-cargo").fadeOut();
                }
                else
                {
                    $("#div-institucion").fadeIn();
                    $("#div-cargo").fadeIn();
                }
            });

            $("#id_institucion").change(function () {
                if($(this).val()=='otros')
                {
                    $("#div-nombre-institucion").fadeIn();
                }
                else
                {
                    $("#div-nombre-institucion").fadeOut();
                }
            });
        });
    </script>
@endsection