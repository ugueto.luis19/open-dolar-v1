<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlertaProgramada extends Model
{
    use HasFactory;
    protected $table = 'alertas_programadas';
    protected $fillable = [
        'tipo',
        'condicional',
        'valor_deseado',
        'id_perfil',
        'estado'
    ];

}
