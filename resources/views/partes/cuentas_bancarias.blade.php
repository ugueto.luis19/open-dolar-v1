@if(count($cuentas_bancarias)>0)
    @foreach($cuentas_bancarias as $cuenta_bancaria)
        <div class="row">
            <div class="col-md-8 col-lg-9 col-xl-9">
                <div class="card card-c" style="margin: 11px 11px 11px 11px;padding: 0;border-radius: 11px;">
                    <div class="card-body" style="padding: 1rem 1rem !important;">
                        <div class="row align-items-center no-gutters">
                            <div class="col text-center">
                                <input type="checkbox" class="eliminar-cuentas" name="eliminar_cuentas[]" value="{{$cuenta_bancaria->id}}">
                            </div>
                            <div class="col-xl-2" style="text-align: center;">
                            @if($cuenta_bancaria->banco->logo==null || $cuenta_bancaria->banco->logo=='')
                                <span class="span-circulo-destino"></span>
                            @else
                                {!! $cuenta_bancaria->banco->logo !!}
                            @endif
                            </div>
                            <div class="col-xl-2">
                                <div class="fuente6-card"><span>Moneda</span></div><span class="contenido-cuentas">{{$cuenta_bancaria->moneda->nombre}}</span>
                            </div>
                            <div class="col-xl-2">
                                <div class="fuente6-card"><span>Tipo&nbsp;</span></div><span class="contenido-cuentas">{{$cuenta_bancaria->tipo->nombre}}</span>
                            </div>
                            <div class="col-xl-3">
                                <div class="fuente6-card"><span>Nº de cuenta&nbsp;</span ></div><span class="contenido-cuentas">{{$cuenta_bancaria->nro_cuenta}}</span>
                            </div>
                            <div class="col-xl-2">
                                <div class="fuente6-card"><span>Titular&nbsp;</span></div><span class="contenido-cuentas">{{$cuenta_bancaria->titular}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
<div class="mt-3">
    {{ $cuentas_bancarias->links() }}
</div>