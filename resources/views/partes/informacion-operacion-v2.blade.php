<?php
if($operacion->tipo=='0')
{
    $moneda_1='S/';
    $moneda_2='$';
    if(is_object($operacion->cupon))
    {
        $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta-$operacion->cupon->valor_soles;
    }
    else
    {
        $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta;
    }
}
else{
    $moneda_1='$';
    $moneda_2='S/';
    if(is_object($operacion->cupon))
    {
        $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra+($operacion->cupon->valor_soles/$operacion->tasa->tasa_compra);
    }
    else
    {
        $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra;
    }
}


switch($operacion->estado){
    case '0':

        $color_borde="#297BFC";
        $o_alerta = "o_pendientes";
        $class_alerta = "pendientes";
        $spanEstado = "Pendiente";
        $nombre_icono="pendiente.svg";
        break;
    case '1':

        $color_borde="#328397";
        $o_alerta = "o_proceso";
        $class_alerta = "proceso";
        $spanEstado = "En Proceso";
        $nombre_icono="proceso.svg";
        break;
    case '2':

        $color_borde="#31A745";
        $o_alerta = "o_realizada";
        $class_alerta = "realizada";
        $spanEstado = "Realizada";
        $nombre_icono="realizada.svg";
        break;
    case '3':

        $color_borde="#DC3545";
        $o_alerta = "o_devolucion";
        $class_alerta = "devolucion";
        $spanEstado = "En Devolución";
        $nombre_icono="devolucion.svg";
        break;
    case '4':

        $color_borde="#30A2B8";
        $o_alerta = "o_devuelta";
        $class_alerta = "devuelta";
        $spanEstado = "Devuelta";
        $nombre_icono="devuelta.svg";
        break;
    case '5':

        $color_borde="#E2B33B";
        $o_alerta = "o_observado";
        $class_alerta = "observado";
        $spanEstado = "Observada";
        $nombre_icono="observada.svg";
        break;
    case '6':

        $color_borde="#F30C04";
        $o_alerta = "o_anulado";
        $class_alerta = "anulado";
        $spanEstado = "Anulada";
        $nombre_icono= "anulada.svg";
        break;
    case '7':

        $color_borde="#31A745";
        $o_alerta = "o_verificado";
        $class_alerta = "verificado";
        $spanEstado = "Verificada";
        $nombre_icono= "realizada.svg";
        break;
    default:
        $color_borde=null;
        $o_alerta = null;
        $class_alerta = null;
        $spanEstado = null;
        $nombre_icono= null;
        break;
}

?>
<div class="card card-d py-2" style="min-height: 525px; border-radius: 11px; border: 1px solid {{$color_borde}} !important; background-color: rgba(253, 235, 235, 0.38) !important;">
    <div class="card-body">
        <div class="row align-items-center no-gutters">
            <div class="col mr-2">
                <div  class="mx-auto d-block justify-content-center align-items-center" style="text-align: center;">
                    <div style="margin-left: 105px; margin-top: -67px; margin-bottom: 25px; width: 95px;  height: 95px; -moz-border-radius: 50%;  -webkit-border-radius: 50%; border-radius: 50%; background: {{$color_borde}};">
                        <img src="/assets-web/img/{{$nombre_icono}}" style="margin-top: 25px;">

                    </div>
                </div>
                <div class="text-success font-weight-bold text-xs mb-1 text-center" style="margin-top: -15px;">
                     <span style="font-weight: bold; font-size: 24px;" class="{{$class_alerta}}_texto">
                         Orden {{$spanEstado}}
                     </span>
                </div>
                <div class="row">
                    <div class="col-lg-5 titulo-operacion ">Nº de Orden:</div>
                    <div class="col-lg-7 PoppinsRegular fuente2-card">{{$operacion->id}}</div>
                </div>
                @if(is_object($operacion->cupon))
                    <div class="row">
                        <div class="col-lg-5 titulo-operacion ">Cupon:</div>
                        <div class="col-lg-7 PoppinsRegular fuente2-card"> {{$operacion->cupon->codigo}}</div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-5 titulo-operacion ">Fecha y hora:</div>
                    <div class="col-lg-7 PoppinsRegular fuente2-card">{{date('d-m-Y H:i:s',strtotime($operacion->fecha_operacion))}}</div>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="row card-blanco-detalle col-sm-8 col-lg-8 d-flex align-items-center" style="margin:0 ; padding:0">
                        <div class="col-sm-6">
                            <div class="titulo-cambio"> Enviado </div>
                            <span class="detalle-orden">{{$moneda_1}} {{number_format($operacion->monto_enviado,2)}}</span>
                        </div>
                        <div class="col-sm-6">
                            <div class="titulo-cambio "> Recibido</div>
                            <span class="detalle-orden">{{$moneda_2}} {{number_format($operacion->monto_recibido,2)}}</span>
                        </div>
                    </div>
                    <div class="card-blanco-detalle col-sm-4 col-lg-4 d-flex align-items-center" style="margin-left: 10px; width:30%">
                        <div style="position: absolute;margin-left: -38px; -moz-border-radius: 50%;  -webkit-border-radius: 50%; border-radius: 50%; height:43px; padding: 10px; background: {{$color_borde}};">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23.542 23.542" style="height: 20px;margin-top: -5px;">
                                <path style="fill:#ffffff !important" id="Icon_awesome-coins" data-name="Icon awesome-coins" d="M0,18.636V20.6c0,1.623,3.954,2.943,8.828,2.943s8.828-1.32,8.828-2.943V18.636c-1.9,1.338-5.371,1.963-8.828,1.963S1.9,19.974,0,18.636ZM14.714,5.886c4.874,0,8.828-1.32,8.828-2.943S19.588,0,14.714,0,5.886,1.32,5.886,2.943,9.84,5.886,14.714,5.886ZM0,13.813v2.373c0,1.623,3.954,2.943,8.828,2.943s8.828-1.32,8.828-2.943V13.813c-1.9,1.563-5.375,2.373-8.828,2.373S1.9,15.376,0,13.813Zm19.128.506c2.635-.51,4.414-1.458,4.414-2.547V9.808a11.292,11.292,0,0,1-4.414,1.586ZM8.828,7.357C3.954,7.357,0,9,0,11.035s3.954,3.678,8.828,3.678,8.828-1.646,8.828-3.678S13.7,7.357,8.828,7.357ZM18.912,9.946c2.759-.5,4.63-1.471,4.63-2.589V5.394c-1.632,1.154-4.437,1.775-7.389,1.922A5.149,5.149,0,0,1,18.912,9.946Z" fill="#fff"/>
                            </svg>
                        </div>
                        <div class="">
                            <div class="titulo-cambio" style="margin-left: 2px;">  Tipo de Cambio</div>
                            <span class="detalle-orden d-flex justify-content-center">{{number_format($monto_tasa,2)}}</span>
                        </div>
                    </div>
                </div>
                @if(is_object($operacion->origen))
                    <div class="row">
                        <div class="titulo-operacion " style="margin-top: 11px;">Origen:</div>
                    </div>
                    <div class="row card-blanco-origen d-flex align-items-center">
                        <div class="col-sm-6">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -8px;"> Beneficiario</div>
                            <span class="card-cambio">{{$operacion->origen->titular}}</span>
                        </div>
                        <div class="col-sm-6">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -8px;"> Nº de operación</div>
                            <span class="card-cambio">
                            @if($operacion->nro_transferencia!='' && $operacion->nro_transferencia!=null)
                                {{$operacion->nro_transferencia}}
                            @else
                                --
                            @endif
                            </span>
                        </div>
                        <div class="col-sm-6" style="margin-top: -22px;">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -8px;"> Banco</div>
                            <span class="card-cambio">{{$operacion->origen->banco->nombre}}</span>
                        </div>
                        <div class="col-sm-6" style="margin-top: -22px;">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -8px;"> Enviado</div>
                            <span class="card-cambio">{{$moneda_1}} {{number_format($operacion->monto_enviado,2)}}</span>
                        </div>
                    </div>
                    
                @endif
                @if(($operacion->uso_opendolar!='' && $operacion->uso_opendolar!=null) && $operacion->uso_opendolar>0)
                    <div class="row card-cambio">
                        <div class="col-lg-5 titulo-operacion">OpenCreditos:</div>
                        <div class="col-lg-7">{{$operacion->uso_opendolar}}</div>
                    </div>
                @endif

                @if(is_object($operacion->destino))
                    <div class="row" style="margin-top: 15px;">
                        <div class="titulo-operacion ">Destino:</div>
                    </div>
                    <div class="row contenedor-destino div-scroll" style="width: 351px;">
                    <div class="row card-blanco-origen d-flex align-items-center" style="margin-bottom: 4px;">
                        
                        <div class="col-sm-6 card-cambio">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -4px;"> Beneficiario</div>
                            <span class="card-cambio">{{$operacion->destino->titular}}</span>
                        </div>
                        <div class="col-sm-6 card-cambio">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -4px;"> Cod. de Transferencia</div>
                            <span class="card-cambio">@if($operacion->nro_referencia!='' && $operacion->nro_referencia!=null)
                                {{$operacion->nro_referencia}}
                            @else
                                --
                            @endif
                            </span>
                        </div>
                        <div class="col-sm-6 card-cambio" style="margin-top: -22px;">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -4px;"> Banco</div>
                            <span class="card-cambio">{{$operacion->destino->banco->nombre}}</span>
                        </div>
                        <div class="col-sm-6 card-cambio"  style="margin-top: -22px;">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -4px;"> Recibido</div>
                            <span class="card-cambio">{{$moneda_2}} {{number_format($operacion->monto_recibido,2)}}</span> 
                        </div>
                    </div>
                    

                @endif
                
                @if($operacion->tipo_transferencia=='1')
                    @if(count($operacion->bancosDestinos)>0)
                        @foreach($operacion->bancosDestinos as $bancoDestino)
                        <div class="row card-blanco-origen d-flex align-items-center" style="margin-bottom: 4px;">
                        
                        <div class="col-sm-6 card-cambio">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -4px;"> Beneficiario</div>
                            <span class="card-cambio">{{$operacion->destino->titular}}</span>
                        </div>
                        <div class="col-sm-6 card-cambio">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -4px;"> Cod. de Transferencia</div>
                            <span class="card-cambio">@if($operacion->nro_referencia!='' && $operacion->nro_referencia!=null)
                                {{$operacion->nro_referencia}}
                            @else
                                --
                            @endif
                            </span>
                        </div>
                        <div class="col-sm-6 card-cambio" style="margin-top: -22px;">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -4px;"> Banco</div>
                            <span class="card-cambio">{{$operacion->destino->banco->nombre}}</span>
                        </div>
                        <div class="col-sm-6 card-cambio"  style="margin-top: -22px;">
                            <div class="titulo-cambio {{$class_alerta}}_texto" style="margin-bottom: -4px;"> Recibido</div>
                            <span class="card-cambio">{{$moneda_2}} {{number_format($operacion->monto_recibido,2)}}</span> 
                        </div>
                    </div>
                        @endforeach
                    @endif
                @endif
                </div>

                

            

            </div>
        </div>
    </div>
    <div class="mt-2" style="display: flex; justify-content: center; align-items: center;">
    @if($operacion->estado==0 || $operacion->estado==6)
        <a href="{{route('continuar-pendiente',['id'=>$operacion->id])}}" class="reactivar">
            @if($operacion->estado==0)
                Continuar Orden
            @else
                Reactivar Orden
            @endif
        </a>
    @endif
    @if($operacion->estado==0)
        <a href="{{route('anular-cambio',['id'=>$operacion->id])}}" class="anular-orden">
            Anular Orden
        </a>
    @endif
    </div>
</div>

