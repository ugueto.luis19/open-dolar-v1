@extends('layouts.index_app')
@section('content')
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start titulo-line-height mb-5">Recomienda y gana</h1>
        <div class="row mx-0 mt-5 mt-lg-5 justify-content-center">
            <div class="col-sm-10 col-md-8 col-lg-6 ps-0 pe-0 pe-lg-2">
                <div class="div-list-gana pb-2">
                    <div class="row">
                        <div class="col-md-6 col-xl-4 mb-4" style="padding-left:5px; padding-right:5px;"><span class="text-center PoppinsRegular etiqueta-referidos d-xl-flex justify-content-xl-center" style="text-align: center;margin-right: auto;margin-left: auto;">¡Comparte!</span>
                            <div class="card-recomienda py-2" style="border-radius: 11px;">
                                <div class="card-body" style="text-align: center;"><span><b>Comparte</b> tu código promocional con amigos y familiares.</span></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4 mb-4" style="padding-left:5px; padding-right:5px;"><span class="text-center PoppinsRegular etiqueta-referidos d-xl-flex justify-content-xl-center" style="text-align: center;margin-right: auto;margin-left: auto;">¡Cambio preferencial!</span>
                            <div class="card-recomienda  py-2" style="border-radius: 11px;">
                                <div class="card-body" style="text-align: center;"><span>Tus amigos ganarán un <b>cambio preferencial</b> en su primera operación.</span></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4 mb-4" style="padding-left:5px; padding-right:5px;"><span class="text-center PoppinsRegular etiqueta-referidos d-xl-flex justify-content-xl-center" style="text-align: center;margin-right: auto;margin-left: auto;">¡Tu ganas!</span>
                            <div class="card-recomienda  py-2" style="border-radius: 11px;">
                                <div class="card-body" style="text-align: center;"><span>Recibe 1 OpenCredito <b>por cada amigo</b> que realice su primera operación*</span></div>
                            </div>
                        </div>
                    </div>
                    <span class="text-center d-xl-flex texto-referidos justify-content-xl-center PoppinsLight" style="text-align: center;">* Tu amigo debe cambiar un monto mínimo de $100 dólares.</span>
                    <div class="col d-xl-flex justify-content-xl-center" style="margin-bottom: -8px;"><img class="d-xl-flex justify-content-xl-center align-items-xl-end" src="/assets-web/img/recomienda-y-gana.png" style="text-align: center;"></div>
                </div>
                <div class="card-blanco text-center card-azul color-blanco-texto py-4">
                    <h6 class="PoppinsBold mt-2 mb-0">Comparte ahora y gana:</h6>
                    <input type="hidden" id="codigo_asignado" value="{{ url('/registro-referido/?ref=').\Session::get('perfil')->codigo_asignado }}">
                    <p class="PoppinsRegular mt-3 mb-0">Mi código es:</p>
                    <h3 class="PoppinsBold ">{{ \Session::get('perfil')->codigo_asignado }} &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="copiarCodigo();"><i class="fas fa-print" style="color:white;"></i></a></h3>
                    <div class="d-flex justify-content-between div-redes-sociales mt-4">
                        <a href="">
                            <i class="fa-solid fa-share-nodes icon-redes-verde"></i>
                        </a>
                        <a href="">
                            <i class="fa-brands fa-whatsapp icon-redes-verde"></i>
                        </a>
                        <a href="">
                            <i class="fa-brands fa-facebook-f icon-redes-verde"></i>
                        </a>
                        <a href="">
                            <i class="fa-brands fa-linkedin-in icon-redes-verde"></i>
                        </a>
                        <a href="">
                            <i class="fa-brands fa-instagram icon-redes-verde"></i>
                        </a>
                        <a href="">
                            <i class="fa-solid fa-envelope icon-redes-verde"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-10 col-md-8 col-lg-6 col-heigth-porcentaje mt-5 mt-lg-0 ps-0 ps-lg-2 pe-0">
                <div class="card-blanco height-ultimas-operaciones text-center div-relative-open mt-3 mt-lg-0">
                    <img src="/assets-web/img/icon-moneda.png" class="img-moneda">
                    <p class="PoppinsRegular text-center p-color-azul mb-0 mt-4">Mis OpenCreditos</p>
                    <p class="PoppinsBold p-color-azul-precio text-center mt-2">{{$perfil->totalOpenCredit()}}</p>
                    <a href="{{route('dashboard')}}" class="btn btn-login PoppinsMedium  btn-padding-top-a mt-0 mb-3">Usar mis OpenCreditos</a>
                </div>
                <div class="card-blanco height-ultimas-operaciones text-center mt-3">
                    <h5 class="text-center arlrdbd mb-4">Mis últimos OpenCreditos</h5>
                    @if(count($comisiones)>0)
                        @foreach($comisiones as $comision)
                            <?php
                            if($comision->operacion->tipo=='0')
                            {
                                $moneda_1='S/';
                                $moneda_2='$';
                                $monto_tasa=$comision->operacion->tasa->tasa_compra;
                            }
                            else{
                                $moneda_1='$';
                                $moneda_2='S/';
                                $monto_tasa=$comision->operacion->tasa->tasa_venta;
                            }
                            ?>
                            <div class="d-flex justify-content-between px-5 align-items-center mb-3">
                                <p class="PoppinsBold my-0 p-color-ultimas">{{date('d-m-Y',strtotime($comision->operacion->fecha_operacion))}}</p>
                                <p class="PoppinsRegular my-0 p-color-ultimas">{{$comision->perfilHijo->nombres}} {{$comision->perfilHijo->apellido_paterno}} {{$comision->perfilHijo->apellido_materno}}</p>
                                <p class="PoppinsRegular my-0 p-color-ultimas">({{number_format($comision->monto,2)}})</p>
                            </div>
                        @endforeach
                    @endif
                    <a href="{{route('historial-referidos')}}" class="btn btn-login btn-azul PoppinsMedium px-5 btn-padding-top-a">Ver todos</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var copiarCodigo = () => {
            var tempInput = document.createElement("input");
            tempInput.style = "position: absolute; left: -1000px; top: -1000px";
            tempInput.value = $("#codigo_asignado").val();
            document.body.appendChild(tempInput);
            tempInput.select();
            document.execCommand("copy");
            document.body.removeChild(tempInput);

            toastr.success("Usted ha copiado su codigo asignado. Por favor compartir!");
        };
    </script>
@endsection