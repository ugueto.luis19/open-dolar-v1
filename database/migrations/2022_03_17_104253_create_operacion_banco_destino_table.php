<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperacionBancoDestinoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operacion_banco_destino', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_operacion');
            $table->foreign('id_operacion')->references('id')->on('operaciones')->onDelete('RESTRICT');
            $table->unsignedBigInteger('id_cuenta_destino');
            $table->foreign('id_cuenta_destino')->references('id')->on('cuentas_bancarias')->onDelete('RESTRICT');
            $table->decimal('monto_banco_destino',8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operacion_banco_destino');
    }
}
