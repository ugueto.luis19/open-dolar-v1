@extends('layouts.index_app')

@section('content')

    <div class="conten_panel conten_panel_login mt-3 mt-lg-0" style=" display: flex; align-content: center; justify-content: center;">
        <div class="div-conten-login pt-0 div-conten-contrasena">

        <div class="d-flex">
        <div class="pt-0 div-conten-contrasena">
            <div class="d-flex justify-content-center">
                <div class="d-flex justify-content-center" style="margin-top: -68px; margin-bottom: 25px; width: 95px;  height: 95px; -moz-border-radius: 50%;  -webkit-border-radius: 50%; border-radius: 50%; background: #1F9F99;">
                    <img src="/assets-web/img/pass.svg" style="    margin-left: -6px; width: 55px;">
                </div>
            </div>
			<h1 class="PoppinsBold text-center mb-0" >Reestablecer contraseña</h1>
            <div class="row mx-0 justify-content-center mt-5 row-padding-left-right-login">
                <form method="POST" action="{{ route('password.update') }}">
                @csrf

                <!-- Password Reset Token -->
                <input type="hidden" name="token" value="{{ $request->route('token') }}">

                <input type="hidden" name="email" value="{{ $request->email }}">

                    <div class="col-12 mb-3">
                        <div class="div-form-login">
                            <label class="label-login PoppinsRegular">Contraseña nueva</label>
                            <input type="password" name="password" id="pass1" required class="input-login PoppinsRegular" style="width: 385px;">
                            <a id="icon-mostrar" class="button-eye" onclick="mostrarPass();"><i id="eye" class="fa fa-eye"></i></a>
                        </div>
                    </div>
                    <div class="col-12 mb-4">
                        <div class="div-form-login">
                            <label class="label-login PoppinsRegular">Repetir contraseña nueva</label>
                            <input type="password" name="password_confirmation" id="pass2" required class="input-login PoppinsRegular" style="width: 385px;">
                            <a id="icon-mostrar2" class="button-eye" onclick="mostrarPass2();"><i id="eye2" class="fa fa-eye"></i></a>
                        </div>
                    </div>
                    <!--div class="col-12 text-center px-sm-5">
                        <div class="form-check text-start">
                          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                          <label class="form-check-label p-login-contrasena" for="flexCheckDefault">
                            Acepto los <a href="">términos y condiciones</a> 
                          </label>
                        </div>
                    </div-->
                    <div class="col-12 text-center mt-4">
                        <button type="submit" class="btn btn-login PoppinsMedium px-5">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        var estado=0, estado2=0;

        var mostrarPass = () => {
            if(estado==0)
            {
                estado=1;
                $("#pass1").attr('type','text');
                $("#eye").removeClass("fa-eye");
                $("#eye").addClass("fa-eye-slash");
            }
            else
            {
                estado=0;
                $("#eye").addClass("fa-eye");
                $("#eye").removeClass("fa-eye-slash");
                $("#pass1").attr('type','password');

            }
        };

        var mostrarPass2 = () => {
            if(estado2==0)
            {
                estado2=1;
                $("#pass2").attr('type','text');
                $("#eye2").removeClass("fa-eye");
                $("#eye2").addClass("fa-eye-slash");
            }
            else
            {
                estado2=0;
                $("#eye2").addClass("fa-eye");
                $("#eye2").removeClass("fa-eye-slash");
                $("#pass2").attr('type','password');

            }
        };
    </script>
@endsection