@extends('layouts.index_app')
@section('content')
    <style>
        .btn-tabs{
            border-radius: 18px !important;
            background-color: transparent !important;
            color: #137188 !important;
            border-color: #C6D05D;
            height: 3.125rem;
        }
        .active>button{
            border-radius: 18px !important;
            background-color: #C6D05D !important;
            color: #137188 !important;
            height: 3.125rem;
        }
    </style>
    <div class="conten_panel_administrador">
        <h3 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Información Detallada del Perfil</h3>
        <div>
            <strong>Nombre Completo:</strong> {{$perfil->nombres}} {{$perfil->apellido_paterno}} {{$perfil->apellido_materno}}
            <br>
            <strong>Correo:</strong> {{$perfil->usuario->email}}
            <br>
            <br>
            <a href="{{route('ver-perfiles',['id_usuario'=>$perfil->id_cuenta])}}" style="text-decoration: none;">
                <p class="PoppinsRegular p-color-ultimas my-0 ms-3">< Regresar</p>
            </a>
        </div>

        <ul class="nav nav-tabs mt-sm-3">
            <li class="tabs-li active" data-tab="perfil-tab">
                <button type="button" class="btn btn-tabs PoppinsMedium">Información del Perfil</button>
            </li>
            <li class="tabs-li" data-tab="cuentas-bancarias-tab">
                <button type="button" class="btn btn-tabs PoppinsMedium">Cuentas Bancarias</button>
            </li>
            <li class="tabs-li" data-tab="operaciones-tab">
                <button type="button" class="btn btn-tabs PoppinsMedium">Operaciones</button>
            </li>
            <li class="tabs-li" data-tab="alertas-tab">
                <button type="button" class="btn btn-tabs PoppinsMedium">Alertas</button>
            </li>
            <li class="tabs-li" data-tab="referidos-tab">
                <button type="button" class="btn btn-tabs PoppinsMedium">Tabla de Referidos Opencreditos</button>
            </li>
        </ul>

        <div class="tab-content">
            <div id="perfil-tab" class="tab-pane active">
                <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right" style="height: auto; min-height: 750px;">
                    <div class="row mx-0 mb-4 align-items-end mt-5 justify-content-center justify-content-md-start">
                        <div class="col-9 col-sm-4 text-center text-md-start">
                            @if(!empty(\Session::get('imagen_perfil') ) || \Session::get('imagen_perfil') != null)
                                <img class="img-perfil-admi" src="/img/perfiles/{{\Session::get('imagen_perfil') }}">
                            @else
                                <img src="/assets-web/img/Grupo 380.png" class="img-perfil-admi">
                            @endif


                        </div>
                        <div class="col-sm-10 col-md-8 col-lg-7 mt-4 mt-md-0">
                            @if(is_object($verificacion))
                                <div class="d-flex align-items-center wrap-guella">
                                    @if($verificacion->estado=='1')
                                        <div class="div-huella">
                                            <i class="fa-solid fa-fingerprint icn-huella"></i>
                                            <i class="fa-solid fa-circle-check icon-check-absolute"></i>
                                        </div>
                                    @else
                                        <div class="div-huella">
                                            <i class="fa-solid fa-fingerprint icn-huella"></i>
                                            <i class="fa-solid fa-circle-xmark icon-close-absolute"></i>
                                        </div>
                                    @endif
                                </div>
                            @else
                                <div class="d-flex align-items-center mt-2 wrap-guella">
                                    <div class="div-huella">
                                        <i class="fa-solid fa-fingerprint icn-huella"></i>
                                        <i class="fa-solid fa-circle-xmark icon-close-absolute"></i>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    @if($perfil->ruc!=null && $perfil->ruc!='')
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">RUC:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->ruc}}</p>
                            </div>
                        </div>
                    @endif

                    @if($perfil->nombre_juridico!=null && $perfil->nombre_juridico!='')
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">Nombre Juridico:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->nombre_juridico}}</p>
                            </div>
                        </div>
                    @endif

                    @if($perfil->direccion_juridico!=null && $perfil->direccion_juridico!='')
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">Dirección:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->direccion_juridico}}</p>
                            </div>
                        </div>
                    @endif

                    @if(is_object($perfil->actividad_economica_obj))
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">Actividad Económica:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->actividad_economica_obj->nombre}}</p>
                            </div>
                        </div>
                    @else
                        @if($perfil->id_actividad_economica==0)
                            <div class="row mx-0 mb-4">
                                <div class="col-sm-5 col-md-4">
                                    <p class="PoppinsRegular p-color-ultimas my-0">Actividad Económica:</p>
                                </div>
                                <div class="col-sm-7">
                                    <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->actividad_economica}}</p>
                                </div>
                            </div>
                        @endif
                    @endif

                    @if(intval($perfil->representante)==0 && $perfil->representante!=null && $perfil->representante!='')
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">Tipo de Documento del Representante Legal:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">
                                    @if($perfil->tipo_doc_repre=='1')
                                        DNI
                                    @elseif($perfil->tipo_doc_repre=='2')
                                        Pasaporte
                                    @elseif($perfil->tipo_doc_repre=='3')
                                        CE
                                    @elseif($perfil->tipo_doc_repre=='4')
                                        PTP
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">Número de Documento del Representante Legal:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">
                                    {{$perfil->nro_doc_repre}}
                                </p>
                            </div>
                        </div>
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">Nombre del Representante Legal:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">
                                    {{$perfil->nombres_repre}}
                                </p>
                            </div>
                        </div>
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">Apellido Paterno del Representante Legal:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">
                                    {{$perfil->apellido_paterno_repre}}
                                </p>
                            </div>
                        </div>
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">Apellido Materno del Representante Legal:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">
                                    {{$perfil->apellido_materno_repre}}
                                </p>
                            </div>
                        </div>
                    @endif

                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Nombres y Apellidos:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->nombres}} {{$perfil->apellido_paterno}} {{$perfil->apellido_materno}}</p>
                        </div>
                    </div>
                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Tipo de Documento:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">
                                @if($perfil->tipo_doc=='1')
                                    DNI
                                @elseif($perfil->tipo_doc=='2')
                                    Pasaporte
                                @elseif($perfil->tipo_doc=='3')
                                    CE
                                @elseif($perfil->tipo_doc=='4')
                                    PTP
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Número de Documento:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->nro_doc}}</p>
                        </div>
                    </div>

                    @if(is_object($perfil->nacionalidad))
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">Nacionalidad:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->nacionalidad->nombre}}</p>
                            </div>
                        </div>
                    @endif

                    @if($perfil->fecha_nacimiento!='' && $perfil->fecha_nacimiento!=null)
                        <div class="row mx-0 mb-4">
                            <div class="col-sm-5 col-md-4">
                                <p class="PoppinsRegular p-color-ultimas my-0">Fecha Nacimiento:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="PoppinsRegular p-color-ultimas my-0">{{date('d-m-Y',strtotime($perfil->fecha_nacimiento))}}</p>
                            </div>
                        </div>
                    @endif

                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Número de teléfono:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->nro_celular}}</p>
                        </div>
                    </div>
                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Correo electrónico:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->usuario->email}}</p>
                        </div>
                    </div>
                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Ocupación:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">{{$perfil->ocupacion->nombre}}</p>
                        </div>
                    </div>

                    <div class="row mx-0 mb-4">
                        <div class="col-sm-5 col-md-4">
                            <p class="PoppinsRegular p-color-ultimas my-0">Publicamente expuesto:</p>
                        </div>
                        <div class="col-sm-7">
                            <p class="PoppinsRegular p-color-ultimas my-0">
                                @if($perfil->publicamente_expuesto=='1')
                                    Si
                                @else
                                    No
                                @endif
                            </p>
                        </div>
                    </div>
                    @if($perfil->publicamente_expuesto=='1')
                        @if(is_object($perfil->institucion))
                            <div class="row mx-0 mb-4">
                                <div class="col-sm-5 col-md-4">
                                    <p class="PoppinsRegular p-color-ultimas my-0">Institución:</p>
                                </div>
                                <div class="col-sm-7">
                                    <p class="PoppinsRegular p-color-ultimas my-0">
                                        {{$perfil->institucion->nombre}}
                                    </p>
                                </div>
                            </div>
                        @else
                            <div class="row mx-0 mb-4">
                                <div class="col-sm-5 col-md-4">
                                    <p class="PoppinsRegular p-color-ultimas my-0">Institución:</p>
                                </div>
                                <div class="col-sm-7">
                                    <p class="PoppinsRegular p-color-ultimas my-0">
                                        {{$perfil->nombre_institucion}}
                                    </p>
                                </div>
                            </div>
                        @endif
                        @if(is_object($perfil->cargo))
                            <div class="row mx-0 mb-4">
                                <div class="col-sm-5 col-md-4">
                                    <p class="PoppinsRegular p-color-ultimas my-0">Cargo:</p>
                                </div>
                                <div class="col-sm-7">
                                    <p class="PoppinsRegular p-color-ultimas my-0">
                                        {{$perfil->cargo->nombre}}
                                    </p>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
            <div id="cuentas-bancarias-tab" class="tab-pane">
                <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right" style="height: auto; min-height: 750px;">
                    <div class="div-scrolll">
                        <table class="table table-border-cero" id="mis-cuentas">
                            <thead>
                            <tr>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Banco</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Moneda</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Tipo</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Nro Cuenta</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Propiedad</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Estado</th>
                            </tr>
                            </thead>
                            <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                            @if(count($perfil->cuentasBancarias)>0)
                                @foreach($perfil->cuentasBancarias as $cuenta_bancaria)
                                    <tr class="tr-border-top">
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            <div class="text-center d-flex align-items-center td-color-datos PoppinsMedium justify-content-center">
                                                {{$cuenta_bancaria->banco->nombre}} &nbsp;&nbsp;
                                                @if($cuenta_bancaria->banco->logo==null || $cuenta_bancaria->banco->logo=='')
                                                    <span class="span-circulo-destino"></span>
                                                @else
                                                    <img src="/assets-web/img/{{$cuenta_bancaria->banco->logo}}">
                                                @endif
                                            </div>
                                        </th>
                                        <td class="text-center td-color-datos PoppinsMedium">
                                            {{$cuenta_bancaria->moneda->nombre}}
                                        </td>
                                        <td class="text-center td-color-datos PoppinsMedium">
                                            {{$cuenta_bancaria->tipo->nombre}}
                                        </td>
                                        <td class="text-center td-color-datos PoppinsMedium">
                                            {{$cuenta_bancaria->nro_cuenta}}
                                        </td>
                                        <td class="text-center td-color-datos PoppinsMedium">
                                            @if($cuenta_bancaria->propiedad=='0')
                                                Terceros
                                            @else
                                                Propia
                                            @endif
                                        </td>
                                        <td class="text-center td-color-datos PoppinsMedium">
                                            @if($cuenta_bancaria->estado=='0')
                                                <strong style="color: red;">Anulada</strong>
                                            @else
                                                <strong style="color: green;">Activa</strong>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="operaciones-tab" class="tab-pane">
                <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right" style="height: auto; min-height: 750px;">
                    <div class="div-form-login div-select-widht-operaciones">
                        <select class="select-fomr-azul" onchange="filtradoOperacion(this);">
                            <option value="" selected>Filtrado por estado de la Operación</option>
                            <option value="0">Pendiente</option>
                            <option value="1">En Proceso</option>
                            <option value="2">Realizado</option>
                            <option value="3">Devolución</option>
                            <option value="4">Devuelto</option>
                            <option value="5">Observados</option>
                            <option value="6">Anulado</option>
                        </select>
                        <i class="fa fa-angle-down icon-select"></i>
                    </div>
                    <div class="div-scrolll mt-2">
                        <table class="table mt-4 table-border-cero" id="operaciones">
                            <thead>
                            <tr>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center"># Orden</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Estado</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Fecha</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Enviado</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Recibido</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Cupon</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Cambio</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Opciones</th>
                            </tr>
                            </thead>
                            <tbody class="tbody-border-cero">
                            @if(count($operaciones)>0)
                                @foreach($operaciones as $operacion)
                                    <?php
                                    if($operacion->tipo=='0')
                                    {
                                        $moneda_1='S/';
                                        $moneda_2='$';
                                        if(is_object($operacion->cupon))
                                        {
                                            $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta-$operacion->cupon->valor_soles;
                                        }
                                        else
                                        {
                                            $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta;
                                        }
                                    }
                                    else{
                                        $moneda_1='$';
                                        $moneda_2='S/';
                                        if(is_object($operacion->cupon))
                                        {
                                            $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra+($operacion->cupon->valor_soles/$operacion->tasa->tasa_compra);
                                        }
                                        else
                                        {
                                            $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra;
                                        }
                                    }
                                    switch($operacion->estado){
                                        case '0':
                                            $o_alerta = "o_pendientes";
                                            $class_alerta = "pendientes";
                                            $spanEstado = "Pendiente";
                                            break;
                                        case '1':
                                            $o_alerta = "o_proceso";
                                            $class_alerta = "proceso";
                                            $spanEstado = "En Proceso";
                                            break;
                                        case '2':
                                            $o_alerta = "o_realizada";
                                            $class_alerta = "realizada";
                                            $spanEstado = "Realizado";
                                            break;
                                        case '3':
                                            $o_alerta = "o_devolucion";
                                            $class_alerta = "devolucion";
                                            $spanEstado = "Devolución";
                                            break;
                                        case '4':
                                            $o_alerta = "o_devuelta";
                                            $class_alerta = "devuelta";
                                            $spanEstado = "Devuelto";
                                            break;
                                        case '5':
                                            $o_alerta = "o_observado";
                                            $class_alerta = "observado";
                                            $spanEstado = "Observados";
                                            break;
                                        case '6':
                                            $o_alerta = "o_anulado";
                                            $class_alerta = "anulado";
                                            $spanEstado = "Anulado";
                                            break;
                                        case '7':
                                            $o_alerta = "o_realizada";
                                            $class_alerta = "realizada";
                                            $spanEstado = "Verificado";
                                            break;
                                        default:
                                            $o_alerta = null;
                                            $class_alerta = null;
                                            $spanEstado = null;
                                            break;
                                    }
                                    ?>
                                    <tr class="tr-border-top operaciones {{ $o_alerta }}">
                                        <th scope="row" class="text-start">
                                            {{$operacion->id}}
                                        </th>
                                        <th scope="row" class="text-start">
                                            <span class="PoppinsMedium span-radius-datos {{ $class_alerta }}">{{ $spanEstado }}</span>
                                        </th>
                                        <td class="text-start td-color-datos PoppinsMedium">
                                            {{date('d-m-Y H:i:s',strtotime($operacion->fecha_operacion))}}
                                        </td>
                                        <td class="text-start td-color-datos PoppinsMedium">
                                            {{$moneda_1}} {{number_format($operacion->monto_enviado,2)}}
                                        </td>
                                        <td class="text-start td-color-datos PoppinsMedium">{{$moneda_2}} {{number_format($operacion->monto_recibido,2)}}</td>
                                        <td class="text-end td-color-datos PoppinsMedium">
                                            @if(is_object($operacion->cupon))
                                                {{$operacion->cupon->codigo}}
                                            @else
                                                --
                                            @endif
                                        </td>
                                        <td class="text-start td-color-datos PoppinsMedium">({{number_format($monto_tasa,3)}})</td>
                                        <td class="text-start td-color-datos PoppinsMedium">
                                            <a href="#" data-idope="{{$operacion->id}}"  class="abrir-detalle link-descarga-table p-2" style="text-decoration: none; margin-left: 10px;">
                                                <i class="fa-solid fa-eye" style="font-size: 18px; width: 35px;"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="alertas-tab" class="tab-pane">
                <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right" style="height: auto; min-height: 750px;">
                    @if(is_object($perfil->alerta))
                        <?php
                        $tipo = ''; $condicional = '';

                        switch($perfil->alerta->tipo){
                            case '1':
                                $tipo = 'compra';
                                break;
                            case '2':
                                $tipo = 'venta';
                                break;
                        }

                        switch($perfil->alerta->condicional){
                            case '1':
                                $condicional = 'Menor o Igual';
                                break;
                            case '2':
                                $condicional = 'Igual';
                                break;
                            case '3':
                                $condicional = 'Mayor o Igual';
                                break;
                        }

                        ?>
                        <div class="col-sm-4 px-0 px-sm-2 px-lg-4 mb-5">
                            <div class="div-notificacion text-center px-3">
                                <img src="/assets-web/img/icon-camapana.png" class="img-campana">
                                <p class="PoppinsRegular my-0 p-alerts">El valor de {{ $tipo }} del dólar es {{ $condicional }} a S/. {{ $perfil->alerta->valor_deseado }}</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div id="referidos-tab" class="tab-pane">
                <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right" style="height: auto; min-height: 750px;">
                    <table class="table table-border-cero" id="mis-ref">
                        <thead>
                        <tr>
                            <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Fecha</th>
                            <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Nombre Completo</th>
                            <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Open Credit</th>
                        </tr>
                        </thead>
                        <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                        @if(count($comisiones)>0)
                            @foreach($comisiones as $comision)
                                <tr class="tr-border-top">
                                    <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                        {{date('d-m-Y',strtotime($comision->operacion->fecha_operacion))}}
                                    </th>
                                    <td class="text-center td-color-datos PoppinsMedium">
                                        {{$comision->perfilHijo->nombres}} {{$comision->perfilHijo->apellido_paterno}} {{$comision->perfilHijo->apellido_materno}}
                                    </td>
                                    <td class="text-center td-color-datos PoppinsMedium">
                                        {{number_format($comision->monto,2)}}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content" style="height: 700px; width: 50%; margin-left: 206px;">
                <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body px-0 py-0" style="position: relative; background-color: #f9f9f9; border-radius:25px;">
                    <div class="row mx-0 justify-content-center mt-5 align-items-center" style="height: 600px">
                        <div class="col-12">
                            <div class="row" id="div-informacion-operacion">

                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="button" id="buttonRegresar" class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-5" style="color: #137188 !important;">Regresar</button><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#mis-cuentas').DataTable();
            $('#operaciones').DataTable( {
                "order": [[ 0, "desc" ]]
            } );
            $('#mis-ref').DataTable();

            $(".tabs-li").click(function () {
                var id_elemento=$(this).data('tab');
                $(".tabs-li").removeClass('active');
                $(".tab-pane").removeClass('active');
                $(this).addClass('active');
                $("#"+id_elemento).addClass('active');
            });
            $("#buttonRegresar").click(function () {
                $("#exampleModal").modal('hide');
            });

            $('#operaciones tbody').on('click', '.abrir-detalle', function (e) {
                var id_operacion=$(this).data('idope');
                $.ajax({
                    url:  '/detalle-operacion-ajax/'+id_operacion,
                    type: 'get',
                    dataType:  'json',
                    beforeSend: function() {

                    },
                    success: function(response) {
                        if(response.status){
                            $("#div-informacion-operacion").html(response.htmlview);
                            $("#exampleModal").modal("show");
                        }
                    },
                    error: function (request, status, error) {
                        $("#exampleModal").modal("hide");
                        console.log(fail);
                        var errors = fail.responseJSON.errors;
                        $.each(errors,(i, item)=>{
                            toastr.error(item[0]);
                        });
                    }
                });
            });
        } );

        var filtradoOperacion = (e) => {
            $(".operaciones").css('display', 'none');

            switch(e.value){
                case '0':
                    $(".o_pendientes").css('display', 'table-row');
                    break;
                case '1':
                    $(".o_proceso").css('display', 'table-row');
                    break;
                case '2':
                    $(".o_realizada").css('display', 'table-row');
                    break;
                case '3':
                    $(".o_devolucion").css('display', 'table-row');
                    break;
                case '4':
                    $(".o_devuelta").css('display', 'table-row');
                    break;
                case '5':
                    $(".o_observado").css('display', 'table-row');
                    break;
                case '6':
                    $(".o_anulado").css('display', 'table-row');
                    break;
                default:
                    $(".operaciones").css('display', 'table-row');
                    break;
            }
        };
    </script>
@endsection