<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Verificacion extends Model
{
    use HasFactory;
    protected $table = 'verificaciones';
    protected $fillable = [
        'imagen_frontal',
        'imagen_posterior',
        'video',
        'documento_identidad',
        'estado',
        'id_perfil',
    ];

    public function perfil()
    {
        return $this->hasOne(Perfil::class, 'id', 'id_perfil');
    }

}
