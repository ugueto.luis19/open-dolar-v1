<?php

namespace App\Http\Controllers;

use App\Models\Cargo;
use App\Models\Institucion;
use App\Models\Ocupacion;
use App\Models\Pais;
use App\Models\ActividadEconomica;
use App\Models\User;
use App\Models\Perfil;
use function Couchbase\basicDecoderV1;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Redirect;
use Session;


class UsuarioController extends Controller
{
    //
    public $menu;

    public function __construct()
    {
        $this->menu =0;
        \View::share('menu', $this->menu);
    }
    public function guardarRegistro(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users,email',
            'nro_celular' => 'required|min:8',
            'password' => 'required|min:8',
            'password_confirm' => 'required|min:8|required|same:password',
        ]);

        $id_perfil=Session::get('id_perfil');
        $perfil=Perfil::find($id_perfil);

        if($perfil==null){
            session_unset();
            $notification = array(
                'message' => 'La sesión ha vencido!',
                'alert-type' => 'error'
            );

            return redirect()->route('dashboard')->with($notification);
        }

        $request['password']=Hash::make($request->password);
        $new_user=new User($request->all());
        $new_user->save();

        $new_user->assignRole('Cliente');
        event(new Registered($new_user));

        Auth::login($new_user);

        if(Auth::check())
            $perfil->nro_celular = Auth::user()->nro_celular;

        $perfil->id_cuenta=$new_user->id;
        $perfil->save();

        if($perfil->tipo==2)
        {
            // Es RUC a su perfil 2 de tipo natural le asignamos la misma informacion
            $perfiles=Perfil::where('tipo_doc',$perfil->tipo_doc)->where('nro_doc',$perfil->nro_doc)->get();

            if(count($perfiles)>1)
            {
                foreach ($perfiles as $perfil_r)
                {
                    if ($perfil_r->tipo==1)
                    {
                        $perfil_r->nro_celular=Auth::user()->nro_celular;
                        $perfil_r->id_cuenta=$new_user->id;
                        $perfil_r->save();
                    }
                }
            }

        }

        $notification = array(
            'message' => 'El Usuario se registro correctamente.',
            'alert-type' => 'success'
        );
        return redirect()->route('dashboard')->with($notification);

    }

    public function formPersonaNatural()
    {

        if(Auth::check())
        {
            if(Auth::user()->perfiles_natural()==true)
            {
                $ocupaciones=Ocupacion::all();
                $paises=Pais::all();
                $instituciones=Institucion::where('estado','1')->get();
                $cargos=Cargo::all();
                return view('auth.persona_natural',compact('ocupaciones','instituciones','paises','cargos'));
            }
            else
            {
                return redirect()->route('elegir-perfil');
            }
        }
        else
        {
            $ocupaciones=Ocupacion::all();
            $paises=Pais::all();
            $instituciones=Institucion::where('estado','1')->get();
            $cargos=Cargo::all();
            return view('auth.persona_natural',compact('ocupaciones','instituciones','paises','cargos'));
        }
    }

    private function verifyNum()
    {
        $num = sprintf('%X', mt_rand());
        
        while(is_numeric($num) || strlen($num) < 8){ $num = sprintf("%X", mt_rand()); }
  
        return $num;
    }

    public function formSavePersonaNatural(Request $request)
    {
        $this->validate($request, [
            'nombres' => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
            'tipo_doc' => 'required',
            'nro_doc' => 'required',
            'id_ocupacion' => 'required',
            'publicamente_expuesto' => 'required',
        ]);

        $input = $request->all();
        // Validamos que el tipo de documento y nro de documento no esten registrados conjunto al tipo de registro
        $validar=Perfil::where('tipo','1')->where('tipo_doc',$request->tipo_doc)->where('nro_doc',$request->nro_doc)->get();
        if (count($validar)>0)
        {
            $notification = array(
                'message' => 'El Tipo de Documento y el Nro de Documento ya se encuentran registrados!',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification)->withInput();
        }
        //////////////////////////////
        $num = $this->verifyNum();
        ////// VERIFICANDO SI EL CODIGO CREADO YA EXISTE EN UN PERFIL
        while(Perfil::where('codigo_asignado', $num)->count() > 0){
            $num = $this->verifyNum();
        }

        if($request->id_institucion=='otros')
        {
            $input['id_institucion']=null;
        }
        $perfil = new Perfil($input);
        $perfil->codigo_asignado = $num;

        if(Session::get('codigo_referido')!=null)
        {
            $perfil->codigo_referido = Session::get('codigo_referido');
        }

        if(Auth::check())
            $perfil->nro_celular = Auth::user()->nro_celular;

        $perfil->ruc = '';
        $perfil->nombre_juridico = '';
        $perfil->direccion_juridico = '';

        if(Session::get('id_perfil')!=null)
        {
            $perfil_primario=Perfil::find(Session::get('id_perfil'));
            $perfil->id_cuenta=$perfil_primario->id_cuenta;
        }

        $perfil->save();

        $notification = array(
            'message' => 'El perfil se registro correctamente.',
            'alert-type' => 'success'
        );

        Session::put('id_perfil',$perfil->id);

        if($perfil->id_cuenta!=null)
        {
            // este registro es de una segunda cuenta
            return redirect()->route('dashboard')->with($notification);
        }
        else
        {
            return redirect()->route('registro')->with($notification);
        }

    }

    public function registro()
    {
        if(is_null(Session::get('id_perfil')))
        {
            return redirect()->route('login');
        }
        else
        {
            return view('auth.registro');
        }
    }

    public function apiRUC(Request $request)
    {
        if($request->ajax())
        {
            $ruc=$request->ruc;

            // Validamos que el tipo de documento y nro de documento no esten registrados
            $validar=Perfil::where('ruc',$ruc)->get();
            if (count($validar)>0)
            {
                return response()->json(['estado' => 10]);
            }

            $apiKey = 'zfeFnMukSKSrrGWW8TltRqeDXVRH4gvAOyrRDxIoTyatNyeZELxjCu9Ur298';
            $url = 'https://api.peruapis.com/v1/ruc';
            $document = $ruc;

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('document' => $document),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer '.$apiKey,
                    'Accept: application/json'
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);

            return response()->json(['resultado' => json_decode($response,true), 'estado' => 1]);
        }
    }

    public function apiDNI(Request $request)
    {
        if($request->ajax())
        {
            $dni=$request->dni;

            $apiKey = 'zfeFnMukSKSrrGWW8TltRqeDXVRH4gvAOyrRDxIoTyatNyeZELxjCu9Ur298';
            $url = 'https://api.peruapis.com/v1/dni';
            $document = $dni;

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('document' => $document),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer '.$apiKey,
                    'Accept: application/json'
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);

            return response()->json(['resultado' => json_decode($response,true)]);
        }
    }

    public function continuarPersonaJuridica(Request $request)
    {
        $ruc = $request->ruc;
        $nombre_juridico = $request->nombre_juridico;
        $direccion_juridico = $request->direccion_juridico;
        $representante = $request->representante;
        $tipo_doc_repre = $request->tipo_doc_repre;
        $nro_doc_repre = $request->nro_doc_repre;
        $nombres_repre = $request->nombres_repre;
        $apellido_paterno_repre = $request->apellido_paterno_repre;
        $apellido_materno_repre = $request->apellido_materno_repre;

        session(['ruc' => $ruc]);
        session(['nombre_juridico' => $nombre_juridico]);
        session(['direccion_juridico' => $direccion_juridico]);
        session(['representante' => $representante]);
        session(['tipo_doc_repre' => $tipo_doc_repre]);
        session(['nro_doc_repre' => $nro_doc_repre]);
        session(['nombres_repre' => $nombres_repre]);
        session(['apellido_paterno_repre' => $apellido_paterno_repre]);
        session(['apellido_materno_repre' => $apellido_materno_repre]);
        session(['id_actividad_economica' => $request->id_actividad_economica]);
        session(['actividad_economica' => $request->actividad_economica]);



        if(Session::get('id_perfil')!=null)
        {
            //////////////////////////////
            $num = $this->verifyNum();
            ////// VERIFICANDO SI EL CODIGO CREADO YA EXISTE EN UN PERFIL
            while(Perfil::where('codigo_asignado', $num)->count() > 0){
                $num = $this->verifyNum();
            }
            $perfil_primario=Perfil::find(Session::get('id_perfil'));
            $perfil_empresa = new Perfil();
            $perfil_empresa->id_cuenta=$perfil_primario->id_cuenta;
            $perfil_empresa->codigo_asignado = $num;
            $perfil_empresa->nro_celular=$perfil_primario->nro_celular;
            $perfil_empresa->nombres=$perfil_primario->nombres;
            $perfil_empresa->apellido_paterno=$perfil_primario->apellido_paterno;
            $perfil_empresa->apellido_materno=$perfil_primario->apellido_materno;
            $perfil_empresa->tipo_doc=$perfil_primario->tipo_doc;
            $perfil_empresa->nro_doc=$perfil_primario->nro_doc;
            $perfil_empresa->id_ocupacion=$perfil_primario->id_ocupacion;
            $perfil_empresa->publicamente_expuesto=$perfil_primario->publicamente_expuesto;
            $perfil_empresa->id_institucion=$perfil_primario->id_institucion;
            $perfil_empresa->id_cargo=$perfil_primario->id_cargo;
            $perfil_empresa->nombre_institucion=$perfil_primario->nombre_institucion;
            $perfil_empresa->tipo=2;
            $perfil_empresa->ruc=$ruc;
            $perfil_empresa->nombre_juridico=$nombre_juridico;
            $perfil_empresa->direccion_juridico=$direccion_juridico;
            $perfil_empresa->fecha_nacimiento=$perfil_primario->fecha_nacimiento;
            $perfil_empresa->id_nacionalidad=$perfil_primario->id_nacionalidad;
            $perfil_empresa->representante=$representante;
            $perfil_empresa->tipo_doc_repre=$tipo_doc_repre;
            $perfil_empresa->nro_doc_repre=$nro_doc_repre;
            $perfil_empresa->nombres_repre=$nombres_repre;
            $perfil_empresa->apellido_paterno_repre=$apellido_paterno_repre;
            $perfil_empresa->apellido_materno_repre=$apellido_materno_repre;
            $perfil_empresa->id_actividad_economica=$request->id_actividad_economica;
            $perfil_empresa->actividad_economica=$request->actividad_economica;
            $perfil_empresa->save();

            Session::put('perfil', $perfil_empresa);
            Session::put('id_perfil', $perfil_empresa->id);
            Session::put('imagen_perfil', $perfil_empresa->imagen_perfil);
            Session::put('tipo', $perfil_empresa->tipo);
            if($perfil_empresa->tipo==1)
            {
                Session::put('nombres', $perfil_empresa->nombres);
                Session::put('apellido_paterno', $perfil_empresa->apellido_paterno);
            }
            else
            {
                Session::put('nombre_juridico', $perfil_empresa->nombre_juridico);
            }

            $notification = array(
                'message' => 'El perfil se registro correctamente.',
                'alert-type' => 'success'
            );
            return redirect()->route('dashboard')->with($notification);
        }
        else
        {
            return redirect()->action([UsuarioController::class, 'continuarPersonaJuridicaView']);
        }

    }

    public function continuarPersonaJuridicaView(Request $request)
    {
        $ruc = session('ruc');
        $nombre_juridico = session('nombre_juridico');
        $direccion_juridico = session('direccion_juridico');
        $representante = session('representante');
        $tipo_doc_repre = session('tipo_doc_repre');
        $nro_doc_repre = session('nro_doc_repre');
        $nombres_repre = session('nombres_repre');
        $apellido_paterno_repre = session('apellido_paterno_repre');
        $apellido_materno_repre = session('apellido_materno_repre');
        $id_actividad_economica = session('id_actividad_economica');
        $actividad_economica = session('actividad_economica');
        $ocupaciones=Ocupacion::all();
        $instituciones=Institucion::where('estado','1')->get();
        $paises=Pais::all();
        $cargos=Cargo::all();
        return view('auth.persona_juridica2', compact('cargos','id_actividad_economica','actividad_economica','paises','tipo_doc_repre','nro_doc_repre','nombres_repre','apellido_paterno_repre','apellido_materno_repre','ocupaciones', 'ruc', 'representante','nombre_juridico', 'direccion_juridico','instituciones'));
    }

    public function saveRucPersonaJuridica(Request $request)
    {
        $this->validate($request, [
            'nombres' => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
            'tipo_doc' => 'required',
            'nro_doc' => 'required',
            'id_ocupacion' => 'required',
            'publicamente_expuesto' => 'required',
            'ruc' => 'required',
            'nombre_juridico' => 'required',
            'id_actividad_economica' => 'required',
            //'direccion_juridico' => 'required',
        ]);

        // Validamos que el tipo de documento y nro de documento y ruc no esten registrados
        $validar=Perfil::where('tipo','2')
            ->where('ruc',$request->ruc)
            ->where('tipo_doc',$request->tipo_doc)
            ->where('nro_doc',$request->nro_doc)->get();

        if (count($validar)>0)
        {
            $notification = array(
                'message' => 'El Tipo de Documento y el Nro de Documento ya se encuentran registrados con el mismo RUC!',
                'alert-type' => 'error'
            );

            return redirect()->back()->with($notification)->withInput();
        }
        //////////////////////////////
        $num = $this->verifyNum();
        ////// VERIFICANDO SI EL CODIGO CREADO YA EXISTE EN UN PERFIL
        while(Perfil::where('codigo_asignado', $num)->count() > 0){
            $num = $this->verifyNum();
        }

        $perfil = new Perfil($request->all());
        $perfil->codigo_asignado = $num;

        if(Session::get('codigo_referido')!=null)
        {
            $perfil->codigo_referido = Session::get('codigo_referido');
        }


        if(Session::get('id_perfil')!=null)
        {
            $perfil_primario=Perfil::find(Session::get('id_perfil'));
            $perfil->id_cuenta=$perfil_primario->id_cuenta;
        }

        $perfil->save();

        // perfil natural se crea una perfil Natural cuando es una cuenra RUC
        $perfil_natural=new Perfil();
        $perfil_natural->nro_celular=$request->nro_celular;
        $perfil_natural->nombres=$request->nombres;
        $perfil_natural->apellido_paterno=$request->apellido_paterno;
        $perfil_natural->apellido_materno=$request->apellido_materno;
        $perfil_natural->tipo_doc=$request->tipo_doc;
        $perfil_natural->nro_doc=$request->nro_doc;
        $perfil_natural->id_ocupacion=$request->id_ocupacion;
        $perfil_natural->publicamente_expuesto=$request->publicamente_expuesto;

        if(Session::get('codigo_referido')!=null)
        {
            $perfil_natural->codigo_referido = $request->codigo_referido;
        }

        //////////////////////////////
        $num_2 = $this->verifyNum();
        ////// VERIFICANDO SI EL CODIGO CREADO YA EXISTE EN UN PERFIL
        while(Perfil::where('codigo_asignado', $num_2)->count() > 0){
            $num_2 = $this->verifyNum();
        }
        $perfil_natural->codigo_asignado=$num_2;
        $perfil_natural->id_institucion=$request->id_institucion;
        $perfil_natural->nombre_institucion=$request->nombre_institucion;

        if(Session::get('id_perfil')!=null)
        {
            $perfil_primario=Perfil::find(Session::get('id_perfil'));
            $perfil_natural->id_cuenta=$perfil_primario->id_cuenta;
        }

        $perfil_natural->tipo=1;
        $perfil_natural->fecha_nacimiento=$request->fecha_nacimiento;
        $perfil_natural->id_nacionalidad=$request->id_nacionalidad;
        $perfil_natural->save();


        Session::put('id_perfil',$perfil->id);

        $notification = array(
            'message' => 'El perfil se registro correctamente.',
            'alert-type' => 'success'
        );

        if($perfil->id_cuenta!=null)
        {
            // este registro es de una segunda cuenta
            return redirect()->route('dashboard')->with($notification);
        }
        else
        {
            return redirect()->route('registro')->with($notification);
        }
    }

    public function seleccionarRegistro()
    {
        return view('auth.seleccionar_registro');
    }

    public function personaJuridica()
    {
        if(Auth::check())
        {
            if(Auth::user()->perfiles_juridicos()==true)
            {
                $actividad_economica=ActividadEconomica::all();
                return view('auth.persona_juridica',compact('actividad_economica'));
            }
            else
            {
                return redirect()->route('elegir-perfil');
            }
        }
        else
        {
            $actividad_economica=ActividadEconomica::all();
            return view('auth.persona_juridica',compact('actividad_economica'));
        }
    }

    public function registroReferido(Request $request)
    {
        $perfil = Perfil::where('codigo_asignado', $request->ref);

        if($perfil->count() == 0){
            $notification = array(
                'message' => 'El codigo de referido no existe. Por favor proceda con su registro!',
                'alert-type' => 'warning'
            );

            return redirect()->route('login')->with($notification);

        }elseif($perfil->count() == 1){
            $getPerfil = $perfil->first();

            Session::put('codigo_referido', $getPerfil->codigo_asignado);

            $notification = array(
                'message' => 'El codigo de referido procesado correctamente!',
                'alert-type' => 'success'
            );

            return redirect()->route('seleccionar-registro')->with($notification);
        }
    }

}
