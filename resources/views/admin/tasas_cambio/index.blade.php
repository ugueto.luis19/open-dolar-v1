@extends('layouts.index_app')
@section('content')
    <style>
        .btn-tabs{
            border-radius: 18px !important;
            background-color: transparent !important;
            color: #137188 !important;
            border-color: #C6D05D;
            height: 3.125rem;
        }
        .active>button{
            border-radius: 18px !important;
            background-color: #C6D05D !important;
            color: #137188 !important;
            height: 3.125rem;
        }
        .activar-tr{
            background-color: #C6D05D !important;
        }
        .activar-tr>th{
            color: #000;
            font-weight: bold;
        }
    </style>
    <div class="conten_panel_administrador">

        <div class="row mx-0">
            <div class="col-md-4 px-0">
                <h4 class="arlrdbd">Tasas de Cambio</h4>
            </div>
            <div class="col-md-8 px-0 mt-3 mt-md-0">
                <form method="post" action="{{ route('filtrar-tasas-admin') }}" style="display: inherit;">
                    <div class="row mx-0">
                        @csrf
                        <input name="tabs" id="tabs-input" type="hidden" @if(isset($tabs)) value="{{$tabs}}" @else value="operaciones-tab" @endif >
                        <div class="col-sm-4 px-1 mb-3 mb-sm-0">
                            <div class="div-input-operaciones">
                                <input type="date" name="desde" autocomplete="off" class="form-control input-operacioes PoppinsRegular" value="{{ isset($desde) ? $desde : '' }}" placeholder="Fecha de Inicio" required>
                            </div>
                        </div>
                        <div class="col-sm-4 px-1 mb-3 mb-sm-0">
                            <div class="div-input-operaciones">
                                <input type="date" name="hasta" autocomplete="off" class="form-control input-operacioes PoppinsRegular" value="{{ isset($hasta) ? $hasta : '' }}" placeholder="Fecha Final" required>
                            </div>
                        </div>
                        <div class="col-sm-3 px-1 mb-3 mb-sm-0">
                            <div class="div-input-operaciones">
                                <i class="fa-solid fa-magnifying-glass"></i>
                                <input type="submit" placeholder="Buscar operacion" class="input-operacioes PoppinsRegular">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <?php

            if(session()->has('tabs'))
                {
                    $tabs = session('tabs');
                }

        ?>

        <ul class="nav nav-tabs mt-sm-3">
            <li class="tabs-li @if(isset($tabs)) @if($tabs=='operaciones-tab') active @endif @else active @endif " data-tab="operaciones-tab">
                <button type="button" class="btn btn-tabs PoppinsMedium">Ordenes</button>
            </li>
            <li class="tabs-li @if(isset($tabs)) @if($tabs=='cambistas-tab') active @endif @endif " data-tab="cambistas-tab">
                <button type="button" class="btn btn-tabs PoppinsMedium">Cambistas</button>
            </li>
            <li class="tabs-li @if(isset($tabs)) @if($tabs=='bancos-tab') active @endif @endif " data-tab="bancos-tab">
                <button type="button" class="btn btn-tabs PoppinsMedium">Bancos</button>
            </li>
        </ul>
        <div class="tab-content">
            <div id="operaciones-tab" class="tab-pane @if(isset($tabs)) @if($tabs=='operaciones-tab') active @endif @else active @endif ">
                <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
                    <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                        <div class="col-sm-7 col-md-6 col-lg-5 text-center text-sm-start">

                        </div>
                        <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                            @canany('tasas_cambio_agregar')
                                <button class="btn btn-login PoppinsMedium" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar Tasa Cambio</button>
                            @endcanany
                        </div>
                    </div>
                    <br>

                    <div class="div-scrolll">
                        <table class="table table-border-cero" id="tasas">
                            <thead>
                            <tr>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Fecha y Hora</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Compra</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Pips Compra</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Venta</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Pips Venta</th>
                            </tr>
                            </thead>
                            <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                            @if(count($tasas)>0)
                                @foreach($tasas as $tasa)
                                    <tr class="tr-border-top @if($tasa->id==$tasa_actual->id) activar-tr @endif"  >
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{date('d-m-Y g:i a',strtotime($tasa->created_at))}}
                                        </th>
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{$tasa->tasa_compra}}
                                        </th>
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{$tasa->pips_compra}}
                                        </th>
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{$tasa->tasa_venta}}
                                        </th>
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{$tasa->pips_venta}}
                                        </th>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{ $tasas->links() }}
                    </div>
                </div>
            </div>

            <div id="cambistas-tab" class="tab-pane @if(isset($tabs)) @if($tabs=='cambistas-tab') active @endif @endif ">
                <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
                    <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                        <div class="col-sm-7 col-md-6 col-lg-5 text-center text-sm-start">

                        </div>
                        <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                            @canany('tasas_cambio_agregar')
                                <button class="btn btn-login PoppinsMedium" data-bs-toggle="modal" data-bs-target="#exampleModal2">Agregar Tasa Cambio</button>
                            @endcanany
                        </div>
                    </div>
                    <br>

                    <div class="div-scrolll">
                        <table class="table table-border-cero" id="tasas_cambistas">
                            <thead>
                            <tr>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Fecha y Hora</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Compra</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Venta</th>
                            </tr>
                            </thead>
                            <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                            @if(count($tasas_cambistas)>0)
                                @foreach($tasas_cambistas as $tasa_cambista)
                                    <tr class="tr-border-top @if($tasa_cambista->id==$tasa_cambista_actual->id) activar-tr @endif" >
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{date('d-m-Y g:i a',strtotime($tasa_cambista->created_at))}}
                                        </th>
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{$tasa_cambista->tasa_compra}}
                                        </th>
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{$tasa_cambista->tasa_venta}}
                                        </th>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{ $tasas_cambistas->links() }}
                    </div>
                </div>
            </div>

            <div id="bancos-tab" class="tab-pane @if(isset($tabs)) @if($tabs=='bancos-tab') active @endif @endif ">
                <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
                    <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                        <div class="col-sm-7 col-md-6 col-lg-5 text-center text-sm-start">

                        </div>
                        <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                            @canany('tasas_cambio_agregar')
                                <button class="btn btn-login PoppinsMedium" data-bs-toggle="modal" data-bs-target="#exampleModal3">Agregar Tasa Cambio</button>
                            @endcanany
                        </div>
                    </div>
                    <br>

                    <div class="div-scrolll">
                        <table class="table table-border-cero" id="tasas_bancos">
                            <thead>
                            <tr>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Fecha y Hora</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Compra</th>
                                <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Venta</th>
                            </tr>
                            </thead>
                            <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                            @if(count($tasas_bancos)>0)
                                @foreach($tasas_bancos as $tasa_banco)
                                    <tr class="tr-border-top @if($tasa_banco->id==$tasa_banco_actual->id) activar-tr @endif ">
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{date('d-m-Y g:i a',strtotime($tasa_banco->created_at))}}
                                        </th>
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{$tasa_banco->tasa_compra}}
                                        </th>
                                        <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                            {{$tasa_banco->tasa_venta}}
                                        </th>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{ $tasas_bancos->links() }}
                    </div>
                </div>
            </div>

        </div>


    </div>

    @canany('tasas_cambio_agregar')
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center" style="line-height: 1.5;">Agregar nueva tasa de cambio de Operaciones</h1>

                            <form method="post" action="{{ route('registrar-tasa') }}">
                                @csrf
                                <input name="tabs" type="hidden" value="operaciones-tab" >
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="tasa_compra" class="input-login PoppinsRegular" placeholder="Ingrese tasa de compra" required>
                                        </div>
                                    </div>
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="pips_compra" class="input-login PoppinsRegular" placeholder="Ingrese Pips de compra" required>
                                        </div>
                                    </div>
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="tasa_venta" class="input-login PoppinsRegular" placeholder="Ingrese tasa de venta" required>
                                        </div>
                                    </div>
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="pips_venta" class="input-login PoppinsRegular" placeholder="Ingrese Pips de venta" required>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center" style="line-height: 1.5;">Agregar nueva tasa de cambio de Cambistas</h1>

                            <form method="post" action="{{ route('registrar-tasa-cambistas') }}">
                                @csrf
                                <input name="tabs" type="hidden" value="cambistas-tab" >
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="tasa_compra" class="input-login PoppinsRegular" placeholder="Ingrese tasa de compra" required>
                                        </div>
                                    </div>

                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="tasa_venta" class="input-login PoppinsRegular" placeholder="Ingrese tasa de venta" required>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center" style="line-height: 1.5;">Agregar nueva tasa de cambio de Bancos</h1>

                            <form method="post" action="{{ route('registrar-tasa-bancos') }}">
                                @csrf
                                <input name="tabs" type="hidden" value="bancos-tab" >
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="tasa_compra" class="input-login PoppinsRegular" placeholder="Ingrese tasa de compra" required>
                                        </div>
                                    </div>

                                    <div class="col-12 mb-3 px-lg-5">
                                        <div class="div-form-login">
                                            <input type="number" step="any" name="tasa_venta" class="input-login PoppinsRegular" placeholder="Ingrese tasa de venta" required>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
         /* $('#tasas').DataTable();
            $('#tasas_cambistas').DataTable();
            $('#tasas_bancos').DataTable();
*/

            $(".tabs-li").click(function () {
                var id_elemento=$(this).data('tab');
                $(".tabs-li").removeClass('active');
                $(".tab-pane").removeClass('active');
                $(this).addClass('active');
                $("#"+id_elemento).addClass('active');
                $("#tabs-input").val(id_elemento);
            });
        } );
    </script>
@endsection