<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nro_celular',20);
            $table->string('nombres',191);
            $table->string('apellidos',191);
            $table->char('tipo_doc',1);
            $table->string('nro_doc',15);
            $table->string('ocupacion');
            $table->char('publicamente_expuesto',1);
            $table->string('codigo_referido',20)->nullable();
            $table->string('codigo_asignado',20);
            $table->string('imagen_perfil');
            $table->unsignedInteger('id_institucion');
            $table->foreign('id_institucion')->references('id')->on('instituciones')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->unsignedBigInteger('id_cuenta');
            $table->foreign('id_cuenta')->references('id')->on('users')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfiles');
    }
}
