<?php

namespace App\Http\Controllers;

use App\Models\Banco;
use App\Models\Comision;
use App\Models\ComisionReferidoConfig;
use App\Models\CuentaBancaria;
use App\Models\CuentaBancariaAdmin;
use App\Models\Moneda;
use App\Models\Operacion;
use App\Models\OperacionBancoDestino;
use App\Models\OperacionOrigenFondo;
use App\Models\Perfil;
use App\Models\Tasa;
use App\Models\TasaBanco;
use App\Models\Horario;
use App\Models\Cupon;
use App\Models\TiposCuenta;
use App\Models\Verificacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use View;
use Session;
use Redirect;

class CambioController extends Controller
{
    public function dashboard($id=null){
        $menu=1;
        $dia = Horario::diaActual();
        $diaSiguiente = Horario::diaSiguiente();

        if($id!=null)
        {
            $operacion=Operacion::find($id);
        }
        else
        {
            $operacion=null;
        }


        if(Auth::user()->hasRol->rol->id != 1){
            $menu = 0;
            return view('admin.index', compact('menu'));
        }

        $tasa=Tasa::orderBy('id', 'desc')->first();

        if(Auth::user()->perfiles->count() == 0){ // SINO POSEE PERFIL SE REDIRECCIONA AL REGISTRO
            $notification = array(
                'message' => 'Usted no posee registros de perfiles.',
                'alert-type' => 'warning'
            );
            return redirect()->route('seleccionar-registro')->with($notification);
        }
        elseif(Session::get('id_perfil') == null && Auth::user()->perfiles->count() > 1){ // VERIFICANDO SI POSEE MAS DE UN PERFIL, SI POSEE MAS DE UN PERFIL SE PROCEDE A REDIRECCIONAR PARA LA SELECCION DE DICHO PERFIL
            $perfiles = Auth::user()->perfiles;
            session(['id_perfil' => $perfiles[0]->id]);
            return view('perfil.seleccionar-perfil',compact('menu', 'perfiles'));
        }
        elseif(Session::get('id_perfil') == null && Auth::user()->perfiles->count() == 1){ // VERIFICANDO SI SOLO POSEE UN PERFIL
            Session::put('perfil', Auth::user()->perfiles->first());
            Session::put('id_perfil', Auth::user()->perfiles->first()->id);
            Session::put('imagen_perfil', Auth::user()->perfiles->first()->imagen_perfil);
            Session::put('nombres', Auth::user()->perfiles->first()->nombres);
            Session::put('apellido_paterno', Auth::user()->perfiles->first()->apellido_paterno);

            $cuentas_bancarias=CuentaBancaria::where('id_perfil',Session::get('id_perfil'))
                ->where('estado','1')
                ->get();

            $fecha_actual = date("d-m-Y");

            $fecha_validacion_menos_1_mes=date("Y-m-d",strtotime($fecha_actual."- 1 month"));

            $operacionesAll = Operacion::orderBy('id','desc')
                ->where('id_perfil',Session::get('id_perfil'))
                ->where('estado','2')
                ->whereDate('created_at','>=',$fecha_validacion_menos_1_mes)
                ->get();

            $suma = 0;
            foreach ($operacionesAll as $key => $op) {
                if($op->tipo == 0){
                    $suma+=$op->monto_recibido;
                }elseif($op->tipo == 1){
                    $suma+=$op->monto_enviado;
                }
            }
            $verificacion = Verificacion::where('id_perfil', session('id_perfil'))->orderBy('created_at', 'desc')->first();
            $tasaBancos = TasaBanco::orderBy('id','desc')->first();

            return view('dashboard-1',compact('menu','tasa','cuentas_bancarias', 'dia', 'diaSiguiente', 'suma', 'tasaBancos','verificacion','operacion'));
        }
        else{
            // si se acaba de registrar el perfil entra aqui
            $cuentas_bancarias=CuentaBancaria::where('id_perfil',Session::get('id_perfil'))
                ->where('estado','1')
                ->get();

            $perfil=Perfil::find(Session::get('id_perfil'));
        
            Session::put('perfil', $perfil);
            Session::put('id_perfil', $perfil->id);
            Session::put('imagen_perfil', $perfil->imagen_perfil);
            Session::put('nombres', $perfil->nombres);
            Session::put('apellido_paterno', $perfil->apellido_paterno);

            $fecha_actual = date("d-m-Y");

            $fecha_validacion_menos_1_mes=date("Y-m-d",strtotime($fecha_actual."- 1 month"));

            $operacionesAll = Operacion::orderBy('id','desc')
                ->where('id_perfil',Session::get('id_perfil'))
                ->where('estado','2')
                ->whereDate('created_at','>=',$fecha_validacion_menos_1_mes)
                ->get();

            $suma = 0;
            foreach ($operacionesAll as $key => $op) {
                if($op->tipo == 0){
                    $suma+=$op->monto_recibido;
                }elseif($op->tipo == 1){
                    $suma+=$op->monto_enviado;
                }
            }
            $verificacion = Verificacion::where('id_perfil', session('id_perfil'))->orderBy('created_at', 'desc')->first();
            $tasaBancos = TasaBanco::orderBy('id','desc')->first();

            return view('dashboard-1',compact('menu','tasa','cuentas_bancarias', 'dia', 'diaSiguiente', 'suma', 'tasaBancos','verificacion','operacion'));
        }
    }

    public function dashboardWP($tipo,$monto_envio){
        Session::put('tipo_wp',$tipo);
        Session::put('monto_envio_wp',$monto_envio);
        $menu=1;
        $dia = Horario::diaActual();
        $diaSiguiente = Horario::diaSiguiente();

        $operacion=null;

        if(Auth::user()->hasRol->rol->id != 1){
            $menu = 0;
            return view('admin.index', compact('menu'));
        }

        $tasa=Tasa::orderBy('id', 'desc')->first();

        if(Auth::user()->perfiles->count() == 0){ // SINO POSEE PERFIL SE REDIRECCIONA AL REGISTRO
            $notification = array(
                'message' => 'Usted no posee registros de perfiles.',
                'alert-type' => 'warning'
            );
            return redirect()->route('seleccionar-registro')->with($notification);
        }
        elseif(Session::get('id_perfil') == null && Auth::user()->perfiles->count() > 1){ // VERIFICANDO SI POSEE MAS DE UN PERFIL, SI POSEE MAS DE UN PERFIL SE PROCEDE A REDIRECCIONAR PARA LA SELECCION DE DICHO PERFIL
            $perfiles = Auth::user()->perfiles;
            session(['id_perfil' => $perfiles[0]->id]);
            return view('perfil.seleccionar-perfil',compact('menu', 'perfiles'));
        }
        elseif(Session::get('id_perfil') == null && Auth::user()->perfiles->count() == 1){ // VERIFICANDO SI SOLO POSEE UN PERFIL
            Session::put('perfil', Auth::user()->perfiles->first());
            Session::put('id_perfil', Auth::user()->perfiles->first()->id);
            Session::put('imagen_perfil', Auth::user()->perfiles->first()->imagen_perfil);
            Session::put('nombres', Auth::user()->perfiles->first()->nombres);
            Session::put('apellido_paterno', Auth::user()->perfiles->first()->apellido_paterno);

            $cuentas_bancarias=CuentaBancaria::where('id_perfil',Session::get('id_perfil'))
                ->where('estado','1')
                ->get();

            $fecha_actual = date("d-m-Y");

            $fecha_validacion_menos_1_mes=date("Y-m-d",strtotime($fecha_actual."- 1 month"));

            $operacionesAll = Operacion::orderBy('id','desc')
                ->where('id_perfil',Session::get('id_perfil'))
                ->where('estado','2')
                ->whereDate('created_at','>=',$fecha_validacion_menos_1_mes)
                ->get();

            $suma = 0;
            foreach ($operacionesAll as $key => $op) {
                if($op->tipo == 0){
                    $suma+=$op->monto_recibido;
                }elseif($op->tipo == 1){
                    $suma+=$op->monto_enviado;
                }
            }
            $verificacion = Verificacion::where('id_perfil', session('id_perfil'))->orderBy('created_at', 'desc')->first();
            $tasaBancos = TasaBanco::orderBy('id','desc')->first();


            return view('dashboard-1',compact('tipo','monto_envio','menu','tasa','cuentas_bancarias', 'dia', 'diaSiguiente', 'suma', 'tasaBancos','verificacion','operacion'));
        }
        else{
            // si se acaba de registrar el perfil entra aqui
            $cuentas_bancarias=CuentaBancaria::where('id_perfil',Session::get('id_perfil'))
                ->where('estado','1')
                ->get();

            $perfil=Perfil::find(Session::get('id_perfil'));

            Session::put('perfil', $perfil);
            Session::put('id_perfil', $perfil->id);
            Session::put('imagen_perfil', $perfil->imagen_perfil);
            Session::put('nombres', $perfil->nombres);
            Session::put('apellido_paterno', $perfil->apellido_paterno);

            $fecha_actual = date("d-m-Y");

            $fecha_validacion_menos_1_mes=date("Y-m-d",strtotime($fecha_actual."- 1 month"));

            $operacionesAll = Operacion::orderBy('id','desc')
                ->where('id_perfil',Session::get('id_perfil'))
                ->where('estado','2')
                ->whereDate('created_at','>=',$fecha_validacion_menos_1_mes)
                ->get();

            $suma = 0;
            foreach ($operacionesAll as $key => $op) {
                if($op->tipo == 0){
                    $suma+=$op->monto_recibido;
                }elseif($op->tipo == 1){
                    $suma+=$op->monto_enviado;
                }
            }
            $verificacion = Verificacion::where('id_perfil', session('id_perfil'))->orderBy('created_at', 'desc')->first();
            $tasaBancos = TasaBanco::orderBy('id','desc')->first();

            return view('dashboard-1',compact('tipo','monto_envio','menu','tasa','cuentas_bancarias', 'dia', 'diaSiguiente', 'suma', 'tasaBancos','verificacion','operacion'));
        }
    }


    public function procesarCupon(Request $request){
        if($request->ajax())
        {
            $cupon = Cupon::where('codigo', $request->cupon)->where('estado', 1)->get();

            // verificando existencia de cupon
            if($cupon->count() > 0){
                if(date('Y-m-d') <= $cupon->first()->fecha_caducidad){ // verificando si la fecha no ha caducado
                    $estado = ['status' => 200, 'cupon' => $cupon->first()];

                    return response()->json(['resultado' => $estado]);
                }
                else{
                    $estado = ['status' => 201]; // caducado
                    
                    return response()->json(['resultado' => $estado]);
                }
            }else{
                $estado = ['status' => 300];
                    
                return response()->json(['resultado' => $estado]);
            }

        }
    }

    public function misOperaciones(){
        $menu=2;
        $operaciones = Operacion::orderBy('id','desc')
            ->where('id_perfil',Session::get('id_perfil'))
            ->paginate(5);
        return view('operaciones.mis-operaciones',compact('menu','operaciones'));
    }

    public function procesoCambio1(Request $request)
    {
        $this->validate($request, [
            'monto_enviado' => 'required',
            'monto_recibido' => 'required',
            'tipo_transferencia' => 'required'
        ]);

        $cuenta_usd = CuentaBancaria::where('id_perfil',Session::get('id_perfil'))
            ->where('estado','1')
            ->where('id_moneda', '1')
            ->count();

        $cuenta_soles = CuentaBancaria::where('id_perfil',Session::get('id_perfil'))
            ->where('estado','1')
            ->where('id_moneda', '2')
            ->count();

        if($cuenta_usd == 0){
            $notification = array(
                'message' => 'No se puede procesar la solicitud, usted no posee cuentas en USD. Por favor registre!',
                'alert-type' => 'warning'
            );

            return redirect('mis-cuentas')->with($notification)->withInput();
        }
        if($cuenta_soles == 0){
            $notification = array(
                'message' => 'No se puede procesar la solicitud, usted no posee cuentas en Soles. Por favor registre!',
                'alert-type' => 'warning'
            );

            return redirect('mis-cuentas')->with($notification)->withInput();
        }

        // VERIFICANDO QUE SOLO SE APLIQUE UN CUPON O OPENCREDITOS
        if((isset($request->cupon) && !empty($request->cupon)) && (isset($request->opencredits) && !empty($request->opencredits))){
            $notification = array(
                'message' => 'No se puede procesar la solicitud, solo se puede procesar un cupon o opencredits, no ambos!',
                'alert-type' => 'warning'
            );

            return redirect('mis-cuentas')->with($notification)->withInput();
        }

        if(isset($request->id_operacion))
        {
            $operacion=Operacion::find($request->id_operacion);
        }
        else
        {
            $operacion=new Operacion();
        }

        $operacion->estado=0;
        $operacion->tipo=$request->tipo;
        $operacion->tipo_transferencia=$request->tipo_transferencia;
        $operacion->uso_opendolar=$request->opencredits;
        $operacion->monto_enviado=$request->monto_enviado;
        $operacion->monto_recibido=$request->monto_recibido;

        // fecha y hora de inicio de la operación
        $operacion->fecha_operacion=date('Y-m-d H:i:s');

        if(!is_null($request->cupon))
            {
                // verificando si se ingreso codigo cupon
                $cupon = Cupon::where('codigo', $request->cupon)->where('estado', 1)->get();
                $operacion->id_cupon = $cupon->first()->id;
            }

        $operacion->id_tasa=$request->id_tasa;
        $operacion->id_perfil=Session::get('id_perfil');
        $operacion->save();

        return redirect()->route('proceso-cambio2',['id'=>$operacion->id]);

    }

    public function procesoCambio2($id)
    {
        $menu=1;
        $operacion=Operacion::find($id);
        $tasa=Tasa::orderBy('id', 'desc')->first();
        $cuentas_bancarias=CuentaBancaria::where('id_perfil',Session::get('id_perfil'))
            ->where('estado','1')
            ->get();

        if($operacion->tipo_transferencia=='0')
        {
            return view('cambio-paso-2-una-cuenta-destino',compact('menu','tasa', 'cuentas_bancarias','operacion'));
        }
        else
        {
            $bancos=Banco::all();
            $monedas=Moneda::all();
            $tipos_cuentas=TiposCuenta::all();
            return view('cambio-paso-2-varias-cuentas-destino',compact('menu','tasa', 'cuentas_bancarias','operacion','bancos','monedas','tipos_cuentas'));
        }
    }

    public function procesoCambio3($id)
    {
        $menu=1;
        $operacion=Operacion::find($id);
        $tasa=Tasa::orderBy('id', 'desc')->first();
        $verificacion = Verificacion::where('id_perfil', session('id_perfil'))->orderBy('created_at', 'desc')->first();


        if($operacion->tipo=='0')
        {
            // el cliente envia soles le mostramos la cuenta en soles
            $cuenta_bancaria_admin=CuentaBancariaAdmin::where('id_moneda','2')->where('estado', 1)->orderBy('id','desc')->first();
        }
        else
        {
            // el cliente envia dolares le mostramos la cuenta en dolares
            $cuenta_bancaria_admin=CuentaBancariaAdmin::where('id_moneda','1')->where('estado', 1)->orderBy('id','desc')->first();
        }

        $operaciones_evaluar = Operacion::where('id_perfil',Session::get('id_perfil'))
            ->whereIn('estado',[1,2])
            ->whereDate('created_at','>=',date('Y-m-d'))
            ->get();

        return view('cambio-paso-3',compact('operaciones_evaluar','menu','tasa','operacion','cuenta_bancaria_admin', 'verificacion'));
    }


    public function procesoCambio3Confirmacion($id)
    {
        $menu=1;
        $operacion=Operacion::find($id);
        $tasa=Tasa::orderBy('id', 'desc')->first();

        if(!is_object($operacion->origenFondos) || is_null($operacion->origenFondos)){
            if(($operacion->tipo == 0 && $operacion->monto_recibido > 5000) || ($operacion->tipo == 1 && $operacion->monto_enviado > 5000)){
                return Redirect::back()->withInput();
            }
        }

        return view('cambio-paso-3-confirmacion',compact('menu','tasa','operacion'));
    }


    public function continuarCambio3(Request $request)
    {
        $id=$request->id;

        $operacion=Operacion::find($id);

        return redirect()->route('proceso-cambio3-confirmar',['id'=>$operacion->id]);
    }

    public function registrarOrigenFondos(Request $request)
    {
        if($request->ajax()) {

          $this->validate($request, [
              'origen' => 'required',
              //'descripcion' => 'required'
          ]);

            $origen_fondos=new OperacionOrigenFondo();
            $origen_fondos->origen=$request->origen;
            $origen_fondos->descripcion=$request->descripcion;
            $origen_fondos->id_operacion=$request->id_operacion;
            $origen_fondos->save();

            return response()->json(['status' => true]);
        }
    }
    public function guardarCambio2UnaCuenta(Request $request)
    {
        $this->validate($request, [
            'id_cuenta_origen' => 'required',
            'id_cuenta_destino' => 'required'
        ]);

        $operacion=Operacion::find($request->id);

        //////////////// VERIFICANDO CADUCACIÓN DE LA OPERACION
        $to_time = strtotime($operacion->fecha_operacion);
        $from_time = strtotime(date('Y-m-d H:i:s'));

        if(round(abs($to_time - $from_time) / 60,2) >= 15){
            $operacion->estado = 6;
            $operacion->update();
            
            $notification = array(
                'message' => 'Su hora de vencimiento ha finalizado, la operación se anulará!',
                'alert-type' => 'error'
            );

            return redirect()->route('mis-operaciones')->with($notification);
        }

        $operacion->update($request->all());


        $operacion->monto_banco_destino=$operacion->monto_recibido;
        $operacion->save();


        return redirect()->route('proceso-cambio3',['id'=>$operacion->id]);
    }

    public function guardarCambio2VariasCuentas(Request $request)
    {
        $this->validate($request, [
            'id_cuenta_origen' => 'required',
            'id_cuenta_destino'    => 'required|array',
            'monto_banco'    => 'required|array',
        ]);
        $suma = 0;
        foreach($request->monto_banco as $monto){
            $suma+=$monto;
        }

        $operacion=Operacion::find($request->id);

        //////////////// VERIFICANDO CADUCACIÓN DE LA OPERACION
        $to_time = strtotime($operacion->fecha_operacion);
        $from_time = strtotime(date('Y-m-d H:i:s'));

        if(round(abs($to_time - $from_time) / 60,2) >= 15){
            $operacion->estado = 6;
            $operacion->update();
            
            $notification = array(
                'message' => 'Su hora de vencimiento ha finalizado, la operación se anulará!',
                'alert-type' => 'error'
            );

            return redirect()->route('mis-operaciones')->with($notification);
        }

        if($suma != $operacion->monto_recibido){
            $notification = array(
                    'message' => 'Usted ha ingresado valores en los monto de manera incorrecta.',
                    'alert-type' => 'error'
                );

            return redirect()->back()->with($notification)->withInput();
        }

        $pos=0;
        foreach ($request->id_cuenta_destino as $id_cuenta)
        {
            if($pos==0)
            {
                // guardamos la primera cuenta  en el objeto de operacion
                $operacion->id_cuenta_destino=$id_cuenta;
                $operacion->monto_banco_destino=$request->monto_banco[$pos];
                $operacion->save();
            }
            else
            {
                // guardamos las demas cuentas en la relación
                $nueva_cuenta_destino=new OperacionBancoDestino;
                $nueva_cuenta_destino->id_operacion=$operacion->id;
                $nueva_cuenta_destino->id_cuenta_destino=$id_cuenta;
                $nueva_cuenta_destino->monto_banco_destino=$request->monto_banco[$pos];
                $nueva_cuenta_destino->save();
            }
            $pos++;
        }

        $operacion->id_cuenta_origen = $request->id_cuenta_origen;
        $operacion->update();

        return redirect()->route('proceso-cambio3',['id'=>$operacion->id]);
    }

    public function procesoCambio(Request $request)
    {
        $this->validate($request, [
            'monto_enviado' => 'required',
            'monto_recibido' => 'required',
            'id_cuenta_origen' => 'required',
            'id_cuenta_destino' => 'required',
            'declaro_cuenta_propia' => 'required'
        ]);

        $nuevo_cambio=new Operacion();
        $nuevo_cambio->estado=0;
        $nuevo_cambio->tipo=$request->tipo;
        $nuevo_cambio->uso_opendolar=$request->uso_opendolar;
        $nuevo_cambio->monto_enviado=$request->monto_enviado;
        $nuevo_cambio->monto_recibido=$request->monto_recibido;

        // fecha y hora de inicio de la operación
        $nuevo_cambio->fecha_operacion=date('Y-m-d H:i:s');

        if(!is_null($request->cupon)){ // verificando si se ingreso codigo cupon
            $cupon = Cupon::where('codigo', $request->cupon)->where('estado', 1)->get();

            // verificando existencia de cupon
            if($cupon->count() > 0){
                if(date('Y-m-d') <= $cupon->first()->fecha_caducidad){ // verificando si la fecha no ha caducado
                    $nuevo_cambio->id_cupon = $cupon->first()->id;
                    $nuevo_cambio->monto_recibido+=$cupon->first()->valor_soles*$cupon->first()->valor_pips;
                }
                else{
                    $notification = array(
                        'message' => 'El cupon ingresado ha caducado.',
                        'alert-type' => 'warning'
                    );

                    return redirect()->back()->with($notification)->withInput();
                }
            }else{

                $notification = array(
                    'message' => 'Usted ha ingresado un cupon incorrecto.',
                    'alert-type' => 'error'
                );

                return redirect()->back()->with($notification)->withInput();
            }
        }

        $nuevo_cambio->id_tasa=$request->id_tasa;
        $nuevo_cambio->id_perfil=Session::get('id_perfil');
        $nuevo_cambio->id_cuenta_origen=$request->id_cuenta_origen;
        $nuevo_cambio->id_cuenta_destino=$request->id_cuenta_destino;
        $nuevo_cambio->save();

        $notification = array(
            'message' => 'Cambio Iniciado correctamente.',
            'alert-type' => 'success'
        );

        return redirect()->route('confirmar-cambio',['id'=>$nuevo_cambio->id])->with($notification);
    }

    public function confirmarCambio($id)
    {
        $menu=1;
        $operacion=Operacion::find($id);

        $comision_config=ComisionReferidoConfig::orderBy('id', 'desc')->first();

        //////////////// VERIFICANDO CADUCACIÓN DE LA OPERACION
        $to_time = strtotime($operacion->fecha_operacion);
        $from_time = strtotime(date('Y-m-d H:i:s'));

        if(round(abs($to_time - $from_time) / 60,2) >= 20){
            $operacion->estado = 6;
            $operacion->update();

            $notification = array(
                'message' => 'Su hora de vencimiento ha finalizado, la operación se anulará!',
                'alert-type' => 'error'
            );

            return redirect()->route('mis-operaciones')->with($notification);
        }
        else
        {
            return view('confirmar-cambio',compact('menu','operacion','comision_config'));
        }

    }

    public function continuarPendiente($id)
    {
        $menu=1;
        $operacion=Operacion::find($id);

        //////////////// VERIFICANDO CADUCACIÓN DE LA OPERACION
        $to_time = strtotime($operacion->fecha_operacion);
        $from_time = strtotime(date('Y-m-d H:i:s'));

        if(round(abs($to_time - $from_time) / 60,2) >= 15){
            /*$operacion->estado = 6;
            $operacion->update();

            $notification = array(
                'message' => 'Su hora de vencimiento ha finalizado, la operación se anulará!',
                'alert-type' => 'error'
            );

            return redirect()->route('mis-operaciones')->with($notification);*/
            $operacion->estado = 6;
            $operacion->update();

            $notification = array(
                'message' => 'Su hora de vencimiento ha finalizado, la operación iniciara con la nueva Tasa!',
                'alert-type' => 'error'
            );

            return redirect()->route('dashboard',['id'=>$operacion->id])->with($notification);
        }
        else
        {
            if($operacion->id_cuenta_origen == NULL && $operacion->id_cuenta_destino == NULL)
                return redirect()->route('proceso-cambio2', ['id' => $operacion->id]);
            elseif($operacion->imagen_comprobante == NULL)
                return redirect()->route('proceso-cambio3', ['id' => $operacion->id]);
        }
    }

    public function finalizarCambio(Request $request)
    {
        $menu=1;
        $operacion=Operacion::find($request->id);

        //////////////// VERIFICANDO CADUCACIÓN DE LA OPERACION
        $to_time = strtotime($operacion->fecha_operacion);
        $from_time = strtotime(date('Y-m-d H:i:s'));

        if(round(abs($to_time - $from_time) / 60,2) >= 15){
            $operacion->estado = 6;
            $operacion->update();
            
            $notification = array(
                'message' => 'Su hora de vencimiento ha finalizado, la operación se anulará!',
                'alert-type' => 'error'
            );

            return redirect()->route('mis-operaciones')->with($notification);
        }
        

        $comision_config=ComisionReferidoConfig::orderBy('id', 'desc')->first();


        $path = public_path().'/img/operaciones/';

        if($request->hasFile('imagen_comprobante')){
            $file = $request->file('imagen_comprobante');
            $filenameWithExt = $file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $fileNameToStore = time() . '.' . $extension;
            $operacion->imagen_comprobante = $fileNameToStore;
            $file->move($path, $fileNameToStore);
        }

        $operacion->estado=1;
        $operacion->nro_transferencia=$request->nro_transferencia;
        $operacion->save();

        $validacion=Comision::where('id_perfil_hijo',Session::get('id_perfil'))->first();
        $perfil_cambio=Perfil::find(Session::get('id_perfil'));
        // Gana comision solo una vez como referido
        if(!is_object($validacion) && $perfil_cambio->codigo_referido!=null)
        {
            $comision=ComisionReferidoConfig::orderBy('id', 'desc')->first();
            if(is_object($comision))
            {

                // Obtenemos el monto en USD del cambio
                if($operacion->tipo=='0')
                {
                    $monto_enviado_reflejado_usd=$operacion->monto_recibido;
                }
                else
                {
                    $monto_enviado_reflejado_usd=$operacion->monto_enviado;
                }

                if($monto_enviado_reflejado_usd>=$comision->monto_operacion_minimo)
                {
                    $new_comision=new Comision();
                    $new_comision->id_perfil_padre=$perfil_cambio->perfilPadre($perfil_cambio->codigo_referido)->id;
                    $new_comision->id_perfil_hijo=$perfil_cambio->id;
                    $new_comision->monto=($operacion->monto_enviado/$comision->monto_operacion_minimo)*$comision->monto_ganar;
                    $new_comision->id_operacion=$operacion->id;
                    $new_comision->save();
                }
            }
        }

        return view('cambio-finalizado',compact('menu','operacion'));
    }

    public function anular($id)
    {
        $operacion = Operacion::where('id_perfil', Session::get('id_perfil'))->where('id', $id)->get();

        if($operacion->count() > 0){
            $operacion=Operacion::find($id);
            $operacion->estado=6;
            $operacion->save();

            $notification = array(
                'message' => 'La Operación se anuló correctamente.',
                'alert-type' => 'success'
            );

            return redirect()->route('mis-operaciones')->with($notification);
        }
        else{
            $notification = array(
                'message' => 'La Operación no coincide con sus registros.',
                'alert-type' => 'warning'
            );

            return redirect()->route('mis-operaciones')->with($notification);
        }
    }

    public function filtrarOperaciones(Request $request)
    {
        $this->validate($request, [
            'desde' => 'required',
            'hasta' => 'required'
        ]);

        $menu=2;
        $desde = $request->desde;
        $hasta = $request->hasta;

        $operaciones = Operacion::orderBy('id','desc')
            ->where('id_perfil',Session::get('id_perfil'))
            ->whereDate('fecha_operacion','>=',date("Y-m-d", strtotime($desde)))
            ->whereDate('fecha_operacion','<=',date("Y-m-d", strtotime($hasta)))
            ->orderBy('id','desc')
            ->paginate(5);

        return view('operaciones.mis-operaciones',compact('menu','operaciones','desde', 'hasta'));
    }

    public function detalleOperacionAjax(Request $request, $id)
    {
        if($request->ajax())
        {
            $operacion=Operacion::find($id);
            $view = View::make('partes.informacion-operacion')
                ->with('operacion', $operacion)
                ->render();
            return response()->json(['status' => true, 'htmlview' => $view, 'operacion' => $operacion]);
        }
    }

    public function detalleOperacionAjaxv2(Request $request, $id)
    {
        if($request->ajax())
        {
            $operacion=Operacion::find($id);
            $view = View::make('partes.informacion-operacion-v2')
                ->with('operacion', $operacion)
                ->render();
            return response()->json(['status' => true, 'htmlview'=>$view]);
        }
    }

    public function listadoOperacionesFiltro(Request $request, $estado)
    {
        if($request->ajax())
        {
            $operaciones=Operacion::where('id_perfil',session('id_perfil'))->where('estado',$estado)->paginate(5);

            $view = View::make('partes.listado-operaciones')
                ->with('operaciones', $operaciones)
                ->render();

            return response()->json(['status' => true, 'htmlview'=>$view]);
        }
    }

    public function elegirPerfil()
    {
        Session::put('perfil', null);
        Session::put('id_perfil', null);
        Session::put('imagen_perfil', null);
        Session::put('nombres', null);
        Session::put('apellido_paterno', null);

        $notification = array(
            'message' => 'Debe Seleccionar un Perfil.',
            'alert-type' => 'success'
        );

        return redirect()->route('dashboard')->with($notification);
    }

}
