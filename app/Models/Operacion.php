<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operacion extends Model
{
    use HasFactory;
    protected $table = 'operaciones';
    protected $fillable = [
        'estado',
        'tipo',
        'uso_opendolar',
        'monto_enviado',
        'monto_recibido',
        'fecha_operacion',
        'id_tasa',
        'id_cupon',
        'id_perfil',
        'id_cuenta_origen',
        'id_cuenta_destino',
        'imagen_comprobante',
        'nro_transferencia',
        'observacion',
        'tipo_transferencia',
        'monto_banco_destino',
        'nro_referencia',
        'url_factura',
        'key_nubefact'
    ];

    public function origenFondos()
    {
        return $this->hasOne(OperacionOrigenFondo::class, 'id_operacion', 'id');
    }

    public function tasa()
    {
        return $this->hasOne(Tasa::class, 'id', 'id_tasa');
    }

    public function origen()
    {
        return $this->hasOne(CuentaBancaria::class, 'id', 'id_cuenta_origen');
    }

    public function bancosDestinos()
    {
        return $this->hasMany(OperacionBancoDestino::class, 'id_operacion', 'id');
    }

    public function destino()
    {
        return $this->hasOne(CuentaBancaria::class, 'id', 'id_cuenta_destino');
    }

    public function perfil()
    {
        return $this->hasOne(Perfil::class, 'id', 'id_perfil');
    }

    public function cupon()
    {
        return $this->hasOne(Cupon::class, 'id', 'id_cupon');
    }

}
