<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CuentaBancariaAdmin extends Model
{
    use HasFactory;

    protected $table = 'cuentas_bancarias_admin';
    protected $fillable = [
        'nro_cuenta',
        'titular',
        'ruc',
        'id_banco',
        'id_moneda',
        'id_tipo_cuenta'
    ];

    public function banco()
    {
        return $this->hasOne(Banco::class, 'id', 'id_banco');
    }
    public function tipo()
    {
        return $this->hasOne(TiposCuenta::class, 'id', 'id_tipo_cuenta');
    }
    public function moneda()
    {
        return $this->hasOne(Moneda::class, 'id', 'id_moneda');
    }
}
