@extends('layouts.index_app')
@section('content')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgb(198 208 93 / 55%);
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 16px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 50%;
        }

        input:checked + .slider {
            background-color: #C6D05D;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

    <div class="conten_panel_administrador">

        <div class="row mx-0">
            <div class="col-md-4 px-0">
                <h4 class="arlrdbd">Usuarios</h4>
            </div>
        </div>


        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
            <div class="row mx-0 justify-content-between align-items-center row-border-bottom pb-3">
                <div class="col-sm-7 col-md-6 col-lg-5 text-center text-sm-start">

                </div>
                <div class="col-sm-4 text-center text-sm-end mt-3 mt-sm-0">
                    @canany('usuarios_agregar')
                        <button class="btn btn-login PoppinsMedium" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar Usuario</button>
                    @endcanany
                </div>
            </div>

            <div class="div-scrolll">
                <table class="table mt-4 table-border-cero" id="usuarios">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Correo Electrónico</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Nro Celular</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Estado</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Rol</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">
                            Opciones
                        </th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero">
                    @if(count($usuarios)>0)
                        @foreach($usuarios as $user)
                            <tr class="tr-border-top">
                                <th scope="row" class="text-start">
                                    {{$user->email}}
                                </th>
                                <th scope="row" class="text-start">
                                    {{$user->nro_celular}}
                                </th>
                                <th scope="row" class="text-start">
                                    <label class="switch">
                                        @if($user->estado == 0)
                                            <input type="checkbox" class="cambiarEstado" value="{{ $user->id }}">
                                        @else
                                            <input type="checkbox" class="cambiarEstado" value="{{ $user->id }}" checked>
                                        @endif
                                        <span class="slider"></span>
                                    </label>
                                </th>
                                <th scope="row" class="text-start">
                                    {{$user->hasRol->rol->name}}
                                </th>
                                <th class="text-start d-flex align-items-center justify-content-center">
                                    @canany('usuarios_editar')
                                        <a href="#" class="btn-with-iconos" style="text-decoration: none;" onclick="modalEdit('{{$user->id}}', '{{$user->email}}', '{{$user->nro_celular}}', '{{$user->password}}', '{{ $user->hasRol->rol->id }}')">
                                            <i class="fa-solid fa-pencil"></i>
                                        </a>
                                    @endcanany

                                    @canany('usuarios_eliminar')
                                        <a title="Eliminar Usuario" href="#" onclick="eliminarUsuario(this, '{{ $user->id }}', '{{route("eliminar-usuario",["id"=>$user->id])}}');" style="text-decoration: none;" id="eliminar-institucion-{{ $user->id }}">
                                            <button type="button" class="btn btn-borrar btn-with-iconos ms-3" >
                                                <i class="fa-solid fa-trash-can" ></i>
                                            </button>
                                        </a>
                                    @endcanany
                                </th>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
                {{$usuarios->links()}}
            </div>
        </div>
    </div>

    @canany('usuarios_agregar')
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten-modal titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Nuevo Usuario</h1>

                            <form method="post" action="{{ route('registrar-usuario') }}">
                                @csrf
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-12 px-lg-5 mb-3">
                                        <div class="div-form-login">
                                            <input name="email" class="input-login PoppinsRegular"  placeholder="Ingrese Correo Electrónico" required>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-12 px-lg-5 mb-3">
                                        <div class="div-form-login">
                                            <input type="tel" name="nro_celular" class="input-login PoppinsRegular phone" value="{{old('nro_celular')}}" required>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-12 px-lg-5 mb-3" >
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Contraseña</label>
                                            <input type="password" name="password" min="8" max="16" pattern="^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$" id="pass1" class="input-login PoppinsRegular" required>
                                            <a id="icon-mostrar" class="button-eye" onclick="mostrarPass();"><i id="eye" class="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-12 px-lg-5 mb-3">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Repite tu contraseña</label>
                                            <input type="password" name="password_confirm" min="8" max="16" pattern="^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$" id="pass2" class="input-login PoppinsRegular" required>
                                            <a id="icon-mostrar2" class="button-eye" onclick="mostrarPass2();"><i id="eye2" class="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-12 px-lg-5 mb-3">
                                        <p class="text-start PoppinsRegular" style="font-size: 14px;color: #137188;">La contraseña debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.</p>
                                        <button type="button" class="btn btn-primary PoppinsMedium px-5" style="border-radius: 12px;" onclick="generarPass()">Generar Contraseña</button>
                                    </div>
                                    <br>
                                    <div class="col-12 px-lg-5 mb-3">
                                        <div class="div-form-login">
                                            <select name="rol" required class="input-login PoppinsRegular">
                                                <option value="" disabled selected>Debe seleccionar una Rol</option>
                                                @if(count($roles)>0)
                                                    @foreach($roles as $rol)
                                                        <option value="{{$rol->id}}">{{$rol->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i>Registrar Usuario</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany

    @canany('usuarios_editar')
        <div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="width: 50%; margin-left: 25%;">
                    <button type="button" class="btn-close btn-close-modal" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body px-0 py-0" style="position: relative;">
                        <div class="modal-padding-left-right-alert pt-5 pb-5" style="padding-left: 5%; padding-right: 5%;">
                            <h1 class="PoppinsBold titulo-conten-modal titutlo-modal-line-height mb-0 mt-5 text-center d-flex align-items-center justify-content-center">Editar Usuario</h1>

                            <form method="post" action="{{ route('editar-usuario') }}">
                                @csrf
                                <input type="hidden" name="id" id="id" value="">
                                <div class="row mx-0 justify-content-start mt-5">
                                    <div class="col-12 px-lg-5 mb-3">
                                        <div class="div-form-login">
                                            <input name="email" id="email" class="input-login PoppinsRegular"  placeholder="Ingrese Correo Electrónico" required>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-12 px-lg-5 mb-3">
                                        <div class="div-form-login">
                                            <input type="tel" id="nro_celular" name="nro_celular" class="input-login PoppinsRegular phone" value="{{old('nro_celular')}}" required>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-12 px-lg-5 mb-3">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Contraseña</label>
                                            <input type="password" name="password" min="8" max="16" pattern="^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$" id="pass3" class="input-login PoppinsRegular" >
                                            <a id="icon-mostrar3" class="button-eye" onclick="mostrarPass3();"><i id="eye3" class="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-12 px-lg-5 mb-3">
                                        <div class="div-form-login">
                                            <label class="label-login PoppinsRegular">Repite tu contraseña</label>
                                            <input type="password" name="password_confirm" min="8" max="16" pattern="^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$" id="pass4" class="input-login PoppinsRegular" >
                                            <a id="icon-mostrar4" class="button-eye" onclick="mostrarPass4();"><i id="eye4" class="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-12 px-lg-5 mb-3">
                                        <p class="text-start PoppinsRegular" style="font-size: 14px;color: #137188;">La contraseña debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.</p>
                                        <button type="button" class="btn btn-primary PoppinsMedium px-5" style="border-radius: 12px;" onclick="generarPass2()">Generar Contraseña</button>
                                    </div>

                                    <div class="col-12 px-lg-5 mb-3">
                                        <div class="div-form-login">
                                            <select name="rol" id="rol" required class="input-login PoppinsRegular">
                                                <option value="" disabled selected>Debe seleccionar un Rol</option>
                                                @if(count($roles)>0)
                                                    @foreach($roles as $rol)
                                                        <option value="{{$rol->id}}">{{$rol->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <i class="fa fa-angle-down icon-select"></i>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-login PoppinsMedium px-5 mt-5 btn-top-natural mb-5" style="color: #1F9F99 !important;"><i class="fa-solid fa-circle-check"></i> Actualizar Usuario</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcanany
@endsection
@section('scripts')
    <script>
        var modalEdit = (id,email,nro,pass,rol) => {
            var myModal = new bootstrap.Modal(document.getElementById('modalEdit'));
            $("#id").val(id);
            $("#email").val(email);
            $("#nro_celular").val(nro);
            $("#rol").val(rol).trigger('change');
            myModal.show();
        };

        var eliminarUsuario = (e, id, ruta) => {
            e.preventDefault;

            toastr.warning("<br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Si</button>",'Desea eliminar el usuario?',
                {
                    closeButton: true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#confirmationRevertYes").click(function(){
                            window.location.href = ruta;
                        });
                    }
                });
        };


        var generarPass = () => {
            var characters = "0123456789abcdefghijkl0123456789mnopqrstuvwxyz0123456789ABCDEFGHIJKL0123456789MNOPQRSTUVWXYZ0123456789";

            var pass = "";
            for (var i=0; i < 10; i++){
                pass += characters.charAt(Math.floor(Math.random()*characters.length));
            }

            $("#pass1").val(pass);
            $("#pass2").val(pass);

            $("#pass1").attr('type','text');
            estado = 1;
            $("#eye").removeClass("fa-eye");
            $("#eye").addClass("fa-eye-slash");

            $("#pass2").attr('type','text');
            estado2 = 1;
            $("#eye2").removeClass("fa-eye");
            $("#eye2").addClass("fa-eye-slash");

        };

        var generarPass2 = () => {
            var characters = "0123456789abcdefghijkl0123456789mnopqrstuvwxyz0123456789ABCDEFGHIJKL0123456789MNOPQRSTUVWXYZ0123456789";

            var pass = "";
            for (var i=0; i < 10; i++){
                pass += characters.charAt(Math.floor(Math.random()*characters.length));
            }

            $("#pass3").val(pass);
            $("#pass4").val(pass);

            $("#pass3").attr('type','text');
            estado = 1;
            $("#eye3").removeClass("fa-eye");
            $("#eye3").addClass("fa-eye-slash");

            $("#pass4").attr('type','text');
            estado2 = 1;
            $("#eye4").removeClass("fa-eye");
            $("#eye4").addClass("fa-eye-slash");

        };

        var estado=0, estado2=0;

        var mostrarPass = () => {
            if(estado==0)
            {
                estado=1;
                $("#pass1").attr('type','text');
                $("#eye").removeClass("fa-eye");
                $("#eye").addClass("fa-eye-slash");
            }
            else
            {
                estado=0;
                $("#eye").addClass("fa-eye");
                $("#eye").removeClass("fa-eye-slash");
                $("#pass1").attr('type','password');

            }
        };

        var mostrarPass2 = () => {
            if(estado2==0)
            {
                estado2=1;
                $("#pass2").attr('type','text');
                $("#eye2").removeClass("fa-eye");
                $("#eye2").addClass("fa-eye-slash");
            }
            else
            {
                estado2=0;
                $("#eye2").addClass("fa-eye");
                $("#eye2").removeClass("fa-eye-slash");
                $("#pass2").attr('type','password');

            }
        };

        var mostrarPass3 = () => {
            if(estado==0)
            {
                estado=1;
                $("#pass3").attr('type','text');
                $("#eye3").removeClass("fa-eye");
                $("#eye3").addClass("fa-eye-slash");
            }
            else
            {
                estado=0;
                $("#eye3").addClass("fa-eye");
                $("#eye3").removeClass("fa-eye-slash");
                $("#pass3").attr('type','password');

            }
        };

        var mostrarPass4 = () => {
            if(estado2==0)
            {
                estado2=1;
                $("#pass4").attr('type','text');
                $("#eye4").removeClass("fa-eye");
                $("#eye4").addClass("fa-eye-slash");
            }
            else
            {
                estado2=0;
                $("#eye4").addClass("fa-eye");
                $("#eye4").removeClass("fa-eye-slash");
                $("#pass4").attr('type','password');

            }
        };

        $(document).ready(function() {
            var input = document.querySelector(".phone");
            window.intlTelInput(input, {
                autoHideDialCode: false,
                nationalMode: false,
                preferredCountries: ['pe'],
                utilsScript: "/plugin-telf/js/utils.js",
            });

            $(".cambiarEstado").click(function () {
                let activate = $(this).is(':checked');
                let id = $(this).val();

                $.ajax({
                    url: "{{ route('cambiar-estado-usuario') }}?id=" + id,
                    method: 'GET',
                    success: function (data) {
                        if(activate)
                            toastr.success("Usuario activado!");
                        else
                            toastr.success("Usuario desactivado!");
                    },
                    error:function (error) {
                        console.log(error);
                        toastr.error(error.message);
                    }
                });
            });
        });

    </script>

@endsection