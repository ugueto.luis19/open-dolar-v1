<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Perfil;
use App\Models\Verificacion;
use App\Models\Operacion;
use Redirect;
use Session;

class SoporteController extends Controller
{
    //
    public function limpiarPerfiles()
    {
        $no_cuentas = Perfil::where('id_cuenta', null)->delete();

        return response()->json([
            'estado' => 'success',
            'message' => 'Perfiles eliminados correctamente!',
        ],201);
    }

    public function anularOperaciones()
    {
        $operaciones=Operacion::where('estado', 0)->get();

        foreach($operaciones as $operacion){
            $operacion = Operacion::find($operacion->id);

            //////////////// VERIFICANDO CADUCACIÓN DE LA OPERACION
            $to_time = strtotime($operacion->fecha_operacion);
            $from_time = strtotime(date('Y-m-d H:i:s'));

            if(round(abs($to_time - $from_time) / 60,2) >= 15){
                $operacion->estado = 6;
                $operacion->update();
            }
        }

        return response()->json([
            'estado' => 'success',
            'message' => 'Operaciones en estado "Pendiente" anuladas por tiempo caducado!',
        ],201);
    }

    

    public function registroBiometrico()
    {
        $menu=4;
        return view('perfil.registro-biometrico', compact('menu'));
    }

    public function registroBiometrico1()
    {
        $menu=4;
        return view('perfil.registro-biometrico-paso-1', compact('menu'));
    }

    public function registroBiometrico2()
    {
        $menu=4;
        return view('perfil.registro-biometrico-paso-2', compact('menu'));
    }

    public function biometricoPaso1(Request $request)
    {
        $this->validate($request, [
            'imagen1' => 'required|image',
            'imagen2' => 'required|image'
        ]);

        $path = public_path().'/img/perfiles/documentos/';
        $imagen1 = ''; $imagen2 = '';

        if($request->hasFile('imagen1')){
            $file = $request->file('imagen1');
            $filenameWithExt = $file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $fileNameToStore = time() . '.' . $extension;
            $imagen1 = $fileNameToStore;
            $file->move($path, $fileNameToStore);
        }

        if($request->hasFile('imagen2')){
            $file2 = $request->file('imagen2');
            $filenameWithExt2 = $file2->getClientOriginalName();
            $filename2 = pathinfo($filenameWithExt2, PATHINFO_FILENAME);
            $extension2 = $file2->getClientOriginalExtension();
            $fileNameToStore2 = '2'.time() . '.' . $extension2;
            $imagen2 = $fileNameToStore2;
            $file2->move($path, $fileNameToStore2);
        }

        $perfil = Perfil::find(session('id_perfil'));

        $verificacion = new Verificacion();
        $verificacion->imagen_frontal = $imagen1;
        $verificacion->imagen_posterior = $imagen2;   
        $verificacion->documento_identidad = $perfil->nro_doc;
        $verificacion->id_perfil = $perfil->id;
        $verificacion->estado = 0;
        $verificacion->save();

        $notification = array(
            'message' => 'Imagenes del documento cargadas correctamente.',
            'alert-type' => 'success'
        );

        return redirect()->route('biometrico-2')->with($notification);

    }

    public function biometricoPaso2(Request $request)
    {
        $this->validate($request, [
            'video' => 'required'
        ]);

        $path = public_path().'/img/perfiles/videos/';
        $video = '';

        if($request->hasFile('video')){
            $file = $request->file('video');
            $filenameWithExt = $file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $fileNameToStore = time() . '.' . $extension;
            $video = $fileNameToStore;
            $file->move($path, $fileNameToStore);
        }

        $update_verificacion = Verificacion::where('id_perfil', session('id_perfil'))->orderBy('created_at', 'desc')->first();
        $update_verificacion->video = $video;
        $update_verificacion->update();

        $notification = array(
            'message' => 'Video cargado correctamente.',
            'alert-type' => 'success'
        );

        return redirect()->route('biometricoFinal')->with($notification);
    }

    public function biometricoFinal()
    {
        $menu=4;
        return view('perfil.registro-biometrico-final', compact('menu'));
    }    
}
