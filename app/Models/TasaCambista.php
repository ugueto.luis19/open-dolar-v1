<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TasaCambista extends Model
{
    use HasFactory;
    protected $table = 'tasas_cambistas';
    protected $fillable = [
        'tasa_compra',
        'tasa_venta'
    ];

}
