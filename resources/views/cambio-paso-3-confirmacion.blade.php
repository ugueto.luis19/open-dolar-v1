@extends('layouts.index_app')
@section('content')
    <!-- card de necesitas ayuda-->
    <div class="div-flotante">
        <img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 40%">
    </div>

    <div class="div-flotante-azul px-5 py-4">
        <button class="btn btn-close-ayuda">
            <i class="fa-solid fa-circle-xmark icon-close-ayuda"></i>
        </button>
        <h4 class="Arialregular titulo-ayuda"><img src="{{ asset('assets-web/img/Icon mhelp.png') }}" style="width: 8%;">Necesitas ayuda?</h4>
        <h5 class="ArialBold mt-5 mb-2">Temas recurrentes</h5>
        <p class="Arialregular p-link-temas my-0"><a href="">Problemas con transacción</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Perfil y registro</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Privacidad y seguridad</a></p>
        <p class="Arialregular p-link-temas my-0"><a href="">Cancelaciones y devoluciones</a></p>
        <h5 class="ArialBold mt-5 mb-2">Contacto</h5>
        <p class="Arialregular">
            Con el fin de ahorrar tu tiempo, recomendamos revisar las preguntas frecuentes. Si sigues sin encontrar respuesta, puedes contactarnos de la manera que mas te acomode:
        </p>
        <ul class="ps-0">
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-phone me-3"></i>
                (+51) 1 1234567
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-brands fa-whatsapp me-3"></i>
                998877665
            </li>
            <li class="d-flex Arialregular align-items-center list-contacto mb-2">
                <i class="fa-solid fa-envelope me-3"></i>
                confianza@opendolar.com
            </li>
            <li class="d-flex Arialregular align-items-start list-contacto">
                <i class="fa-solid fa-location-dot me-3"></i>
                Centro Empresarial El Trigal, Calle Antares 320, Of. 802-C, Torre B, Surco - Lima.
            </li>
        </ul>
        <div class="d-flex justify-content-start mt-4">
            <a href="" class="me-4">
                <i class="fa-solid fa-share-nodes icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-whatsapp icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-facebook-f icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-linkedin-in icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="me-4">
                <i class="fa-brands fa-instagram icon-redes-verde" style="color: #fff !important;"></i>
            </a>
            <a href="" class="">
                <i class="fa-solid fa-envelope icon-redes-verde" style="color: #fff !important;"></i>
            </a>
        </div>
    </div>
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-alerta PoppinsBold my-0 text-center text-xl-start"><span>A Cambiar</span></h1>
        <div class="row mx-0 mt-4 justify-content-center">
            <div class="col-12">
                <a href="{{ \URL::previous() }}" class="btn btn-volver-azul PoppinsRegular">< Volver</a>
            </div>
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6 px-0 px-md-4 px-lg-4 px-xl-3" style="position: relative;">
                <div class="div-check">
                    <div class="div-chec-radius">
                        <i class="fa-solid fa-circle-check check-activado"></i>
                        <i class="fa-solid fa-circle-check check-activado"></i>
                        <i class="fa-solid fa-circle-check check-activado"></i>
                    </div>
                </div>
                <form action="{{route('finalizar-cambio')}}" method="post" id="formularioProceso" enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="{{ $operacion->id }}">
                    <div class="card-blanco card-padding-top mt-4">
                        <h2 class="PoppinsMedium subtitulo-card text-center" id="titulo">Comprobante</h2>

                        <?php


                        if($operacion->tipo=='0')
                        {
                            $moneda_1='S/';
                            $moneda_2='$';
                            if(is_object($operacion->cupon))
                            {
                                $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta-$operacion->cupon->valor_soles;
                            }
                            else
                            {
                                $monto_tasa=$operacion->tasa->tasa_venta+$operacion->tasa->pips_venta;
                            }
                        }
                        else{
                            $moneda_1='$';
                            $moneda_2='S/';
                            if(is_object($operacion->cupon))
                            {
                                $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra+($operacion->cupon->valor_soles/$operacion->tasa->tasa_compra);
                            }
                            else
                            {
                                $monto_tasa=$operacion->tasa->tasa_compra+$operacion->tasa->pips_compra;
                            }
                        }
                        ?>

                        <div class="row mx-0 mt-1" >

                            <div class="col-12 mb-2 mt-3 PoppinsRegular" style="color: #137188; text-align: center;">
                                Para finalizar la transacción, ingrese el número de operación o adjunte comprobante
                            </div>

                            <div class="col-12 mb-2 mt-3">
                                <input type="text" class="input-login input-numero-operacion text-center" placeholder="Ingrese número de la operación" id="nro_transferencia" name="nro_transferencia">
                            </div>

                            <div class="row mx-0 align-items-center">
                                <div class="col-3 ps-0">
                                    <div class="div-img-edit-perfil" style="width: 75% !important;">
                                        <img id="img-comprobante" src="{{ asset('assets-web/img/icono-imagen-new.png') }}" style="width: 100%;">
                                    </div>
                                </div>
                                <div class="col-9 px-0">
                                    <span class="btn btn-file-adjuntar PoppinsMedium" style="background-color: #C6D05D !important; color: #1B7187 !important;width: 100% !important;">

                                        <i class="fa-solid fa-image me-2"></i>
                                        Cargar Imagen
                                        <input type="file" name="imagen_comprobante" id="imageFile1">
                                        <label id="reportar_imagen" style="font-size: 11px;"></label>
                                    </span>
                                </div>
                            </div>
                            <?php
                                 $hora_vencimiento= date('H:i:s',strtotime($operacion->fecha_operacion.' +15 minute'));
                            ?>
                            <div class="col-6 mb-2 mt-3" style="background-color: #FEF8DD; border-radius: 20px 0 0 20px;">
                                <p class="my-3 text-center" style="color: #137188;"><b>Hora de inicio:</b> <br> {{date('H:i:s',strtotime($operacion->fecha_operacion))}}</p>
                                <p class="my-3 text-center" style="color: #137188;"><b>Hora de finalización: </b> <br> <?php echo $hora_vencimiento ;?></p>
                            </div>
                            <div class="col-6 mb-2 mt-3 mybox" style="background-color: #FEF8DD; border-radius: 0 20px 20px 0;">
                                <p class="my-3 text-center" style="color: #137188;"><b>Operación vence en:</b> </p>
                                <p class="my-3 text-center" style="color: #137188; font-size: 18px;" id="contador"><b><span id="minutes"></span>:<span id="seconds"></span></b></p>
                            </div>

                            <div class="col-12 text-center">
                                <button type="button" id="validar-form" class="btn btn-login PoppinsMedium px-5 mt-1 btn-top-natural mb-0" style="color: #137188 !important;">Enviar</button><br>
                            </div>
                        </div>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        //// CONTADOR
        let DATE_TARGET = new Date('{{date("Y-m-d H:i:s",strtotime($operacion->fecha_operacion."+15 minute"))}}');

        let SPAN_MINUTES = document.querySelector('span#minutes');
        let SPAN_SECONDS = document.querySelector('span#seconds');

        // CALCULOS EN MILISEGUNDOS
        let MILLISECONDS_OF_A_SECOND = 1000;
        let MILLISECONDS_OF_A_MINUTE = MILLISECONDS_OF_A_SECOND * 60;
        let MILLISECONDS_OF_A_HOUR = MILLISECONDS_OF_A_MINUTE * 60;
        let MILLISECONDS_OF_A_DAY = MILLISECONDS_OF_A_HOUR * 24

        var TIEMPO;

        var getDate = () => {
            $.ajax({
                url: "{{ route('get-time') }}",
                method: 'GET',
                success: function (data) {
                    TIEMPO=data.date;
                },
                error:function (error) {
                }
            });

        };

        var updateCountdown = () => {
            if(TIEMPO != undefined){
                var NOW = new Date(TIEMPO);
                let DURATION = DATE_TARGET - NOW;
                let REMAINING_DAYS = Math.floor(DURATION / MILLISECONDS_OF_A_DAY);
                let REMAINING_HOURS = Math.floor((DURATION % MILLISECONDS_OF_A_DAY) / MILLISECONDS_OF_A_HOUR);
                let REMAINING_MINUTES = Math.floor((DURATION % MILLISECONDS_OF_A_HOUR) / MILLISECONDS_OF_A_MINUTE);
                let REMAINING_SECONDS = Math.floor((DURATION % MILLISECONDS_OF_A_MINUTE) / MILLISECONDS_OF_A_SECOND);

                if((REMAINING_MINUTES == 0 && REMAINING_SECONDS == 0) || (REMAINING_MINUTES < 0 && REMAINING_SECONDS < 0)){ // SI EL CONTADOR LLEGA A 0, REFRESCAR PÁGINA
                    $("#contador").html('Anulada');
                    SPAN_MINUTES.textContent = 0;
                    SPAN_SECONDS.textContent = 0;
                    $("#buttonProcess").attr('disabled', 'disabled');
                    window.location.href = '{{route("anular-cambio",["id"=>$operacion->id])}}';
                    return;
                }

                SPAN_MINUTES.textContent = REMAINING_MINUTES;

                if(REMAINING_SECONDS < 10)
                    SPAN_SECONDS.textContent = "0"+REMAINING_SECONDS;
                else
                    SPAN_SECONDS.textContent = REMAINING_SECONDS;
            }
        }
        updateCountdown();
        setInterval(updateCountdown, MILLISECONDS_OF_A_SECOND);  // REFRESCAR CADA SEGUNDO
        getDate();
        setInterval(getDate, MILLISECONDS_OF_A_SECOND);  // REFRESCAR CADA SEGUNDO

        ///////////////////// FIN CONTADOR /////////////////////////////////////////////////
        $( document ).ready(function() {

            $("#imageFile1").change(function () {
                $("#reportar_imagen").html('Archivo Cargado Correctamente!');
            });

            $("#validar-form").click(function () {
               if($("#nro_transferencia").val()!=null && $("#nro_transferencia").val()!='')
               {
                   $("#formularioProceso").submit();
               }
               else
               {
                   if($("#imageFile1").val()!=null && $("#imageFile1").val()!='')
                   {
                       $("#formularioProceso").submit();
                   }
                   else
                   {
                       toastr.warning("Debe ingresar el numero de operación o adjuntar la imagen de la operación!");
                   }
               }
            });

            $('#imageFile1').change(function(e){
                var img = ' ';
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.onload = function (event) {
                    var img = new Image();
                    img.src = event.target.result;
                    img.onload = function() {
                        img=event.target.result;
                        $("#img-comprobante").css("display","block");
                        $("#img-comprobante").attr("src",img);

                    }
                }
            });
        });
    </script>
@endsection