@extends('layouts.index_app')
@section('content')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgb(198 208 93 / 55%);
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 16px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
            border-radius: 50%;
        }

        input:checked + .slider {
            background-color: #C6D05D;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .EstadoVerificado {
            color:#31A745;
        }

        .errorVerificacion {
            color:#F30C04;
        }
    </style>
    <div class="conten_panel_administrador">
        <h1 class="titulo-general-admi PoppinsBold my-0 text-center text-xl-start">Verificaciones</h1>

        <div class="card-blanco card-height-operaciones mt-5 card-blanco-padding-left-right text-end text-xl-start" style="position: relative; height: auto;">
            <div class="div-scrolll" id="div-verificaciones-padre">
                <table class="table table-border-cero" id="verificaciones">
                    <thead>
                    <tr>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Fecha</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Imagen Frontal</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Imagen Posterior</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Video</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Documento de Identidad</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Estado</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Perfil</th>
                        <th scope="col" class="PoppinsMedium th-titulo-cabecera text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody class="tbody-border-cero" style="border-top: 0px !important;">
                    @if(count($verificaciones)>0)
                        @foreach($verificaciones as $verif)
                            <tr class="tr-border-top">
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{date('d-m-Y g:i a',strtotime($verif->created_at))}}
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    <a href="{{ asset('img/perfiles/documentos/'.$verif->imagen_frontal) }}" target="_blank" class="p-2">
                                        <img src="{{ asset('assets-web/img/icono-imagen-new.png') }}" style="width: 25px;">
                                    </a>
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    <a href="{{ asset('img/perfiles/documentos/'.$verif->imagen_posterior) }}" target="_blank" class="p-2">
                                        <img src="{{ asset('assets-web/img/icono-imagen-new.png') }}" style="width: 25px;">
                                    </a>
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    <a href="{{ asset('img/perfiles/videos/'.$verif->video) }}" target="_blank" class="p-2">
                                        <img src="{{ asset('assets-web/img/icono-video-new.png') }}" style="width: 25px;">
                                    </a>
                                </th>
                                <th scope="row" class="text-center td-color-datos PoppinsMedium">
                                    {{$verif->documento_identidad}}
                                </th>
                                <th style="cursor: pointer;" scope="row" class="text-center td-color-datos PoppinsMedium" id="estado-{{$verif->id}}" onclick="anularVerificacion(this, '{{$verif->estado}}','{{ $verif->id }}', '{{route("anular-verificacion",["id"=>$verif->id])}}');">
                                    <?php
                                    $estado = '';
                                    $claseEstado ='';
                                    switch($verif->estado){
                                        case '0':
                                            $estado = "Por Verificar";
                                            $claseEstado="porVerificar";
                                            break;
                                        case '1':
                                            $estado = "Verificado";
                                            $claseEstado="EstadoVerificado";
                                            break;
                                        case '2':
                                            $estado = "Error en la verificación";
                                            $claseEstado="errorVerificacion";
                                            break;
                                    }
                                    ?>
                                    <span class="{{ $claseEstado }}">{{ $estado }}</span>
                                </th>
                                <th class="text-center td-color-datos PoppinsMedium">
                                    {{$verif->perfil->nombres}} {{$verif->perfil->apellido_paterno}}
                                </th>
                                <th class="text-end td-color-datos PoppinsMedium" style="display: flex;justify-content: end; align-items: center;">
                                    @canany('verificaciones_aprobar','verificaciones_anular')
                                        <label class="switch" id="check-verif-{{ $verif->id }}">
                                            @if($verif->estado == 0 || $verif->estado == 2)
                                                <input type="checkbox" class="cambiarEstado" value="{{ $verif->id }}">
                                            @else
                                                <input type="checkbox" class="cambiarEstado" value="{{ $verif->id }}" checked>
                                            @endif
                                            <span class="slider"></span>
                                        </label>
                                    @endcanany
                                </th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{ $verificaciones->links() }}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            //$('#verificaciones').DataTable();
            $(".cambiarEstado").click(function () {
                let activate = $(this).is(':checked');
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('cambiar-estado-verificacion') }}?id=" + id,
                    method: 'GET',
                    success: function (data) {
                        if(activate)
                        {
                            $("#estado-"+id).html('Verificado');
                            toastr.success("Verificado!");
                        }
                        else
                        {
                            $("#estado-"+id).html('Error en la verificación');
                            toastr.success("Error en la verificación!");
                        }

                    },
                    error:function (error) {
                        console.log(error);
                        toastr.error(error.message);
                    }
                });
            });
        });

        var anularVerificacion = (e, estado,id, ruta) => {
            e.preventDefault;

            toastr.warning("<br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Si</button>",'Desea anular la verificación?',
                {
                    closeButton: true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#confirmationRevertYes").click(function(){
                            $.ajax({
                                url: ruta,
                                method: 'GET',
                                success: function (data) {

                                    $("#estado-"+id).html('Error en la verificación');
                                    if(data.estado_old==1)
                                    {
                                        $("#check-verif-"+id).click();
                                    }
                                },
                                error:function (error) {
                                    console.log(error);
                                    toastr.error(error.message);
                                }
                            });

                        });
                    }
                });
        };

    </script>
@endsection