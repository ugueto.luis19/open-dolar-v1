<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comisiones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_perfil_padre');
            $table->foreign('id_perfil_padre')->references('id')->on('perfiles')->onDelete('RESTRICT');
            $table->unsignedBigInteger('id_perfil_hijo');
            $table->foreign('id_perfil_hijo')->references('id')->on('perfiles')->onDelete('RESTRICT');
            $table->decimal('monto',8,2);
            $table->unsignedBigInteger('id_operacion');
            $table->foreign('id_operacion')->references('id')->on('operaciones')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comisiones');
    }
}
